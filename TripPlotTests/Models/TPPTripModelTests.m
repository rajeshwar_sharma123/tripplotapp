//
//  TPPTripModelTests.m
//  TripPlot
//
//  Created by Vishwas Singh on 26/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TPPTripModel.h"



@interface TPPTripModelTests : XCTestCase

@end

@implementation TPPTripModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testTripModel
{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"test id",@"_id",
                          @"test title",@"title",
                          @[@"url1",@"url2"],@"images",
                          @"Test",@"termConditionText",
                          @"26 dec,15",@"updatedOn",
                          @[@"abc",@"xyz"],@"tripMembers",
                          
                           nil];
    
    TPPTripModel *model = [[TPPTripModel alloc]initWithDictionary:dict error:nil];
    
    
    XCTAssertNotNil(model);
    XCTAssertEqual(model._id, @"test id",@"Model data binding not correct");
    XCTAssertEqual(model.title, @"test title",@"Model data binding not correct");
    XCTAssertEqual(model.termConditionText, @"Test",@"Model data binding not correct");
    XCTAssertEqual(model.updatedOn, @"26 dec,15",@"Model data binding not correct");
    
    
}


@end

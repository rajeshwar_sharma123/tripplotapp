//
//  TPPUserProfileModelTests.m
//  TripPlot
//
//  Created by Vishwas Singh on 26/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TPPUserProfileModel.h"


@interface TPPUserProfileModelTests : XCTestCase

@end

@implementation TPPUserProfileModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}



- (void)testTripModel
{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"test id",@"_id",
                          @"12345",@"contact",
                          @"India",@"country",
                          @"Test@gmail.com",@"emailid",
                          @"vishwas",@"firstname",
                          @"vishwas singh",@"fullname",
                          @"singh",@"lastname",
                          @"test url",@"profileImage",
                          @"2",@"status",
                          @"2",@"userType",
                          nil];
    
    TPPUserProfileModel *model = [[TPPUserProfileModel alloc]initWithDictionary:dict error:nil];;
    
    
    XCTAssertNotNil(model);
    XCTAssertEqual(model._id, @"test id", @"Model data binding not correct");
    XCTAssertEqual(model.contact, @"12345",@"Model data binding not correct");
    XCTAssertEqual(model.country, @"India",@"Model data binding not correct");
    XCTAssertEqual(model.emailid, @"Test@gmail.com",@"Model data binding not correct");
    XCTAssertEqual(model.firstname, @"vishwas",@"Model data binding not correct");
    XCTAssertEqual(model.fullname, @"vishwas singh",@"Model data binding not correct");
    XCTAssertEqual(model.lastname, @"singh",@"Model data binding not correct");
    XCTAssertEqual(model.profileImage, @"test url",@"Model data binding not correct");
    XCTAssertEqual(model.status, @"2",@"Model data binding not correct");
    XCTAssertEqual(model.userType, @"2",@"Model data binding not correct");
    


    
}




@end

//
//  UIView+ComponentsTests.m
//  FDCustomer
//
//  Created by Daffolap-mac-22 on 06/10/15.
//  Copyright (c) 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "UIView+Components.h"

@interface UIView_ComponentsTests : XCTestCase

@end

@implementation UIView_ComponentsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
}




- (void)testShowLoader
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [view showLoader];
    
    XCTAssert(YES, @"Pass");
}

- (void)testHideLoader
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [view hideLoader];
    
    XCTAssert(YES, @"Pass");
}

- (void)testShowSpinner
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [view showSpinner];
    
    XCTAssert(YES, @"Pass");
}

- (void)testHideSpinner
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [view hideSpinner];
    
    XCTAssert(YES, @"Pass");
}


- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end

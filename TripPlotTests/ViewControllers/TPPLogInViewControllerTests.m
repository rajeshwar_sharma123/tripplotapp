//
//  TPPLogInViewControllerTests.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 06/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "TPPLogInViewController.h"

@interface TPPLogInViewControllerTests : XCTestCase
{
    TPPLogInViewController *viewController;
    id mockObject;
}
@end

@implementation TPPLogInViewControllerTests

- (void)setUp {
    [super setUp];
    
    viewController = [[TPPLogInViewController alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    UIApplication* application = [UIApplication sharedApplication];
    [application.delegate applicationDidEnterBackground:application];
}



#pragma mark- ViewController lifecycle tests

- (void)testDidReceiveMemoryWarning
{
    [mockObject didReceiveMemoryWarning];
    [mockObject verify];
    
}

- (void)testDidload
{
    
    [mockObject verify];
}

- (void)testDidAppear
{
    [mockObject viewDidAppear:YES];
    [mockObject verify];
}

-(void)testViewWillAppear
{
    [mockObject viewWillAppear:YES];
    [mockObject verify];
}

-(void)testViewWillDisAppear
{
    [mockObject viewWillDisappear:YES];
    [mockObject verify];
}

-(void)testViewDidDisAppear
{
    [mockObject viewDidDisappear:YES];
    [mockObject verify];
}

-(void)testPreferredStatusBarStyle
{
    [mockObject preferredStatusBarStyle];
    [mockObject verify];
}

- (void)testPrefersStatusBarHidden
{
    [mockObject prefersStatusBarHidden];
    [mockObject verify];
}


-(void)testFacebookLogIn
{
    
    id mockViewController = OCMClassMock([viewController class]);
  
    
    NSString *str = @"Stubbing";
    OCMStub([mockViewController facebookLogInButtonTapped:[OCMArg any]]).andReturn(str);
    
    [mockViewController navigateToHomeViewController];
    
    OCMVerify([mockViewController navigateToHomeViewController]);
}


@end

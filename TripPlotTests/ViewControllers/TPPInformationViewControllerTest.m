//
//  TPPInformationViewControllerTest.m
//  TripPlot
//
//  Created by Vishwas Singh on 26/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import <OCMock.h>
#import "TPPInformationViewController.h"

@interface TPPInformationViewControllerTest : XCTestCase
{
    TPPInformationViewController *viewController;
    id mockObject;
}

@end

@implementation TPPInformationViewControllerTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark- ViewController lifecycle tests

- (void)testDidReceiveMemoryWarning
{
    [mockObject didReceiveMemoryWarning];
    [mockObject verify];
    
}

- (void)testDidload
{
    
    [mockObject verify];
}

- (void)testDidAppear
{
    [mockObject viewDidAppear:YES];
    [mockObject verify];
}

-(void)testViewWillAppear
{
    [mockObject viewWillAppear:YES];
    [mockObject verify];
}

-(void)testViewWillDisAppear
{
    [mockObject viewWillDisappear:YES];
    [mockObject verify];
}

-(void)testViewDidDisAppear
{
    [mockObject viewDidDisappear:YES];
    [mockObject verify];
}

-(void)testPreferredStatusBarStyle
{
    [mockObject preferredStatusBarStyle];
    [mockObject verify];
}

- (void)testPrefersStatusBarHidden
{
    [mockObject prefersStatusBarHidden];
    [mockObject verify];
}

@end

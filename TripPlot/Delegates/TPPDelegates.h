//
//  TPPDelegates.h
//  TripPlot
//
//  Created by Vishwas Singh on 20/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>



@protocol TPPMenuViewDelegate <NSObject>

@optional
-(void)buttonTappedAtRowIndex:(NSInteger)index;
-(void)locationMenuTapped:(id)sender info:(NSDictionary *)infoDic atIndex: (long)index;

@end


@protocol TPPSeeMoreButtonDelegate <NSObject>

-(void)seeMoreButtonTappedWithIndexPath:(NSIndexPath *)indexPath;

@end



@protocol TPPCellTapDelegate <NSObject>

-(void)cellTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel;

@end



@protocol TPPShareCellDelegate <NSObject>
@optional
-(void)shareButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel snapShot:(UIImage *)snapShot;
-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel;
-(void)challengeAFrinedButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel;
@end


@protocol TPPTripPointTapDelegate <NSObject>

-(void)tripPointTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel buttonIndex:(NSInteger)buttonIndex;

@end



@protocol TPPNoDataFoundDelegate <NSObject>

-(void)retryButtonTapped;

@end

@protocol TPPDropDownViewDelegate <NSObject>
@optional
- (void)hideDropDownView;

@end






@protocol TPPSearchDelegate <NSObject>

-(void)didSearchButtonTapped:(NSString *) searchString withSearchObject:(NSDictionary *)searchObject;

@end





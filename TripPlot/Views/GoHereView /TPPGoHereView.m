//
//  TPPGoHereView.m
//  TripPlot
//
//  Created by Vishwas Singh on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPGoHereView.h"

@implementation TPPGoHereView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.plotTripButton.layer.cornerRadius = 2.0;
    self.plotTripButton.clipsToBounds = YES;
    
    self.addToBucketButton.layer.cornerRadius = 2.0;
    self.addToBucketButton.clipsToBounds = YES;
}


- (IBAction)plotTripButtonTapped:(id)sender
{
    UIViewController *currentVC = [TPPUtilities getCurrentViewController];
    TPPTripModel *tripModel;
    
    if (_tripModel.tripStatus == TripStatus_RecommendedTrip)
    {
        NSArray *trips = _tripModel.totalTrips[@"trips"];
        if (trips.count>0)
        {
            tripModel = [[TPPTripModel alloc]initWithDictionary:trips[0] error:nil];
        }
        else
        {
            [currentVC showErrorMessage:@"No trip found to plot"];
            
            return;
        }
    }
    else
    {
        tripModel = _tripModel;
    }
    
    
    [TPPCreateTripModel clearInstance];
    
    TPPCreateTripModel *newTripObject = [TPPCreateTripModel sharedInstance];
    NSDictionary *profileDetail = [UserDeafaults objectForKey:UserProfile];
    newTripObject.token = [UserDeafaults objectForKey:Token];
    newTripObject.username = profileDetail[@"emailid"];
    newTripObject.profileImage = profileDetail[@"profileImage"];
    newTripObject._id = profileDetail[@"_id"];
    newTripObject.firstname = profileDetail[@"firstname"];
    newTripObject.createdBy = profileDetail[@"_id"];
    
    newTripObject.isNew = YES;
    newTripObject.typeNames = tripModel.tripTypes.tokensNameArray;
    newTripObject.modeNames = tripModel.tripModes.tokensNameArray;
    newTripObject.title = tripModel.title;
    
    TPPCreateNewTripViewController *VC = [currentVC.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
    
    VC.sourceDestinationsArray = [tripModel.tripLocations getLocationsInSourceDestinationModelFormat:tripModel.tripLocations];
    
    [currentVC.navigationController pushViewController:VC animated:YES];
    
    
    [self outSideTapped:nil];
    
}

- (IBAction)addToBucketButtonTapped:(id)sender
{
    TPPTripModel *tripModel;
    if (_tripModel.tripStatus == TripStatus_RecommendedTrip)
    {
        NSArray *trips = _tripModel.totalTrips[@"trips"];
        if (trips.count>0)
        {
            tripModel = [[TPPTripModel alloc]initWithDictionary:trips[0] error:nil];
        }
        else
        {
            tripModel = nil;
        }
    }
    else
    {
        tripModel = _tripModel;
    }
    
    [self callServiceToAddTripInBucket:tripModel];
    
    [self outSideTapped:nil];
}

- (IBAction)outSideTapped:(id)sender
{
    [self removeFromSuperview];
}


- (void)callServiceToAddTripInBucket:(TPPTripModel *)tripModel
{
   UIViewController *currentVC = [TPPUtilities getCurrentViewController];
    if (tripModel == nil)
    {
        [currentVC showErrorMessage:@"No trip found to add in Bucket"];
        return;
    }
    
    
    [currentVC.view showLoader];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id ,@"tripId",
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ADDInBucket] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [currentVC.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
             [currentVC showSuccessMessage:@"Successfully added to bucket"];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [currentVC showErrorMessage:errorMessage];
         [currentVC.view hideLoader];
     }];
    
}




@end

//
//  TPPGoHereView.h
//  TripPlot
//
//  Created by Vishwas Singh on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPGoHereView : UIView

@property (weak, nonatomic) IBOutlet UIButton *plotTripButton;
@property (weak, nonatomic) IBOutlet UIButton *addToBucketButton;

@property (retain, nonatomic) TPPTripModel *tripModel;

@end

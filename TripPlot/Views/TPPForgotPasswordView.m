//
//  TPPForgotPasswordView.m
//  TripPlot
//
//  Created by Daffolap-21 on 22/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPForgotPasswordView.h"

@implementation TPPForgotPasswordView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)cancelButtonTapped:(id)sender {

	[self removeFromSuperview];
}

- (IBAction)submitButtonTapped:(id)sender {

	if ([self.emailTextField.text.trimmedString isEqualToString:@""])
	{
		[self.parentVC showErrorMessage:EMAIL_EMPTY];
		return;
	}
	else if (!self.emailTextField.text.isValidEmail)
	{
		[self.parentVC showErrorMessage:EMAIL_INVALID];
		return;
	}

	[self showLoader];

	NSDictionary *credentialsDictionary = [NSDictionary dictionaryWithObject:self.emailTextField.text.trimmedString forKey:@"username"];

	[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ForgotPasswordUrl] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 [self hideLoader];

		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 [self.parentVC showSuccessMessage:@"Mail has been sent to your mail"];

			 [self cancelButtonTapped:nil];
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 [self hideLoader];
		 [self.parentVC showErrorMessage:errorMessage];

	 }];

}

- (IBAction)outSideTapped:(id)sender
{
    [self cancelButtonTapped:nil];
}

@end

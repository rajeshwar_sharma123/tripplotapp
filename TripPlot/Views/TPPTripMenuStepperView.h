//
//  TPPTripMenuStepperView.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPTripMenuStepperView : UIView

@property (nonatomic, strong) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *step1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *step2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *step3ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *step1ArrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *step2ArrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *step3ArrowImageView;




@property (weak, nonatomic) IBOutlet UILabel *infoTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *routeTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *mediaTitleLabel;

-(void)setSelectedIndex:(NSInteger)index  isNew:(BOOL)isNew;

@end

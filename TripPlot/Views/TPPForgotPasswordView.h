//
//  TPPForgotPasswordView.h
//  TripPlot
//
//  Created by Daffolap-21 on 22/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPLogInViewController.h"

@interface TPPForgotPasswordView : UIView
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)submitButtonTapped:(id)sender;
@property TPPLogInViewController *parentVC;



@end

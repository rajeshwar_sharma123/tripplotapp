//
//  TPPReachedDestinationView.h
//  TripPlot
//
//  Created by Daffodil iPhone on 2/3/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPReachedDestinationView : UIView

@property (retain,atomic) TPPTripModel *tripModel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *endTripBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak) UIViewController  *parrentObj;
-(void)bindDataWithModel:(TPPTripModel *)model;

@end

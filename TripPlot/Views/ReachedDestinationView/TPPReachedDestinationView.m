//
//  TPPReachedDestinationView.m
//  TripPlot
//
//  Created by Daffodil iPhone on 2/3/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPReachedDestinationView.h"

@implementation TPPReachedDestinationView
{
TPPCreateTripModel *newTripObject;
    __weak IBOutlet UIView *checkBtnBackgroundView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib
{
    
    self.checkButton.layer.cornerRadius = 2.5;
    self.checkButton.clipsToBounds = YES;
    self.endTripBtn.layer.cornerRadius = 3.5;
    self.endTripBtn.clipsToBounds = YES;
    self.imgView.clipsToBounds = YES;

}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{

    if (touch.view == checkBtnBackgroundView) {
        return NO;
    }
    
       return YES;

}
- (IBAction)outSideTapped:(id)sender
{
    
    
    
    [self removeFromSuperview];
}
- (IBAction)endTripButtonAction:(UIButton *)sender {
    
    [self callServiceToEndTrip:self.tripModel];
    
   
    
}
- (IBAction)checkButtonAction:(UIButton *)sender {
    
    if (sender.selected) {
        
        self.checkButton.layer.cornerRadius = 2.5;
        sender.selected = NO;

    }
    else
    {
        self.checkButton.layer.cornerRadius = self.checkButton.frame.size.width/2;
        [self.checkButton setBackgroundImage:[UIImage imageNamed:@"icn_endtrip"] forState:UIControlStateSelected];
        sender.selected = YES;
        
    }

}

-(void)bindDataWithModel:(TPPTripModel *)model
{
    _tripModel = model;
    self.titleLabel.text = model.title;
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",model.tripLocations[0][@"name"],[model.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }

    
    self.tripDurationLabel.text = [TPPUtilities getTripDurationStringFromDictionary:model.tripDate];
    NSString *startDateStr = model.tripDate[@"startDate"];
    self.startDateLabel .text = startDateStr.currentDayWithStartDateString;
    
    
    [self.imgView sd_setImageWithURL:[model.images firstObject]
                          placeholderImage:[UIImage imageNamed:@"icn_placeholderImage"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    
    
    
}

- (void)callServiceToEndTrip:(TPPTripModel *)tripModel
{
    
    [self showLoader];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id ,@"tripId",
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:EndTripUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [self hideLivePlotOptionOnHome];
             
             if (self.checkButton.selected)
             {
                 [self createNewTrip];
                 [self outSideTapped:nil];
             }
             else
             {
                 [self outSideTapped:nil];
                
                 UIViewController *currentVc = [TPPUtilities getCurrentViewController];
                 [currentVc.navigationController popViewControllerAnimated:YES];
             }
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
         [self hideLoader];
     }];
    
    
    
}

-(void)hideLivePlotOptionOnHome
{
    TPPSharedTripCountsModel *trip = [TPPSharedTripCountsModel sharedInstance];
    trip.onGoingTripId = @"";
    trip.isOngoing = NO;
    
    UIViewController *currentVc = [TPPUtilities getCurrentViewController];
    
    for (UIViewController *vc in currentVc.navigationController.viewControllers)
    {
        if ([vc isKindOfClass:[TPPHomeViewController class]])
        {
            TPPHomeViewController *homeVC = (TPPHomeViewController *)vc;
            homeVC.liveTripButton.hidden = YES;
            
            break;
        }
    }
}


- (void)createNewTrip
{
    
    newTripObject = [TPPCreateTripModel sharedInstance];
    NSDictionary *profileDetail = [UserDeafaults objectForKey:UserProfile];
    newTripObject.token = [UserDeafaults objectForKey:Token];
    newTripObject.username = profileDetail[@"emailid"];
    newTripObject.profileImage = profileDetail[@"profileImage"];
    newTripObject._id = profileDetail[@"_id"];
    newTripObject.firstname = profileDetail[@"firstname"];
    newTripObject.createdBy = profileDetail[@"_id"];
    
    newTripObject.isNew = YES;
    newTripObject.tripStatus = TripStatus_UpcomingTrip;
    
    TPPCreateNewTripViewController *VC = [_parrentObj.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
    //[self.parrentObj.navigationController pushViewController:VC animated:YES];

    
    UIViewController *currentVc = [TPPUtilities getCurrentViewController];
    [currentVc.navigationController popViewControllerAnimated:NO];
    
    currentVc = [TPPUtilities getCurrentViewController];
    [currentVc.navigationController pushViewController:VC animated:YES];

}


@end

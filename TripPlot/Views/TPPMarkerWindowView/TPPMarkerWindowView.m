//
//  TPPMarkerWindowView.m
//  TripPlot
//
//  Created by Daffolap-21 on 11/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPMarkerWindowView.h"

@implementation TPPMarkerWindowView



-(void) configureMarkerWindow:(TPPTripModel *)trip withLocationTypeImageURL:(NSString *) url selectedIndex:(NSInteger )selectedLocationTypeIndex
{
	self.layer.cornerRadius = self.frame.size.height/2;
	self.locationTypeImageView.layer.cornerRadius = self.locationTypeImageView.frame.size.height/2;
	self.titleLabel.text = [NSString stringWithFormat:@"%@",trip.title];
	self.likeCountLabel.text = [NSString stringWithFormat:@"%ld",trip.likeCount];
	self.distanceLabel.text = [NSString stringWithFormat:@"%ld kms away",[trip.distance integerValue]/1000 ];

	//if (selectedLocationTypeIndex>0) {
	[self.locationTypeImageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"icn_placeholderImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
		self.locationTypeImageView.layer.cornerRadius = self.locationTypeImageView.frame.size.height/2;
	}];
//	}
//	else
//	{
//		self.locationTypeImageView.image = [UIImage imageNamed:@"avatar_tripplot"];
//	}
}

@end

//
//  TPPMarkerWindowView.h
//  TripPlot
//
//  Created by Daffolap-21 on 11/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.

#import <UIKit/UIKit.h>

@interface TPPMarkerWindowView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *locationTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

-(void) configureMarkerWindow:(TPPTripModel *)trip withLocationTypeImageURL:(NSString *) url selectedIndex:(NSInteger )selectedLocationTypeIndex;

@end

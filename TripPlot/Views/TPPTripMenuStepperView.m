//
//  TPPTripMenuStepperView.m
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPTripMenuStepperView.h"

@implementation TPPTripMenuStepperView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    // [[NSBundle mainBundle] loadNibNamed:@"TPPTripMenuStepperView" owner:self options:nil];
    // [self addSubview:self.view];
}

-(void)setSelectedIndex:(NSInteger)index  isNew:(BOOL)isNew
{
    _infoTitleLabel.text = @" Info";
    _routeTitleLabel.text = @" Route";
    if (isNew) {
        _mediaTitleLabel.text = @"  Members";
    }
    else{
        _mediaTitleLabel.text = @" Media";
    }
    if (index == 1) {
        [_step1ArrowImageView setHighlighted:YES];
        [_step1ImageView setHighlighted:YES];
        _infoTitleLabel.textColor = COLOR_ORANGE;
        _routeTitleLabel.textColor = TPPTextColor;
        _mediaTitleLabel.textColor = TPPTextColor;
    }
    else if (index == 2) {
        [_step2ArrowImageView setHighlighted:YES];
        [_step2ImageView setHighlighted:YES];
        _infoTitleLabel.textColor = TPPTextColor;
        _routeTitleLabel.textColor = COLOR_ORANGE;
        _mediaTitleLabel.textColor = TPPTextColor;
    }
    else if (index == 3) {
        [_step3ArrowImageView setHighlighted:YES];
        [_step3ImageView setHighlighted:YES];
        _infoTitleLabel.textColor = TPPTextColor;
        _routeTitleLabel.textColor = TPPTextColor;
        _mediaTitleLabel.textColor = COLOR_ORANGE;
    }
}

@end

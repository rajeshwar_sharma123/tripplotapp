//
//  TPPNoDataFoundView.m
//  TripPlot
//
//  Created by Vishwas Singh on 20/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPNoDataFoundView.h"

@implementation TPPNoDataFoundView


-(void)awakeFromNib
{
    self.retryButton.layer.cornerRadius  = 2.0;
    self.retryButton.clipsToBounds = YES;
}

- (IBAction)retryButtonTapped:(id)sender
{
    [self.delegate retryButtonTapped];
}




@end

//
//  TPPNoDataFoundView.h
//  TripPlot
//
//  Created by Vishwas Singh on 20/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPNoDataFoundView : UIView

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UIImageView *retryImageView;

@property (weak,nonatomic) id<TPPNoDataFoundDelegate> delegate;


@end

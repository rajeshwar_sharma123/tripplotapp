//
//  TPPCompletedTripExpandedTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPCompletedTripExpandedTableViewCell.h"
#import "MontageView_FiveImage.h"
#import "MontageView_FourImage.h"
#import "MontageView_OneImage.h"
#import "MontageView_ThreeImage.h"
#import "MontageView_TwoImage.h"


@implementation TPPCompletedTripExpandedTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
   
    NSURL *url = [NSURL URLWithString:model.montage];
  
    if (!_moviePlayerController)
    {
        _moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    }
    else
    {
        [_moviePlayerController pause];
        [_moviePlayerController.view removeFromSuperview];
        _moviePlayerController = nil;

        _moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    }
    
   // [self setMontageViewImagesFrameWithImagesArray:model.images];
    [self setMontageViewWithImageArray:model.images];
}


- (IBAction)playButtonTapped:(id)sender
{
    if (_moviePlayerController.contentURL == nil)
    {
        [[TPPUtilities getCurrentViewController] showErrorMessage:@"No video found"];
        
        return;
    }
    
    
    CGRect frame = _montageView.frame;
    _moviePlayerController.view.frame = frame;
    
    [self addSubview:_moviePlayerController.view];

    [_moviePlayerController play];
}

//-(UIImage *)getThumbNailImage
//{
//    UIImage *thumnailImage = [_moviePlayerController thumbnailImageAtTime:0 timeOption:MPMovieTimeOptionNearestKeyFrame];
//    
//    return thumnailImage;
//}

-(void)setMontageViewImagesFrameWithImagesArray:(NSArray *)_images
{
    CGSize montageSize = _montageView.frame.size;
    
    UIImage *placeholderImage = [UIImage imageNamed:@"icn_placeholderImage"];
    if (_images.count >= 5)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]]placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]]placeholderImage:placeholderImage];
        [_image4 sd_setImageWithURL:[NSURL URLWithString:_images[3]]placeholderImage:placeholderImage];
        [_image5 sd_setImageWithURL:[NSURL URLWithString:_images[4]]placeholderImage:placeholderImage];
    }
    else if (_images.count == 4)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]]placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]]placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]]placeholderImage:placeholderImage];
        [_image4 sd_setImageWithURL:[NSURL URLWithString:_images[3]]placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width/2, montageSize.height/2);
        _image2.frame = CGRectMake(montageSize.width/2, 0, montageSize.width/2, montageSize.height/2);
        _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width/2, montageSize.height/2);
        _image4.frame = CGRectMake(montageSize.width/2, montageSize.height/2, montageSize.width/2, montageSize.height/2);
        
        _image5.frame = CGRectZero;
        
    }
    else if (_images.count == 3)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]]placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]]placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]]placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width/2, montageSize.height/2);
        _image2.frame = CGRectMake(montageSize.width/2, 0, montageSize.width/2, montageSize.height/2);
         _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width, montageSize.height/2);
        
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;
    }
    else if (_images.count == 2)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]]placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[1]]placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height/2);
        _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width, montageSize.height/2);
        
        _image2.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;

    }
    else if (_images.count == 1)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]]placeholderImage:placeholderImage];
        
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height);
        
        _image2.frame = CGRectZero;
        _image3.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;

    }
    else if (!_images || (_images.count == 0))
    {
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height);
        
        _image2.frame = CGRectZero;
        _image3.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;

    }
}

-(void)setMontageViewWithImageArray:(NSArray *)images
{
    MontageView_FiveImage *montageView;
    
    if (images.count == 0)
    {
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_OneImage" owner:self options:nil] objectAtIndex:0];
         montageView.frame = CGRectMake(0, 0, self.montageView.frame.size.width,  self.montageView.frame.size.height);
        
        [self.montageView removeSubviewsOfKindOfClasses:@[@"UIView"]];
        [self.montageView addSubview:montageView];
        
        [self bringSubviewToFront:self.playView];
        
        return;
    }
    else if (images.count >= 5)
    {
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_FiveImage" owner:self options:nil] objectAtIndex:0];
    }
    else if (images.count == 4)
    {
        
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_FourImage" owner:self options:nil] objectAtIndex:0];
    }
    else if (images.count == 3)
    {
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_ThreeImage" owner:self options:nil] objectAtIndex:0];
    }
    else if (images.count == 2)
    {
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_TwoImage" owner:self options:nil] objectAtIndex:0];
    }
    else if (images.count == 1)
    {
        montageView = [[[NSBundle mainBundle] loadNibNamed:@"MontageView_OneImage" owner:self options:nil] objectAtIndex:0];
    }

    [montageView setImages:images];
    montageView.frame = CGRectMake(0, 0, self.montageView.frame.size.width,  self.montageView.frame.size.height);

    [self.montageView removeSubviewsOfKindOfClasses:@[@"UIView"]];
    [self.montageView addSubview:montageView];
    
    [self bringSubviewToFront:self.playView];
}



@end

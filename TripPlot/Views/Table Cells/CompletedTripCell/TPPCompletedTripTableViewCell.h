//
//  TPPCompletedTripTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TPPCompletedTripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *travellerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDetailDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property(retain,nonatomic) TPPTripModel *tripModel;


@property (weak, nonatomic) IBOutlet UIView *tapViewForDetailScreen;
@property (weak, nonatomic) IBOutlet UIView *tripInfoView;


@property (weak,nonatomic) id<TPPCellTapDelegate> cellTapDelegate;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tapViewHeightConstraint;

-(void)setUpForMyTripsScreen;
-(void)bindDataWithModel:(TPPTripModel *)model;


@end

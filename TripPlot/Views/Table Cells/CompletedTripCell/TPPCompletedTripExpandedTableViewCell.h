//
//  TPPCompletedTripExpandedTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface TPPCompletedTripExpandedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet UIImageView *image4;
@property (weak, nonatomic) IBOutlet UIImageView *image5;

@property (weak, nonatomic) IBOutlet UIView *montageView;
@property (weak, nonatomic) IBOutlet UIView *playView;


@property MPMoviePlayerController *moviePlayerController;


-(void)bindDataWithModel:(TPPTripModel *)model;

@end

//
//  TPPCompletedTripTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPCompletedTripTableViewCell.h"

@implementation TPPCompletedTripTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.clipsToBounds = YES;
    
    self.tripInfoView.layer.cornerRadius = 2.0;
    self.tripInfoView.clipsToBounds = YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.travellerNameLabel.text = [NSString stringWithFormat:@" %@", model.createdBy[@"firstname"]];
    self.titleLabel.text = model.title;
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",model.tripLocations[0][@"name"],[model.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }

    self.likeCountLabel.text = [NSString stringWithFormat:@"%ld",model.like.count];
    self.locationDetailDescriptionLabel.text = model.locationDescription;
    
    NSString *dateString = model.tripDate[@"endDate"];
    self.timeLabel.text = dateString.timeAgoString;
    
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:model.createdBy[@"profileImage"]]
                      placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                               options:SDWebImageRefreshCached
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    
    self.tripModel = model;
    
    TPPScrollMenu *menu = (TPPScrollMenu *)[self.expandableView subViewOfKindOfClass:[TPPScrollMenu class]];
    if (!menu) {
        menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    }
  
    menu.tripModel = _tripModel;
    menu.backgroundColorForButtons = Color_CompletedTripCellButtonBackground;
    
    [self.expandableView addSubview:menu];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapViewTapped:)];
    [self.tapViewForDetailScreen addGestureRecognizer:tapGesture];
    
}


-(void)setUpForMyTripsScreen
{
    _tapViewHeightConstraint.constant = 0;
    _tapViewForDetailScreen.hidden = YES;
}

- (IBAction)tapViewTapped:(id)sender
{
    UITableView *tableView = (UITableView *)self.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.cellTapDelegate cellTappedAtIndexPath:indexPath tripModel: _tripModel];
}


@end

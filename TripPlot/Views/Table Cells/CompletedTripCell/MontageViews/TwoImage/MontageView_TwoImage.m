//
//  MontageView_TwoImage.m
//  TripPlot
//
//  Created by Vishwas Singh on 18/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "MontageView_TwoImage.h"

@implementation MontageView_TwoImage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setImages:(NSArray *)images
{
    UIImage *placeholderImage = [UIImage imageNamed:@"icn_placeholderImage"];
    
    [self.image1 sd_setImageWithURL:images[0] placeholderImage:placeholderImage];
    [self.image2 sd_setImageWithURL:images[1] placeholderImage:placeholderImage];
    
}

@end

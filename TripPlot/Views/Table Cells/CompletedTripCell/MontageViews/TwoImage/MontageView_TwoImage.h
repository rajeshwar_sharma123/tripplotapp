//
//  MontageView_TwoImage.h
//  TripPlot
//
//  Created by Vishwas Singh on 18/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MontageView_TwoImage : UIView

@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;

-(void)setImages:(NSArray *)images;

@end

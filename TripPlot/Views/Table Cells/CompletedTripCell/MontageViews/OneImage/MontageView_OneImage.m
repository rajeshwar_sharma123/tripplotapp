//
//  MontageView_OneImage.m
//  TripPlot
//
//  Created by Vishwas Singh on 18/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "MontageView_OneImage.h"

@implementation MontageView_OneImage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)setImages:(NSArray *)images
{
    
    
    UIImage *placeholderImage = [UIImage imageNamed:@"icn_placeholderImage"];
    
    [self.imageView sd_setImageWithURL:images[0] placeholderImage:placeholderImage];
}


@end

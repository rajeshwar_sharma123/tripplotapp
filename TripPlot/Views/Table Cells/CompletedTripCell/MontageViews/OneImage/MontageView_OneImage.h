//
//  MontageView_OneImage.h
//  TripPlot
//
//  Created by Vishwas Singh on 18/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MontageView_OneImage : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

-(void)setImages:(NSArray *)images;

@end

//
//  TPPContactListingCell.m
//  TripPlot
//
//  Created by daffolapmac on 24/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPContactListingCell.h"

@implementation TPPContactListingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

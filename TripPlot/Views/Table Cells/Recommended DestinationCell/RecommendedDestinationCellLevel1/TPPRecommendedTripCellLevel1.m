//
//  TPPRecommendedTripCellLevel1.m
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPRecommendedTripCellLevel1.h"

@implementation TPPRecommendedTripCellLevel1

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.titleLabel.text = model.descriptions;
    self.friendsCountLabel.text = [NSString stringWithFormat:@"%ld Friends have been there",model.tripMembers.count];
    self.tripsCountLabel.text = [NSString stringWithFormat:@"%ld trip plots",[model.totalTrips[@"count"] integerValue]];
    
    if(model.tripMembers.count>0)
    {
        [self addTripMembersView:model.tripMembers];
    }
    else
    {
        [self.friendsScrollView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
    }
}


-(void)addTripMembersView:(NSArray *)tripMembers
{
    int x = self.friendsScrollView.frame.origin.x, i=0;
    
    [self.friendsScrollView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
    
    for (NSDictionary *memberDict in tripMembers)
    {
        CGRect frame =  CGRectMake(x, 0, 40, 40);
        UIButton *button = [[UIButton alloc]initWithFrame:frame];
        button.layer.cornerRadius = button.frame.size.height/2;
        button.clipsToBounds = YES;
        
        [button addTarget:self action:@selector(memberButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        NSURL *imageUrl = [NSURL URLWithString:memberDict[@"profileImage"]];
        [button.imageView sd_setImageWithURL: imageUrl
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                [button setImage:image forState:UIControlStateNormal];
             }
             else
             {
                 [button setImage:[UIImage imageNamed:@"icn_defaultuser"] forState:UIControlStateNormal];
             }
            
         }];
        
        button.tag = i++;
        x = x+45;
        
        [self.friendsScrollView addSubview:button];
    }

    self.friendsScrollView.contentSize = CGSizeMake(x, self.friendsScrollView.frame.size.height);

}

-(void)memberButtonTapped:(UIButton *)sender
{
    
}


-(void)rotateArrowWithData:(TPPCommonTripModel *)commonModel
{
    
    float radians =commonModel.isExpanded?0 : M_PI;
    
    [UIView animateWithDuration:AnimationDuration
                     animations:^{
                         self.arrowImageView.transform = CGAffineTransformMakeRotation(radians);
                     }];
    
}


@end

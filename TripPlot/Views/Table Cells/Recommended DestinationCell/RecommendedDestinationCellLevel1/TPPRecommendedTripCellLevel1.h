//
//  TPPRecommendedTripCellLevel1.h
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPRecommendedTripCellLevel1 : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripsCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *friendsScrollView;


-(void)bindDataWithModel:(TPPTripModel *)model;
-(void)rotateArrowWithData:(TPPCommonTripModel *)commonModel;

@end

//
//  TPPRecommendedTripTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPRecommendedTripTableViewCell.h"

@implementation TPPRecommendedTripTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.mainView.layer.cornerRadius = 2.0;
    self.mainView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.tripModel = model;
    
    self.placeNameLabel.text = model.title;
    self.distanceLabel.text = model.locationDescription;
    self.tripsLabel.text =  [NSString stringWithFormat:@"%ld Trips",[model.totalTrips[@"count"] integerValue]];
    self.likesCountLabel.text = [NSString stringWithFormat:@"%ld",model.likeCount];
        
    [self setUpGalleryWithImageArray:model.images];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapViewTapped:)];
    [self.tapViewForDetailScreen addGestureRecognizer:tapGesture];
}



-(void)setUpGalleryWithImageArray:(NSArray *)imageArray
{

    NSMutableArray *items = [NSMutableArray new];
    for (NSString *imageUrl in imageArray)
    {
        DKCarouselURLItem *urlAD = [DKCarouselURLItem new];
        urlAD.imageUrl = imageUrl;
        
        [items addObject:urlAD];
    }
    
    self.galleryView.indicatorTintColor = TPPGreenColor;
    self.galleryView.indicatorOffset = CGPointMake(10, 0); // to move the indicator on left
    self.galleryView.finite = NO;
    self.galleryView.defaultImage = [UIImage imageNamed:@"nature"];
    //[self.galleryView setAutoPagingForInterval:5];
    [self.galleryView setItems:items];
    
    

    [self.galleryView setDidSelectBlock:^(DKCarouselItem *item, NSInteger index) {
        
    }];
    
    [self.galleryView setDidChangeBlock:^(DKCarouselView *view, NSInteger index) {
        
    }];
}

-(void)hideHeader
{
    self.tapViewForDetailScreen.hidden = YES;
    __tapViewHeightConstraint.constant = 0;
}

- (IBAction)addButtonTapped:(id)sender
{
	[self.plotTripDelegate goHereButtonTapped:sender indexPath:self.indexPath tripModel:_tripModel];
}

- (IBAction)tapViewTapped:(id)sender
{
    UITableView *tableView = (UITableView *)self.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    [self.cellTapDelegate cellTappedAtIndexPath:indexPath tripModel: _tripModel];
}



@end

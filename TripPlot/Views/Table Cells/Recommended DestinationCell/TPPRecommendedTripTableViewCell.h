//
//  TPPRecommendedTripTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>





@interface TPPRecommendedTripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *placeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripsLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesCountLabel;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet DKCarouselView *galleryView;

@property(retain,nonatomic) TPPTripModel *tripModel;
@property (weak, nonatomic) IBOutlet UIView *tapViewForDetailScreen;
@property (weak,nonatomic) id<TPPCellTapDelegate> cellTapDelegate;
@property (weak,nonatomic) id<TPPShareCellDelegate> plotTripDelegate;

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *_tapViewHeightConstraint;

-(void)bindDataWithModel:(TPPTripModel *)model;
-(void)hideHeader;


@end

//
//  TPPRecommendedTripCellLevel2.h
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface TPPRecommendedTripCellLevel2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *travellerDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *seeMoreButton;

@property (weak,nonatomic) id<TPPSeeMoreButtonDelegate> seeMoreButtonDelegate;


-(void)bindDataWithModel:(TPPTripModel *)model;


@end

//
//  TPPRecommendedTripCellLevel2.m
//  TripPlot
//
//  Created by Vishwas Singh on 29/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPRecommendedTripCellLevel2.h"

@implementation TPPRecommendedTripCellLevel2

- (void)awakeFromNib {
    // Initialization code
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.travellerDescriptionLabel.text = [NSString stringWithFormat:@"%@ by %@",model.statusLable, model.createdBy[@"firstname"]];
    self.titleLabel.text = model.title;

    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:model.createdBy[@"profileImage"]]
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];

}

- (IBAction)seeMoreButtonTapped:(id)sender
{
    UITableView *tableView = (UITableView *)self.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    [self.seeMoreButtonDelegate seeMoreButtonTappedWithIndexPath:indexPath];
}


@end

//
//  TPPAcceptRejectTableViewCell.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/21/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPAcceptRejectTableViewCell.h"

@implementation TPPAcceptRejectTableViewCell
{
   TPPCreateTripModel *newTripObject;
}
- (void)awakeFromNib {
    // Initialization code
}
- (IBAction)acceptButtonClickedAction:(id)sender {
   
   
    
    if (self.tripModel.tripStatus == TripStatus_CompletedTrip) {

        UIViewController *challengeVC = (UIViewController *)self.delegate;
        newTripObject = [TPPCreateTripModel sharedInstance];
        NSDictionary *profileDetail = [UserDeafaults objectForKey:UserProfile];
        newTripObject.token = [UserDeafaults objectForKey:Token];
        newTripObject.username = profileDetail[@"emailid"];
        newTripObject.profileImage = profileDetail[@"profileImage"];
        newTripObject._id = profileDetail[@"_id"];
        newTripObject.firstname = profileDetail[@"firstname"];
        newTripObject.createdBy = profileDetail[@"_id"];
        newTripObject.title = self.tripModel.title;
        newTripObject.typeNames = self.tripModel.tripTypes.tokensNameArray;
        newTripObject.modeNames = self.tripModel.tripModes.tokensNameArray;
        newTripObject.createTripSource = CreateTripSource_Challenge;
        newTripObject.isNew = YES;
        newTripObject.tripStatus = TripStatus_UpcomingTrip;
        if (_selectedIndexPath)newTripObject.selectedIndex = self.selectedIndexPath.row;
        else newTripObject.selectedIndex = self.indexPath.row;
        
        newTripObject.tripModel = self.tripModel;
        
        TPPCreateNewTripViewController *VC = [challengeVC.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
         VC.sourceDestinationsArray = [self.tripModel.tripLocations getLocationsInSourceDestinationModelFormat:self.tripModel.tripLocations];
        
        [challengeVC.navigationController pushViewController:VC animated:YES];
           
    }
    else if (self.tripModel.tripStatus == TripStatus_UpcomingTrip) {
        
         [self callServiceToAcceptRejectInviteTripWithTripModel:self.tripModel url:[TPPServiceUrls getCompleteUrlFor:AcceptInviteUrl] tag:[NSNumber numberWithInteger:RequestOption_Accept] button:sender];
        
    }
    
    
    
    
}
- (IBAction)rejectButtonClickedAction:(id)sender {
  
    
    if (self.tripModel.tripStatus == TripStatus_CompletedTrip) {
        
        [self callServiceToAddRejectedTripInBucket:self.tripModel button:sender];
    }
    else if (self.tripModel.tripStatus == TripStatus_UpcomingTrip) {
        
        [self callServiceToAddRejectedTripInBucket:self.tripModel button:sender];
    }


}

- (void)popViewController
{
    UIViewController *vc = [TPPUtilities getCurrentViewController];
    [vc.navigationController popViewControllerAnimated:YES];
}

-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
    self.tripModel = tripModel;
    if (_tripModel.tripStatus == TripStatus_CompletedTrip)
    {
        _btnAccept.backgroundColor = [UIColor blackColor];
        _btnReject.backgroundColor = [UIColor blackColor];
        
        [_btnAccept setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal ];
        [_btnReject setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal ];
        
    }
    
}

- (void)callServiceToAddRejectedTripInBucket:(TPPTripModel *)tripModel button:(UIButton *)sender
{
    
    [sender showSpinnerWithUserInteractionDisabled:YES];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id ,@"tripId",
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ADDInBucket] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [sender hideSpinner];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
           
             [self callApiToRejectTrip:sender];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         if ([responseObject[@"response"][@"code"] integerValue] == 201)
         {
             [self callApiToRejectTrip:sender];
         }
         
         [sender hideSpinner];
     }];
    

    
}

- (void)callApiToRejectTrip:(UIButton *)sender
{
    if (self.tripModel.tripStatus == TripStatus_CompletedTrip) {
        
        [self callServiceToRejectChallenge:self.tripModel button:sender];
    }
    else if (self.tripModel.tripStatus == TripStatus_UpcomingTrip) {
        
        [self callServiceToAcceptRejectInviteTripWithTripModel:self.tripModel url:[TPPServiceUrls getCompleteUrlFor:RejectInviteUrl] tag:[NSNumber numberWithInteger:RequestOption_Reject] button:sender];
    }

}

-(void)callServiceToAcceptRejectInviteTripWithTripModel:(TPPTripModel *)tripModel url:(NSString *)url tag:(NSNumber *)tag button:(UIButton *)sender
{
    [sender showSpinnerWithUserInteractionDisabled:YES];
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tag ,@"accept",
                                           tripModel._id ,@"tripId",
                                           tripModel.createdBy[@"_id"],@"createdById",
                                        nil];
    
    
    [TPPAppServices postServiceWithUrl:url data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [sender hideSpinner];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             if ([_delegate respondsToSelector:@selector(acceptRejectButtonTapped:)])
             {
                 if (_selectedIndexPath) {
                     
                     [_delegate acceptRejectButtonTapped:self.selectedIndexPath];
                     [self popViewController];
                 }
                 else
                 {
                     [_delegate acceptRejectButtonTapped:self.indexPath];
                 }
                 
             }
            
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         if ([responseObject[@"response"][@"code"] integerValue] == 204)
         {
             UIViewController *vc = [TPPUtilities getCurrentViewController];
             [vc showErrorMessage:errorMessage];
         }
         [sender hideSpinner];
     }];
    
}
-(void)callServiceToRejectChallenge:(TPPTripModel *)tripModel button:(UIButton *)sender
{
   [sender showSpinnerWithUserInteractionDisabled:YES];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           [NSNumber numberWithInteger:RequestOption_Reject] ,@"acceptChallenge",
                                           tripModel.title ,@"tripTitle",
                                           tripModel.createdBy[@"_id"] ,@"createdById",
                                           tripModel.createdBy[@"firstname"] ,@"createdByFirstName",
                                           tripModel.challenge[@"challengeId"],@"challangeInfo",
                                           tripModel.challenge[@"challengeFrom"][@"_id"],@"challengeFromId",
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:AcceptChallengeUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         [sender hideSpinner];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
//             if ([_delegate respondsToSelector:@selector(acceptRejectButtonTapped:)])
//             {
//                 [_delegate acceptRejectButtonTapped:self.indexPath];
//             }
             
             if (_selectedIndexPath) {
                 
                 [_delegate acceptRejectButtonTapped:self.selectedIndexPath];
                 [self popViewController];
             }
             else
             {
                 [_delegate acceptRejectButtonTapped:self.indexPath];
             }
             
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [sender hideSpinner];
     }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

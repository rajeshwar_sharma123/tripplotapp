//
//  TPPAcceptRejectTableViewCell.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/21/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TPPAcceptRejectCellDelegate <NSObject>

-(void)acceptRejectButtonTapped:(NSIndexPath *)indexPath;

@end
@interface TPPAcceptRejectTableViewCell : UITableViewCell

@property (retain,atomic) TPPTripModel *tripModel;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
-(void)bindDataWithModel:(TPPTripModel *)tripModel;
@property(weak) id<TPPAcceptRejectCellDelegate> delegate;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@end

//
//  TPPNotificationDropDownTableViewCell.m
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPNotificationDropDownTableViewCell.h"

@interface TPPNotificationDropDownTableViewCell ()
{
//    UIImageView *userImageView;
//    UILabel *notificationLabel;
//    UILabel *timeLabel;
}
@end

@implementation TPPNotificationDropDownTableViewCell

- (void)awakeFromNib
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    [_userImageView makeCircular];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController indexPath:(NSIndexPath *)indexPath
{
    // Fetch Record
    TripEntity *trip = [fetchedResultsController objectAtIndexPath:indexPath];
    MemberEntity *member = [[trip.members allObjects] lastObject];
    
    NSString *message = [TPPCoreDataHandler getFormattedNotification:trip];
    
    if ([trip.event isEqualToString:inviteAcceptEvent])
    {
        _userImageView.image = [UIImage imageNamed:@"icn__0001_Accepted"];
    }
    else
    {
        [_userImageView sd_setImageWithURL:[NSURL URLWithString:member.imageUrl] placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]];
    }
    
    _timeLabel.text = [trip.updatedOn getStringFromat].timeAgoString;
    _notificationLabel.text = message;
    
    [self layoutIfNeeded];

    if (indexPath.row > ([TPPNotificationEventHandler sharedInstance].notificationCount-1))
    {
        _notificationLabel.textColor = [UIColor lightGrayColor];
    }
    
}


@end

//
//  TPPNotificationDropDownTableViewCell.h
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPNotificationDropDownTableViewCell : UITableViewCell


@property(nonatomic, weak) IBOutlet UIImageView *userImageView;
@property(nonatomic, weak) IBOutlet  UILabel *notificationLabel;
@property(nonatomic, weak) IBOutlet  UILabel *timeLabel;

-(void)configureCellWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController indexPath:(NSIndexPath *)indexPath;

@end
;
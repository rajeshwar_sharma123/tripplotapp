//
//  TPPUpcomingTripCellLevel1.m
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPUpcomingTripCellLevel1.h"


@interface TPPUpcomingTripCellLevel1 ()

{
    TPPLocationMenuView *tripPointsView;
}

@end


@implementation TPPUpcomingTripCellLevel1



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.tripModel = model;

	NSString *encodedPolyline = model.route[@"overviewPolyline"];
    [_mapSnapShotImageView showSpinner];
    [_mapSnapShotImageView sd_setImageWithURL:[TPPUtilities getMapSnapshotUrlWithSegmentsArray: encodedPolyline snapShotSize:_mapSnapShotImageView.frame.size] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [_mapSnapShotImageView hideSpinner];
     }];

    self.routeLabel.text = [TPPUtilities getRouteForTrip:model];
    
    [self.bottomScrollView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
    
    [self addTripPointsView];
    
    
    [tripPointsView setBadgesWithTripModel:model];
    
 
}

-(void)tripPointButtonTapped:(UIButton *)sender
{
    [self.tripPointTapDelegate tripPointTappedAtIndexPath:self.indexPath tripModel:_tripModel buttonIndex:sender.tag];
}

-(void)addTripPointsView
{
    [self removeSubviewsOfKindOfClasses:@[@"TPPLocationMenuView"]];
    
    tripPointsView = [[[NSBundle mainBundle] loadNibNamed:@"TPPLocationMenuView" owner:self options:nil] objectAtIndex:0];
    [tripPointsView.restaurantButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.fuelStationButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.stopageButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.atmButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.hotelButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.hospitalButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    tripPointsView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    
    [self addSubview:tripPointsView];
    
    tripPointsView.frame = CGRectMake(_mapSnapShotImageView.frame.origin.x+8, (_mapSnapShotImageView.frame.origin.y+_mapSnapShotImageView.frame.size.height), _mapSnapShotImageView.frame.size.width, tripPointsView.frame.size.height);
    
}


@end

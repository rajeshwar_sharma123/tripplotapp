//
//  TPPUpcomingTripCellLevel1.h
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TPPUpcomingTripCellLevel1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *routeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mapSnapShotImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *bottomScrollView;

@property (weak,nonatomic) id<TPPTripPointTapDelegate> tripPointTapDelegate;

@property(retain,nonatomic) TPPTripModel *tripModel;

-(void)bindDataWithModel:(TPPTripModel *)model;


@end

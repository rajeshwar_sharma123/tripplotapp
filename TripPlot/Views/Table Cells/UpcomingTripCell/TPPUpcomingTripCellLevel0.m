//
//  TPPUpcomingTripCellLevel0.m
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPUpcomingTripCellLevel0.h"

@implementation TPPUpcomingTripCellLevel0

- (void)awakeFromNib {
    // Initialization code
    
    
    [self.userImageView makeCircular];
    
   
    self.gradientBGView.layer.cornerRadius = 2.0;
    self.gradientBGView.clipsToBounds = YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    self.travellerNameLabel.text =  model.createdBy[@"firstname"];
    self.titleLabel.text = model.title;
    
    if (model.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",model.tripLocations[0][@"name"],[model.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }
    
    self.tripDurationLabel.text = [TPPUtilities getTripDurationStringFromDictionary:model.tripDate];
    
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:model.createdBy[@"profileImage"]]
                      placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                               options:SDWebImageRefreshCached
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    

    TPPScrollMenu *menu ;
    [self.expandableView  removeSubviewsOfKindOfClasses:@[@"TPPScrollMenu"]];
//    menu = (TPPScrollMenu *)[self.expandableView subViewOfKindOfClass:[TPPScrollMenu class]];
    
    if (!menu)
    {
        menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    }
    menu.tripModel = model;
    menu.backgroundColorForButtons = Color_UpcomingTripCellButtonBackground;
    
    [self.expandableView addSubview:menu];
    [menu configure];
    _menu = menu;
    
    
    
    self.gradientBGView.gradientOrientation = GRKGradientOrientationDown;
    self.gradientBGView.gradientColors = [NSArray arrayWithObjects:TPPUpcomingCellGradientColorStart, TPPUpcomingCellGradientColorMidle,TPPUpcomingCellGradientColorEnd,nil];
    
}

-(void)setUpForMyTripsScreen
{
    self.userView.hidden = YES;
    _userViewHeightConstraint.constant = 0;
}



@end

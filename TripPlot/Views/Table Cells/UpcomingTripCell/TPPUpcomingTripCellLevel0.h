//
//  TPPUpcomingTripCellLevel0.h
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPUpcomingTripCellLevel0 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *travellerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet GRKGradientView *gradientBGView;

@property TPPScrollMenu *menu;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userViewHeightConstraint;

-(void)setUpForMyTripsScreen;
-(void)bindDataWithModel:(TPPTripModel *)model;


@end

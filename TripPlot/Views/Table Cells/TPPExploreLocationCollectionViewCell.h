//
//  TPPExploreLocationCollectionViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 10/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPExploreLocationCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *locationTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

-(void) configureCell:(NSDictionary *) locationType forIndex:(NSIndexPath *)indexPath withBadgeValue:(NSInteger )badgeCount selectedIndex:(NSInteger ) selectedIndex;

@end

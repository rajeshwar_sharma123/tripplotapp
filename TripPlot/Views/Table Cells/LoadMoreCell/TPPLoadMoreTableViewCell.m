//
//  TPPLoadMoreTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPLoadMoreTableViewCell.h"

@implementation TPPLoadMoreTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [_spinner startAnimating];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TPPLoadMoreTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPLoadMoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

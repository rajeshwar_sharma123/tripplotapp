//
//  TPPPlotDropDownTableViewCell.m
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPPlotDropDownTableViewCell.h"

@implementation TPPPlotDropDownTableViewCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
    UILabel *lblDescription = (UILabel *)[self viewWithTag:11];
    UILabel *lblTime = (UILabel *)[self viewWithTag:12];
    
    
    lblDescription.textColor = ColorFromRGB(0xEDEDED);
    lblDescription.font = [UIFont systemFontOfSize:17.0f];
    lblDescription.text = tripModel.title;
    
    lblTime.textColor = ColorFromRGB(0x626061);
    lblTime.font = [UIFont systemFontOfSize:14.0f];
    lblTime.text = [tripModel.tripDate[@"endDate"] timeAgoString];


}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

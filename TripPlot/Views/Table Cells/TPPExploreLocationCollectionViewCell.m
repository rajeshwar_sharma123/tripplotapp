//
//  TPPExploreLocationCollectionViewCell.m
//  TripPlot
//
//  Created by Daffolap-21 on 10/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPExploreLocationCollectionViewCell.h"

@implementation TPPExploreLocationCollectionViewCell



-(void) configureCell:(NSDictionary *) locationType forIndex:(NSIndexPath *)indexPath withBadgeValue:(NSInteger )badgeCount selectedIndex:(NSInteger ) selectedIndex
{
	self.locationTypeImageView.layer.cornerRadius = self.locationTypeImageView.frame.size.height/2;
	if (indexPath.row == selectedIndex) {
		self.nameLabel.textColor = COLOR_ORANGE;

		if (badgeCount == 0) {
            
			[self.badgeLabel setHidden:YES];
		}
		else
		{
			self.badgeLabel.backgroundColor = [UIColor redColor];
			self.badgeLabel.layer.cornerRadius = self.badgeLabel.frame.size.height/2;
			self.badgeLabel.clipsToBounds = YES;
			[self.badgeLabel setHidden:NO];
			self.badgeLabel.text = [NSString stringWithFormat:@"%ld",(long)badgeCount];
		}
	}
	else{
		self.nameLabel.textColor = [UIColor whiteColor];
		[self.badgeLabel setHidden:YES];
	}
	if (indexPath.row == 0) {
		self.nameLabel.text = @"ALL";
		self.locationTypeImageView.image = [UIImage imageNamed:@"01__0002_all_normal"];

	}
	else
	{
		self.nameLabel.text = locationType[@"name"];
		self.nameLabel.text = locationType[@"name"];
		[self.locationTypeImageView showSpinner];
		[self.locationTypeImageView sd_setImageWithURL:[NSURL URLWithString:locationType[@"image"]] placeholderImage:[UIImage imageNamed:@"icn_placeholderImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
			self.locationTypeImageView.layer.cornerRadius = self.locationTypeImageView.frame.size.height/2;
			[self.locationTypeImageView hideSpinner];
			
		}];
	}




}
@end

//
//  TPPShareUpcomingTripTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPShareUpcomingTripTableViewCell.h"

@interface TPPShareUpcomingTripTableViewCell ()
{
    CGRect shareOriginalFrame;
    CGRect joinOriginalFrame;
    CGRect followOriginalFrame;
}
@end

@implementation TPPShareUpcomingTripTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.mainView.layer.cornerRadius = 2.0;
    self.mainView.clipsToBounds = YES;
    
    shareOriginalFrame = self.shareButton.frame;
    joinOriginalFrame = self.joinButton.frame;
    followOriginalFrame = self.followButton.frame;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
    self.tripModel = tripModel;
    
    if (tripModel.isFollowedByMe)
    {
        [self.followButton setTitle:Txt_UnFollow forState:UIControlStateNormal];
    }
    else
    {
        [self.followButton setTitle:Txt_Follow forState:UIControlStateNormal];
    }
    
    if (tripModel.isJoinedByMe)
    {
        [self.joinButton setTitle:Txt_UnJoin forState:UIControlStateNormal];
    }
    else
    {
        [self.joinButton setTitle:Txt_Join forState:UIControlStateNormal];
    }
    
  //  [self handleJoinOptionRemoval];
}

-(void)handleJoinOptionRemoval
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    if ([_tripModel.createdBy[@"_id"] isEqualToString:userProfile[@"_id"]] || !_tripModel.isOpen || !_tripModel.isGroup)
    {
        CGFloat joinButtonWidth = self.joinButton.frame.size.width;
        
        self.shareButton.frame = CGRectMake(0, 0, self.shareButton.frame.size.width+joinButtonWidth/2, self.shareButton.frame.size.height);
        
        self.followButton.frame = CGRectMake(self.shareButton.frame.size.width+1, 0, self.shareButton.frame.size.width, self.followButton.frame.size.height);
        
        self.joinButton.frame = CGRectZero;
        
    }
    else
    {
        self.shareButton.frame = shareOriginalFrame;
        self.followButton.frame = followOriginalFrame;
        self.joinButton.frame = joinOriginalFrame;
    }
}


-(void)setButtonsBackGroundColor:(UIColor *)color
{
    self.shareButton.backgroundColor = color;
    self.followButton.backgroundColor = color;
    self.joinButton.backgroundColor = color;
}

- (IBAction)shareButtonTapped:(id)sender
{
    [self.shareCellDelegate shareButtonTapped:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}

- (IBAction)joinButtonTapped:(id)sender
{
    [TPPAppServices callServiceForJoinUnjoinTripWithTripModel:self.tripModel button:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath];
}

- (IBAction)followButtonTapped:(id)sender
{
    [TPPAppServices callServiceForFollowUnfollowTripWithTripModel:_tripModel button:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath];
}



@end

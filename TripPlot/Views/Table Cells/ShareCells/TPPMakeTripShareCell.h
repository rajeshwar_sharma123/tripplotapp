//
//  TPPMakeTripShareCell.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/28/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPMakeTripShareCell : UITableViewCell

@property (retain,atomic) TPPTripModel *tripModel;
@property (weak,nonatomic) id<TPPShareCellDelegate> shareCellDelegate;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnMakeTrip;

-(void)bindDataWithModel:(TPPTripModel *)tripModel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;
@end

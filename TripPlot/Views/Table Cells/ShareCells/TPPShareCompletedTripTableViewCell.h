//
//  TPPShareCompletedTripTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPShareCompletedTripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@property (weak, nonatomic) IBOutlet UIView *mainView;


@property (weak,nonatomic) id<TPPShareCellDelegate> shareCellDelegate;
@property (retain,atomic) TPPTripModel *tripModel;

@property (weak, nonatomic) IBOutlet UIButton *goHereButton;



-(void)bindDataWithModel:(TPPTripModel *)tripModel;

@end

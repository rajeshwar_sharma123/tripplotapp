//
//  TPPMakeTripShareCell.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/28/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPMakeTripShareCell.h"

@implementation TPPMakeTripShareCell
{
    TPPCreateTripModel *newTripObject;
}

- (void)awakeFromNib {
    // Initialization code
}
- (IBAction)shareButtonClickedAction:(id)sender {
    
    [self.shareCellDelegate shareButtonTapped:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}
- (IBAction)makeTripButtonClickedAction:(id)sender {
    UIViewController *bucketListVC = (UIViewController *)self.shareCellDelegate;
    newTripObject = [TPPCreateTripModel sharedInstance];
    NSDictionary *profileDetail = [UserDeafaults objectForKey:UserProfile];
    newTripObject.token = [UserDeafaults objectForKey:Token];
    newTripObject.username = profileDetail[@"emailid"];
    newTripObject.profileImage = profileDetail[@"profileImage"];
    newTripObject._id = profileDetail[@"_id"];
    newTripObject.firstname = profileDetail[@"firstname"];
    newTripObject.createdBy = profileDetail[@"_id"];
    newTripObject.title = self.tripModel.title;
    newTripObject.typeNames = self.tripModel.tripTypes.tokensNameArray;
    newTripObject.modeNames = self.tripModel.tripModes.tokensNameArray;
    newTripObject.createTripSource = CreateTripSource_BucketList;
    newTripObject.isNew = YES;
    newTripObject.tripStatus = self.tripModel.tripStatus;
    if (_selectedIndexPath)newTripObject.selectedIndex = self.selectedIndexPath.row;
    else newTripObject.selectedIndex = self.indexPath.row;
    
    newTripObject.tripModel = self.tripModel;
    TPPCreateNewTripViewController *VC = [bucketListVC.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
    VC.sourceDestinationsArray = [self.tripModel.tripLocations getLocationsInSourceDestinationModelFormat:self.tripModel.tripLocations];
    
    [bucketListVC.navigationController pushViewController:VC animated:YES];

}

-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
   self.tripModel = tripModel;
    
    if (_tripModel.tripStatus == TripStatus_CompletedTrip)
    {
        _btnShare.backgroundColor = [UIColor blackColor];
        _btnMakeTrip.backgroundColor = [UIColor blackColor];
        
        [_btnShare setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal ];
        [_btnMakeTrip setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal ];
        
    }
    else if (_tripModel.tripStatus == TripStatus_UpcomingTrip)
    {
        _btnShare.backgroundColor = ColorFromRGB(0x2F1515);
        _btnMakeTrip.backgroundColor = ColorFromRGB(0x2F1515);
        
        [_btnShare setTitleColor:ColorFromRGB(0xED911F) forState:UIControlStateNormal ];
        [_btnMakeTrip setTitleColor:ColorFromRGB(0xED911F) forState:UIControlStateNormal ];
        
    }
    else if (_tripModel.tripStatus == TripStatus_OngoingTrip)
    {
        _btnShare.backgroundColor = [UIColor blackColor];
        _btnMakeTrip.backgroundColor = [UIColor blackColor];
        
        [_btnShare setTitleColor:ColorFromRGB(0x6AA42D) forState:UIControlStateNormal ];
        [_btnMakeTrip setTitleColor:ColorFromRGB(0x6AA42D) forState:UIControlStateNormal ];
        
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

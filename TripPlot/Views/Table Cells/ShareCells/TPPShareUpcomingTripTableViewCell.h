//
//  TPPShareUpcomingTripTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>





@interface TPPShareUpcomingTripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@property (weak,nonatomic) id<TPPShareCellDelegate> shareCellDelegate;
@property (retain,atomic) TPPTripModel *tripModel;

@property (weak, nonatomic) IBOutlet UIView *mainView;

-(void)bindDataWithModel:(TPPTripModel *)tripModel;
-(void)setButtonsBackGroundColor:(UIColor *)color;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;
@end

//
//  TPPShareUpcomingWithoutJoinTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 11/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPShareUpcomingWithoutJoinTableViewCell.h"

@implementation TPPShareUpcomingWithoutJoinTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.mainView.layer.cornerRadius = 2.0;
    self.mainView.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
   
}

-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
    self.tripModel = tripModel;
    
    if (tripModel.isFollowedByMe)
    {
        [self.followButton setTitle:Txt_UnFollow forState:UIControlStateNormal];
    }
    else
    {
        [self.followButton setTitle:Txt_Follow forState:UIControlStateNormal];
    }
    
}



-(void)setButtonsBackGroundColor:(UIColor *)color
{
    self.shareButton.backgroundColor = color;
    self.followButton.backgroundColor = color;
}

- (IBAction)shareButtonTapped:(id)sender
{
    [self.shareCellDelegate shareButtonTapped:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}


- (IBAction)followButtonTapped:(id)sender
{
    [TPPAppServices callServiceForFollowUnfollowTripWithTripModel:_tripModel button:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath];
}



@end

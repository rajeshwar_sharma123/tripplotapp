//
//  TPPChallengeFriendShareCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPChallengeFriendShareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak,nonatomic) id<TPPShareCellDelegate> delegate;
@property (retain,atomic) TPPTripModel *tripModel;


@end

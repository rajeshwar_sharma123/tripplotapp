//
//  TPPChallengeFriendShareCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPChallengeFriendShareCell.h"

@implementation TPPChallengeFriendShareCell

- (void)awakeFromNib {
    // Initialization code
    
    _mainView.layer.cornerRadius = 2.0;
    _mainView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    

}

- (IBAction)shareButtonTapped:(id)sender
{
    
    [self.delegate shareButtonTapped:sender indexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}

- (IBAction)challengeAFriendButtonTapped:(id)sender
{
    [self.delegate challengeAFrinedButtonTapped:sender indexPath:self.indexPath tripModel:_tripModel];
}



@end

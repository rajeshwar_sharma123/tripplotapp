//
//  TPPInviteFriendShareCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPInviteFriendShareCell.h"
#import "TPPContactViewController.h"

@implementation TPPInviteFriendShareCell

- (void)awakeFromNib {
    // Initialization code
    
    _mainView.layer.cornerRadius = 2.0;
    _mainView.clipsToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)shareButtonTapped:(id)sender
{
    [self.delegate shareButtonTapped:sender indexPath:self.selectedIndexPath?self.selectedIndexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}

- (IBAction)inviteButtonTapped:(id)sender
{
    
    UIViewController *currentVC = [TPPUtilities getCurrentViewController];
    
    TPPContactViewController *VC = [currentVC.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
    VC.shouldEnableSelection = YES;
    VC.shouldEnableInvite = YES;
    VC.shouldChallange = NO;
    VC.trip = _tripModel;
    
    TPPCreateTripModel *newTripObj = [TPPCreateTripModel sharedInstance];
    [newTripObj.addedEmailIDs removeAllObjects];
    [newTripObj.addedContacts removeAllObjects];
    
    [currentVC.navigationController pushViewController:VC animated:YES];
}



@end

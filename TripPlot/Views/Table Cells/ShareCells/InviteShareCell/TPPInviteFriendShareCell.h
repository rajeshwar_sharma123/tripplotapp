//
//  TPPInviteFriendShareCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPInviteFriendShareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak,nonatomic) id<TPPShareCellDelegate> delegate;
@property (retain,atomic) TPPTripModel *tripModel;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;

@end

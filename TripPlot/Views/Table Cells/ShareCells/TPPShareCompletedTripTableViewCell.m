//
//  TPPShareCompletedTripTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPShareCompletedTripTableViewCell.h"

@implementation TPPShareCompletedTripTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    
    self.mainView.layer.cornerRadius = 2.0;
    self.mainView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    

    // Configure the view for the selected state
}



-(void)bindDataWithModel:(TPPTripModel *)tripModel
{
    self.tripModel = tripModel;
    
    if (tripModel.isLikedByMe)
    {
    
        [self.likeButton setTitle:Txt_UnLike forState:UIControlStateNormal];
    }
    else
    {
        [self.likeButton setTitle:Txt_Like forState:UIControlStateNormal];
    }
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:tripModel.createdBy[@"_id"]] && (tripModel.tripStatus == TripStatus_CompletedTrip))
    {
        [self.goHereButton setTitle:Txt_ChallengeTrip forState:UIControlStateNormal];
        [self.goHereButton setImage:[UIImage imageNamed:@"icn_challenge"] forState:UIControlStateNormal];
    }
    else
    {
        [self.goHereButton setTitle:Txt_GoHere forState:UIControlStateNormal];
    }
}


- (IBAction)shareButtonTapped:(id)sender
{
    [self.shareCellDelegate shareButtonTapped:sender indexPath:self.indexPath tripModel:_tripModel snapShot:nil];
}

- (IBAction)likeButtonTapped:(UIButton *)sender
{
    [TPPAppServices callServiceForLikeUnlikeTripWithTripModel:_tripModel button:sender indexPath:self.indexPath];
}

- (IBAction)goHereButtonTapped:(id)sender
{
    [self.shareCellDelegate goHereButtonTapped:sender indexPath:self.indexPath tripModel:_tripModel];
    
}




@end

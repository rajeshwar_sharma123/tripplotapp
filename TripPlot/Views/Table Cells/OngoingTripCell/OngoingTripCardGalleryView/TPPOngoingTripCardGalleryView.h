//
//  TPPOngoingTripCardGalleryView.h
//  TripPlot
//
//  Created by Vishwas Singh on 21/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TPPOngoingTripCardGalleryView : UIView


@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *photoCountLabel;

@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property (weak, nonatomic) IBOutlet GRKGradientView *gradientBGView;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *goHereButton;

@property (weak, nonatomic) IBOutlet DKCarouselView *galleryView;



@property (weak,nonatomic) id<TPPShareCellDelegate> delegate;

@property (retain,atomic) TPPTripModel *tripModel;

-(void)bindDataWithModel:(TPPTripModel *)model;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@end

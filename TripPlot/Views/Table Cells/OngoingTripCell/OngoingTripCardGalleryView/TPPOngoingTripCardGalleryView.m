//
//  TPPOngoingTripCardGalleryView.m
//  TripPlot
//
//  Created by Vishwas Singh on 21/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPOngoingTripCardGalleryView.h"

@implementation TPPOngoingTripCardGalleryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    self.layer.cornerRadius = 2.0;
    self.clipsToBounds = YES;

    self.gradientBGView.gradientOrientation = GRKGradientOrientationDown;
    self.gradientBGView.gradientColors = [NSArray arrayWithObjects:TPPOngoingCellGradientColorStart, TPPOngoingCellGradientColorMidle,TPPOngoingCellGradientColorEnd,nil];
    
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    _tripModel = model;
    
    if (model.isLikedByMe)
    {
        [self.likeButton setTitle:Txt_UnLike forState:UIControlStateNormal];
    }
    else
    {
        [self.likeButton setTitle:Txt_Like forState:UIControlStateNormal];
    }
    
    
    self.photoCountLabel.text = [NSString stringWithFormat:@"%ld photos taken",model.images.count];
    self.titleLabel.text = model.title;
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",model.tripLocations[0][@"name"],[model.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }
    
    
    self.tripDurationLabel.text = [TPPUtilities getTripDurationStringFromDictionary:model.tripDate];
    
    
    [self setUpGalleryWithImageArray:model.images];
    
    TPPScrollMenu *menu = (TPPScrollMenu *)[self.expandableView subViewOfKindOfClass:[TPPScrollMenu class]];
    if (!menu)
    {
        menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    }

    menu.tripModel = model;
    menu.backgroundColorForButtons = Color_UpcomingTripCellButtonBackground;
    [menu configure];
    [menu hideDistance];
   
    [self.expandableView addSubview:menu];
    
}



-(void)setUpGalleryWithImageArray:(NSArray *)imageArray
{
    NSArray *arr = @[@"https://triplot.s3.amazonaws.com/trip/6a32cc14eafa15f3f543eb212f9602fd4b150d04_1452606480971.jpg",
                    @"https://triplot.s3.amazonaws.com/trip/6a32cc14eafa15f3f543eb212f9602fd4b150d04_1452606481176.jpg",
                    @"https://triplot.s3.amazonaws.com/trip/6a32cc14eafa15f3f543eb212f9602fd4b150d04_1452606481345.jpg"];
    
    NSMutableArray *items = [NSMutableArray new];
    for (NSString *imageUrl in imageArray)
    {
        DKCarouselURLItem *urlAD = [DKCarouselURLItem new];
        urlAD.imageUrl = imageUrl;
        
        [items addObject:urlAD];
    }
    
    self.galleryView.indicatorTintColor = TPPGreenColor;
    self.galleryView.indicatorOffset = CGPointMake(10, 0); // to move the indicator on left
    self.galleryView.finite = NO;
    self.galleryView.defaultImage = [UIImage imageNamed:@"icn_placeholderImage"];
    //[self.galleryView setAutoPagingForInterval:5];
    [self.galleryView setItems:items];
    
    
    
    [self.galleryView setDidSelectBlock:^(DKCarouselItem *item, NSInteger index) {
        
    }];
    
    [self.galleryView setDidChangeBlock:^(DKCarouselView *view, NSInteger index) {
        
    }];
}


#pragma mark- IBAction methods

- (IBAction)likeButtonTapped:(id)sender
{
    [TPPAppServices callServiceForLikeUnlikeTripWithTripModel:_tripModel button:sender indexPath:nil];
}

- (IBAction)shareButtonTapped:(id)sender
{
    UIImage *image = self.snapshotImage;
    
    [self.delegate shareButtonTapped:sender indexPath:nil tripModel:_tripModel snapShot:image];
}

- (IBAction)goHereButtonTapped:(id)sender
{
    [self.delegate goHereButtonTapped:sender indexPath:nil tripModel:_tripModel];
}




@end

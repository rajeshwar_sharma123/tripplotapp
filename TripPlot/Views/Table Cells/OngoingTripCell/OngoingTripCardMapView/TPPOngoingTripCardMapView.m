//
//  TPPOngoingTripCardMapView.m
//  TripPlot
//
//  Created by Vishwas Singh on 21/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPOngoingTripCardMapView.h"




@implementation TPPOngoingTripCardMapView


-(void)awakeFromNib
{
    self.layer.cornerRadius = 2.0;
    self.clipsToBounds = YES;

    self.gradientBGView.gradientOrientation = GRKGradientOrientationDown;
    self.gradientBGView.gradientColors = [NSArray arrayWithObjects:TPPOngoingCellGradientColorStart, TPPOngoingCellGradientColorMidle,TPPOngoingCellGradientColorEnd,nil];
    
    self.mapView.settings.compassButton = YES;
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    _tripModel = model;
    
    if (model.isLikedByMe)
    {
        [self.likeButton setTitle:Txt_UnLike forState:UIControlStateNormal];
    }
    else
    {
        [self.likeButton setTitle:Txt_Like forState:UIControlStateNormal];
    }
    
    
    self.routeLabel.text = [TPPUtilities getRouteForTrip:model];
    
    
    self.titleLabel.text = model.title;
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",model.tripLocations[0][@"name"],[model.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }

    
    
    self.tripDurationLabel.text = [TPPUtilities getTripDurationStringFromDictionary:model.tripDate];
    
        
    [_mapSnapshotImageView showSpinner];



	NSString *encodedPolyline = model.route[@"overviewPolyline"];

    [_mapSnapshotImageView sd_setImageWithURL:[TPPUtilities getMapSnapshotUrlWithSegmentsArray:encodedPolyline snapShotSize:_mapSnapshotImageView.frame.size] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [_mapSnapshotImageView hideSpinner];
     }];
    
    
    TPPScrollMenu *menu = (TPPScrollMenu *)[self.expandableView subViewOfKindOfClass:[TPPScrollMenu class]];
    if (!menu) {
        menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    }

    menu.tripModel = model;
    menu.backgroundColorForButtons = Color_UpcomingTripCellButtonBackground;
    [menu configure];
    [menu hideDistance];
    
    [self.expandableView addSubview:menu];
    
    
}


#pragma mark- Map related

-(void)initMapView
{
    self.mapSnapshotImageView.hidden = YES;
    self.mapView.hidden = NO;
    self.boundsButton.hidden = NO;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:28.4700
                                                                longitude:77.0300
                                                                zoom:6];
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    
    [self drawPolyline:_tripModel.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
    

    [self boundCameraToFitAllMarkers:_tripModel.tripLocations onMap:self.mapView];
}






-(void) drawPolyline:(NSArray *) polylines map:(GMSMapView *)map shouldSelected:(BOOL) isSelected withTitle:(NSInteger ) title
{
    for (NSInteger atIndex2 = 0; atIndex2 <polylines.count; atIndex2++ )
    {
        NSDictionary *routeOverviewPolyline = [polylines objectAtIndex:atIndex2];
        NSString *points = [routeOverviewPolyline objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:points];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.title = [NSString stringWithFormat:@"%ld", title];
        polyline.tappable = YES;
        polyline.strokeWidth = 4;
        if (isSelected)
        {
            polyline.strokeColor = [UIColor blueColor];
        }
        else
        {
            polyline.strokeColor = COLOR_FOR_MENU_CONTENT;
        }
        polyline.map = map;
    }
}


- (void)boundCameraToFitAllMarkers:(NSArray *)allMarkersMArray onMap:(GMSMapView *)map
{
    GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] init];
    
    for (NSInteger atIndex = 0;atIndex <allMarkersMArray.count; atIndex ++)
    {
        NSDictionary *tripLocation = [allMarkersMArray objectAtIndex:atIndex];
        NSDictionary *geoCode = tripLocation[@"geoCode"];
        
        CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"latitude"] doubleValue] longitude:[geoCode[@"longitude"] doubleValue]];
        
        
        if (location.coordinate.longitude > 0 && location.coordinate.latitude > 0) {
            bounds = [bounds includingCoordinate:location.coordinate];
        }
        //add markers
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        
        if (atIndex == 0) {
            marker.icon = [UIImage imageNamed:@"circle"];
        }
        else if (atIndex == allMarkersMArray.count-1)
        {
            marker.icon = [UIImage imageNamed:@"circle_Red"];
        }
        else
        {
            marker.icon = [UIImage imageNamed:@"circle_blue"];
        }
        marker.tappable = YES;
        marker.snippet = tripLocation[@"name"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = map;
        
    }
    [map animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:32]];
    
    [self drawPolyline:_tripModel.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
}


#pragma mark- IBAction methods

- (IBAction)boundsButtonTapped:(id)sender
{
    [self boundCameraToFitAllMarkers:_tripModel.tripLocations onMap:self.mapView];
}


- (IBAction)likeButtonTapped:(id)sender
{
    [TPPAppServices callServiceForLikeUnlikeTripWithTripModel:_tripModel button:sender indexPath:nil];
}

- (IBAction)shareButtonTapped:(id)sender
{
    UIImage *image = self.snapshotImage;
    
    [self.delegate shareButtonTapped:sender indexPath:nil tripModel:_tripModel snapShot:image];
}

- (IBAction)goHereButtonTapped:(id)sender
{
    [self.delegate goHereButtonTapped:sender indexPath:nil tripModel:_tripModel];
}



@end

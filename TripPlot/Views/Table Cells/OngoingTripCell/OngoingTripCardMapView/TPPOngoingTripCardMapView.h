//
//  TPPOngoingTripCardMapView.h
//  TripPlot
//
//  Created by Vishwas Singh on 21/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>


@interface TPPOngoingTripCardMapView : UIView<GMSMapViewDelegate>



@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *mapSnapshotImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UILabel *routeLabel;

@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property (weak, nonatomic) IBOutlet GRKGradientView *gradientBGView;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *goHereButton;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *boundsButton;


@property (weak,nonatomic) id<TPPShareCellDelegate> delegate;

@property (retain,atomic) TPPTripModel *tripModel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraintConstant;

-(void)bindDataWithModel:(TPPTripModel *)model;
-(void)initMapView;

@end

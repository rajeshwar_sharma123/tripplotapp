//
//  TPPOngoingTripTableViewCell.h
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TPPOngoingTripTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *travellerNameLabel;







@property (weak,nonatomic) id<TPPShareCellDelegate> delegate;

@property (retain,atomic) TPPTripModel *tripModel;

@property (weak, nonatomic) IBOutlet UIView *userView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userViewHeightConstraint;

-(void)setUpForMyTripsScreen;
-(void)setUpForBucketListScreen;
-(void)bindDataWithModel:(TPPTripModel *)model;


@end

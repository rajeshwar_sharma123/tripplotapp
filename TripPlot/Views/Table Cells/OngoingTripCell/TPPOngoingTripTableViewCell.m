//
//  TPPOngoingTripTableViewCell.m
//  TripPlot
//
//  Created by Vishwas Singh on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPOngoingTripTableViewCell.h"
#import "TPPStackView.h"
#import "TPPOngoingTripCardMapView.h"
#import "TPPOngoingTripCardGalleryView.h"

@interface TPPOngoingTripTableViewCell ()<TPPStackViewDelegate, TPPStackViewShiftDelegate>
@property (weak, nonatomic) IBOutlet TPPStackView *stack;
@end

@implementation TPPOngoingTripTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.clipsToBounds = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)bindDataWithModel:(TPPTripModel *)model
{
    _tripModel = model;
    
    self.travellerNameLabel.text = model.createdBy[@"firstname"];
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:model.createdBy[@"profileImage"]]
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];

   // [self initStackView]; // if need stack implementation uncomment this line and comment following lines
    
    TPPOngoingTripCardMapView *mapView = [[[NSBundle mainBundle] loadNibNamed:@"TPPOngoingTripCardMapView" owner:self options:nil] objectAtIndex:0];
    mapView.frame = CGRectMake(0, 0, _stack.frame.size.width, _stack.frame.size.height);
    mapView.delegate = self.delegate;
    mapView.tag = 11;
    [mapView bindDataWithModel:_tripModel];
    
    
    [_stack removeSubviewsOfKindOfClasses:@[@"TPPOngoingTripCardMapView"]];
    [_stack addSubview:mapView];

}


-(void)initStackView
{
    
    _stack.delegate = self;
    _stack.shiftDelegate = self;
    _stack.slidingTransparentEffect = YES;
    _stack.typeSliding = TPPStackViewTypeSlidingHorizontal;
    
    [_stack reloadData];
}

-(void)setUpForMyTripsScreen
{
    self.userView.hidden = YES;
    _userViewHeightConstraint.constant = 0;
}

-(void)setUpForBucketListScreen
{
    TPPOngoingTripCardMapView *mapView = [self viewWithTag:11];
    mapView.bottomView.hidden = YES;
    mapView.bottomViewHeightConstraintConstant.constant = 0;
}

#pragma mark - TPPStackViewDelegate

-(NSInteger)numberOfViews {
    return 2;
}

-(UIView *)stackView:(TPPStackView *)stackView viewForRowAtIndex:(NSInteger)index
{
    UIView *view;
    
    if (index == 0)
    {
        [_stack removeSubviewsOfKindOfClasses:@[@"TPPOngoingTripCardMapView"]];
        
        TPPOngoingTripCardMapView *mapView = [[[NSBundle mainBundle] loadNibNamed:@"TPPOngoingTripCardMapView" owner:self options:nil] objectAtIndex:0];
        mapView.delegate = self.delegate;
        [mapView bindDataWithModel:_tripModel];
        
        view = mapView;
    }
    else
    {
        [_stack removeSubviewsOfKindOfClasses:@[@"TPPOngoingTripCardGalleryView"]];
        
        TPPOngoingTripCardGalleryView *galleryView = [[[NSBundle mainBundle] loadNibNamed:@"TPPOngoingTripCardGalleryView" owner:self options:nil] objectAtIndex:0];
        galleryView.delegate = self.delegate;
        [galleryView bindDataWithModel:_tripModel];
        
        view = galleryView;
    }
    
    view.frame =  CGRectMake(0, 0, _stack.frame.size.width, _stack.frame.size.height);
    
    return view;
}

-(UIImage *)imageFromView :(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0f);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

-(void)stackView:(TPPStackView *)stackView didSelectViewAtIndex:(NSInteger)index {
    NSLog(@"Вы тапнули на View под индексом: %@", @(index));
}

-(void)stackView:(TPPStackView *)stackView willChangeViewAtIndex:(NSInteger)index {
    NSLog(@"willChangeViewAtIndex: %@", @(index));
}

-(void)stackView:(TPPStackView *)stackView didChangeViewAtIndex:(NSInteger)index {
    NSLog(@"didChangeViewAtIndex: %@", @(index));
}

#pragma mark - CSVStackViewShiftDelegate

-(CGFloat)sizeOfShiftStack
{
    return 20.0f;
}

-(CGRect)stackItem:(UIView *)stackItem rectViewAtIndex:(NSInteger)index andShift:(CGFloat)shift
{
    CGSize newSize = [self sizeByShiftWidth:shift * index andSize:stackItem.frame.size];
    CGPoint newPoint = [self pointByShift:shift * index andPoint:stackItem.center];
    return CGRectMake(newPoint.x - (newSize.width / 2), newPoint.y - (newSize.height / 2), newSize.width, newSize.height);
}

//helpers shift calculate

-(CGPoint)pointByShift:(CGFloat)shift andPoint:(CGPoint)point {
    return CGPointMake(point.x, point.y + shift);
}

-(CGSize)sizeByShiftWidth : (CGFloat)shift andSize:(CGSize)size {
    
    CGFloat k = (size.width - shift) / size.width;
    CGFloat new_width = k * size.width;
    CGFloat new_height = k * size.height;
    
    return CGSizeMake(new_width, new_height);
}




@end

//
//  TPPExploreMapCell.h
//  TripPlot
//
//  Created by daffolapmac on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TPPExploreViewController.h"


@interface TPPExploreMapCell : UITableViewCell<CLLocationManagerDelegate,GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@property TPPExploreViewController *exploreViewController;
-(void)drawMarkersOnMap:(GMSMapView *)map;

@end

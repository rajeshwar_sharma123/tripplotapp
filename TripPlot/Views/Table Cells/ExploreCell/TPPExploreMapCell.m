//
//  TPPExploreMapCell.m
//  TripPlot
//
//  Created by daffolapmac on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPExploreMapCell.h"
#import "TPPMarkerWindowView.h"
#import "GMSMapView+Utility.h"



@interface TPPExploreMapCell ()
{
    
    BOOL locationUpdateForFirstTime;
    GMSMarker *currentLocationMarker;
    
}
@end

@implementation TPPExploreMapCell

- (void)awakeFromNib {
    // Initialization code

    [self.resetButton addTarget:self action:@selector(resetMapOnTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.mapView.delegate = self;
	self.mapView.myLocationEnabled = YES;
    locationUpdateForFirstTime = YES;
    self.mapView.settings.compassButton = YES;
    
    [self setUserLocation];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)resetMapOnTap:(id) sender
{
    [self.mapView boundAllMarkersWithPadding:150 shouldIncludeMyLocation:YES];
}





#pragma -mark CLLocation methods

-(void)setUserLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    
    if(SYSTEM_VERSION_LESS_THAN(@"8.0"))
        
    {
        
        _locationManager.delegate = self;
        
        
        
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
        
        [_locationManager startUpdatingLocation];
        
        
        
    }
    
    else
        
    {
        
        _locationManager.delegate = self;
        
        
        
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        
        if(authorizationStatus==kCLAuthorizationStatusNotDetermined||authorizationStatus==kCLAuthorizationStatusDenied)
            
        {
            
            [_locationManager requestWhenInUseAuthorization];
            
        }
        
        if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
            
            authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse)
            
        {
            
            _locationManager.delegate = self;
            
            
            
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
            
            
            
            [_locationManager startUpdatingLocation];
            
        }
        
    }
    
}





#pragma -mark Location delegates

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations

{
//    currentLocationMarker.map = nil;
//    
//    currentLocationMarker = [GMSMarker markerWithPosition:self.mapView.myLocation.coordinate];
//    currentLocationMarker.icon = [UIImage imageNamed:@"icn__0000_locator"];
//    currentLocationMarker.map = self.mapView;
//
    
    _exploreViewController.requestObject[@"longitude"] = [NSString stringWithFormat:@"%f", _mapView.myLocation.coordinate.longitude];
    
    _exploreViewController.requestObject[@"latitude"] = [NSString stringWithFormat:@"%f", _mapView.myLocation.coordinate.latitude];
    
    
    
    if (locationUpdateForFirstTime)
    {
        locationUpdateForFirstTime = NO;
        
        [_exploreViewController callServiceToGetTripList];
        
    }
    
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusDenied)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"TripPlot"
                                              message:@"Kindly allow TripPlot to access your location"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [_exploreViewController.navigationController popViewControllerAnimated:YES];
                                       
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                   }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [_exploreViewController.navigationController popViewControllerAnimated:YES];
                                       }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [_exploreViewController presentViewController:alertController animated:YES completion:nil];
        
    }
    
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        _locationManager.delegate = self;
        
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
        [_locationManager startUpdatingLocation];
    }
}



#pragma mark- Map related



- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{

    TPPMarkerWindowView *view = [[[NSBundle mainBundle] loadNibNamed:@"TPPMarkerWindowView" owner:self options:nil] objectAtIndex:0];
    
    
    NSInteger atIndex = [marker.title integerValue];
    
    TPPTripModel *trip = [_exploreViewController.tripsArray objectAtIndex:atIndex];
    
    NSString *imageURL = [_exploreViewController.locationTypeListArry objectAtIndex:_exploreViewController.selectedIndex>0?_exploreViewController.selectedIndex-1:_exploreViewController.selectedIndex][@"image"];
    if (trip.images.count>0) {
        imageURL = [trip.images objectAtIndex:0];
    }
    
    [view configureMarkerWindow:trip withLocationTypeImageURL:imageURL selectedIndex:_exploreViewController.selectedIndex];
    
    return view;
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    NSInteger atIndex = [marker.title integerValue];
    TPPTripModel *trip = [_exploreViewController.tripsArray objectAtIndex:atIndex];

    [_exploreViewController callServiceToGetTripDetailWithTripId:trip._id];
}



-(void)drawMarkersOnMap:(GMSMapView *)map

{
    [map clear];
    
    NSMutableArray *markers = [NSMutableArray new];
    for (NSInteger atIndex = 0; atIndex <_exploreViewController.tripsArray.count; atIndex++)
        
    {
        
        TPPTripModel *tripObj = [_exploreViewController.tripsArray objectAtIndex:atIndex];
        
        NSDictionary *placeDetails = tripObj.tripLocations.count>0? [tripObj.tripLocations objectAtIndex:0]:nil;
        
        NSDictionary *geoCode = placeDetails[@"geoCode"];

        CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"latitude"] doubleValue] longitude:[geoCode[@"longitude"] doubleValue]];
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
		marker.icon = [UIImage imageNamed:@"icn_defaultMarker"];
        marker.tappable = YES;
        marker.snippet = tripObj.title;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = map;
        marker.title = [NSString stringWithFormat:@"%ld",atIndex];
        [markers addObject:marker];
    }
    self.mapView.markers = markers;
    
    [self resetMapOnTap:nil];
    
}







@end

//
//  TPPExploreListCell.h
//  TripPlot
//
//  Created by daffolapmac on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPExploreListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tripImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

@end

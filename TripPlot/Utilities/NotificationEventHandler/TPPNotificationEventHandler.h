//
//  TPPNotificationEventHandler.h
//  TripPlot
//
//  Created by Vishwas Singh on 23/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPNotificationEventHandler : NSObject

@property NSInteger notificationCount;

+(instancetype)sharedInstance;
-(void)handleNotification:(NSDictionary *)notificationDictionary;

@end

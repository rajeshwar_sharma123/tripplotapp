//
//  TPPNotificationEventHandler.m
//  TripPlot
//
//  Created by Vishwas Singh on 23/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPNotificationEventHandler.h"
#import "TPPCoreDataHandler.h"

@implementation TPPNotificationEventHandler


+(instancetype)sharedInstance
{
    static TPPNotificationEventHandler *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.notificationCount = 0;
    });
    
    return instance;
}


-(void)handleNotification:(NSDictionary *)notificationDictionary
{
    TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:notificationDictionary error:nil];
    NSString *eventName = model.event;
    
    if ([eventName isEqualToString:joinEvent])
    {
        model.event = joinEvent;
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unJoinEvent])
    {
        model.event = joinEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
    }
    else if ([eventName isEqualToString:likeEvent])
    {
        model.event = likeEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unlikeEvent])
    {
        model.event = likeEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
    }
    else if ([eventName isEqualToString:inviteAcceptEvent])
    {
        model.event = inviteAcceptEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];

    }
    else if ([eventName isEqualToString:inviteRejectEvent])
    {
        model.event = inviteRejectEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];

    }
    else if ([eventName isEqualToString:followEvent])
    {
        model.event = followEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unfollowEvent])
    {
        model.event = followEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
    }
    else if ([eventName isEqualToString:challengeAcceptEvent])
    {
        model.event = challengeAcceptEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];

    }
    else if ([eventName isEqualToString:challengeRejectEvent])
    {
        model.event = challengeRejectEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:endTripEvent])
    {
        model.event = endTripEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    
    //---------------------------------------- Broadcast events
    
    else if ([eventName isEqualToString:joinEventBroadcast])
    {
        model.event = joinEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unJoinEventBroadcast])
    {
        model.event = joinEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
        
    }
    else if ([eventName isEqualToString:followBroadcast])
    {
        model.event = followEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unfollowBroadcast])
    {
        model.event = followEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];

    }
    else if ([eventName isEqualToString:likeBroadcast])
    {
        model.event = likeEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:unlikeBroadcast])
    {
        model.event = likeEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
    }
    else if ([eventName isEqualToString:challengeAcceptBroadcast])
    {
        model.event = challengeAcceptEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:challengeRejectBroadcast])
    {
        model.event = challengeRejectEvent;
        
        [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
        [self updateNotificationCount];

    }
    else if ([eventName isEqualToString:inviteAcceptBroadcast])
    {
        model.event = inviteAcceptEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:inviteRejectBroadcast])
    {
        model.event = inviteRejectEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];
    }
    else if ([eventName isEqualToString:endTripEventBroadcast])
    {
        model.event = endTripEvent;
        
        [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
        [self updateNotificationCount];

    }
    else if ([eventName isEqualToString:cronjobBroadcastEvent])
    {
        model.event = cronjobBroadcastEvent;
        

    }
    
}


-(void)updateNotificationCount
{
    _notificationCount++;
    [TPPUtilities updateNotificationBadgeWithValue:[TPPNotificationEventHandler sharedInstance].notificationCount];
}


@end

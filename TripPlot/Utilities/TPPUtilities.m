//
//  TPPUtilities.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPUtilities.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AppDelegate.h"


@implementation TPPUtilities


+(void)saveUserProfile:(NSMutableDictionary *)userProfile
{
    [UserDeafaults setObject:[userProfile removeAllNullValues] forKey:UserProfile];
    [UserDeafaults synchronize];
}

+(void)saveUserToken:(NSString *)token
{
    [UserDeafaults setObject:token forKey:Token];
    
    [UserDeafaults synchronize];
}


+(NSString *)getTripDurationStringFromDictionary:(NSDictionary *)dateDict
{
    
    NSString *finalString;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSString *startDateString  = dateDict[@"startDate"];
    NSString *endDateString  = dateDict[@"endDate"];
    
    NSDate *startDate = [dateFormatter dateFromString:startDateString];
    NSDate *endDate = [dateFormatter dateFromString:endDateString];
    
    [dateFormatter setDateFormat:@"MMM"];
    NSString *date1Month = [dateFormatter stringFromDate:startDate];
    NSString *date2Month = [dateFormatter stringFromDate:endDate];
    
    
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *date1Year = [dateFormatter stringFromDate:startDate];
    NSString *date2Year = [dateFormatter stringFromDate:endDate];
    
    
    if (![date1Year isEqualToString:date2Year])
    {
        [dateFormatter setDateFormat:@"MMM dd,yyyy"];
        NSString *date1 = [dateFormatter stringFromDate:startDate];
        NSString *date2 = [dateFormatter stringFromDate:endDate];
        
        finalString = [NSString stringWithFormat:@"%@-%@",date1,date2];
    }
    else if(![date1Month isEqualToString:date2Month])
    {
        [dateFormatter setDateFormat:@"MMM dd"];
        NSString *date1 = [dateFormatter stringFromDate:startDate];
        NSString *date2 = [dateFormatter stringFromDate:endDate];
        
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *year = [dateFormatter stringFromDate:startDate];
        
        finalString = [NSString stringWithFormat:@"%@-%@,%@",date1,date2,year];
    }
    else if([date1Month isEqualToString:date2Month])
    {
        [dateFormatter setDateFormat:@"MMM dd"];
        NSString *date1 = [dateFormatter stringFromDate:startDate];
        
        [dateFormatter setDateFormat:@"dd"];
        NSString *date2 = [dateFormatter stringFromDate:endDate];
        
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *year = [dateFormatter stringFromDate:startDate];
        
        finalString = [NSString stringWithFormat:@"%@-%@,%@",date1,date2,year];
    }
    
    
    return finalString;
}



+(NSURL *)getMapSnapshotUrlWithSegmentsArray: (NSString *) encodedPolyline snapShotSize:(CGSize)snapShotSize
{
	GMSPath *polyLinePath = [GMSPath pathFromEncodedPath:encodedPolyline];

	NSInteger totalCoordinates = polyLinePath.count;
		//NSInteger noOfCoordinates = totalCoordinates <= 50? totalCoordinates:50;
	NSInteger nextSkip = 1;

	if (totalCoordinates> 50) {
		nextSkip = totalCoordinates/50;
	}



    
    NSString *size = [NSString stringWithFormat:@"%ldx%ld",(NSInteger)snapShotSize.width,(NSInteger)snapShotSize.height];
    
//    NSInteger middleIndex = totalCoordinates /2;
//    CLLocationCoordinate2D midCo = [polyLinePath coordinateAtIndex:middleIndex];
//    NSInteger zoom = 5;
//    NSString *center = [NSString stringWithFormat:@"%f,%f",midCo.latitude , midCo.longitude];
    
    NSMutableString *path = [[NSMutableString alloc]init];
    [path appendString:@"color:0x3333ff|weight:4"];
	for (NSInteger atIndex = 0; atIndex < totalCoordinates  ;atIndex = atIndex + nextSkip)
    {
		CLLocationCoordinate2D loc = [polyLinePath coordinateAtIndex:atIndex];
        NSString *coordinate = [NSString stringWithFormat:@"%f,%f", loc.latitude, loc.longitude ];
        
        [path appendString:[NSString stringWithFormat:@"|%@",coordinate]];
    }

//    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?center=%@&zoom=%ld&size=%@&path=%@&key=%@",center,zoom,size,path,Google_API_Key];
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?size=%@&path=%@&key=%@&sensor=false",size,path,Google_API_Key];
    NSURL *mapUrl = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    return mapUrl;
}


+(NSInteger)getNoOfDaysBetweenStartDate:(NSString *)startDate endDate:(NSString *)endDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate *fromDate = [dateFormatter dateFromString:startDate];
    NSDate *toDate =   [dateFormatter dateFromString:endDate];
    
    NSTimeInterval secondsBetweenDates = [toDate timeIntervalSinceDate:fromDate];
    
    NSInteger daysBetweenDates = secondsBetweenDates / (24*3600);
    
    if (daysBetweenDates>0)
    {
        return daysBetweenDates;
    }
    else
    {
        return 0;
    }
    
}


+(TPPNoDataFoundView *)addNoDataFoundViewInTableView:(UITableView *)tableView viewController:(UIViewController *)viewController
{
    TPPNoDataFoundView *backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"TPPNoDataFoundView" owner:self options:nil] objectAtIndex:0];
    backgroundView.delegate = viewController;
    
    tableView.backgroundView = backgroundView;
    tableView.backgroundView.hidden = YES;
    
    return backgroundView;
}


+(UIViewController *)getCurrentViewController
{
    UIViewController *currentVC = [[[UIApplication sharedApplication].delegate window] rootViewController];
    if ([currentVC isKindOfClass:[MFSideMenuContainerViewController class]])
    {
        MFSideMenuContainerViewController *sideMenuVC = (MFSideMenuContainerViewController *)currentVC;
        UINavigationController *navigationController = sideMenuVC.centerViewController;
        
        currentVC = navigationController.topViewController;
    }
    
    return currentVC;
    
}


#pragma mark- Register/UnRegister for Push Notifications

+(void)registerForPushNotifications
{
    UIApplication *application = [UIApplication sharedApplication];
    //To register Device for push notification
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
        
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
        
        [application registerUserNotificationSettings:settings];
        
        [application registerForRemoteNotifications];
        
    }
    else
    {
        // Register for Push Notifications, if running iOS version < 8
        
        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        
    }
    
    [TPPAppServices callServiceToRegisterDeviceForPushNotifications:YES];
}



+(void)unRegisterForPushNotifications
{
    UIApplication *application = [UIApplication sharedApplication];
    [application unregisterForRemoteNotifications];
    
    [TPPAppServices callServiceToRegisterDeviceForPushNotifications:NO];
}


/**
 *  To remove userProfile from the array
 *
 *  @param arr
 *  @param isJoin used because of inconsistency at service end
 */
+(void)removeUserProfileFromArray:(NSMutableArray *)arr isJoin:(BOOL)isJoin
{
    NSDictionary *myProfile = [UserDeafaults objectForKey:UserProfile];
    NSDictionary *toBeRemovedDictionary;
    
    NSString *idKey;
    if (isJoin)
    {
        idKey = @"userId";
    }
    else
    {
        idKey = @"_id";
    }
    
    for (NSDictionary *dict in arr)
    {
        if ([dict[idKey] isEqualToString:myProfile[@"_id"]])
        {
            toBeRemovedDictionary = dict;
            break;
        }
    }
    
    if (toBeRemovedDictionary)
    {
        [arr removeObject:toBeRemovedDictionary];
    }
}



+(void)updateNotificationBadgeWithValue:(NSInteger)badgeValue
{
    UIViewController *vc = [TPPUtilities getCurrentViewController];
    if ([vc isKindOfClass:[TPPHomeViewController class]])
    {
        UIBarButtonItem *notificationButton = [vc.navigationItem.rightBarButtonItems lastObject];
        notificationButton.badgeValue = [NSString stringWithFormat:@"%ld",badgeValue];
        notificationButton.badgeBGColor = [UIColor redColor];
    }
}


+(NSString *)getRouteForTrip:(TPPTripModel *)tripModel
{
    if (tripModel.route[@"summary"])
    {
        return [NSString stringWithFormat:@"Route via %@",tripModel.route[@"summary"]];
    }
    else
    {
        return @"";
    }
}


@end

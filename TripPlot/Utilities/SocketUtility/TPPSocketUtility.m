//
//  TPPSocketUtility.m
//  TripPlot
//
//  Created by Daffolap-21 on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPSocketUtility.h"


@implementation TPPSocketUtility

+(instancetype)sharedInstance
{
    static TPPSocketUtility *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.notificationCount = 0;
    });
    
    return instance;
}

-(void)connectToSocket
{
    NSDictionary *userDetails = [UserDeafaults objectForKey:UserProfile];
   
    if (_isSocketConnected || !userDetails)
    {
        return;
    }
    
    
    NSString *urlForConnection = [NSString stringWithFormat:@"%@?auth_token=%@",  URL_FOR_BASE_SOCKET,userDetails[@"_id"]];
    
    [SIOSocket socketWithHost:urlForConnection
       reconnectAutomatically:YES attemptLimit:INFINITY withDelay:0 maximumDelay:5 timeout:20 response: ^(SIOSocket *socket)
     {
         self.socket = socket;
         
         self.socket.onConnect = ^(id *object)
         {
             _isSocketConnected = YES;
             NSLog(@"connect");
         };
         self.socket.onDisconnect=^(NSDictionary *errorInfo)
         {
             _isSocketConnected = NO;
             NSLog(@"disconnect");
         };
         
         [self.socket on:joinEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = joinEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
          }];

         [self.socket on:unJoinEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = joinEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
          }];

         
         [self.socket on:likeEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = likeEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
          }];
         
         [self.socket on:unlikeEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = likeEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
              
          }];
         
         [self.socket on:inviteAcceptEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = inviteAcceptEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];

          }];
         
         [self.socket on:inviteRejectEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = inviteRejectEvent;
             
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
              
          }];
         
         [self.socket on:followEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = followEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];

          }];
         
         [self.socket on:unfollowEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = followEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
          }];
         
         [self.socket on:challengeAcceptEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = challengeAcceptEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];

          }];
         
         
         [self.socket on:challengeRejectEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = challengeRejectEvent;
             
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
              
          }];
         
         [self.socket on:endTripEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = endTripEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
          }];

         
         
         
         //---------------------------------------- Broadcast events
         
         [self.socket on:joinEventBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = joinEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
          }];
         [self.socket on:unJoinEventBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = joinEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
              
          }];
         
         [self.socket on:followBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = followEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
               [self updateNotificationCount];
          }];
         [self.socket on:unfollowBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = followEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
              
          }];
         
         
         [self.socket on:likeBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = likeEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
               [self updateNotificationCount];
          }];
         [self.socket on:unlikeBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = likeEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
          }];
         
         [self.socket on:challengeAcceptBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = challengeAcceptEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
               [self updateNotificationCount];
          }];

         [self.socket on:challengeRejectBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = challengeRejectEvent;
              
              [[TPPCoreDataHandler sharedInstance] removeNotificationFromDB:model];
               [self updateNotificationCount];
          }];
         
         [self.socket on:inviteAcceptBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = inviteAcceptEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
               [self updateNotificationCount];
              
          }];
         [self.socket on:inviteRejectBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = inviteRejectEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
               [self updateNotificationCount];
          }];
         
         
         [self.socket on:cronjobBroadcastEvent callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = cronjobBroadcastEvent;
              
              
          }];
         
         
         [self.socket on:endTripEventBroadcast callback:^(SIOParameterArray *args)
          {
              TPPNotificationModel *model =  [[TPPNotificationModel alloc] initWithDictionary:args[0] error:nil];
              model.event = endTripEvent;
              
              [[TPPCoreDataHandler sharedInstance] addOrUpdateNotificationInDB:model];
              [self updateNotificationCount];
          }];

         
         [self.socket on:@"reconnect" callback:^(SIOParameterArray *args)
          {
              
          }];
         
         [self.socket on:@"error" callback:^(SIOParameterArray *args)
          {
              _isSocketConnected = NO;
          }];
         
         
         
         
         self.socket.onError=^(NSDictionary *errorInfo)
         {
             
             _isSocketConnected = NO;
         };
         self.socket.onReconnect=^(NSInteger numberOfAttempts)
         {
             
         };
         self.socket.onReconnectionAttempt=^(NSInteger numberOfAttempts)
         {
             
             
         };
         self.socket.onReconnectionError=^(NSDictionary *errorInfo)
         {
             
         };
         
     }];
    
}


-(void)updateNotificationCount
{
    _notificationCount++;
    [TPPUtilities updateNotificationBadgeWithValue:[TPPSocketUtility sharedInstance].notificationCount];
}







@end

//
//  TPPSocketUtility.h
//  TripPlot
//
//  Created by Daffolap-21 on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPPNotificationModel.h"

@interface TPPSocketUtility : NSObject

@property SIOSocket *socket;
@property NSInteger notificationCount;
@property BOOL isSocketConnected;

+(instancetype)sharedInstance;
-(void)connectToSocket;


@end

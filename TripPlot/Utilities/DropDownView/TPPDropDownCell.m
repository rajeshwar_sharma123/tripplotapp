//
//  TPPDropDownCell.m
//  PageMenuDemoStoryboard
//
//  Created by Daffolap-mac-22 on 26/10/15.
//  Copyright © 2015 Jin Sasaki. All rights reserved.
//

#import "TPPDropDownCell.h"

@implementation TPPDropDownCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

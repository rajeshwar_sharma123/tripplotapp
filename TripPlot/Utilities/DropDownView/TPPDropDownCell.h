//
//  TPPDropDownCell.h
//  PageMenuDemoStoryboard
//
//  Created by Daffolap-mac-22 on 26/10/15.
//  Copyright © 2015 Jin Sasaki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPDropDownCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;


@end

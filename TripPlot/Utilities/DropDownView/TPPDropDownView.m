//
//  TPPDropDownView.m
//  PageMenuDemoStoryboard
//
//  Created by Daffolap-mac-22 on 26/10/15.
//  Copyright © 2015 Jin Sasaki. All rights reserved.
//

#import "TPPDropDownView.h"
#import "TPPDropDownCell.h"

CGRect originalFrame;
BOOL isHidden;
UIView *callingView;

@implementation TPPDropDownView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(TPPDropDownView *)initWithData:(NSArray *)arr attachedView:(UIView *)view
{
    
     callingView = view;
    _dataArray = [NSArray arrayWithArray:arr];
    
    self = [super init];
    
    [self registerNib:[UINib nibWithNibName: @"TPPDropDownCell" bundle:nil] forCellReuseIdentifier:@"TPPDropDownCell"];
    

    self.dataSource = self;
    self.delegate = self;
    
    isHidden = YES;
    self.hidden = isHidden;
    
    self.frame = CGRectMake(view.frame.size.width-225, 64, 220, arr.count*50);
    
    self.backgroundColor = [UIColor clearColor];
    [self addBackgroundView1];
    
    self.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    return self;
}

-(void)addBackgroundView1
{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(outsideTapped:)];

    self.backgroundView1 = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.backgroundView1.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:66.0/255 blue:60.0/255 alpha:0.4];
    [self.backgroundView1 addGestureRecognizer:tapGestureRecognizer];

    [callingView insertSubview:self.backgroundView1 belowSubview:self];
    
    self.backgroundView1.hidden = YES;
}

-(void)outsideTapped:(UITapGestureRecognizer *)sender
{
    [self hideView];
}


#pragma mark- UITableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    originalFrame = self.frame;
   
    return _dataArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPPDropDownCell *cell= [tableView dequeueReusableCellWithIdentifier:@"TPPDropDownCell"];
    if (cell == nil)
    {
        cell=[tableView dequeueReusableCellWithIdentifier:@"TPPDropDownCell"];
    }
    cell.menuNameLabel.text = _dataArray[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.dropdownDelegate buttonTappedAtRowIndex:indexPath.row];
    [self hideView];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(viewDragged:)];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 10)];
    view.backgroundColor = [UIColor orangeColor];
    
    [view addGestureRecognizer:panGesture];
    
    
    return view;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 10;
//}

-(void)hideView
{
    self.backgroundView1.hidden = YES;
    isHidden = YES;

    [UIView animateWithDuration:0.8 animations:^{
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 0);
       // self.backgroundView1.frame = CGRectMake(0, 0, self.backgroundView1.frame.size.width, 0);
    }];
}

-(void)showView
{
    self.backgroundView1.hidden = NO;
    self.hidden = NO;
    isHidden = NO;
    
    [UIView animateWithDuration:0.8 animations:^{
        self.frame = originalFrame;
        self.backgroundView1.frame = [UIScreen mainScreen].bounds;
    }];

}

-(void)viewDragged:(UIPanGestureRecognizer *)sender
{
    [self hideView];
    
//    CGPoint point = [sender translationInView:self];
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height+point.y);
}

-(void)toggleView
{
    if (isHidden) {
        [self showView];
    }
    else
    {
        [self hideView];
    }
}




@end

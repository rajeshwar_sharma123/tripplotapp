//
//  TPPDropDownView.h
//  PageMenuDemoStoryboard
//
//  Created by Daffolap-mac-22 on 26/10/15.
//  Copyright © 2015 Jin Sasaki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPDelegates.h"



@interface TPPDropDownView : UITableView<UITableViewDataSource,UITableViewDelegate>


@property(nonatomic) UIView *backgroundView1;
@property(nonatomic) NSArray *dataArray;
@property(nonatomic) NSString *cellIdentifier;

@property (nonatomic, weak) id <TPPMenuViewDelegate> dropdownDelegate;

-(TPPDropDownView *)initWithData:(NSArray *)arr attachedView:(UIView *)view;

-(void)hideView;
-(void)showView;
-(void)toggleView;


@end

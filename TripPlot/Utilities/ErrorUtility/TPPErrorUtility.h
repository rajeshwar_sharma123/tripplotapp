//
//  TPPErrorUtility.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPErrorUtility : NSObject

//+(NSString *)getErrorMessageForResponse:(NSHTTPURLResponse *)httpResponse error:(NSError *)error;

+(NSString *)getErrorMessageForResponseObject:(id)responseObject   response:(NSHTTPURLResponse *)httpResponse error:(NSError *)error;

@end

//
//  TPPErrorUtility.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPErrorUtility.h"

@implementation TPPErrorUtility

typedef NS_ENUM(NSInteger, FDCErrorCode) {
    FDCBadRequest = 400,
    FDCUnAuthorized = 401,
    FDCForbidden = 403,
    FDCNotFound = 404,
    FDCParameterMissing = 406,
    FDCUserAlreadyExists = 409,
    FDCAccountNotActivated = 417,
    FDCServerError = 500,
    FDCNotModified = 304,
    FDCNoInternetConnection = -1009,
    FDCCannotFindHost = -1003,
    FDCCannotConnectToHost = -1004,
    FDCNetworkConnectionLost = -1005,
    FDCRequestStopped = -999
};

+(NSString *)getErrorMessageForResponseObject:(id)responseObject   response:(NSHTTPURLResponse *)httpResponse error:(NSError *)error
{
    if (responseObject != nil)
    {
        return responseObject[@"response"][@"message"];
    }
    
    
    
    NSInteger errorCode;
    if (httpResponse!=nil)
    {
        errorCode = httpResponse.statusCode;
    }
    else
    {
        errorCode = error.code;
    }
    
    if ((errorCode == -1004) && ([[error.userInfo objectForKey:@"_kCFStreamErrorDomainKey"] longValue] == 1) && ([[error.userInfo objectForKey:@"_kCFStreamErrorCodeKey"]integerValue] == 51))
    {
        return k_Error_Internet_Connection_Error;
    }
    
    return [self getErrorMessageForErrorCode:errorCode];
}

+(NSString *)getErrorMessageForErrorCode:(NSInteger)errorcode
{
    NSString *errorString;
    
    switch (errorcode)
    {
        case FDCBadRequest :
            errorString = @"Bad Request";
            break;
        case FDCUnAuthorized :
            errorString = @"Unauthorized Access";
            break;
        case FDCForbidden :
            errorString =  k_Error_Some_Erorr_Occurred;
            break;
        case FDCNotFound :
            errorString =  @"No data found";
            break;
        case FDCParameterMissing :
            errorString = @"Some input missing";
            break;
        case FDCUserAlreadyExists :
            errorString = @"User already exits.";
            break;
        case FDCAccountNotActivated :
            errorString = @"Account Not Activated";
            break;
        case FDCServerError :
            errorString =  k_Error_Some_Erorr_Occurred;
            break;
        case FDCNotModified :
            errorString = @"Data not modified";
            break;
        case FDCNoInternetConnection :
            errorString = k_Error_Internet_Connection_Error;
            break;
        case FDCCannotFindHost :
            errorString = k_Error_Some_Erorr_Occurred;
            break;
        case FDCCannotConnectToHost :
            errorString = k_Error_Some_Erorr_Occurred;
            break;
        case FDCNetworkConnectionLost :
            errorString = @"The network connection was lost.";
            break;
        case FDCRequestStopped :
            errorString = k_Error_Some_Erorr_Occurred;
            break;
        default:
            errorString = k_Error_Some_Erorr_Occurred;
    }
    
    return errorString;
}


@end

//
//  TPPUtilities.h
//  TripPlot
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPPTripModel.h"
#import "TPPNoDataFoundView.h"

@interface TPPUtilities : NSObject

+(void)saveUserProfile:(NSDictionary *)response;
+(void)saveUserToken:(NSString *)token;

+(NSString *)getTripDurationStringFromDictionary:(NSDictionary *)dateDict;
+(NSURL *)getMapSnapshotUrlWithSegmentsArray: (NSString *) encodedPolyline snapShotSize:(CGSize)snapShotSize;
+(NSInteger)getNoOfDaysBetweenStartDate:(NSString *)startDate endDate:(NSString *)endDate;

+(TPPNoDataFoundView *)addNoDataFoundViewInTableView:(UITableView *)tableView viewController:(UIViewController *)viewController;

+(UIViewController *)getCurrentViewController;
+(void)unRegisterForPushNotifications;
+(void)registerForPushNotifications;

+(void)removeUserProfileFromArray:(NSMutableArray *)arr isJoin:(BOOL)isJoin;
+(void)updateNotificationBadgeWithValue:(NSInteger)badgeValue;

+(NSString *)getRouteForTrip:(TPPTripModel *)tripModel;

@end

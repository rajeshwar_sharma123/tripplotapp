//
//  TPPVerticalMenuCell.h
//  AutoLayout_Demo
//
//  Created by Daffodil iPhone on 2/1/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPVerticalMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon;

@end

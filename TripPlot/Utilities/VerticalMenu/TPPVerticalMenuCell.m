//
//  TPPVerticalMenuCell.m
//  AutoLayout_Demo
//
//  Created by Daffodil iPhone on 2/1/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

#import "TPPVerticalMenuCell.h"

@implementation TPPVerticalMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

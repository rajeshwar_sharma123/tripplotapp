//
//  TPPVerticalMenu.m
//  AutoLayout_Demo
//
//  Created by Daffodil iPhone on 2/1/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

#import "TPPVerticalMenu.h"
BOOL isHidden;
@implementation TPPVerticalMenu
-(TPPVerticalMenu *)initWithData:(NSArray *)arr height:(float)height width:(float)width bottomGap:(float)bottomGap
{
    self = [super init];
    heightOffset = height;
    widthOffset = width;
    bottomOffset = bottomGap;
    self.backgroundColor = ColorFromRGB(0x2A2626);
    isHidden = YES;
    rowsNum = (int)arr.count+1;
    dataArray = arr;
    _tableView =  [[UITableView alloc] init];
    _tableView.frame = CGRectMake(0, 0, width, 0);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = ColorFromRGB(0x2A2626);
    [self addSubview:_tableView];
    [_tableView registerNib:[UINib nibWithNibName:@"TPPVerticalMenuCell" bundle:nil] forCellReuseIdentifier:@"TPPVerticalMenuCell"];
    return self;
    
}

#pragma mark- UITableView Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
    return dataArray.count;
}

-(TPPVerticalMenuCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPPVerticalMenuCell *cell= [tableView dequeueReusableCellWithIdentifier:@"TPPVerticalMenuCell"];
    if (cell == nil)
    {
        cell = [[TPPVerticalMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPVerticalMenuCell"];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(TPPVerticalMenuCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [dataArray objectAtIndex:indexPath.row];
    cell.lblName.text = dic[@"name"];
    [cell.btnIcon setImage:dic[@"img_normal"] forState:UIControlStateNormal];
    [cell.btnIcon setImage:dic[@"img_pressed"] forState:UIControlStateHighlighted];
    [cell.btnIcon addTarget:self action:@selector(buttonIconClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heightOffset/rowsNum;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
   
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(23, 15, 20, 20)];
    [view addSubview:imgView];
    imgView.image = [UIImage imageNamed:@"btn_cancel"];
    view.backgroundColor = ColorFromRGB(0x2A2626);
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tapRecognizer];
    
    
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate buttonTappedAtRowIndex:indexPath.row];
    [self hideView];
}

-(void)buttonIconClicked:(UIButton*)sender
{

    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

-(void)showView
{
    self.hidden = NO;
    isHidden = NO;
    self.frame =  CGRectMake(0, ScreenHeight-bottomOffset, widthOffset, 0);
    [UIView animateWithDuration:0.25 animations:^{
        self.frame =  CGRectMake(0, ScreenHeight-bottomOffset-heightOffset, widthOffset, heightOffset);
        _tableView.frame = CGRectMake(0, 0, widthOffset, heightOffset);
        
    }];
    
    
}
- (void)tapAction:(UITapGestureRecognizer *)tap
{
    [self hideView];
}

-(void)hideView
{
    isHidden = YES;
    [UIView animateWithDuration:0.25 animations:^{
        self.frame =  CGRectMake(0, ScreenHeight-bottomOffset+5, widthOffset, 0);
        _tableView.frame = CGRectMake(0, 0, widthOffset, 0);
    }];
}


-(void)toggleView
{
    if (isHidden) {
        [self showView];
    }
    else
    {
        [self hideView];
    }
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end

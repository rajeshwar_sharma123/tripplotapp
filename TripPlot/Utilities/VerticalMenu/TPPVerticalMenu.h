//
//  TPPVerticalMenu.h
//  AutoLayout_Demo
//
//  Created by Daffodil iPhone on 2/1/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPVerticalMenuCell.h"
#import "TPPDelegates.h"

@interface TPPVerticalMenu : UIView<UITableViewDataSource,UITableViewDelegate>
{
    float heightOffset;
    float widthOffset;
    float bottomOffset;
    int rowsNum;
    NSArray *dataArray;
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic, weak) id <TPPMenuViewDelegate> delegate;
-(TPPVerticalMenu *)initWithData:(NSArray *)arr height:(float)height width:(float)width bottomGap:(float)bottomOffset;

-(void)hideView;
-(void)showView;
-(void)toggleView;

@end

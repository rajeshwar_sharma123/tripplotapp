	//
	//  TPPCoreDataHandler.m
	//  TripPlot
	//
	//  Created by Vishwas Singh on 05/02/16.
	//  Copyright © 2016 Daffodil. All rights reserved.
	//

#import "TPPCoreDataHandler.h"

@implementation TPPCoreDataHandler

+(instancetype)sharedInstance
{
	static TPPCoreDataHandler *instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[self alloc] init];

	});

	return instance;
}




-(TripEntity *)saveTrip:(TPPNotificationModel *)model
{
		// Create a new managed object
	TripEntity *newTrip = (TripEntity*) [NSEntityDescription insertNewObjectForEntityForName:@"TripEntity" inManagedObjectContext:self.managedObjectContext];

	newTrip.updatedOn = [model.updatedOn getDateFromString];
	newTrip.tripTitle = model.tripTitle;
	newTrip.tripId = model.tripId;
	newTrip.event = model.event;

	NSError *error = nil;
		// Save the object to persistent store
	if (![self.managedObjectContext save:&error])
	{
		NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
	}

	return newTrip;
}

- (MemberEntity * )saveMember:(TPPNotificationModel *)model
{
		// Create a new managed object
	MemberEntity *newMember = (MemberEntity*) [NSEntityDescription insertNewObjectForEntityForName:@"MemberEntity" inManagedObjectContext:self.managedObjectContext];

	newMember.userId = model.userId;
	newMember.firstName = model.firstname;
	newMember.lastName = model.lastname;
	newMember.imageUrl = model.profileImage;
	newMember.updatedOn = [model.updatedOn getDateFromString];

	NSError *error = nil;
		// Save the object to persistent store
	if (![self.managedObjectContext save:&error])
	{
		NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
	}

	return newMember;
}

-(TripEntity *)fetchTripEntity:(TPPNotificationModel *)notificationModel
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TripEntity"];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tripId == %@ AND event == %@", notificationModel.tripId,notificationModel.event];
	[fetchRequest setPredicate:predicate];
	NSArray *trips = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

	if (trips.count>0)
	{
		TripEntity *trip = [trips objectAtIndex:0];
		trip.updatedOn = [notificationModel.updatedOn getDateFromString];

		NSError *error;
		[self.managedObjectContext save:&error];

		return trip;
	}
	else{
		return [self saveTrip:notificationModel];
	}

}


-(MemberEntity *)fetchMemberEntity:(TPPNotificationModel *)notificationModel
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"MemberEntity"];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %@", notificationModel.userId];
	[fetchRequest setPredicate:predicate];

	NSError *error;
	NSArray *members = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
	if (members.count>0)
	{
		MemberEntity *member = [members objectAtIndex:0];
		member.updatedOn = [notificationModel.updatedOn getDateFromString];
		[self.managedObjectContext save:&error];

		return member;
	}
	else
	{
		return [self saveMember:notificationModel];
	}

}

-(NSArray *)fetchAllDataForEntity:(NSString *)entityName sortKey:(NSString *)sortKey
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
	NSSortDescriptor *sortDescriptor =
	[[NSSortDescriptor alloc] initWithKey:sortKey ascending:NO];

	if (![sortKey isEqualToString:@""])
	{
		[fetchRequest setSortDescriptors:@[sortDescriptor]];
	}

	NSArray *notifications = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

	return notifications;
}

-(void)addOrUpdateNotificationInDB:(TPPNotificationModel *) notification
{
	TripEntity *trip = [self fetchTripEntity:notification];
	[trip addMembersObject:[self fetchMemberEntity:notification]];

	NSError *error;
	[self.managedObjectContext save:&error];

}

-(void)removeNotificationFromDB:(TPPNotificationModel *)model
{
	NSError *error;

	TripEntity *trip = [self fetchTripEntity:model];

	[trip removeMembersObject:[self fetchMemberEntity:model]];

		//delete trip with events if member count is zero, re-insert next time
	if (trip.members.count==0) {
		[self.managedObjectContext deleteObject:trip];
	}

	[self.managedObjectContext save:&error];

}


#pragma -mark message notifcation customisation utilities

+(NSString *) getFormattedNotification:(TripEntity *)trip
{
    if ([trip.event isEqualToString:endTripEvent])
    {
        return [NSString stringWithFormat:@"Congratulations '%@' has been completed successfully", trip.tripTitle];
    }
    
    
	NSArray *members =  [trip.members allObjects];
	NSString *message;

	NSString *currentUserId = [UserDeafaults objectForKey:UserProfile][@"_id"];

	if (members.count == 1)
    {
		MemberEntity *member = [members objectAtIndex:0];
		if ([member.userId isEqualToString:currentUserId])
        {
			message = [NSString stringWithFormat:@"You %@ a trip '%@'.",[self getEventName:trip.event],trip.tripTitle];
		}
		else
		{
			message = [NSString stringWithFormat:@"%@ %@ a trip '%@'.",member.firstName ,[self getEventName:trip.event],trip.tripTitle];
		}
	}
	else if (members.count == 2)
    {
		MemberEntity *myself = [members objectAtIndex:0];
		MemberEntity *other = [members objectAtIndex:1];

		NSInteger myIndex = [members indexOfObject:[self findMemberById:currentUserId in:members]];

		myself = [members objectAtIndex:myIndex];

		if (myIndex == 0) {
			other = [members objectAtIndex:1];
		}
		else if(myIndex == 1)
		{
			other = [members objectAtIndex:0];
		}


		if ([myself.userId isEqualToString:currentUserId]) {
			message = [NSString stringWithFormat:@"You and %@ %@ '%@'.", other.firstName,[self getEventName:trip.event],trip.tripTitle];
		}
		else
		{
			message = [NSString stringWithFormat:@"%@ and %@ %@ a trip '%@'.",myself.firstName, other.firstName ,[self getEventName:trip.event],trip.tripTitle];
		}
	}
	else if (members.count > 2) {
		MemberEntity *other = [members lastObject];
		if ([self findMemberById:currentUserId in:members]) {

			message = [NSString stringWithFormat:@"You and %u others %@ a trip '%@'.", members.count-1,[self getEventName:trip.event],trip.tripTitle];
		}
		else
		{
			message = [NSString stringWithFormat:@"%@ and %u others %@ a trip '%@'.",other.firstName, members.count-1,[self getEventName:trip.event],trip.tripTitle];
		}

	}
	return message;
}

+(NSString *)getEventName:(NSString *) event
{
	if ([event isEqualToString:likeEvent]) {
		return @"liked";
	}
	else if ([event isEqualToString:followEvent]) {
		return @"now following";
	}
	else if ([event isEqualToString:joinEvent]) {
		return @"joined";
	}
	else if ([event isEqualToString:challengeAcceptEvent]) {
		return @"accepted challenge for";
	}
	else if ([event isEqualToString:challengeRejectEvent]) {
		return @"rejected your challenge for";
	}
	else if ([event isEqualToString:inviteRejectEvent]) {
		return @"rejected your invitation for ";
	}
	else if ([event isEqualToString:inviteAcceptEvent]) {
		return @"accepted invite for ";
	}
    else if ([event isEqualToString:endTripEvent])
    {
        return @"completed";
    }
	else
	{
		return @"";
	}

}

+(MemberEntity *) findMemberById:(NSString *) userId in:(NSArray *)members
{
	for (MemberEntity * member in members) {
		if ([userId isEqualToString:member.userId]) {
			return member;
		}
	}
	return nil;
}


-(void)deleteAllObjectsInCoreData
{
	NSArray *allEntities = self.managedObjectModel.entities;
	for (NSEntityDescription *entityDescription in allEntities)
	{
		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
		[fetchRequest setEntity:entityDescription];

		fetchRequest.includesPropertyValues = NO;
		fetchRequest.includesSubentities = NO;

		NSError *error;
		NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

		if (error)
		{
			NSLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
		}

		for (NSManagedObject *managedObject in items)
		{
			[self.managedObjectContext deleteObject:managedObject];
		}

		if (![self.managedObjectContext save:&error])
		{
			NSLog(@"Error deleting %@ - error:%@", entityDescription, [error localizedDescription]);
		}
	}
}




#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
		// The directory the application uses to store the Core Data store file. This code uses a directory named "daffodil.CoreDataDemo" in the application's documents directory.
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
		// The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TripPlot" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
		// The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}

		// Create the coordinator and store

	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TripPlot.sqlite"];
	NSError *error = nil;
	NSString *failureReason = @"There was an error creating or loading the application's saved data.";
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
			// Report any error we got.
		NSMutableDictionary *dict = [NSMutableDictionary dictionary];
		dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
		dict[NSLocalizedFailureReasonErrorKey] = failureReason;
		dict[NSUnderlyingErrorKey] = error;
		error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
			// Replace this with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}

	return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
		// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}

	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (!coordinator) {
		return nil;
	}
	_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		NSError *error = nil;
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
				// Replace this implementation with code to handle the error appropriately.
				// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}








@end

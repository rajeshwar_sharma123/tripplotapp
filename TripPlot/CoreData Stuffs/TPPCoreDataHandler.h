	//
	//  TPPCoreDataHandler.h
	//  TripPlot
	//
	//  Created by Vishwas Singh on 05/02/16.
	//  Copyright © 2016 Daffodil. All rights reserved.
	//

#import <Foundation/Foundation.h>
#import "TripEntity+CoreDataProperties.h"
#import "TripEntity.h"
#import "MemberEntity+CoreDataProperties.h"
#import "MemberEntity.h"


@interface TPPCoreDataHandler : NSObject

+(instancetype)sharedInstance;

-(TripEntity *)saveTrip:(TPPNotificationModel *)model;
- (MemberEntity * )saveMember:(TPPNotificationModel *)model;
-(TripEntity *)fetchTripEntity:(TPPNotificationModel *)notificationModel;
-(MemberEntity *)fetchMemberEntity:(TPPNotificationModel *)notificationModel;
-(void)addOrUpdateNotificationInDB:(TPPNotificationModel *) notification;
-(NSArray *)fetchAllDataForEntity:(NSString *)entityName sortKey:(NSString *)sortKey;
-(void)removeNotificationFromDB:(TPPNotificationModel *)model;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+(NSString *) getFormattedNotification:(TripEntity *)trip;
-(void)deleteAllObjectsInCoreData;

@end

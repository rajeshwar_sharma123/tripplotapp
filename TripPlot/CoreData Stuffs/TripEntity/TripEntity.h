//
//  TripEntity.h
//  TripPlot
//
//  Created by Vishwas Singh on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MemberEntity;

NS_ASSUME_NONNULL_BEGIN

@interface TripEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TripEntity+CoreDataProperties.h"

//
//  TripEntity+CoreDataProperties.h
//  TripPlot
//
//  Created by Vishwas Singh on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TripEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface TripEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *event;
@property (nullable, nonatomic, retain) NSString *tripId;
@property (nullable, nonatomic, retain) NSString *tripTitle;
@property (nullable, nonatomic, retain) NSDate *updatedOn;
@property (nullable, nonatomic, retain) NSSet<MemberEntity *> *members;

@end

@interface TripEntity (CoreDataGeneratedAccessors)

- (void)addMembersObject:(MemberEntity *)value;
- (void)removeMembersObject:(MemberEntity *)value;
- (void)addMembers:(NSSet<MemberEntity *> *)values;
- (void)removeMembers:(NSSet<MemberEntity *> *)values;

@end

NS_ASSUME_NONNULL_END

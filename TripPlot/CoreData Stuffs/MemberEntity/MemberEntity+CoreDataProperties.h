//
//  MemberEntity+CoreDataProperties.h
//  TripPlot
//
//  Created by Vishwas Singh on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MemberEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface MemberEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSDate *updatedOn;
@property (nullable, nonatomic, retain) NSString *userId;
@property (nullable, nonatomic, retain) NSSet<TripEntity *> *trips;

@end

@interface MemberEntity (CoreDataGeneratedAccessors)

- (void)addTripsObject:(TripEntity *)value;
- (void)removeTripsObject:(TripEntity *)value;
- (void)addTrips:(NSSet<TripEntity *> *)values;
- (void)removeTrips:(NSSet<TripEntity *> *)values;

@end

NS_ASSUME_NONNULL_END

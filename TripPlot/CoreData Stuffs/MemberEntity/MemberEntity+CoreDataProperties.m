//
//  MemberEntity+CoreDataProperties.m
//  TripPlot
//
//  Created by Vishwas Singh on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MemberEntity+CoreDataProperties.h"

@implementation MemberEntity (CoreDataProperties)

@dynamic firstName;
@dynamic imageUrl;
@dynamic lastName;
@dynamic updatedOn;
@dynamic userId;
@dynamic trips;

@end

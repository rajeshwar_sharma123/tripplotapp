//
//  AppDelegate.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "AppDelegate.h"
#import "TPPLeftMenuViewController.h"
#import "TPPHomeViewController.h"
#import <AWSCore/AWSCore.h>



@interface AppDelegate ()
{
    MFSideMenuContainerViewController *sideMenuContainerViewController;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (launchOptions)
    {
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        [[TPPNotificationEventHandler sharedInstance] handleNotification:userInfo[@"data"]];
    }
    
    [NewRelicAgent startWithApplicationToken:NewRelic_API_Key];
    [application setApplicationIconBadgeNumber:0];
    [[UITableViewCell appearance] setBackgroundColor:[UIColor clearColor]];
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
   
    [TPPUtilities registerForPushNotifications];

    //set Appearances
    [[UITextField appearance] setTintColor:TPPOrangeColor];
    [[UINavigationBar appearance] setBarTintColor:TPPNavigationBarColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    

    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textAttributes];
    
   [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    
    
    //google maps
    [GMSServices provideAPIKey:Google_API_Key];
    
    [TwitterKit startWithConsumerKey:Twitter_ConsumerKey consumerSecret:Twitter_ConsumerSecretKey];
    
    [Fabric with:@[[TwitterKit class],[CrashlyticsKit class]]];
    
    //enable IQKeyboard manager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:YES];
    
    
    [self setRootViewController];

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}







- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    //test case corresponding code
//    extern void __gcov_flush(void);
//    __gcov_flush();
    
    [[TPPSocketUtility sharedInstance].socket close];
    [TPPSocketUtility sharedInstance].isSocketConnected = NO;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //[[TPPSocketUtility sharedInstance] connectToSocket];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[TPPCoreDataHandler sharedInstance] saveContext];
}




-(void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *token = [[[[deviceToken description]
                            
                            stringByReplacingOccurrencesOfString:@"<"withString:@""]
                           
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                          
                          stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    [UserDeafaults setObject:token forKey:DeviceToken];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [[TPPNotificationEventHandler sharedInstance] handleNotification:userInfo[@"data"]];
    
}



#pragma mark- BL Methods

-(void)setRootViewController
{
    UIStoryboard *storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TPPLeftMenuViewController *leftMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"TPPLeftMenuViewController"];


    UINavigationController* navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationVC"];
    
    if([UserDeafaults objectForKey:Token] != nil)
    {
        // Navigate to Home Screen if already Logged In.
        TPPHomeViewController *homeViewController = [storyboard instantiateViewControllerWithIdentifier:@"TPPHomeViewController"];
        
        navigationController = [[UINavigationController alloc]initWithRootViewController:homeViewController];
    }
    else
    {
        
    }
    
     sideMenuContainerViewController = [MFSideMenuContainerViewController containerWithCenterViewController:navigationController leftMenuViewController:leftMenuViewController rightMenuViewController:nil];

    sideMenuContainerViewController.panMode = MFSideMenuPanModeDefault;
    sideMenuContainerViewController.shadow.enabled = YES;
    sideMenuContainerViewController.menuWidth = self.window.bounds.size.width*0.82;
    if (IS_IPAD )
    {
        sideMenuContainerViewController.menuWidth = self.window.bounds.size.width*0.52;
    }
    self.window.rootViewController = sideMenuContainerViewController;
    
}

//- (void)getTwitterAccountOnCompletion:(void (^)(ACAccount *))completionHandler
//{
//    ACAccountStore *store = [[ACAccountStore alloc] init];
//    ACAccountType *twitterType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
//    [store requestAccessToAccountsWithType:twitterType withCompletionHandler:^(BOOL granted, NSError *error)
//     {
//         if(granted)
//         {
//             // Remember that twitterType was instantiated above
//             NSArray *twitterAccounts = [store accountsWithAccountType:twitterType];
//             
//             // If there are no accounts, we need to pop up an alert
//             if(twitterAccounts == nil || [twitterAccounts count] == 0)
//             {
//                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
//                                                                 message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings."
//                                                                delegate:nil
//                                                       cancelButtonTitle:@"OK"
//                                                       otherButtonTitles:nil];
//                 [alert show];
//             }
//             else
//             {
//                 //Get the first account in the array
//                 ACAccount *twitterAccount = [twitterAccounts objectAtIndex:0];
//                 
//                 completionHandler(twitterAccount);
//             }
//         }
//     }];
//}




@end

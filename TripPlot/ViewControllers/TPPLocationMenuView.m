//
//  TPPLocationMenuView.m
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPLocationMenuView.h"

@implementation TPPLocationMenuView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)resetAllMenuButtonsImage:(TPPLocationMenuView *)menuView
{

    [menuView.fuelStationButton setBackgroundImage:[UIImage imageNamed:@"btn_fuel_gray"] forState:UIControlStateNormal];

    [menuView.restaurantButton setBackgroundImage:[UIImage imageNamed:@"btn_eat_gray"] forState:UIControlStateNormal];

    [menuView.hotelButton setBackgroundImage:[UIImage imageNamed:@"btn_stay_gray"] forState:UIControlStateNormal];

    [menuView.stopageButton setBackgroundImage:[UIImage imageNamed:@"btn_POI_gray"] forState:UIControlStateNormal];

    [menuView.hospitalButton setBackgroundImage:[UIImage imageNamed:@"btn_medical_gray"] forState:UIControlStateNormal];

    [menuView.atmButton setBackgroundImage:[UIImage imageNamed:@"btn_atm_gray"] forState:UIControlStateNormal];
}


-(void)setBadgesWithTripModel:(TPPTripModel *)tripModel
{
        NSArray *poiArray = tripModel.tripPoints[@"poi"];
        NSArray *atmArray = tripModel.tripPoints[@"atm"];
        NSArray *fuelArray = tripModel.tripPoints[@"fuel"];
        NSArray *restroomArray = tripModel.tripPoints[@"restroom"];
        NSArray *medicalArray = tripModel.tripPoints[@"medical"];
        NSArray *foodArray = tripModel.tripPoints[@"food"];
    
    
        if (poiArray.count>0)
        {
            self.stopageButton.badgeValue = [NSString stringWithFormat:@"%ld",poiArray.count];
            self.stopageButton.badgePostion = BadgePositionon_MiddleRight;
            self.stopageButton.badgeBGColor = Badge_BlueColor;
    
        }
    
        if (atmArray.count>0)
        {
            self.atmButton.badgeValue = [NSString stringWithFormat:@"%ld",atmArray.count];
            self.atmButton.badgePostion = BadgePositionon_MiddleRight;
            self.atmButton.badgeBGColor = Badge_BlueColor;
    
        }
    
        if (fuelArray.count>0)
        {
            
            self.fuelStationButton.badgeValue = [NSString stringWithFormat:@"%ld",fuelArray.count];
            self.fuelStationButton.badgePostion = BadgePositionon_MiddleRight;
            self.fuelStationButton.badgeBGColor = Badge_BlueColor;
        }
    
        if (restroomArray.count>0)
        {
            self.hotelButton.badgeValue = [NSString stringWithFormat:@"%ld",restroomArray.count];
            self.hotelButton.badgePostion = BadgePositionon_MiddleRight;
            self.hotelButton.badgeBGColor = Badge_BlueColor;
           
        }
    
        if (medicalArray.count>0)
        {
            self.hospitalButton.badgeValue = [NSString stringWithFormat:@"%ld",medicalArray.count];
            self.hospitalButton.badgePostion = BadgePositionon_MiddleRight;
            self.hospitalButton.badgeBGColor = Badge_BlueColor;
    
        }
    
        if (foodArray.count>0)
        {
            self.restaurantButton.badgeValue = [NSString stringWithFormat:@"%ld",foodArray.count];
            self.restaurantButton.badgePostion = BadgePositionon_MiddleRight;
            self.restaurantButton.badgeBGColor = Badge_BlueColor;
        }
}




@end

//
//  TPPSettingsViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPSettingsViewController.h"
#import "TPPChangePasswordViewController.h"

@interface TPPSettingsViewController ()

@end

@implementation TPPSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Settings";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.hidden = NO;
}

- (IBAction)notificationSwitchTapped:(UISwitch *)sender
{
    if(sender.isOn)
    {
        //[[TPPSocketUtility sharedInstance] connectToSocket];
        [TPPUtilities registerForPushNotifications];
    }
    else
    {
        [[TPPSocketUtility sharedInstance].socket close];
        [TPPSocketUtility sharedInstance].isSocketConnected = NO;
        [TPPUtilities unRegisterForPushNotifications];
    }
}



- (IBAction)changePasswordButtonTapped:(id)sender
{
    [self pushViewControllerWithIdentifier:@"TPPChangePasswordViewController"];
}




@end

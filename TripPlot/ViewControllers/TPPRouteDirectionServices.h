//
//  TPPRouteDirectionServices.h
//  TripPlot
//
//  Created by Daffolap-21 on 04/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import <GoogleMaps/GoogleMaps.h>
@interface TPPRouteDirectionServices : NSObject
typedef enum tagTravelMode
{
    TravelModeDriving,
    TravelModeBicycling,
    TravelModeTransit,
    TravelModeWalking
}TravelMode;

typedef enum placeType
{
    FuelStations,
    Restaurant,
    Hotel,
    Stopage,
    Hospital,
    ATM,
}PlaceType;


+ (void)getRoutesWithLocations:(NSArray *)locations travelMode:(TravelMode)travelMode andCompletitionBlock:(void (^)(id routes, NSError *error))completitionBlock;


+ (void)getRoutesWithLocations:(NSArray *)locations andCompletitionBlock:(void (^)(id routes, NSError *error))completitionBlock;

+ (void)getPlacesWithLocations:(CLLocation *)location travelMode:(NSString *)placesType radius:(NSInteger )radius pagetoken:(NSString *)pagetoken andCompletitionBlock:(void (^)(id places, NSError *error))completitionBlock;

+ (void)getDistanceWithLocations:(CLLocation *)source andDestination:(CLLocation *)destination andCompletitionBlock:(void (^)(id distance, NSError *error))completitionBlock;
+(NSArray *) getAllDistanceBreakUp: (NSArray *) coordinates;

+(CLLocation *) findNewCenterLocation:(long )radius and: (NSArray *)distanses withLocations:(NSArray *)coordinates;

+ (NSMutableArray *) decodePolylineWithEncodedString:(NSString *)encodedString;

@end

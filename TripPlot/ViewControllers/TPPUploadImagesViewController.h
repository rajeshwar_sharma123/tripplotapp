//
//  TPPUploadImagesViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#ifndef AWS_MULTI_FRAMEWORK
#define AWS_MULTI_FRAMEWORK
#endif

#import "TPPBaseViewController.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AWSCore/AWSClientContext.h>
#import <AWSS3/AWSS3TransferUtility.h>
#import "LDProgressView.h"

@interface TPPUploadImagesViewController : TPPBaseViewController


- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)doneButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet DKCarouselView *carouselView;
@property (strong,nonatomic) NSMutableArray *amazonImgURLArry;
@property (strong,nonatomic) NSMutableArray *fileNameArry;
@property (weak, nonatomic) IBOutlet LDProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *iLbl;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

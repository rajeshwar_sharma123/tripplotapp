//
//  TPPCreateNewTripViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPCreateNewTripViewController.h"
#import "TPPTripLocationTableViewCell.h"
#import "TPPTripStartTimeCellTableViewCell.h"
#import "TPPTripTypeTableViewCell.h"
#import "TPPNewLocationTableViewCell.h"
#import "TPPAddLocationView.h"
#import "TPPAddTagView.h"
#import "TPPRoutesViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "IQUIView+IQKeyboardToolbar.h"

@interface TPPCreateNewTripViewController ()<GMSAutocompleteFetcherDelegate>
{
    //NSMutableArray *locations;
    
    NSInteger totalRows;
    NSMutableArray *typeTokens;
    TPPAddLocationView *addLocationPopup;
    //TPPTripLocation *tripLocation;
    TPPSourceDestinationModel *sourceDestinationObj;
    NSInteger tappedIndex;
    BOOL isSource;
    NSMutableArray *typesArray;
    NSMutableArray *modesArray;
    TPPAddTagView *addTagPopupView;
    NSString *currentTag;

    NSArray *allTypesArray;
    NSArray *allModesArray;
    NSArray *filteredTokensArray;

    NSMutableDictionary *requestData;
    TPPTripLocation *tripLocation;
    TPPCreateTripModel *newTripObject;
    NSArray *searchLocation;
    NSString *titleText;
    UIDatePicker *selectedDate;
    
}
@property NSMutableArray *queryResults;
@property NSMutableArray *recentSearchResult;
@end

@implementation TPPCreateNewTripViewController
GMSPlacesClient *placesClient;
GMSAutocompleteFetcher * _fetcher;
#pragma -mark VC life cycle
- (void)viewDidLoad {
   [super viewDidLoad];
    //locations = [NSMutableArray new];
     newTripObject = [TPPCreateTripModel sharedInstance];
    if(newTripObject.isNew)
    {
    newTripObject.startDate = [[[NSDate date] getDateAfterDay:1] getStringFromat];
    newTripObject.endDate = [[[newTripObject.startDate getDateFromString] getDateAfterDay:2] getStringFromat];
    }
    else
    {
        newTripObject.startDate = [[[NSDate date] getDateAfterDay:-3] getStringFromat];
        newTripObject.endDate = [[[newTripObject.startDate getDateFromString] getDateAfterDay:2] getStringFromat];
    }

	if (_sourceDestinationsArray.count == 0 || _sourceDestinationsArray== nil) {
		_sourceDestinationsArray = [NSMutableArray new];
		sourceDestinationObj = [[TPPSourceDestinationModel alloc]init];
		sourceDestinationObj.destination = [TPPTripLocation new];
		sourceDestinationObj.source = [TPPTripLocation new];
		[_sourceDestinationsArray addObject:sourceDestinationObj];
	}

    typeTokens = [NSMutableArray new];
    isSource = YES;
    tripLocation = [[TPPTripLocation alloc]init];


    typesArray = [[NSMutableArray alloc]initWithArray:newTripObject.typeNames];
	[typesArray addObject:@"+"];
    modesArray = [[NSMutableArray alloc]initWithArray:newTripObject.modeNames];
	[modesArray addObject:@"+"];
	 titleText = newTripObject.title;


    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TPPAddLocationView" owner:nil options:nil];
    addLocationPopup = nibContents[0];

    NSArray *nibContents2 = [[NSBundle mainBundle] loadNibNamed:@"TPPAddTagView" owner:nil options:nil];
    addTagPopupView = nibContents2[0];
    [addTagPopupView setFrame:self.view.frame];
    addTagPopupView.backgroundColor = [UIColor clearColor];
    addTagPopupView.tagTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    [self.view addSubview:addTagPopupView];
    [addTagPopupView setHidden:YES];


    [addLocationPopup.locationTextField addTarget:self action:@selector(searchLocEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    addLocationPopup.tableForLocations.delegate=self;
        addLocationPopup.tableForLocations.dataSource=self;
    _queryResults=[[NSMutableArray alloc]init];
    placesClient=[[GMSPlacesClient alloc]init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
	filter.country = @"IN";
    CLLocationCoordinate2D neBoundsCorner = CLLocationCoordinate2DMake(49.00, 2.00);
    CLLocationCoordinate2D swBoundsCorner = CLLocationCoordinate2DMake(59.00, 8.00);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:neBoundsCorner
                                                                       coordinate:swBoundsCorner];
    if (_sourceDestinationsArray.count == 0)
        totalRows = 5;
    else{
       totalRows =  _sourceDestinationsArray.count+ 4;
    }

    addTagPopupView.tagTableView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    addLocationPopup.tableForLocations.backgroundColor = COLOR_FOR_MENU_CONTENT;
    self.tableView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    self.tableView.estimatedRowHeight = 44;
    self.tableView.tableFooterView =  [[UIView alloc] initWithFrame:CGRectZero];
    filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
    _fetcher = [[GMSAutocompleteFetcher alloc] initWithBounds:bounds
                                                       filter:filter];
    _fetcher.delegate = self;

    // Do any additional setup after loading the view.


    //call web services to get predefined type and modes
    requestData = [NSMutableDictionary new];
    requestData[@"token"] =[UserDeafaults objectForKey:Token];
    [UserDeafaults objectForKey:Token];
    [self callServiceToGetAllTypes:requestData];
    [self callServiceToGetAllTokenMode:requestData];

    [self.cancelButton setImage:[UIImage imageNamed:@"btn_cancel"] forState:UIControlStateNormal];
    [self.nextButton setImage:[UIImage imageNamed:@"btn_next"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    TPPTripMenuStepperView *menuStepper = [[[NSBundle mainBundle] loadNibNamed:@"TPPTripMenuStepperView" owner:self options:nil] objectAtIndex:0];
    CGRect frame = self.menuView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [menuStepper setFrame:frame];

    [self.menuView addSubview:menuStepper];


        [menuStepper setSelectedIndex:1 isNew:newTripObject.isNew];

}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)searchLocEditingChanged:(id)sender {

    NSMutableDictionary *searchRequest = [NSMutableDictionary new];
    searchRequest[@"token"] = [UserDeafaults objectForKey:Token];
    searchRequest[@"keySearch"] = addLocationPopup.locationTextField.text;

    [self callServiceTogetSearchLocation:searchRequest];
}


#pragma mark - GMSAutocompleteFetcherDelegate
- (void)didAutocompleteWithPredictions:(NSArray *)predictions {
    NSMutableString *resultsStr = [NSMutableString string];
    _queryResults=[[NSMutableArray alloc]init];
    _recentSearchResult=[predictions mutableCopy];
    for (GMSAutocompletePrediction *prediction in predictions) {
        [_queryResults addObject:prediction.attributedFullText.string];
        if ([_queryResults count]!=0) {

           [addLocationPopup.tableForLocations reloadData];

            [resultsStr appendFormat:@"%@\n", [prediction.attributedPrimaryText string]];
        }
        //_resultText.text = resultsStr;
    }}

- (void)didFailAutocompleteWithError:(NSError *)error {
    // _resultText.text = [NSString stringWithFormat:@"%@", error.localizedDescription];
}

#pragma -mark UITableView life cycle

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==addLocationPopup.tableForLocations) {
        if (searchLocation.count > 0) {
            return searchLocation.count;
        }

       return  [_queryResults count];
    }
    else if (tableView==addTagPopupView.tagTableView) {
        return  [filteredTokensArray count];
    }
    else
    return totalRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.tableView == tableView) {
        return 101;
    }
    return 0;

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.tableView == tableView) {
        TPPTripTitleView *view = [[[NSBundle mainBundle] loadNibNamed:@"TPPTripTitleView" owner:self options:nil] objectAtIndex:0];
        view.backgroundColor = COLOR_FOR_MENU_CONTENT;
        [view.titleTextField addTarget:self action:@selector(titleTextChanging: ) forControlEvents:UIControlEventAllEditingEvents];
        view.titleTextField.text = newTripObject.title;

        return view;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == totalRows -2) {
//        return self.tokenFieldForTypes.frame.size.height;
//    }

    if ((indexPath.row == totalRows-1 || indexPath.row == totalRows-2) && tableView == self.tableView) {
        return 100;
    }
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==addTagPopupView.tagTableView) {
        static NSString* simpleTableIdentifier = @"defaultCell";

        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];

        }
        if (indexPath.row<[filteredTokensArray count]) {

            NSDictionary *tempDict = [filteredTokensArray objectAtIndex:indexPath.row];
            cell.textLabel.text = tempDict[@"name"];
        }

        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.numberOfLines=0;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }

    else if (tableView==addLocationPopup.tableForLocations) {
        static NSString* simpleTableIdentifier = @"defaultCell";

        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        if (searchLocation.count > 0) {
            NSDictionary *tempDict = [searchLocation objectAtIndex:indexPath.row];
            cell.textLabel.text = tempDict[@"name"];

        }
        else
        {
        if (indexPath.row<[_queryResults count]) {
            cell.textLabel.text = [_queryResults objectAtIndex:indexPath.row];
         }
        }
        cell.textLabel.numberOfLines=0;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.backgroundColor=[UIColor clearColor];
        return cell;
    }

    else
    {
    if (indexPath.row == totalRows - 1) {

        TPPTripTypeTableViewCell *cell = (TPPTripTypeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"typeCell"];
        if (cell == nil) {

            cell = [[TPPTripTypeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"typeCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;

        [cell.tagList setAutomaticResize:YES];
        [cell.tagList setTags:modesArray];
        [cell.tagList setTagDelegate:self];
        [cell.tagList setTagBackgroundColor:COLOR_FOR_MODE_TAG];
        cell.tagList.tag = 2;
        cell.titleLabel.text = @"Mode";
        [cell layoutIfNeeded];
        return cell;

    }
    else if (indexPath.row == totalRows - 2) {
        TPPTripTypeTableViewCell *cell = (TPPTripTypeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"typeCell"];
        if (cell == nil) {

            cell = [[TPPTripTypeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"typeCell"];

        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //[cell.tokenField.textField becomeFirstResponder];
        [cell.tagList setAutomaticResize:YES];
        [cell.tagList setTags:typesArray];
        [cell.tagList setTagDelegate:self];
        cell.tagList.tag = 1;
         [cell.tagList setTagBackgroundColor:COLOR_FOR_TYPE_TAG];

        cell.titleLabel.text = @"Type";
        [cell layoutIfNeeded];
        return cell;
    }
    else if (indexPath.row == totalRows - 3) {

        TPPTripStartTimeCellTableViewCell *cell = (TPPTripStartTimeCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"timeCell"];
        if (cell == nil) {

            cell = [[TPPTripStartTimeCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"timeCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.datePicker = [UIDatePicker new];
        if (newTripObject.endDate == nil || [newTripObject.endDate isEqualToString:@""]) {
            [cell.datePicker setDate:[NSDate date] animated:YES];
        }
        else{
            [cell.datePicker setDate:[newTripObject.endDate getDateFromString] animated:YES];
             cell.timeTextField.text = [newTripObject.endDate getDisplayFormatFromString];
        }
        
        if (!newTripObject.isNew) {
            [cell.datePicker setMaximumDate:[[NSDate date] getDateAfterDay:-1]];
        }
		else
		{
			[cell.datePicker setMinimumDate:[NSDate new]];
		}
			
       cell.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        [cell.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.timeTextField addDoneOnKeyboardWithTarget:self action:@selector(datePickerButtonTapped:) shouldShowPlaceholder:YES];
        cell.datePicker.tag = indexPath.row;
        [cell.timeTextField setInputView:  cell.datePicker];
        cell.titleLabel.text = @"End";
        return cell;
    }
    else if (indexPath.row == totalRows - 4) {
        TPPTripStartTimeCellTableViewCell *cell = (TPPTripStartTimeCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"timeCell"];
        if (cell == nil) {

            cell = [[TPPTripStartTimeCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"timeCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.datePicker = [UIDatePicker new];
        if (newTripObject.startDate == nil || [newTripObject.startDate isEqualToString:@""]) {
            [cell.datePicker setDate:[NSDate date] animated:YES];
        }
        else{
            [cell.datePicker setDate:[newTripObject.startDate getDateFromString] animated:YES];
             cell.timeTextField.text = [newTripObject.startDate getDisplayFormatFromString];
        }
        
        if (!newTripObject.isNew) {
            [cell.datePicker setMaximumDate:[[NSDate date] getDateAfterDay:-1]];
        }
		else
		{
			[cell.datePicker setMinimumDate:[NSDate new]];
		}

        cell.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        [cell.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.timeTextField addDoneOnKeyboardWithTarget:self action:@selector(datePickerButtonTapped:) shouldShowPlaceholder:YES];
        cell.datePicker.tag = indexPath.row;
        [cell.timeTextField setInputView:  cell.datePicker];
        cell.titleLabel.text = @"Start";
        
        
        
        
        return cell;
    }
    else
    {
        if (indexPath.row == 0) {
        TPPTripLocationTableViewCell *cell = (TPPTripLocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationCell"];
        if (cell == nil) {

            cell = [[TPPTripLocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"locationCell"];
        }
            if (_sourceDestinationsArray.count>0) {
                sourceDestinationObj = [_sourceDestinationsArray objectAtIndex:0];
                TPPTripLocation *source = sourceDestinationObj.source;
                TPPTripLocation *destination = sourceDestinationObj.destination;

                if (source.name==nil || [source.name isEqualToString:@""]) {
                    cell.sourceLabel.text =@"From";
                    cell.sourceLabel.textColor = TPPTextColor;
                }
                else
                {
                    cell.sourceLabel.text =source.name;
                    cell.sourceLabel.textColor = [UIColor whiteColor];
                }
                if (destination.name==nil || [destination.name isEqualToString:@""]) {
                    cell.destinationLabel.text =@"To";
                    cell.destinationLabel.textColor = TPPTextColor;
					
                }
                else
                {
                    cell.destinationLabel.text = destination.name;
                    cell.destinationLabel.textColor = [UIColor whiteColor];
                }
            }
            else
            {
                cell.sourceLabel.text = @"From";
                cell.destinationLabel.text = @"To";
            }

			cell.sourceLabel.tag = indexPath.row;
			cell.destinationLabel.tag = indexPath.row;

            UITapGestureRecognizer *singleTapForSourceLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addSourceLacation:)];
            [singleTapForSourceLabel setNumberOfTapsRequired:1];
            [singleTapForSourceLabel setNumberOfTouchesRequired:1];

            [cell.sourceLabel addGestureRecognizer:singleTapForSourceLabel];

            UITapGestureRecognizer *singleTapForDestinationLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDestinationLocation:)];
            [singleTapForDestinationLabel setNumberOfTapsRequired:1];
            [singleTapForDestinationLabel setNumberOfTouchesRequired:1];

            [cell.destinationLabel addGestureRecognizer:singleTapForDestinationLabel];

            [cell.addLocationButton addTarget:self action:@selector(AddLocationTapped:) forControlEvents:UIControlEventTouchUpInside];

             cell.titleLabel.text = @"From";
            [cell layoutIfNeeded];
            return cell;
        }
        else
        {
            TPPNewLocationTableViewCell *cell = (TPPNewLocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"newLocationCell"];
            if (cell == nil) {

                cell = [[TPPNewLocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"newLocationCell"];
            }
			cell.selectionStyle = UITableViewCellSelectionStyleNone;

            if (_sourceDestinationsArray.count>1) {
                sourceDestinationObj = [_sourceDestinationsArray objectAtIndex:indexPath.row];
                if (sourceDestinationObj.source.name==nil || [sourceDestinationObj.source.name isEqualToString:@""]) {
                    cell.sourceLabel.text =@"Source";
                    cell.sourceLabel.textColor = [UIColor lightGrayColor];

                }
                else
                {
                    cell.sourceLabel.text =sourceDestinationObj.source.name;
                    cell.sourceLabel.textColor = [UIColor whiteColor];
                }
                if (sourceDestinationObj.destination.name==nil || [sourceDestinationObj.destination.name isEqualToString:@""]) {
                    cell.destinationLabel.text =@"Destination";
                    cell.destinationLabel.textColor = [UIColor lightGrayColor];
                }
                else
                {
                    cell.destinationLabel.text =sourceDestinationObj.destination.name;
                    cell.destinationLabel.textColor = [UIColor whiteColor];
                }

            }
            else{
                cell.sourceLabel.text = sourceDestinationObj.source.name;
                cell.destinationLabel.text = @"To";
            }
            cell.sourceLabel.tag = indexPath.row;
            cell.destinationLabel.tag = indexPath.row;


//            UITapGestureRecognizer *singleTapForSourceLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addSourceLacation:)];
//            [singleTapForSourceLabel setNumberOfTapsRequired:1];
//            [singleTapForSourceLabel setNumberOfTouchesRequired:1];
//
//            [cell.sourceLabel addGestureRecognizer:singleTapForSourceLabel];

            UITapGestureRecognizer *singleTapForDestinationLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDestinationLocation:)];
            [singleTapForDestinationLabel setNumberOfTapsRequired:1];
            [singleTapForDestinationLabel setNumberOfTouchesRequired:1];

            [cell.destinationLabel addGestureRecognizer:singleTapForDestinationLabel];

            [cell.removeLocationButton addTarget:self action:@selector(removeLocationTapped:) forControlEvents:UIControlEventTouchUpInside];
            cell.removeLocationButton.tag = indexPath.row;

            [cell layoutIfNeeded];
            return cell;
        }





    }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSError *error;
    if (tableView == addTagPopupView.tagTableView) {
        NSDictionary *tempDict = [filteredTokensArray objectAtIndex:indexPath.row];
        currentTag = tempDict[@"name"];

        [self addTag:tableView.tag];

    }
   else if (tableView==addLocationPopup.tableForLocations) {


        [addLocationPopup.locationTextField resignFirstResponder];
//       UIButton *tempButton = [[UIButton alloc]init];
//       tempButton.tag = index
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
       if (searchLocation.count>0) {
           tripLocation = [[TPPTripLocation alloc]initWithDictionary:[searchLocation objectAtIndex:indexPath.row] error:&error];
           addLocationPopup.locationTextField.text = tripLocation.name;
           [self addLocationViaPop:nil];

       }
       else{
        GMSAutocompletePrediction *prediction=[_recentSearchResult objectAtIndexedSubscript:indexPath.row];
        NSString *placeID = prediction.placeID;

        [placesClient lookUpPlaceID:placeID callback:^(GMSPlace *place, NSError *error) {
            if (error != nil) {
                NSLog(@"Place Details error %@", [error localizedDescription]);
                return;
            }

            if (place != nil) {
                tripLocation.name = addLocationPopup.locationTextField.text;
                NSMutableDictionary *geoCode = [NSMutableDictionary new];
                geoCode[@"latitude"] = [NSString stringWithFormat:@"%f", place.coordinate.latitude];
                geoCode[@"longitude"] = [NSString stringWithFormat:@"%f", place.coordinate.longitude];

                // tripLocation.geoCode = geoCode;
                // tripLocation.country =place.ad

                NSLog(@"Place name %@", place.name);
                NSLog(@"Place name %@", place.name);
                NSLog(@"Place address %@",place.formattedAddress);
                NSLog(@"Place placeID %@",place.placeID);
                NSLog(@"Place attributions %@", place.attributions);
            } else {
                NSLog(@"No place details for %@", placeID);
            }

            CLLocationCoordinate2D addressCoordinates = CLLocationCoordinate2DMake(place.coordinate.latitude,place.coordinate.longitude);

            GMSGeocoder* coder = [[GMSGeocoder alloc] init];
            [addLocationPopup showLoader];
            [coder reverseGeocodeCoordinate:addressCoordinates completionHandler:^(GMSReverseGeocodeResponse *results, NSError *error) {
                [addLocationPopup hideLoader];

                if (error) {
                    NSLog(@"Error %@", error.description);


                } else {
                    GMSAddress* address = [results firstResult];
//                    NSLog(@"thoroughfare %@",address.thoroughfare);
//                    NSLog(@"locality %@",address.locality);
//                    NSLog(@"subLocality %@",address.subLocality);
//                    NSLog(@"administrativeArea %@",address.administrativeArea);
//                    NSLog(@"country %@",address.country);
//                    NSLog(@"lines %@",address.lines);
                    tripLocation.country = address.country;
                    tripLocation.abbrState = address.locality;

                    //gkjdkgjdg
                    [self addLocationViaPop:nil];
                }
            }];
            
        }];
        
        addLocationPopup.locationTextField.text = cell.textLabel.text;

    }
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
   }
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}





- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex forTagList:(DWTagList *)tagList
{
    NSLog(@"tag list :  %ld", (long)tagList.tag);
    if (tagList.tag == Type) {
        if (![tagName isEqualToString:@"+"]) {

            [typesArray removeObjectAtIndex:tagIndex];
            [tagList setTags:typesArray];
        }
        else
        {
            addTagPopupView.tagTextField.text = @"";
            addTagPopupView.titleLabel.text = @"Add Types";
            addTagPopupView.tagTableView.delegate = self;
            addTagPopupView.tagTableView.dataSource = self;
            addTagPopupView.tagTableView.tag = Type;
            addTagPopupView.tagTextField.tag = Type;
            [addTagPopupView.tagTextField addTarget:self action:@selector(tagTextChanging:) forControlEvents:UIControlEventAllEditingEvents];
            addTagPopupView.doneButton.tag = Type;
            [addTagPopupView.doneButton addTarget:self action:@selector(addTagText:) forControlEvents:UIControlEventTouchUpInside];
            [addTagPopupView setHidden:NO];
            filteredTokensArray = nil;
            
            [addTagPopupView.tagTableView reloadData];
        }

    }
    else
    {
        if (![tagName isEqualToString:@"+"]) {

            [modesArray removeObjectAtIndex:tagIndex];
            [tagList setTags:modesArray];
        }
        else
        {
            addTagPopupView.tagTextField.text = @"";
            addTagPopupView.titleLabel.text = @"Add Modes";
            addTagPopupView.tagTableView.delegate = self;
             addTagPopupView.tagTableView.dataSource = self;
            addTagPopupView.tagTableView.tag = Mode;
            addTagPopupView.tagTextField.tag = Mode;
            [addTagPopupView.tagTextField addTarget:self action:@selector(tagTextChanging:) forControlEvents:UIControlEventAllEditingEvents];
            addTagPopupView.doneButton.tag = Mode;
            [addTagPopupView.doneButton addTarget:self action:@selector(addTagText:) forControlEvents:UIControlEventTouchUpInside];
            [addTagPopupView setHidden:NO];
            filteredTokensArray = nil;
            
            [addTagPopupView.tagTableView reloadData];

        }
    }

}


-(void) tagTextChanging:(UITextField *)sender
{
    NSLog(@"current text: %@",sender.text);
    currentTag = sender.text;
    if (sender.tag == Type) {
        filteredTokensArray = [allTypesArray filterArrayOnKey:@"name" value:currentTag];
    }
    else
    {
        filteredTokensArray = [allModesArray filterArrayOnKey:@"name" value:currentTag];
    }
    [addTagPopupView.tagTableView reloadData];
}

- (void)datePickerButtonTapped:(id)sender
{
    UIDatePicker *picker = selectedDate;
    
    NSLog(@"current selected: %ld",(long)picker.tag);
    NSLog(@"selected date: %@",[NSString stringWithFormat:@"%@", picker.date]);
    
    if (picker.tag == totalRows - 4) {
        
//        NSInteger diff = [newTripObject.endDate getTimeIntervalDifference:[picker.date getStringFromat]];
//        if (diff >0) {
            newTripObject.startDate = [picker.date getStringFromat];
//        }
//        else
//        {
//            [self showErrorMessage:MSG_Invalid_Date];
//        }

    }
    else{
//        NSInteger diff = [[picker.date getStringFromat]  getTimeIntervalDifference:newTripObject.startDate];
//        if (diff >= 0) {
            newTripObject.endDate = [picker.date getStringFromat];
//        }
//        else
//        {
//            [self showErrorMessage:MSG_Invalid_Date];
//        }
    }
    
    [self.tableView reloadData];
    
}
-(void)datePickerValueChanged:(UIDatePicker *)picker
{

    selectedDate = picker;
    

    
}
-(void) titleTextChanging:(UITextField *)sender
{
    NSLog(@"current text: %@",sender.text);
    titleText = [sender.text trimmedString];
    newTripObject.title = titleText;
}


-(void) addTagText:(UIButton *)sender
{

    [self addTag:sender.tag];
}

-(void)addTag:(NSInteger )tag
{
    if ([currentTag length]) {
        if (tag == Type) {
            [typesArray removeLastObject];
            [typesArray addObject:currentTag];
            [typesArray addObject:@"+"];
        }
        else
        {
            [modesArray removeLastObject];
            [modesArray addObject:currentTag];
            [modesArray addObject:@"+"];
        }
    }

    [addTagPopupView setHidden:YES];
    // Build the two index paths
    NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:totalRows-1 inSection:0];
    NSIndexPath* indexPath2 = [NSIndexPath indexPathForRow:totalRows-2 inSection:0];
    // Add them in an index path array
    NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, indexPath2, nil];
    // Launch reload for the two index path
    [self.tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    addTagPopupView.tagTextField.text = @"";
}

-(void)addSourceLacation:(id)sender {

    [addLocationPopup setFrame:self.view.frame];
    [self.view addSubview:addLocationPopup];
    UILabel *label = (UILabel *)[sender view] ;
    addLocationPopup.doneButton.tag = label.tag;
    addLocationPopup.locationTextField.text = @"";
    tappedIndex = label.tag;
    isSource = YES;
    [addLocationPopup.doneButton addTarget:self action:@selector(addLocationViaPop:) forControlEvents:UIControlEventTouchUpInside];
   // NSLog(@"source tapped: %ld ",label.tag);
}


- (void)addDestinationLocation:(id)sender {

    UILabel *label = (UILabel *)[sender view];
    NSLog(@"dest tapped: %ld ",label.tag);
    [addLocationPopup setFrame:self.view.frame];
    [self.view addSubview:addLocationPopup];
    addLocationPopup.locationTextField.text = @"";
    tappedIndex = label.tag;
    isSource = NO;
    addLocationPopup.doneButton.tag = label.tag;
    [addLocationPopup.doneButton addTarget:self action:@selector(addLocationViaPop:) forControlEvents:UIControlEventTouchUpInside];

}

-(void) addLocationViaPop: (UIButton *)sender
{
    if (sender) {
         tripLocation.name = addLocationPopup.locationTextField.text;
    }

    TPPSourceDestinationModel *obj = (TPPSourceDestinationModel *) [_sourceDestinationsArray objectAtIndex:tappedIndex];
    if (isSource) {
        obj.source = [tripLocation copy];
    }
    else
    {
        obj.destination = [tripLocation copy];
    }

    //update next source

    if (tappedIndex< _sourceDestinationsArray.count -1) {
        TPPSourceDestinationModel *objNext = (TPPSourceDestinationModel *) [_sourceDestinationsArray objectAtIndex:tappedIndex+1];
        objNext.source = [obj.destination copy];
        [_sourceDestinationsArray replaceObjectAtIndex:tappedIndex+1 withObject:objNext];
    }

    [_sourceDestinationsArray replaceObjectAtIndex:tappedIndex withObject:obj];

    [addLocationPopup removeFromSuperview];
    [self.tableView reloadData];
}


- (void)AddLocationTapped:(id)sender {

    UIButton *btn = (UIButton *)sender ;
    NSLog(@"add button tapped: %ld ",btn.tag);

    if ([self isFilledLocation:_sourceDestinationsArray]) {
        TPPSourceDestinationModel *objOld = (TPPSourceDestinationModel *)[ _sourceDestinationsArray lastObject];
        TPPSourceDestinationModel *objNew = [[TPPSourceDestinationModel alloc]init];
        objNew.source = [objOld.destination copy];
        [_sourceDestinationsArray addObject:objNew];
        if (_sourceDestinationsArray.count == 0)
            totalRows = 5;
        else{
            totalRows =  _sourceDestinationsArray.count+ 4;
        }
        [self.tableView reloadData];
        
        [addLocationPopup removeFromSuperview];
    }
}

- (void)removeLocationTapped:(id)sender {

    UIButton *btn = (UIButton *)sender ;
    NSLog(@"remove button tapped: %ld ",btn.tag);
    if (btn.tag == _sourceDestinationsArray.count-1) {
         [_sourceDestinationsArray removeObjectAtIndex:btn.tag];
    }
    else
    {
        [_sourceDestinationsArray removeObjectAtIndex:btn.tag];
        for (NSInteger atIndex = btn.tag; atIndex < _sourceDestinationsArray.count; atIndex++ ) {
            TPPSourceDestinationModel *prevLoc = [[_sourceDestinationsArray objectAtIndex:atIndex-1] copy];
            TPPSourceDestinationModel *currLoc = [[_sourceDestinationsArray objectAtIndex:atIndex] copy];
            currLoc.source = [prevLoc.destination copy];
            [_sourceDestinationsArray replaceObjectAtIndex:atIndex withObject:currLoc];
        }

    }

    if (_sourceDestinationsArray.count == 0)
        totalRows = 5;
    else{
        totalRows =  _sourceDestinationsArray.count+ 4;
    }
    [self.tableView reloadData];
}

#pragma -mark Web service calls

-(void)callServiceToGetAllTypes:(NSMutableDictionary *)requestData
{
    [self.view showLoader];
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_TYPE_LIST_URL] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];

         if ([[response objectForKey:@"code"] integerValue] == 200)
         {

             allTypesArray = response[@"response"][@"result"];
         }
         else
         {

         }

     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
}

-(void)callServiceToGetAllTokenMode:(NSMutableDictionary *)requestData
{
    [self.view showLoader];
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_MODE_LIST_URL] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];

         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             allModesArray = response[@"response"][@"result"];
         }
         else
         {

         }

     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];

         [self showErrorMessage:errorMessage];
         
     }];
}

-(void) callServiceToValidateTrip
{
	[self.view showLoader];
	NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
	NSDictionary *credentialsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
										   [UserDeafaults objectForKey:Token],@"token",
										   userProfile[@"_id"], @"userId",
										   newTripObject.startDate, @"startDate",
										   newTripObject.endDate, @"endDate",
										   nil];

	[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_VALIDATE_URL] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 [self.view hideLoader];

		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 NSDictionary *data = response[@"response"];
			 if ([data[@"status"] integerValue] == 1) {
			 TPPRoutesViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRoutesViewController"];
			 [self.navigationController pushViewController:vc animated:NO];
			 }else
			 {
				 NSString *errorMessage = [NSString stringWithFormat:@"You are already on the \"%@\" trip",data[@"title"]];

				 [self showErrorMessage:errorMessage];

			 }
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {

		 [self.view hideLoader];

		 [self showErrorMessage:errorMessage];

	 }];
}

-(void) callServiceTogetSearchLocation:(NSMutableDictionary *)requestDictionary
{
		//[self.view showLoader];
	[TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_SEARCH_LOCATION_URL] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
	 {
			 //[self.view hideLoader];

		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 searchLocation = response[@"response"][@"result"];
			 if (searchLocation.count == 0) {
				 searchLocation = nil;
				 [_fetcher sourceTextHasChanged:addLocationPopup.locationTextField.text];
			 }
			 else{
				 [addLocationPopup.tableForLocations reloadData];
			 }
		 }
		 else
		 {
			 searchLocation = nil;
			 [_fetcher sourceTextHasChanged:addLocationPopup.locationTextField.text];
		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 searchLocation = nil;
			 // [self.view hideLoader];
		 [_fetcher sourceTextHasChanged:addLocationPopup.locationTextField.text];

	 }];
}

#pragma -mark UTilty methods

-(BOOL) isFilledLocation:(NSMutableArray *) array
{
    if (array.count >= 7) {
        [self showErrorMessage:you_have_already_add_8_location_error];
        return NO;
    }
    else if (array.count == 1) {
        TPPSourceDestinationModel *loc = (TPPSourceDestinationModel *)[array objectAtIndex:0];

        if (loc.source == nil || loc.source.name == nil || [loc.source.name isEqualToString:@""]) {
            [self showErrorMessage:MSG_TRIP_LOCATION_REQ];
            return NO;
        }

        if (loc.destination == nil || loc.destination.name == nil || [loc.destination.name isEqualToString:@""]) {
            [self showErrorMessage:MSG_TRIP_LOCATION_REQ];
            return NO;
        }
    }
    else
    {
        TPPSourceDestinationModel *loc = (TPPSourceDestinationModel *)[array lastObject];
        if (loc.destination == nil || loc.destination.name == nil || [loc.destination.name isEqualToString:@""]) {
            [self showErrorMessage:@"Destination cannot be empty"];
            return NO;
        }
    }

    return YES;
}

-(BOOL) isAllLocationFilledByUser:(NSMutableArray *) array
{
    if (array.count == 1) {
		TPPSourceDestinationModel *loc = (TPPSourceDestinationModel *)[array objectAtIndex:0];

		if (loc.source == nil || loc.source.name == nil || [loc.source.name isEqualToString:@""]) {
			[self showErrorMessage:MSG_TRIP_LOCATION_REQ];
			return NO;
		}

		if (loc.destination == nil || loc.destination.name == nil || [loc.destination.name isEqualToString:@""]) {
			[self showErrorMessage:MSG_TRIP_LOCATION_REQ];
			return NO;
		}
	}
	else
	{
		TPPSourceDestinationModel *loc = (TPPSourceDestinationModel *)[array lastObject];
		if (loc.destination == nil || loc.destination.name == nil || [loc.destination.name isEqualToString:@""]) {
			[self showErrorMessage:@"Destination cannot be empty"];
			return NO;
		}
	}

	return YES;
}


- (IBAction)cancelButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)nextButtonTapped:(id)sender{
    newTripObject.tripLocations = [[_sourceDestinationsArray getLocationsArrayInRequestForamt]mutableCopy];
    NSMutableArray *tempMode = [[NSMutableArray alloc]initWithArray:[modesArray copy]];
    [tempMode removeObjectAtIndex:modesArray.count-1];
    NSMutableArray *tempTypes = [[NSMutableArray alloc]initWithArray: [typesArray copy]];
    [tempTypes removeObjectAtIndex:typesArray.count-1];
    newTripObject.typeNames = [tempTypes copy];
    newTripObject.modeNames = [tempMode copy];
    newTripObject.title = titleText;

    if ([self isValid]) {
		[self callServiceToValidateTrip];
    }
}

-(BOOL) isValid
{
    if ([newTripObject.title isEqualToString:@""] || newTripObject.title== nil) {
        [self showErrorMessage:MSG_TRIP_NAME_REQ];
        return NO;
    }

    if (![self isAllLocationFilledByUser:_sourceDestinationsArray]){
			//[self showErrorMessage:MSG_TRIP_LOCATION_REQ];
        return NO;
    }

    if (newTripObject.typeNames.count<=0) {
        [self showErrorMessage:MSG_TRIP_TYPE_REQ];
        return NO;
    }
    if (newTripObject.modeNames.count<=0) {
        [self showErrorMessage:MSG_TRIP_MODE_REQ];
        return NO;
    }
	NSInteger diff = [newTripObject.endDate getTimeIntervalDifference:newTripObject.startDate ];
	if (diff <= 0) {
		[self showErrorMessage:MSG_Invalid_Date];
		return NO;

	}
    return YES;
}



@end

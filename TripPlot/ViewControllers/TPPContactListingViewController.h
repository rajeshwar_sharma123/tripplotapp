//
//  TPPContactListingViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPContactListingViewController : TPPBaseViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property BOOL shouldEnableSelection;
@property BOOL shouldEnableInvite;

@end

//
//  TPPAddMembersViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 05/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPTripMenuStepperView.h"

@interface TPPAddMembersViewController : UIViewController<DWTagListDelegate,DWTagViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *groupButton;
@property (weak, nonatomic) IBOutlet UIButton *soloButton;

@property (weak, nonatomic) IBOutlet UIButton *inviteFriendYesButton;
@property (weak, nonatomic) IBOutlet UIButton *inviteFriendNoButton;
@property (weak, nonatomic) IBOutlet UIButton *openToOtherYesButton;
@property (weak, nonatomic) IBOutlet UIButton *openToOtherNoButton;
@property (weak, nonatomic) IBOutlet UIView *addMembersContentView;

@property (weak, nonatomic) IBOutlet DWTagList *tagList;


@property (weak, nonatomic) IBOutlet UIButton *addFriendButton;
- (IBAction)addFriendButton:(id)sender;
@property (weak, nonatomic) IBOutlet TPPTripMenuStepperView *menuView;

- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)nextButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
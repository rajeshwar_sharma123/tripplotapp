//
//  TPPForgotPasswordViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPForgotPasswordViewController.h"

@implementation TPPForgotPasswordViewController



- (void)viewDidLoad
{
   // [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    if (_email) {
        _emailTextField.text = _email;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)submitButtonTapped:(id)sender
{
    if ([self.emailTextField.text.trimmedString isEqualToString:@""])
    {
        [self showErrorMessage:EMAIL_EMPTY];
        return;
    }
    else if (!self.emailTextField.text.isValidEmail)
    {
        [self showErrorMessage:EMAIL_INVALID];
        return;
    }
    
    
    
    [self.view showLoader];
    
    NSDictionary *credentialsDictionary = [NSDictionary dictionaryWithObject:self.emailTextField.text.trimmedString forKey:@"username"];
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ForgotPasswordUrl] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
        [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [self showSuccessMessage:@"Password sent to your Email Address"];
             
             [self.navigationController popViewControllerAnimated:YES];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
    

}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end

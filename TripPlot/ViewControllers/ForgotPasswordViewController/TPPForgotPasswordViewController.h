//
//  TPPForgotPasswordViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPForgotPasswordViewController : TPPBaseViewController


@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property NSString *email;

@end

//
//  TPPBaseViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import "TPPHomeViewController.h"
#import "TPPSearchViewController.h"
#import "TPPExploreViewController.h"

@interface TPPBaseViewController ()<TPPSearchDelegate>
{
    //TPPDropDownView *dropDownView;
    TPPSearchViewController *searchViewCntrl;
    UIImageView *notifyCarrotImgView;
    UIImageView *plotCarrotImgView;
    UIImageView *searchCarrotImgView;
}
@end

@implementation TPPBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self addLeftMenuButton];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (plotPullableView.opened)
    {
        [plotPullableView setOpened:NO animated:YES];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark- Utility Methods

-(void)addRightMenuButtons
{
    _dropDownView = [[TPPDropDownView alloc] initWithData:@[@"All Trips",@"Ongoing Trips",@"Upcoming Trips",@"Completed Trips",@"Recommended Destinations"] attachedView:self.view];
    _dropDownView.scrollEnabled = NO;
    [self.view addSubview:_dropDownView];
    
    
    UIImage *image;
    UIButton *button;
    NSInteger width = 38;
    NSInteger height = 46;
    NSInteger decreaseHeight = 8;
    NSInteger carrotHeight = 6;
    if (IS_IPHONE_4 || IS_IPHONE_5)
    {
        width = 32;
        height = 44;
        decreaseHeight = 12;
    }
    
    
    
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    navigationView.backgroundColor = [UIColor clearColor];
    
    image = [UIImage imageNamed:@"btn_search_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_search_pressed"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    searchCarrotImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, navigationView.frame.size.height-carrotHeight, navigationView.frame.size.width,carrotHeight)];
    searchCarrotImgView.contentMode = UIViewContentModeScaleAspectFit;
    searchCarrotImgView.hidden = YES;
    searchCarrotImgView.image = [UIImage imageNamed:@"icn_carrot_gray"];
    searchCarrotImgView.backgroundColor = [UIColor clearColor];
    [navigationView addSubview:searchCarrotImgView];

    
    
    [button addTarget:self action:@selector(searchButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];

    
    navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width,  height)];
    
    image = [UIImage imageNamed:@"btn_explore_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_explore_pressed"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(exploreButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *exploreButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];
    
    
    navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width,  height)];
    
    image = [UIImage imageNamed:@"btn_plot_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_plot_pressed"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    plotCarrotImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, navigationView.frame.size.height-carrotHeight, navigationView.frame.size.width,carrotHeight)];
    plotCarrotImgView.contentMode = UIViewContentModeScaleAspectFit;
    plotCarrotImgView.hidden = YES;
    plotCarrotImgView.image = [UIImage imageNamed:@"icn_carrot_gray"];
    plotCarrotImgView.backgroundColor = [UIColor clearColor];
    [navigationView addSubview:plotCarrotImgView];
    
    [button addTarget:self action:@selector(plotButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *plotButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];
    
    
    
    navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width,  height)];
    
    image = [UIImage imageNamed:@"btn_notif_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_notif_pressed"] forState:UIControlStateHighlighted];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    notifyCarrotImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, navigationView.frame.size.height-carrotHeight, navigationView.frame.size.width,carrotHeight)];
    notifyCarrotImgView.contentMode = UIViewContentModeScaleAspectFit;
    notifyCarrotImgView.hidden = YES;
    notifyCarrotImgView.image = [UIImage imageNamed:@"icn_carrot_gray"];
    notifyCarrotImgView.backgroundColor = [UIColor clearColor];
    [navigationView addSubview:notifyCarrotImgView];

    [button addTarget:self action:@selector(notificationButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *notificationButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];
    
    
    image = [UIImage imageNamed:@"btn_more_gray"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setImage:[UIImage imageNamed:@"btn_more_pressed"] forState:UIControlStateHighlighted];
   
    button.frame = CGRectMake(0, 0, width,  width);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(moreButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *moreButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    

    
    
    self.navigationItem.rightBarButtonItems = @[moreButtonItem, searchButtonItem, exploreButtonItem, plotButtonItem, notificationButtonItem];
    self.navigationController.navigationBar.hidden = NO;

//    UILabel *titleLabel = [UILabel new];
//    titleLabel.adjustsFontSizeToFitWidth = YES;
//    NSDictionary *attributes = @{
//                                 NSForegroundColorAttributeName: [UIColor whiteColor],
//                                 NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.0]
//                                 };
//    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"TripPlot" attributes:attributes];
//    [titleLabel sizeToFit];
//    titleLabel.frame = CGRectMake(-20, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height);
//    
//    self.navigationItem.titleView = titleLabel;

}



-(void)notificationButtonTapped:(UIButton *)sender
{
   
    
    if (notificationPullableView ==nil)
    {
        notificationPullableView = [[StyledPullableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        notificationPullableView.openedCenter =  CGPointMake(ScreenWidth/2,ScreenHeight/2);
        notificationPullableView.closedCenter =  CGPointMake(ScreenWidth/2,-ScreenHeight/2);
        notificationPullableView.center = notificationPullableView.closedCenter;
        notificationPullableView.delegate = self;
        notificationPullableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:notificationPullableView];
        
    }
    [self.view bringSubviewToFront:notificationPullableView];
    if (notificationPullableView.opened)
    {
        [self hideShowCarrotImgView:notifyCarrotImgView flagHidden:YES];
        [notificationPullableView setOpened:NO animated:YES];
    }
    else
    {
        [self hideShowCarrotImgView:notifyCarrotImgView flagHidden:NO];
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
        TPPNotificationDropDownViewController *notificationViewCntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationDropDown"];
        notificationViewCntrl.notificationButton = sender;
        notificationViewCntrl.delegate = (TPPHomeViewController *) [TPPUtilities getCurrentViewController];
        notificationViewCntrl.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight-165);
        notificationViewCntrl.view.backgroundColor = ColorFromRGB(0x131010);
        notificationViewCntrl.view.backgroundColor = [UIColor blackColor];
        [notificationPullableView addSubview:notificationViewCntrl.view];
        [self addChildViewController:notificationViewCntrl];
        [notificationViewCntrl didMoveToParentViewController:self];
        
        if (plotPullableView.opened) {
           [notificationPullableView setOpened:YES animated:NO];
            [plotPullableView setOpened:NO animated:NO];
        }
        else{
            
            [notificationPullableView setOpened:YES animated:YES];
        }

    }
    [_dropDownView hideView];
    [searchViewCntrl hideView];

}
- (void)hideShowCarrotImgView:(UIImageView *)imgView flagHidden:(BOOL)flag
{
    
    imgView.hidden = flag;
    
}
-(void)plotButtonTapped:(UIButton *)sender
{
    
    
    if (plotPullableView ==nil) {
        
        plotPullableView = [[StyledPullableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
        
        plotPullableView.openedCenter = CGPointMake(ScreenWidth/2,ScreenHeight/2);
        plotPullableView.closedCenter = CGPointMake(ScreenWidth/2,-ScreenHeight/2);
        plotPullableView.delegate = self;
        plotPullableView.center = plotPullableView.closedCenter;
        plotPullableView.backgroundColor = [UIColor clearColor];
      
    }
    [self.view bringSubviewToFront:plotPullableView];
    if (plotPullableView.opened) {
        [self hideShowCarrotImgView:plotCarrotImgView flagHidden:YES];
        [plotPullableView setOpened:NO animated:YES];
    }
    else
    {
        [self hideShowCarrotImgView:plotCarrotImgView flagHidden:NO];
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
        TPPPlotDropDownViewController *plotViewCntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"plotDropDown"];
        plotViewCntrl.delegate = (TPPHomeViewController *) [TPPUtilities getCurrentViewController];
        plotViewCntrl.view.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight-165);
        plotViewCntrl.view.translatesAutoresizingMaskIntoConstraints = YES;
        plotViewCntrl.view.backgroundColor = ColorFromRGB(0x131010);
        plotViewCntrl.view.backgroundColor = [UIColor blackColor];
        [self.view addSubview:plotPullableView];
        [plotPullableView addSubview:plotViewCntrl.view];
        [self addChildViewController:plotViewCntrl];
        [plotViewCntrl didMoveToParentViewController:self];
         if (notificationPullableView.opened) {
             [plotPullableView setOpened:YES animated:NO];
             [notificationPullableView setOpened:NO animated:NO];
         }
         else{
         [plotPullableView setOpened:YES animated:YES];
         }
        
        
    }
    [_dropDownView hideView];
    [searchViewCntrl hideView];
    
}

- (void)hideAllPullableView
{
     [plotPullableView setOpened:NO animated:NO];
     [notificationPullableView setOpened:NO animated:NO];
     [self hideShowCarrotImgView:notifyCarrotImgView flagHidden:YES];
     [self hideShowCarrotImgView:plotCarrotImgView flagHidden:YES];
     [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
    
}

-(void)exploreButtonTapped:(UIBarButtonItem *)sender
{
    [_dropDownView hideView];
    [searchViewCntrl hideView];
    [self hideAllPullableView];
    
    TPPExploreViewController *exploreView = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPExploreViewController"];
    [self.navigationController pushViewController:exploreView animated:YES];
    
    
    
}

-(void)searchButtonTapped:(UIBarButtonItem *)sender
{
    [self hideAllPullableView];
    [_dropDownView hideView];
    if (searchViewCntrl == nil) {
        searchViewCntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPSearchViewController"];
        searchViewCntrl.isHidden = YES;
		searchViewCntrl.delegate =(TPPHomeViewController *) [TPPUtilities getCurrentViewController];
        }
   
    [searchViewCntrl toggleViewOnViewController:self];
    if (searchViewCntrl.isHidden) {
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
    }
    else
    {
    [self hideShowCarrotImgView:searchCarrotImgView flagHidden:NO];
    }
}

-(void)moreButtonTapped:(UIBarButtonItem *)sender
{
    [searchViewCntrl hideView];
    [self hideAllPullableView];
    [_dropDownView toggleView];
}



-(void)addLeftMenuButton
{
    
    UIImage *image = [UIImage imageNamed:@"btn_menu_normal"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(0, 0, image.size.width,  image.size.height);
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_menu_pressed"] forState:UIControlStateHighlighted];


    
    
    [button addTarget:self action:@selector(leftMenuButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftMenuButton = [[UIBarButtonItem alloc]initWithCustomView:button];
    
    self.navigationItem.leftBarButtonItem = leftMenuButton;
    self.navigationController.navigationBar.hidden = NO;
}

-(void)leftMenuButtonTapped:(id)sender
{
    [self toggleLeftSide];
}




-(NSDictionary *)getDummyTripFeeds
{
    ///fetch dummy data of food items
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"TripFeedsJson" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return jsonDictionary;
}



//#define mark -  DropDown View Delegate
-(void)hideDropDownView
{
    [plotPullableView setOpened:NO animated:NO];
    [notificationPullableView setOpened:NO animated:NO];
}


#define mark - Pullable View Delegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void)pullableView:(PullableView *)pView didChangeState:(BOOL)opened {
    
    if (pView == plotPullableView)
    {
        
        if (opened)
        {
            [pView addSubview:pView.transparentView];
            pView.transparentView.hidden = NO;
          
            
        } else {
           [self hideShowCarrotImgView:plotCarrotImgView flagHidden:YES];
        }
    }
    else  if (pView == notificationPullableView)
    {
        if (opened)
        {
            [pView addSubview:pView.transparentView];
            pView.transparentView.hidden = NO;
        }
        else
        {
          [self hideShowCarrotImgView:notifyCarrotImgView flagHidden:YES];
        }
        [TPPNotificationEventHandler sharedInstance].notificationCount = 0;
        [TPPUtilities updateNotificationBadgeWithValue:0];
    }
    
    
}


#pragma mark- BL Methods

-(void)socialLoginWithCredentials:(NSDictionary *)credentialsDictionary
{
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:SocilaLogInUrl] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
             [TPPUtilities saveUserProfile: response[@"response"][@"user"]];
             [TPPUtilities saveUserToken: response[@"response"][@"token"]];
             
             [self navigateToHomeViewController];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage) {
         
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
     }];
}

-(void)navigateToHomeViewController
{
    TPPHomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPHomeViewController"];
    
    [self.menuContainerViewController.centerViewController pushViewController:homeVC animated:YES];
}



@end

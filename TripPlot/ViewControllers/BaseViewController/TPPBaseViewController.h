//
//  TPPBaseViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StyledPullableView.h"
#import "TPPPlotDropDownViewController.h"
#import "TPPNotificationDropDownViewController.h"


@interface TPPBaseViewController : UIViewController<PullableViewDelegate,TPPDropDownViewDelegate>
{
    StyledPullableView *plotPullableView;
    StyledPullableView *notificationPullableView;
    StyledPullableView *searchPullableView;
    
    
}

@property TPPDropDownView *dropDownView;




-(void)addLeftMenuButton;
-(void)addRightMenuButtons;

-(void)socialLoginWithCredentials:(NSDictionary *)credentialsDictionary;
-(void)navigateToHomeViewController;

-(NSDictionary *)getDummyTripFeeds;




@end

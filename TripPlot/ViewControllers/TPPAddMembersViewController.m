//
//  TPPAddMembersViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 05/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPAddMembersViewController.h"
#import "TPPEmailListingViewController.h"
#import "TPPContactViewController.h"
#import "TPPPageMenuBaseViewController.h"



@interface TPPAddMembersViewController ()

{
    TPPCreateTripModel *newTripObject;
}
@end

@implementation TPPAddMembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    newTripObject = [TPPCreateTripModel sharedInstance];
    TPPTripMenuStepperView *menuStepper = [[[NSBundle mainBundle] loadNibNamed:@"TPPTripMenuStepperView" owner:self options:nil] objectAtIndex:0];
    CGRect frame = self.menuView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [menuStepper setFrame:frame];
    
    [self.menuView addSubview:menuStepper];
    [menuStepper setSelectedIndex:3 isNew:newTripObject.isNew];
    
    [self.groupButton addTarget:self action:@selector(travellingModeTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.soloButton addTarget:self action:@selector(travellingModeTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.groupButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateHighlighted];
    [self.groupButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateSelected];
    [self.soloButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateHighlighted];
    [self.soloButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateSelected];
    [self.openToOtherNoButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateHighlighted];
    [self.openToOtherNoButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateSelected];
    [self.openToOtherYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateHighlighted];
    [self.openToOtherYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateSelected];
    [self.inviteFriendYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateHighlighted];
    [self.inviteFriendYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed"] forState:UIControlStateSelected];
    [self.inviteFriendNoButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateHighlighted];
    [self.inviteFriendNoButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_pressed_right"] forState:UIControlStateSelected];
    
    [self.openToOtherNoButton addTarget:self action:@selector(openToOthersTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.openToOtherYesButton addTarget:self action:@selector(openToOthersTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.inviteFriendNoButton addTarget:self action:@selector(inviteFriendTaaped:) forControlEvents:UIControlEventTouchUpInside];
    [self.inviteFriendYesButton addTarget:self action:@selector(inviteFriendTaaped:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.addFriendButton setBackgroundImage:[UIImage imageNamed:@"btn_addfriend_pressed"] forState:UIControlStateSelected];
    
    
    // Do any additional setup after loading the view.
    
    [self.cancelButton setImage:[UIImage imageNamed:@"btn_Next_normal"] forState:UIControlStateNormal];
    [self.nextButton setImage:[UIImage imageNamed:@"btn_confirm"] forState:UIControlStateNormal];
    self.scrollView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    
    newTripObject.isGroup = YES;
    newTripObject.isOpen = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (newTripObject.addedEmailIDs == nil) {
        newTripObject.addedEmailIDs = [NSMutableArray new];
    }
    if (newTripObject.addedContacts == nil) {
        newTripObject.addedContacts = [NSMutableArray new];
    }
    
    if (![[newTripObject.addedEmailIDs lastObject] isEqualToString:@"+"])
    {
        [newTripObject.addedEmailIDs addObject:@"+"];
    }
    
    [_tagList setAutomaticResize:NO];
    _tagList.hideCloseIndicator = YES;
    
    NSMutableArray *temp= [NSMutableArray new];
    [temp addObjectsFromArray:newTripObject.addedContacts];
    [temp addObjectsFromArray:newTripObject.addedEmailIDs];
    
    [_tagList setTags:temp];
    [_tagList setTagDelegate:self];
    [_tagList setTagBackgroundColor:COLOR_ORANGE];
    [_tagList setTextColor:[UIColor whiteColor]];
    [_tagList bringSubviewToFront:self.addFriendButton];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)travellingModeTapped: (UIButton *) sender
{
    if (sender.tag == 1) {
        [self.groupButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_normal"] forState:UIControlStateNormal];
        [self.soloButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.soloButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.groupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        newTripObject.isGroup = YES;

		UIButton *tempButton = [[UIButton alloc]init];
		tempButton.tag = 1;
		[self inviteFriendTaaped:tempButton];
    }
    else
    {
        [self.soloButton setBackgroundImage:[UIImage imageNamed: @"btn_slider_normal_right"] forState:UIControlStateNormal];
        [self.groupButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.groupButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.soloButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        newTripObject.isGroup = NO;

		UIButton *tempButton = [[UIButton alloc]init];
		tempButton.tag = 0;
		[self inviteFriendTaaped:tempButton];
    }
}

-(void)openToOthersTapped:(UIButton *) sender
{

    if (sender.tag == 1) {
        [self.openToOtherYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_normal"] forState:UIControlStateNormal];
        [self.openToOtherNoButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.openToOtherYesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.openToOtherNoButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        newTripObject.isOpen = YES;
    }
    else
    {
        newTripObject.isOpen = NO;
        [self.openToOtherNoButton setBackgroundImage:[UIImage imageNamed: @"btn_slider_normal_right"] forState:UIControlStateNormal];
        [self.openToOtherYesButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.openToOtherYesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.openToOtherNoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }

}

-(void)inviteFriendTaaped:(UIButton *)sender
{
    if (sender.tag == 1) {
        [self.inviteFriendYesButton setBackgroundImage:[UIImage imageNamed:@"btn_slider_normal"] forState:UIControlStateNormal];
        [self.inviteFriendNoButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.inviteFriendNoButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.inviteFriendYesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [newTripObject.addedEmailIDs addObject:@"+"];
        [_tagList setTags:newTripObject.addedEmailIDs];
    }
    else
    {
        [self.inviteFriendNoButton setBackgroundImage:[UIImage imageNamed: @"btn_slider_normal_right"] forState:UIControlStateNormal];
        [self.inviteFriendYesButton setBackgroundImage:[UIImage new] forState:UIControlStateNormal];
        [self.inviteFriendYesButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.inviteFriendNoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [newTripObject.addedEmailIDs removeAllObjects];
        [newTripObject.addedContacts removeAllObjects];
        [_tagList setTags:newTripObject.addedEmailIDs];
    }
}
- (IBAction)addFriendButton:(id)sender {
    
    TPPEmailListingViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPEmailListingViewController"];
    
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)cancelButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)nextButtonTapped:(id)sender{
    
    //[newTripObject.addedContacts removeLastObject];
    
    
    [newTripObject.addedEmailIDs removeLastObject];
    newTripObject.tripLocations = [newTripObject.tripLocations toArray];
    newTripObject.tripMembers = [newTripObject convertContactsToJSONModel];
    NSDictionary *requestDictionary = [NSDictionary new];
    requestDictionary = [newTripObject toDictionary];
    [self callServiceToCreateTrip:requestDictionary];
    

}


-(void) callServiceToCreateTrip:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_FOR_CREATE_TRIP] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {

			//increase my trips count
			 TPPSharedTripCountsModel *tripCounts = [TPPSharedTripCountsModel sharedInstance];
			 tripCounts.myTripCount = tripCounts.myTripCount+1;

             if (newTripObject.createTripSource == CreateTripSource_Challenge) {
                 
                 [self callServiceToAcceptChallenge:newTripObject.tripModel];
                 return;
                 
             }
             else  if (newTripObject.createTripSource == CreateTripSource_BucketList) {
                 
                 [self callServiceToRemoveFromBucket:newTripObject.tripModel];
                 return;
                 
             }

             
             //searchLocation = response[@"response"][@"result"];
             newTripObject = nil;
             [TPPCreateTripModel clearInstance];
             [self popToSpecificViewController:[TPPHomeViewController class]];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"newTripCreatedNotification" object:nil];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];
     }];
}

-(void)callServiceToAcceptChallenge:(TPPTripModel *)tripModel
{
    [self.view showLoader];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           [NSNumber numberWithInteger:RequestOption_Accept] ,@"acceptChallenge",
                                           tripModel.title ,@"tripTitle",
                                           tripModel.createdBy[@"_id"] ,@"createdById",
                                           tripModel.createdBy[@"firstname"] ,@"createdByFirstName",
                                           tripModel.challenge[@"challengeId"],@"challangeInfo",
                                           tripModel.challenge[@"challengeFrom"][@"_id"],@"challengeFromId",
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:AcceptChallengeUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
        
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200) //TPPPageMenuBaseViewController
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateChallengeView" object:self];
             [self popToSpecificViewController:[TPPPageMenuBaseViewController class]];

           
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
     }];
    
}

-(void)callServiceToRemoveFromBucket:(TPPTripModel *)tripModel
{
    [self.view showLoader];
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id,@"tripId",
                                        nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:RemoveBucketUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveFromBucket" object:self];
             [self popToSpecificViewController:[TPPPageMenuBaseViewController class]];
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
     }];
    
}



#pragma -mark DWTagList delegate
- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex forTagList:(DWTagList *)tagList
{
    NSLog(@"tag list :  %ld", (long)tagList.tag);
    
    TPPContactViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
    [newTripObject.addedEmailIDs removeLastObject];
    VC.shouldEnableSelection = YES;
    VC.shouldEnableInvite = NO;
    
    if ([tagName isValidEmail]) {
        
        VC.shouldShowMailListing = YES;
        [self.navigationController pushViewController:VC animated:YES];
    }
    else
    {
        VC.shouldShowMailListing = NO;
        [self.navigationController pushViewController:VC animated:YES];
    }
}

@end

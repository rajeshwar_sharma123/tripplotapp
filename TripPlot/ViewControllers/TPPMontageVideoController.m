//
//  TPPMontageVideoController.m
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPMontageVideoController.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation TPPMontageVideoController
{
    TPPCreateTripModel *pastTripObject;
    MPMoviePlayerController *moviePlayerController;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.doneBtn setImage:[UIImage imageNamed:@"btn_confirm"] forState:UIControlStateNormal];
    [self.cancelBtn setImage:[UIImage imageNamed:@"btn_Next_normal"] forState:UIControlStateNormal];
     pastTripObject = [TPPCreateTripModel sharedInstance];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


    NSURL *fileURL = [NSURL URLWithString:pastTripObject.montage];
    moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    CGRect rect = CGRectMake(0, 0, self.videoContentView.frame.size.width, self.videoContentView.frame.size.height);
    [moviePlayerController.view setFrame:rect];
    [self.videoContentView addSubview:moviePlayerController.view];
    // moviePlayerController.fullscreen = YES;
    moviePlayerController.movieSourceType = MPMovieSourceTypeStreaming;
    [moviePlayerController play];
}

- (IBAction)cancelButtonTapped:(id)sender{

     [self.view removeFromSuperview];

}
- (IBAction)doneButtonTapped:(id)sender
{
    [pastTripObject.addedContacts removeLastObject];
    [pastTripObject.addedEmailIDs removeLastObject];
    pastTripObject.tripLocations = [pastTripObject.tripLocations toArray];
    pastTripObject.tripMembers = [pastTripObject convertContactsToJSONModel];
    NSDictionary *requestDictionary = [NSDictionary new];
    requestDictionary = [pastTripObject toDictionary];
    
    [self callServiceToCreateTrip:requestDictionary];
}

-(void) callServiceToCreateTrip:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_FOR_CREATE_TRIP] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];

		 //increase my trips count
		 TPPSharedTripCountsModel *tripCounts = [TPPSharedTripCountsModel sharedInstance];
		 tripCounts.myTripCount = tripCounts.myTripCount+1;
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
			 [TPPCreateTripModel clearInstance];
             //searchLocation = response[@"response"][@"result"];
             pastTripObject = [TPPCreateTripModel new];
             [self popToSpecificViewController:[TPPHomeViewController class]];
             
              [[NSNotificationCenter defaultCenter] postNotificationName:@"newTripCreatedNotification" object:nil];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];
     }];
}

@end

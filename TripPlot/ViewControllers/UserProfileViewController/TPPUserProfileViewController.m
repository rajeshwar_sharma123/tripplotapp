//
//  TPPUserProfileViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/8/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPUserProfileViewController.h"
#import "ANTagsView.h"
#import "TPPRecommendedDestinationsViewController.h"


@interface TPPUserProfileViewController ()<ANTagsViewProtocol>
{
    ANTagsView *tagsDomesticView;
    ANTagsView *tagsForeignView;
    
    __weak IBOutlet NSLayoutConstraint *contentViewHeightConstrainedConstant;
    __weak IBOutlet NSLayoutConstraint *domesticTagViewHeightConstraintsConstant;
    __weak IBOutlet NSLayoutConstraint *foreignTagViewHeightConstraintsConstant;
    __weak IBOutlet UIView *placeVistedView;
    __weak IBOutlet UIView *domesticView;
    __weak IBOutlet UIView *foreignView;
    __weak IBOutlet UIView *domesticTagView;
    __weak IBOutlet UIView *foreignTagView;
    __weak IBOutlet UILabel *lblForeign;
    __weak IBOutlet UILabel *lblDomestic;
    __weak IBOutlet UILabel *lblUserName;
    __weak IBOutlet UILabel *lblUserAddress;
    __weak IBOutlet UILabel *lblTotalTrips;
    __weak IBOutlet UILabel *lblTotalKM;
    
    
    __weak IBOutlet UIImageView *userImgView;
    
    NSDictionary *userInfoDic;
    UIView *dotMenuBaseView;
    TPPUserProfileModel *userProfileModel;
    NSDictionary *awsCredentials;
    
    NSArray *domesticTags;
    NSArray *foreignTags;
}
@end

@implementation TPPUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    placeVistedView.layer.cornerRadius = 3.0f;
    placeVistedView.clipsToBounds = YES;
    lblDomestic.hidden = YES;
    lblForeign.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"UpdateUserProfile" object:nil];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
    //userInfoDic = [UserDeafaults objectForKey:UserProfile];
    [self getUserTripInfoFromService];
    [self setUserInfo];
    [self getUserLocationsInfoFromService];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    domesticTagViewHeightConstraintsConstant.constant = tagsDomesticView.frame.origin.y+tagsDomesticView.frame.size.height;
    foreignTagViewHeightConstraintsConstant.constant = tagsForeignView.frame.origin.y+tagsForeignView.frame.size.height;
    contentViewHeightConstrainedConstant.constant = tagsDomesticView.frame.size.height+tagsForeignView.frame.size.height+280;
    [self.mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, foreignTagView.frame.origin.y+foreignTagView.frame.size.height+80)];
   [userImgView applyLightBlackTint];
    
}
- (void)receiveNotification:(NSNotification *) notification
{
        if ([[notification name] isEqualToString:@"UpdateUserProfile"])
        {
            [self viewWillAppear:NO];
        }
}


#pragma mark- Custom Method

- (void)setUserInfo
{
    //[self setUserProfilePick];
 
    NSURL *imgurl = [NSURL URLWithString:userProfileModel.profileImage];
    
    [userImgView sd_setImageWithURL:imgurl placeholderImage:[UIImage imageNamed:@"icn_defaultuser"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
    {

    }];
    
        
    lblUserName.text = [NSString stringWithFormat:@"%@ %@",userProfileModel.firstname,userProfileModel.lastname];
    lblUserAddress.text = userProfileModel.locations;
}

- (void)setDomesticTagViewDetail:(NSDictionary *)response
{

    lblDomestic.layer.cornerRadius = lblDomestic.frame.size.width/2;
    lblDomestic.hidden = NO;
    lblDomestic.clipsToBounds = YES;
    lblDomestic.text = [NSString stringWithFormat:@"%@",[response objectForKey:@"count"]];
    NSArray *tagsToDisplay = [NSArray arrayWithArray:[[response objectForKey:@"value"] valueForKey:@"name"]];
    domesticTags = tagsToDisplay;
    
    tagsDomesticView = [[ANTagsView alloc] initWithTags:tagsToDisplay frame:CGRectMake(30, -10, ScreenWidth-40, 10)];
    [tagsDomesticView setTagCornerRadius:3];
    tagsDomesticView.tag = 1;
    tagsDomesticView.delegate = self;
    [tagsDomesticView setTagBackgroundColor:ColorFromRGB(0xB0F464)];
    [tagsDomesticView setTagTextColor:[UIColor blackColor]];
    [tagsDomesticView setBackgroundColor:[UIColor whiteColor]];
    [tagsDomesticView setFrameWidth:ScreenWidth-80];
    [domesticTagView addSubview:tagsDomesticView];
}

- (void)setForeignTagViewDetail:(NSDictionary *)response
{
    lblForeign.layer.cornerRadius = lblDomestic.frame.size.width/2;
    lblForeign.hidden = NO;
    lblForeign.clipsToBounds = YES;
    lblForeign.text = [NSString stringWithFormat:@"%@",[response objectForKey:@"count"]];
    NSArray *tagsToDisplay = [NSArray arrayWithArray:[[response objectForKey:@"value"] valueForKey:@"name"]];
    foreignTags = tagsToDisplay;
    
       tagsForeignView = [[ANTagsView alloc] initWithTags:tagsToDisplay frame:CGRectMake(30, -10, ScreenWidth-40, 10)];
    [tagsForeignView setTagCornerRadius:3];
    tagsForeignView.tag = 2;
    tagsForeignView.delegate = self;
    [tagsForeignView setTagBackgroundColor:ColorFromRGB(0xB0F464)];
    [tagsForeignView setTagTextColor:[UIColor blackColor]];
    [tagsForeignView setBackgroundColor:[UIColor whiteColor]];
    [tagsForeignView setFrameWidth:ScreenWidth-80];
    [foreignTagView addSubview:tagsForeignView];


}
- (IBAction)dotMenuBtnAction:(id)sender {
   
    if (dotMenuBaseView == nil) {
        dotMenuBaseView = [[UIView alloc]initWithFrame:self.view.bounds];
        dotMenuBaseView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapDotMenuBaseView:)];
        [dotMenuBaseView addGestureRecognizer:singleFingerTap];
        
        UIView *subView = [[UIView alloc]initWithFrame:CGRectMake(dotMenuBaseView.frame.size.width-130.0f, userImgView.frame.size.height, 130.0f, 70.0f)];
        subView.backgroundColor = [UIColor grayColor];
        subView.layer.cornerRadius = 3.0f;
        subView.clipsToBounds = YES;
        [dotMenuBaseView addSubview:subView];
        
        UIButton *btnAccount = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAccount addTarget:self action:@selector(btnUpdateProfileAction:) forControlEvents:UIControlEventTouchUpInside];
        btnAccount.tag = 1;
        btnAccount.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [btnAccount setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnAccount.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btnAccount.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [btnAccount setTitle:@"Account" forState:UIControlStateNormal];
        btnAccount.frame = CGRectMake(0.0f, 0.0f, subView.frame.size.width, subView.frame.size.height/2);
        btnAccount.backgroundColor = [UIColor clearColor];
        [subView addSubview:btnAccount];
        
        UIButton *btnProfileImg = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProfileImg addTarget:self  action:@selector(btnUpdateProfileAction:) forControlEvents:UIControlEventTouchUpInside];
        btnProfileImg.tag = 2;
        btnProfileImg.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [btnProfileImg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnProfileImg.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btnProfileImg.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [btnProfileImg setTitle:@"Profile image" forState:UIControlStateNormal];
        btnProfileImg.frame = CGRectMake(btnAccount.frame.origin.x,btnAccount.frame.size.height, btnAccount.frame.size.width, btnAccount.frame.size.height);
         btnProfileImg.backgroundColor = [UIColor clearColor];
        [subView addSubview:btnProfileImg];
        
    }
     [self.view addSubview:dotMenuBaseView];
    
}
- (void)handleSingleTapDotMenuBaseView:(UITapGestureRecognizer *)recognizer {
   
    [dotMenuBaseView removeFromSuperview];
  
}
- (void)btnUpdateProfileAction:(id)sender
{
    [dotMenuBaseView removeFromSuperview];
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 1) {
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"TPPEditUserProfileViewController"] animated:YES];
        
    }
    else if (btn.tag == 2) {
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Profile Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Camera",
                                @"Gallery",
                                nil];
        popup.tag = 1;
        [popup showInView:self.view];
    }

   
}
#pragma mark- UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                    {
                        UIImagePickerController *imagePickerCamera = [[UIImagePickerController alloc] init];
                        [imagePickerCamera setSourceType:UIImagePickerControllerSourceTypeCamera];
                        [imagePickerCamera setDelegate:self];
                        [self presentViewController:imagePickerCamera animated:YES completion:nil];
                    }
                    else
                    {
                        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your device doesn't have a camera." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    }
                    break;
                case 1:
                {
                    UIImagePickerController *imagePickerLibrary = [[UIImagePickerController alloc] init];
                    imagePickerLibrary.delegate = self;
                    [self presentViewController:imagePickerLibrary animated:YES completion:nil];
                    
                }
                    break;
                    
                  default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
#pragma mark- UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    if(image != nil){
       ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:image];
        controller.delegate = self;
        controller.blurredBackground = YES;
        [[self navigationController] pushViewController:controller animated:NO];
    }
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark- ImageCropViewControllerDelegate

- (void)ImageCropViewControllerSuccess:(UIViewController* )controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [self callServiceToGetAWSCredentials:croppedImage];
    [[self navigationController] popViewControllerAnimated:NO];
    
    
}


- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
    //imageView.image = image;
    [[self navigationController] popViewControllerAnimated:NO];
}

-(void)callServiceToGetAWSCredentials:(UIImage *)image
{
    [self.view showLoader];
    NSDictionary *requestData = @{@"token":[UserDeafaults objectForKey:Token]};
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_AWS_CREDENTIALS_URL] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
              awsCredentials = response[@"response"];
              NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".jpg"];
              [self callAWSServiceToupload:[NSData getCompressedImageDataFromImage:image width:800 height:600] withFileName:fileName];
          
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
        [self.view hideLoader]; 
         
     }];
}



-(void) callAWSServiceToupload:(NSData *)imageData withFileName:(NSString *)fileName
{
    
  
    
    fileName = [NSString stringWithFormat:@"%@/%@",awsCredentials[@"folderDetails"][@"userFolder"],fileName];
    
    //AWS configuration
 
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:[awsCredentials[@"regionType"] regionTypeEnumFromString] identityPoolId:awsCredentials[@"poolId"]];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:[awsCredentials[@"region"] regionTypeEnumFromString] credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    
    NSString *stringURL = [NSString stringWithFormat:@"https://%@.%@/%@",awsCredentials[@"bucketName"],awsCredentials[@"endPoint"],fileName];

    NSLog(@"amazon url...%@",stringURL);
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    expression.uploadProgress = ^(AWSS3TransferUtilityTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Update a progress bar.
            
            AWSS3TransferUtilityTask *task1 = task;
            NSLog(@"task.. %@",task1.key);
            NSLog(@"byte sent... %lld",bytesSent);
            NSLog(@"total byte sent... %lld",totalBytesSent);
            NSLog(@"total byte expected to sent... %lld",totalBytesExpectedToSend);
            
           
            
            
            
        });
    };
    
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Alert a user for transfer completion.
            // On failed uploads, `error` contains the error object.
            NSLog(@"task.. completed %@",task.key);
            [userImgView setImage:[UIImage imageWithData:imageData]];
            [self callServiceToSaveUserProfileImgUrl:stringURL];
           [self.view showToasterWithMessage:@"Photos uploaded successfully..."];
            
            
            
        });
    };
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    [[transferUtility uploadData:imageData
                          bucket:awsCredentials[@"bucketName"]
                             key:fileName
                     contentType:@"image/jpg"
                      expression:expression
                completionHander:completionHandler] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
        }
        if (task.exception) {
            NSLog(@"Exception: %@", task.exception);
        }
        if (task.result) {
            AWSS3TransferUtilityUploadTask *uploadTask = task.result;
            
            // Do something with uploadTask.
        }
        
        return nil;
    }];
}

- (void)callServiceToSaveUserProfileImgUrl:(NSString *)userProfileImgUrl
{



NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                      [UserDeafaults objectForKey:Token],@"token",
                      userProfileModel._id,@"_id",
                      userProfileImgUrl,@"profileImage",
                      
                      nil];

[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:SaveUserProfileUrl] data:dict withSuccessBlock:^(id response, NSDictionary *headers)
 {
     [self.view hideLoader];
     
     
     if ([[response objectForKey:@"code"] integerValue] == 200)
     {
         NSMutableDictionary *tmpDic = [[UserDeafaults objectForKey:UserProfile] mutableCopy];
         [tmpDic setObject:userProfileImgUrl forKey:@"profileImage"];
         [TPPUtilities saveUserProfile:[NSDictionary dictionaryWithDictionary:tmpDic]];
         userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
         //[self setUserInfo];
         
         
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserProfile" object:self];
//         [self.navigationController popViewControllerAnimated:YES];
         
     }
     else
     {
         [self showErrorMessage: response[@"message"]];
     }
     
 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage){
     
     [self.view hideLoader];
     [self showErrorMessage:errorMessage];
 }];
}

#pragma mark - ANTagsViewDelegate

- (void)clickedANTagsView:(ANTagsView *)antObj tagIndex:(int)index
{
    NSString *locationString;
    if(antObj.tag ==1) // domestic tags
    {
       locationString = domesticTags[index];
    }
    else if(antObj.tag ==2)
    {
        locationString = foreignTags[index];
    }
    
    
    TPPRecommendedDestinationsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedDestinationsViewController"];
    vc.locationString = locationString;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)getUserTripInfoFromService
{
   
    NSDictionary *emptyDic = [NSDictionary dictionary];
    
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlForGetRequest:UserTripInfoUrl _id:userProfileModel._id] data:emptyDic withSuccessBlock:^(id response, NSDictionary *headers) {
        
        if ([[response objectForKey:@"code"] integerValue] == 200)
                     {
                         NSDictionary *dic = [[[response objectForKey:@"response"]objectForKey:@"result"] firstObject];
                         if (dic == nil)
                         {
                             lblTotalTrips.text = @"0";
                             lblTotalKM.text = @"0";
                             
                         }else{
                             
                             lblTotalTrips.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"totalTrips"]];
                             NSInteger totalKM = [[dic objectForKey:@"totalKM"] integerValue]/1000;
                             
                             lblTotalKM.text = [NSString stringWithFormat:@"%ld",totalKM];
                         }
                        
                         
                     }
                     else
                     {
                         
                     }
        
    } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage) {
           [self showErrorMessage:errorMessage];
    }];
 
    
}
- (void)getUserLocationsInfoFromService
{
    
    NSDictionary *emptyDic = [NSDictionary dictionary];
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlForGetRequest:UserLocationInfoUrl _id:userProfileModel._id] data:emptyDic withSuccessBlock:^(id response, NSDictionary *headers) {
        
        if ([[response objectForKey:@"code"] integerValue] == 200)
        {
            [self setDomesticTagViewDetail:[[response objectForKey:@"response"] lastObject]];
            [self setForeignTagViewDetail:[[response objectForKey:@"response"] firstObject]];
            
        }
        else
        {
            
        }
        
    } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage) {
        
         [self showErrorMessage:errorMessage];
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

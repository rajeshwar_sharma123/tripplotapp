//
//  TPPUserProfileViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/8/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageCropView.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AWSCore/AWSClientContext.h>
#import <AWSS3/AWSS3TransferUtility.h>
@interface TPPUserProfileViewController : TPPBaseViewController<UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,ImageCropViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIView *profileView;


@end

//
//  TPPEditUserProfileViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/12/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPEditUserProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPPTextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet TPPTextField *txtFieldContact;
@property (weak, nonatomic) IBOutlet TPPTextField *txtFieldFirstName;
@property (weak, nonatomic) IBOutlet TPPTextField *txtFieldLastName;
@property (weak, nonatomic) IBOutlet TPPTextField *txtFieldLocation;

@end

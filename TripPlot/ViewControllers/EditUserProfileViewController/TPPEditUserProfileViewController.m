//
//  TPPEditUserProfileViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/12/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPEditUserProfileViewController.h"

@interface TPPEditUserProfileViewController ()
{
   NSDictionary *userInfoDic;
   TPPUserProfileModel *userProfileModel;
}
@end

@implementation TPPEditUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
    // userInfoDic = [UserDeafaults objectForKey:UserProfile];
    _txtFieldEmail.text = userProfileModel.emailid;
    _txtFieldContact.text = userProfileModel.contact;
    _txtFieldFirstName.text = userProfileModel.firstname;
    _txtFieldLastName.text = userProfileModel.lastname;
    _txtFieldLocation.text = userProfileModel.locations;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custom Method

- (IBAction)saveBtnAction:(id)sender {
    if ([self isUserInputsValid])
    {
        [self.view showLoader];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [UserDeafaults objectForKey:Token],@"token",
                               userProfileModel._id,@"_id",
                              _txtFieldContact.text.trimmedString,@"contact",
                              _txtFieldFirstName.text.trimmedString,@"firstname",
                              _txtFieldLastName.text.trimmedString,@"lastname",
                              _txtFieldLocation.text.trimmedString,@"locations",
                              nil];
        
        [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:SaveUserProfileUrl] data:dict withSuccessBlock:^(id response, NSDictionary *headers)
         {
             [self.view hideLoader];
             
             
             if ([[response objectForKey:@"code"] integerValue] == 200)
             {
                 NSMutableDictionary *tmpDic = [[UserDeafaults objectForKey:UserProfile] mutableCopy];
                 [tmpDic setObject:_txtFieldContact.text forKey:@"contact"];
                 [tmpDic setObject:_txtFieldFirstName.text forKey:@"firstname"];
                 [tmpDic setObject:_txtFieldLastName.text forKey:@"lastname"];
                 [tmpDic setObject:_txtFieldLocation.text forKey:@"locations"];
                 [TPPUtilities saveUserProfile:[NSDictionary dictionaryWithDictionary:tmpDic]];
                 //[self showErrorMessage: @"Profile update successfully"];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserProfile" object:self];
                 [self.navigationController popViewControllerAnimated:YES];
                 
             }
             else
             {
                 [self showErrorMessage: response[@"message"]];
             }
             
         } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage){
             
             [self.view hideLoader];
             [self showErrorMessage:errorMessage];
         }];
    }

}
#pragma mark - BL Methods

-(BOOL)isUserInputsValid
{
    if(self.txtFieldFirstName.text.trimmedString.length == 0)
    {
        [self showErrorMessage:@"Required First name"];
        return NO;
    }
    
    
    return YES;
}
-(void)showErrorMessage:(NSString *)message
{
    [CSNotificationView showInViewController:self style:CSNotificationViewStyleError message:message];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

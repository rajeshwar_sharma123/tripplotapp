//
//  TPPRouteDirectionServices.m
//  TripPlot
//
//  Created by Daffolap-21 on 04/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPRouteDirectionServices.h"

#define kDirectionsURL @"https://maps.googleapis.com/maps/api/directions/json?"
#define kPlacesURL @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
#define kDistanceURL @"https://maps.googleapis.com/maps/api/distancematrix/json?"
@implementation TPPRouteDirectionServices


+ (void)getRoutesWithLocations:(NSArray *)locations andCompletitionBlock:(void (^)(id routes, NSError *error))completitionBlock
{
    [self getRoutesWithLocations:locations travelMode:TravelModeDriving andCompletitionBlock:completitionBlock];
}


+ (void)getRoutesWithLocations:(NSArray *)locations travelMode:(TravelMode)travelMode andCompletitionBlock:(void (^)(id routes, NSError *error))completitionBlock
{
    NSUInteger locationsCount = [locations count];

    if (locationsCount < 2) return;


    NSMutableArray *locationStrings = [NSMutableArray new];

    for (CLLocation *location in locations)
    {
        [locationStrings addObject:[[NSString alloc] initWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude]];
    }

    NSString *sensor = @"true";
    NSString *origin = [locationStrings objectAtIndex:0];
    NSString *destination = [locationStrings lastObject];
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@origin=%@&destination=%@&alternatives=true&sensor=%@", kDirectionsURL, origin, destination, sensor];

    if (locationsCount > 2)
    {
        if (locationsCount > 4) {
            [url appendString:@"&waypoints=optimize:true"];
        }
        else
        {
            [url appendString:@"&waypoints=optimize:false"];
        }

        for (int i = 1; i < [locationStrings count] - 1; i++)
        {
            [url appendFormat:@"|%@", [locationStrings objectAtIndex:i]];
        }
    }

    switch (travelMode)
    {
        case TravelModeWalking:
            [url appendString:@"&mode=walking"];
            break;
        case TravelModeBicycling:
            [url appendString:@"&mode=bicycling"];
            break;
        case TravelModeTransit:
            [url appendString:@"&mode=driving"];
            break;
        default:
            [url appendString:@"&mode=driving"];
            break;
    }
    

    url = [NSMutableString stringWithString:[url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];

    [manager GET:url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             NSArray *routesArray = responseObject[@"routes"];
             NSMutableArray *parsedRoutes = [NSMutableArray new];

             for (NSInteger atIndex = 0; atIndex <routesArray.count; atIndex++ ) {

                 NSMutableDictionary *route = [NSMutableDictionary new];
                 NSMutableArray *polyline =[NSMutableArray new];
                 NSDictionary *routeDict = [routesArray objectAtIndex:atIndex];
                 route[@"summary"] =  routeDict[@"summary"];
                 route[@"overviewPolyline"] = routeDict[@"overview_polyline"][@"points"];
                 NSArray *legs = [routeDict objectForKey:@"legs"];


                 for (NSInteger leg = 0; leg < legs.count; leg++) {

                     NSDictionary *legDict = [legs objectAtIndex:leg];
                     if (leg == 0) {
                         route[@"distance"] = legDict[@"distance"][@"value"];
                         route[@"duration"] = legDict[@"duration"][@"value"];
                     }

                     NSArray *steps = [legDict objectForKey:@"steps"];

                     for (NSInteger step = 0; step <steps.count; step++) {
                         NSDictionary *stepDict = [steps objectAtIndex:step];
                         NSDictionary *routeOverviewPolyline = [stepDict objectForKey:@"polyline"];
                         //  NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                         
                         [polyline addObject:routeOverviewPolyline];
                     }
                     
                 }
                 route[@"polylines"] =  polyline;
                 [parsedRoutes addObject:route];
             }
             completitionBlock(parsedRoutes,nil);

         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

             completitionBlock(nil,error);
         }];
}


+ (void)getPlacesWithLocations:(CLLocation *)location travelMode:(NSString *)placesType radius:(NSInteger )radius pagetoken:(NSString *)pagetoken andCompletitionBlock:(void (^)(id places, NSError *error))completitionBlock
{

    NSMutableString *url = [NSMutableString stringWithFormat:@"%@location=%f,%f&radius=%ld&key=%@", kPlacesURL, location.coordinate.latitude, location.coordinate.longitude,(long)radius,tripPlotBrowserKey];

    if (pagetoken )
    {
        [url appendString: [NSString stringWithFormat:@"&pagetoken=%@",pagetoken]];

    }
    [url appendString:[NSString stringWithFormat:@"&types=%@",placesType]];

    url = [NSMutableString stringWithString:[url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];

    [manager GET:url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             completitionBlock(responseObject,nil);

         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             completitionBlock(nil,error);
         }];
}

//origins=54.406505,18.67708&destinations=54.446251,18.570993&mode=driving&language=en-EN&sensor=false


+ (void)getDistanceWithLocations:(CLLocation *)source andDestination:(CLLocation *)destination andCompletitionBlock:(void (^)(id distance, NSError *error))completitionBlock
{
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&sensor=false", kDistanceURL, source.coordinate.latitude, source.coordinate.longitude, destination.coordinate.latitude, destination.coordinate.longitude];


    url = [NSMutableString stringWithString:[url stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];

    [manager GET:url
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             NSArray *rowsArray = [responseObject objectForKey:@"rows"];
             NSDictionary *distanceDict;
             if (rowsArray.count > 0) {
                 distanceDict = [rowsArray objectAtIndex:0];
                 distanceDict = [distanceDict[@"elements"] objectAtIndex:0];
                 distanceDict = distanceDict[@"distance"];
             }
             completitionBlock(distanceDict,nil);

         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

             completitionBlock(nil,error);
         }];
}

+(NSArray *) getAllDistanceBreakUp: (NSArray *) coordinates
{
	NSMutableArray *distanceBreakup = [NSMutableArray new];
	[distanceBreakup addObject:[NSNumber numberWithInteger: 0]];
	NSInteger totalDistance = 0;
	for (NSInteger atIndex = 1; atIndex< coordinates.count; atIndex++) {
		CLLocation *prevLoc = [coordinates objectAtIndex:atIndex-1];
		CLLocation *currLoc = [coordinates objectAtIndex:atIndex];
		NSInteger distanceInMeter = [prevLoc distanceFromLocation:currLoc];
		totalDistance = totalDistance + distanceInMeter;
		[distanceBreakup addObject:[NSNumber numberWithInteger: distanceInMeter]];
	}
	NSLog(@"%lu",(unsigned long)distanceBreakup.count);

	return [distanceBreakup copy];
}

+(CLLocation *) findNewCenterLocation:(long )radius and: (NSArray *)distanses withLocations:(NSArray *)coordinates
{
	for (NSInteger atIndex = 1; atIndex< distanses.count; atIndex++) {
		radius = radius - [[distanses objectAtIndex:atIndex] longValue];
		if ( radius<=0) {

			return [coordinates objectAtIndex:atIndex];
		}

	}

	return nil;
}


+ (NSMutableArray *) decodePolylineWithEncodedString:(NSString *)encodedString {
	const char *bytes = [encodedString UTF8String];
	NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
	NSUInteger idx = 0;

	NSMutableArray *coordinates = [NSMutableArray new];


	NSUInteger count = length / 4;
	CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
	NSUInteger coordIdx = 0;

	float latitude = 0;
	float longitude = 0;
	while (idx < length) {
		char byte = 0;
		int res = 0;
		char shift = 0;

		do {
			byte = bytes[idx++] - 63;
			res |= (byte & 0x1F) << shift;
			shift += 5;
		} while (byte >= 0x20);

		float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
		latitude += deltaLat;

		shift = 0;
		res = 0;

		do {
			byte = bytes[idx++] - 0x3F;
			res |= (byte & 0x1F) << shift;
			shift += 5;
		} while (byte >= 0x20);

		float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
		longitude += deltaLon;

		float finalLat = latitude * 1E-5;
		float finalLon = longitude * 1E-5;

		CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
		coords[coordIdx++] = coord;

		CLLocation *loc = [[CLLocation alloc] initWithLatitude:finalLat longitude:finalLon] ;
		[coordinates addObject:loc];

		if (coordIdx == count) {
			NSUInteger newCount = count + 10;
			coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
			count = newCount;
		}
	}
	
	return coordinates;
}

@end

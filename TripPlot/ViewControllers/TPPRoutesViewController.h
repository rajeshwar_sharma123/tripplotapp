//
//  TPPRoutesViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface TPPRoutesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)cancelButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
- (IBAction)nextButtonTapped:(id)sender;

@property(assign, nonatomic) UILabel *distanceLabel;

@end
//
//  TPPAddImagesViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>

@interface TPPAddImagesViewController : TPPBaseViewController<QBImagePickerControllerDelegate>
- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)doneButtonTapped:(id)sender;
- (IBAction)addImageButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImageView;


@end

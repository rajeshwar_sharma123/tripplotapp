//
//  TPPTripStartTimeCellTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPTripStartTimeCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *timeTextField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (retain, nonatomic) UIDatePicker *datePicker;


@end

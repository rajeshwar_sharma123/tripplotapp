//
//  TPPCompletedTripDetailViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 13/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPCompletedTripDetailViewController.h"
#import "TPPAcceptRejectTableViewCell.h"
#import "TPPMakeTripShareCell.h"
#import "TPPContactViewController.h"

@interface TPPCompletedTripDetailViewController ()

@end

@implementation TPPCompletedTripDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

        // To update the UI if user likes/Follows/Joins etc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userActivitySelector) name:UserActivityNotification object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    

    [self initViews];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [_moviePlayerController pause];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setMontageViewImagesFrameWithImagesArray:_tripModel.images];
    
}

#pragma mark- Init methods

-(void)initViews
{
    [self initShareTab];
    
    
    NSString *string = [NSString stringWithFormat:@"%@ has completed a trip",_tripModel.createdBy[@"firstname"]];
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    NSRange range = [string rangeOfString:@" "];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,range.location)];
    
    self.travellerNameLabel.attributedText = attributedString;
    
    
    self.titleLabel.text = _tripModel.title;
    self.likeCountLabel.text = [NSString stringWithFormat:@"%ld",(unsigned long)_tripModel.like.count];
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",_tripModel.tripLocations[0][@"name"],[_tripModel.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }
    
    NSString *dateString = _tripModel.tripDate[@"endDate"];
    self.timeLabel.text = dateString.timeAgoString;
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.clipsToBounds = YES;
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:_tripModel.createdBy[@"profileImage"]]
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {

         
     }];
    
    
    TPPScrollMenu *menu = (TPPScrollMenu *)[self.expandableView subViewOfKindOfClass:[TPPScrollMenu class]];
    if (!menu) {
        menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    }

    menu.tripModel = _tripModel;
    menu.backgroundColorForButtons = Color_CompletedTripCellButtonBackground;
    [self.expandableView addSubview:menu];
    [menu configure];
    
    _moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:_tripModel.montage]];
    
    [self configureCellForBottomView];

}

- (void)configureCellForBottomView
{
    if ([_strCheck isEqualToString:@"challenge"]) {
        
             
        TPPAcceptRejectTableViewCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPAcceptRejectTableViewCell" owner:self options:nil] objectAtIndex:0];
        
        [cell bindDataWithModel:_tripModel];
        cell.delegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0, ScreenWidth-16, _bottomView.frame.size.height);
        cell.bottomView.frame = cell.frame;
        cell.bottomView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = _selectedIndexPath;
        [self.bottomView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [self.bottomView addSubview:cell];
        
    }
    else if ([self.strCheck isEqualToString:@"bucket"])
    {
        TPPMakeTripShareCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPMakeTripShareCell" owner:self options:nil] objectAtIndex:0];
        
        [cell bindDataWithModel:_tripModel];
        cell.shareCellDelegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0, ScreenWidth-16, self.bottomView.frame.size.height);
        cell.bottomView.frame = cell.frame;
        cell.bottomView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = self.selectedIndexPath;
        [self.bottomView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [self.bottomView addSubview:cell];
    }



}



-(void)initShareTab
{
    if (_tripModel.isLikedByMe)
    {
        [self.likeButton setTitle:Txt_UnLike forState:UIControlStateNormal];
    }
    else
    {
        [self.likeButton setTitle:Txt_Like forState:UIControlStateNormal];
    }
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:_tripModel.createdBy[@"_id"]])
    {
        [self.goHereButton setTitle:Txt_ChallengeTrip forState:UIControlStateNormal];
        [self.goHereButton setImage:[UIImage imageNamed:@"icn_challenge"] forState:UIControlStateNormal];
    }
}




#pragma mark- IBAction methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playButtonTapped:(id)sender
{
    if (_tripModel.montage == nil || [_tripModel.montage.trimmedString isEqualToString:@""])
    {
        [self showErrorMessage:@"No video found"];
       
        return;
    }
    
    
    CGRect frame = _montageView.frame;
    _moviePlayerController.view.frame = frame;
    
    [_montageView.superview addSubview:_moviePlayerController.view];
    
    [_moviePlayerController play];
}


- (IBAction)shareButtonTapped:(id)sender
{
    [self defaultShareWithText:@"" image:self.tripView.snapshotImage url:nil];
}

- (IBAction)goHereButtonTapped:(id)sender
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:_tripModel.createdBy[@"_id"]] && (_tripModel.tripStatus==TripStatus_CompletedTrip))
    {
        // Navigate to Invite screen because button will be invite rather than Go here
        TPPContactViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
        TPPCreateTripModel *newTripObj = [TPPCreateTripModel sharedInstance];
        [newTripObj.addedEmailIDs removeAllObjects];
        [newTripObj.addedContacts removeAllObjects];
        
        VC.shouldEnableSelection = YES;
        VC.shouldEnableInvite = YES;
        VC.shouldChallange = YES;
        VC.trip = _tripModel;
        
        [self.navigationController pushViewController:VC animated:YES];
        
        return;
    }
    
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
    
    
    goHereView.tripModel = _tripModel;
    
    [self.view addSubview:goHereView];
}






- (IBAction)likeButtonTapped:(id)sender
{
    [TPPAppServices callServiceForLikeUnlikeTripWithTripModel:_tripModel button:sender indexPath:nil];
}

-(void)userActivitySelector
{
    [self viewWillAppear:NO];
}


-(void)setMontageViewImagesFrameWithImagesArray:(NSArray *)_images
{
    
    CGSize montageSize = _montageView.frame.size;
    UIImage *placeholderImage = [UIImage imageNamed:@"icn_placeholderImage"];
    if (_images.count >= 5)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]] placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]] placeholderImage:placeholderImage];
        [_image4 sd_setImageWithURL:[NSURL URLWithString:_images[3]] placeholderImage:placeholderImage];
        [_image5 sd_setImageWithURL:[NSURL URLWithString:_images[4]] placeholderImage:placeholderImage];
        
    }
    else if (_images.count == 4)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]] placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]] placeholderImage:placeholderImage];
        [_image4 sd_setImageWithURL:[NSURL URLWithString:_images[3]] placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width/2, montageSize.height/2);
        _image2.frame = CGRectMake(montageSize.width/2, 0, montageSize.width/2, montageSize.height/2);
        _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width/2, montageSize.height/2);
        _image4.frame = CGRectMake(montageSize.width/2, montageSize.height/2, montageSize.width/2, montageSize.height/2);
        
        _image5.frame = CGRectZero;
        
    }
    else if (_images.count == 3)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:placeholderImage];
        [_image2 sd_setImageWithURL:[NSURL URLWithString:_images[1]] placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[2]] placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width/2, montageSize.height/2);
        _image2.frame = CGRectMake(montageSize.width/2, 0, montageSize.width/2, montageSize.height/2);
        _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width, montageSize.height/2);
        
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;
    }
    else if (_images.count == 2)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]] placeholderImage:placeholderImage];
        [_image3 sd_setImageWithURL:[NSURL URLWithString:_images[1]] placeholderImage:placeholderImage];
        
        
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height/2);
        _image3.frame = CGRectMake(0, montageSize.height/2, montageSize.width, montageSize.height/2);
        
        _image2.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;
        
    }
    else if (_images.count == 1)
    {
        [_image1 sd_setImageWithURL:[NSURL URLWithString:_images[0]]];
        
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height);
        
        _image2.frame = CGRectZero;
        _image3.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;
        
    }
    else if (!_images || (_images.count == 0))
    {
        _image1.frame = CGRectMake(0, 0, montageSize.width, montageSize.height);
        
        _image2.frame = CGRectZero;
        _image3.frame = CGRectZero;
        _image4.frame = CGRectZero;
        _image5.frame = CGRectZero;
        
    }
}



@end

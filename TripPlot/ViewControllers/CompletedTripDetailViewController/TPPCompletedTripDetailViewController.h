//
//  TPPCompletedTripDetailViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 13/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPCompletedTripDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *travellerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDetailDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property (weak, nonatomic) IBOutlet UIView *tripView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;
@property (weak, nonatomic) IBOutlet UIImageView *image4;
@property (weak, nonatomic) IBOutlet UIImageView *image5;

@property (weak, nonatomic) IBOutlet UIView *montageView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *goHereButton;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property MPMoviePlayerController *moviePlayerController;

@property TPPTripModel *tripModel;

@property BOOL shouldShowChallengeFriendShareCell;
@property (strong, nonatomic) NSString *strCheck;

@property (weak, nonatomic) UIViewController *vcObj;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;

@end

//
//  TPPInformationViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPInformationViewController.h"
#import <Crashlytics/Crashlytics.h>

@interface TPPInformationViewController ()

@end

@implementation TPPInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   // [[Crashlytics sharedInstance] crash];
    
    self.title = @"Information";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

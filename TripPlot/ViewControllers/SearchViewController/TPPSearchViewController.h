//
//  TPPSearchViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/5/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSDropdown.h"





@interface TPPSearchViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,VSDropdownDelegate>
@property (nonatomic,strong) NSMutableArray *recommendedTripArray;
@property (nonatomic,strong) NSMutableArray *searchResultArray;
@property (nonatomic) BOOL isHidden;

@property(weak, nonatomic) id<TPPSearchDelegate>delegate;

-(void)hideView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(void)showViewOnViewController:(id)obj;
-(void)toggleViewOnViewController:(id)obj;
@end

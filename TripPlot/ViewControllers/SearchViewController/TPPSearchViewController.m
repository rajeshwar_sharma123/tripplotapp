//
//  TPPSearchViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/5/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPSearchViewController.h"
#import "TPPExploreViewController.h"
#import "TPPHomeViewController.h"
@interface TPPSearchViewController ()
{
    
    NSArray *typeListArry;
    NSMutableArray *typeListSelectedArry;
    TPPUserProfileModel *userProfileModel;
    VSDropdown *dropdown;
}
@property (weak, nonatomic) IBOutlet UITextField *searchTxtFld;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;

@end

@implementation TPPSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
    [self getTypeListFromService];
    typeListSelectedArry = [NSMutableArray array];
    
    dropdown = [[VSDropdown alloc]initWithDelegate:self];
    [dropdown setBackgroundColor:ColorFromRGB(0x39342F)];
    [dropdown setAdoptParentTheme:NO];
    [dropdown setShouldSortItems:NO];
    
    
    
}
#pragma mark - Custom Method

- (IBAction)btnActionClicked:(id)sender {
    
    
    if (_searchTxtFld.text.trimmedString.length==0&&typeListSelectedArry.count==0) {
        [self toggleViewOnViewController:self];
        return;
    }
        NSDictionary *dic = @{@"token":[UserDeafaults objectForKey:Token],
                              @"_id":userProfileModel._id,
                              @"tripTypes":[typeListSelectedArry valueForKey:@"_id"],
                              @"tripLocation":_searchTxtFld.text.trimmedString,
                              Txt_PageNumber:[NSNumber numberWithInteger:1],
                              Txt_PageSize:[NSNumber numberWithInteger:PageSize],
                              };

        [self.delegate didSearchButtonTapped:_searchTxtFld.text.trimmedString withSearchObject:dic];

		_searchTxtFld.text = @"";

		[self toggleViewOnViewController:self];

    
    
}

-(void)showDropDownForView:(UIView *)sender adContents:(NSArray *)contents multipleSelection:(BOOL)multipleSelection
{
    
    // [_dropdown setDrodownAnimation:rand()%2];
    
    [dropdown setAllowMultipleSelection:multipleSelection];
    [dropdown setupDropdownForView:sender];
    [dropdown setSeparatorColor:[UIColor whiteColor]];
    [dropdown reloadDropdownWithContents:contents];
    
}

#pragma mark -
#pragma mark - VSDropdown Delegate methods.
- (void)dropdown:(VSDropdown *)dropDown didChangeSelectionForValue:(NSString *)str atIndex:(NSUInteger)index selected:(BOOL)selected
{
    _searchTxtFld.text = str;
    [dropDown remove];
    
}

- (UIColor *)outlineColorForDropdown:(VSDropdown *)dropdown
{
    
    return [UIColor whiteColor];
    
}

- (CGFloat)outlineWidthForDropdown:(VSDropdown *)dropdown
{
    return 0.0;
}

- (CGFloat)cornerRadiusForDropdown:(VSDropdown *)dropdown
{
    return 3.0;
}

- (CGFloat)offsetForDropdown:(VSDropdown *)dropdown
{
    return 12.0;
}




#pragma mark - TableView Delegates and DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return typeListArry.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifire = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifire];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifire];
        
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


-(void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:1];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [imgView makeCircular];
    UILabel *lblName = (UILabel *)[cell viewWithTag:2];
    
    lblName.text =[typeListArry objectAtIndex:indexPath.row][@"name"];
    lblName.textColor = [UIColor whiteColor];
    if ([typeListSelectedArry containsObject:[typeListArry objectAtIndex:indexPath.row]])
    {
        lblName.textColor = ColorFromRGB(0xEA6D24);
    }
    [imgView sd_setImageWithURL:[typeListArry objectAtIndex:indexPath.row][@"image"] placeholderImage:[UIImage imageNamed:@"icn_placeholderImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    
        
    }];

}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if (section == 0) {
        UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)view;
        headerView.backgroundView.backgroundColor = ColorFromRGB(0x39342F);
        
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        [header.textLabel setTextColor:[UIColor whiteColor]];
        
    }
    
    
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Trip Plot recommended searches";
    }
    return @"";
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 45;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 40;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblName = (UILabel *)[cell viewWithTag:2];
    if(cell.selected == TRUE)
    {
        if ([_delegate isKindOfClass:[TPPExploreViewController class] ]) {
            
           
            lblName.textColor = ColorFromRGB(0xEA6D24);
            
            if (typeListSelectedArry.count == 1) {
                
               NSInteger lastSelectedIndex = [typeListArry indexOfObject: [typeListSelectedArry firstObject]];
               UITableViewCell *lastSelectedCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:lastSelectedIndex inSection:0]];
                lblName = (UILabel *)[lastSelectedCell viewWithTag:2];
                lblName.textColor = [UIColor whiteColor];
                if (lastSelectedIndex == indexPath.row) {
                    [typeListSelectedArry removeLastObject];
                    return;
                }
                
                [typeListSelectedArry replaceObjectAtIndex:0 withObject:[typeListArry objectAtIndex:indexPath.row]];
            }
            else{
            [typeListSelectedArry addObject:[typeListArry objectAtIndex:indexPath.row]];
            }
            
            return;
        }
        else if ([_delegate isKindOfClass:[TPPHomeViewController class] ]) {
            
          if ([typeListSelectedArry containsObject:[typeListArry objectAtIndex:indexPath.row]]) {
                lblName.textColor = [UIColor whiteColor];
                [typeListSelectedArry removeObject:[typeListArry objectAtIndex:indexPath.row]];
            }
            else{
            lblName.textColor = ColorFromRGB(0xEA6D24);
            [typeListSelectedArry addObject:[typeListArry objectAtIndex:indexPath.row]];
            }
        }
        
        
        
    }
    
    
}
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
//    if(!cell.selected == TRUE)
//    {
//        UILabel *lblName = (UILabel *)[cell viewWithTag:2];
//        lblName.textColor = [UIColor whiteColor];
//        [typeListSelectedArry  removeObject:[typeListArry objectAtIndex:indexPath.row]];
//    }
//    
//    
//}

#pragma mark - TextField Delegates


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([searchStr isEqualToString:@""]) {
        
    }
    
    NSMutableDictionary *searchRequest = [NSMutableDictionary new];
    searchRequest[@"token"] = [UserDeafaults objectForKey:Token];
    searchRequest[@"keySearch"] = searchStr;
    
    [self callServiceTogetSearchLocation:searchRequest];
    return YES;
}

-(void)hideView
{
    _isHidden = YES;
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}
-(void)showViewOnViewController:(id)obj;
{
    _isHidden = NO;
    UIViewController *vc = (UIViewController *)obj;
    [vc.view addSubview:self.view];
    [vc addChildViewController:self];
    [self didMoveToParentViewController:vc];
    
}
-(void)toggleViewOnViewController:(id)obj
{
    if (_isHidden) {
        [self showViewOnViewController:obj];
    }
    else{
        [self hideView];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Web Services Calling

- (void)getTypeListFromService
{
    
    [self.view showLoader];
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlWithToken:TypeListUrl] data:[NSDictionary dictionary] withSuccessBlock:^(id response, NSDictionary *headers) {
        
        [self.view hideLoader];
        if ([[response objectForKey:@"code"] integerValue] == 200)
        {
            typeListArry = response[@"response"][@"result"];
            [self.tableView reloadData];
            
            
        }
        else
        {
            
        }
        
    } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage) {
        [self.view hideLoader];
        [self showErrorMessage:errorMessage];
    }];
    
    
}
-(void) callServiceTogetSearchLocation:(NSMutableDictionary *)requestDictionary
{
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_SEARCH_LOCATION_URL] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [self showDropDownForView:_searchTxtFld adContents:[response[@"response"][@"result"] valueForKey:@"name"] multipleSelection:YES];
         }
         else
         {
             
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
     }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  TPPContactViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPContactViewController : TPPBaseViewController
@property (weak, nonatomic) IBOutlet UIView *contentView;
- (IBAction)contactButtonTapped:(id)sender;
- (IBAction)mailButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet GRKGradientView *menuView;


@property BOOL isContactSelected;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property BOOL shouldEnableSelection;
@property BOOL shouldEnableInvite;
///if NO then it will be invite
@property BOOL shouldChallange;
@property BOOL shouldShowMailListing;
@property TPPTripModel *trip;
-(void)enableRightBarButton;

@end

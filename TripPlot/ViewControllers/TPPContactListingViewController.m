//
//  TPPContactListingViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPContactListingViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "TPPCreateTripModel.h"
#import "TPPContactViewController.h"
#import "TPPContactListingCell.h"

@interface TPPContactListingViewController ()

@end

@implementation TPPContactListingViewController
{
    NSMutableArray *contactList;
    ABAddressBookRef addressBook;
    TPPCreateTripModel *newTripObject;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    contactList = [NSMutableArray new];
    newTripObject = [TPPCreateTripModel sharedInstance];
    //newTripObject.addedContacts = [NSMutableArray new];
    [self fetchContactDetails];
    
    if (!self.shouldEnableSelection) {
        newTripObject.addedContacts = [NSMutableArray new];
        newTripObject.addedEmailIDs = [NSMutableArray new];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPContactListingCell" bundle:nil] forCellReuseIdentifier:@"TPPContactListingCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma -mark UITableView delegate methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return contactList.count;//selectedArray.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* simpleTableIdentifier = @"TPPContactListingCell";
    
    TPPContactListingCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[TPPContactListingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *person = [contactList objectAtIndex:indexPath.row];
    NSArray *numbersArray = person[@"numbers"];
    
    NSString *phone = numbersArray.count>0?numbersArray[0]:@"N/A";
    
    if (![phone isEqualToString:@"N/A"])
    {
        NSInteger foundAt = [newTripObject.addedContacts isExistString:phone];
        if (foundAt >= 0)
        {
            
            [newTripObject.addedContacts removeObjectAtIndex:foundAt];
            
        }
        else
        {
            [newTripObject.addedContacts addObject:phone];
        }
    }
    else
    {
        [self.view showToasterWithMessage:@"No contact number found"];
    }
    
    TPPContactViewController *vc = (TPPContactViewController *) self.parentViewController;
    [vc.contactButton setBadgePostion:BadgePositionon_MiddleRight];
    vc.contactButton.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)newTripObject.addedContacts.count];
    [vc.contactButton setBadgePostion:BadgePositionon_MiddleRight];
    [vc enableRightBarButton];
    
    
    [self.tableView reloadData];
}

- (void)configureCell:(TPPContactListingCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *person = [contactList objectAtIndex:indexPath.row];
    cell.lblName.text = person[@"name"];
    
    NSArray *numbers = person[@"numbers"];
    NSString *phone =  numbers.count > 0?[numbers objectAtIndex:0]:@"N/A";
    cell.lblNumber.text = phone;
    if ([person objectForKey:@"image"])
    {
        [cell.imgView setImage:person[@"image"]];
    }
    else{
        [cell.imgView setImage:[UIImage imageNamed:@"icn_defaultuser"]];
    }
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.height/2;
    cell.imgView.clipsToBounds = YES;
    
    cell.backgroundColor=[UIColor clearColor];
    
    NSInteger foundAt = newTripObject!=nil?[newTripObject.addedContacts isExistString:phone]:-1;
    if (foundAt >= 0) {
        [cell setBackgroundColor:COLOR_ORANGE];
    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    if (!self.shouldEnableSelection) {
        cell.userInteractionEnabled = NO;
    }

}

- (UIImage*)imageForContact: (ABRecordRef)contactRef {
    UIImage *img = nil;
    
    // can't get image from a ABRecordRef copy
    ABRecordID contactID = ABRecordGetRecordID(contactRef);
    
    ABRecordRef origContactRef = ABAddressBookGetPersonWithRecordID(addressBook, contactID);
    
    if (ABPersonHasImageData(origContactRef)) {
        NSData *imgData = (__bridge NSData*)ABPersonCopyImageDataWithFormat(origContactRef, kABPersonImageFormatOriginalSize);
        img = [UIImage imageWithData: imgData];
        
    }
    return img;
}

-(void)fetchContactDetails
{
    CFErrorRef * error = NULL;
    addressBook = ABAddressBookCreateWithOptions(NULL, error);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     //                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                     CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
                                                     CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
                                                     
                                                     for(int i = 0; i < numberOfPeople; i++){
                                                         ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
                                                         
                                                         NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
                                                         NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
                                                         NSLog(@"Name:%@ %@", firstName, lastName);
                                                         NSString *name = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
                                                         
                                                         ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
                                                         
                                                         NSMutableArray *numbers = [NSMutableArray array];
                                                         for (CFIndex i = 0; i < ABMultiValueGetCount(phoneNumbers); i++) {
                                                             NSString *phoneNumber = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phoneNumbers, i);
                                                             [numbers addObject:phoneNumber];
                                                         }
                                                         
                                                         NSMutableDictionary *contact = [NSMutableDictionary dictionary];
                                                         [contact setObject:name forKey:@"name"];
                                                         [contact setObject:numbers forKey:@"numbers"];
                                                         [contact setValue:[self imageForContact:person] forKey:@"image"];
                                                         
                                                         [contactList addObject:contact];
                                                         
                                                         
                                                         if (i == numberOfPeople -1)
                                                         {
                                                             [self.tableView reloadData];
                                                         }
                                                     }
                                                     [self.tableView reloadData];                                                     // });
                                                 }
                                                 
                                                 [self.tableView reloadData];
                                             });
    
    [self.tableView reloadData];
}
@end

//
//  TPPTripLocationTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPTripLocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UIButton *addLocationButton;
@property (weak, nonatomic)UITapGestureRecognizer *sourceLabelTapRecognizer;
@property (weak, nonatomic)UITapGestureRecognizer *destinationLabelTapRecognizer;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;


@end

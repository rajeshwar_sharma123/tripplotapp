//

//  TPPExploreViewController.m

//  TripPlot

//

//  Created by daffolapmac on 08/02/16.

//  Copyright © 2016 Daffodil. All rights reserved.

//



#import "TPPExploreViewController.h"

#import "TPPExploreListCell.h"

#import "TPPExploreMapCell.h"

#import "TPPSearchViewController.h"

#import "TPPMarkerWindowView.h"



@interface TPPExploreViewController ()<TPPSearchDelegate>

{
    GMSCircle *circle;
    UIBarButtonItem *rangeButtonItem;
    UIButton *switchButton;
    UIImageView *searchCarrotImgView;
    UIImageView *rangeCarrotImgView;
    UIView *lineView;
}

@end



@implementation TPPExploreViewController

{
    BOOL shouldShowMap;
    
    TPPSearchViewController *searchViewCntrl;
    
    NSArray *typeListSelectedArry;
    NSMutableArray <TPPTripModel*> *trips;
    UIRefreshControl *refreshControl;
    
    TPPNoDataFoundView *noDataFoundView;
    
    double radiusInKMs;
}



- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
  
    self.title = @"Explore";
    
    [self.sliderContentView setHidden:YES];
    [self.radiusSliderView addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.radiusSliderView addTarget:self action:@selector(sliderTapped:) forControlEvents:UIControlEventTouchUpInside];
    radiusInKMs = 100.0;
    
    [self sliderValueChanged:self.radiusSliderView];
    
    _tripsArray = [NSMutableArray new];
    _requestObject = [NSMutableDictionary new];
    _locationTypeListArry = [NSMutableArray new];
   
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(pullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
   // [self.tableView addSubview:refreshControl];
    noDataFoundView = [TPPUtilities addNoDataFoundViewInTableView:self.tableView viewController:self];
    
    
    
    NSDictionary *userDic =  [UserDeafaults objectForKey:UserProfile];
    
    _requestObject[@"token"] = [UserDeafaults objectForKey:Token];
    _requestObject[@"_id"] = userDic[@"_id"];
    
    
    
    // Do any additional setup after loading the view.
    
    [self addRightMenuButtons];
    [self registerForCells];
    [self getTypeListFromService];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.locationTypeCollectionView reloadData];
    lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 2)];
    lineView.backgroundColor = [UIColor grayColor];
    lineView.hidden = NO;
    [self.navigationController.view addSubview:lineView];
    [self.navigationController.view bringSubviewToFront:lineView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    lineView.hidden = YES;
}


#pragma mark- BL methods

-(void)registerForCells
{
    //Share tab cells
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPExploreListCell" bundle:nil] forCellReuseIdentifier:@"TPPExploreListCell"];
    
    // Ongoing trip cells
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPExploreMapCell" bundle:nil] forCellReuseIdentifier:@"TPPExploreMapCell"];
    
}

-(void) sliderValueChanged:(UISlider *) sender
{
    radiusInKMs = sender.value*1000.0;
    self.radiusLabel.text = [NSString stringWithFormat:@"%.0f",floor(radiusInKMs)];

}

-(void)sliderTapped:(UISlider *)sender
{
    _requestObject[Txt_Radius] = [NSNumber numberWithDouble: radiusInKMs];
   
    [self callServiceToGetTripList];
}


-(void)drawCircleOnMap
{
    circle.map = nil;
    circle = [GMSCircle circleWithPosition:self.mapView.myLocation.coordinate
                                    radius:radiusInKMs*1000];
    circle.fillColor = [UIColor colorWithRed:0.25 green:0 blue:0 alpha:0.03];
    circle.strokeColor = [UIColor redColor];
    circle.strokeWidth = 3;
    circle.map = self.mapView;
}

-(void)addRightMenuButtons
{
    shouldShowMap = YES;
    
    UIImage *image;
    UIButton *button;
    NSInteger width = 38;
    NSInteger height = 46;
    NSInteger decreaseHeight = 8;
    NSInteger carrotHeight = 6;
    if (IS_IPHONE_4 || IS_IPHONE_5)
    {
        width = 32;
        height = 44;
        decreaseHeight = 12;
    }
    
    
    
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    navigationView.backgroundColor = [UIColor clearColor];
    
    image = [UIImage imageNamed:@"btn_search_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    searchCarrotImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, navigationView.frame.size.height-carrotHeight, navigationView.frame.size.width,carrotHeight)];
    searchCarrotImgView.contentMode = UIViewContentModeScaleAspectFit;
    searchCarrotImgView.hidden = YES;
    searchCarrotImgView.image = [UIImage imageNamed:@"icn_carrot_gray"];
    searchCarrotImgView.backgroundColor = [UIColor clearColor];
    [navigationView addSubview:searchCarrotImgView];
    
    [button addTarget:self action:@selector(searchButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];
    
    navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    navigationView.backgroundColor = [UIColor clearColor];
    
    image = [UIImage imageNamed:@"icn_range_normal"];
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 3, navigationView.frame.size.width,navigationView.frame.size.height-decreaseHeight);
    [navigationView addSubview:button];
    
    rangeCarrotImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, navigationView.frame.size.height-carrotHeight, navigationView.frame.size.width,carrotHeight)];
    rangeCarrotImgView.contentMode = UIViewContentModeScaleAspectFit;
    rangeCarrotImgView.hidden = YES;
    rangeCarrotImgView.image = [UIImage imageNamed:@"icn_carrot_gray"];
    rangeCarrotImgView.backgroundColor = [UIColor clearColor];
    [navigationView addSubview:rangeCarrotImgView];
    
    [button setImage:[UIImage imageNamed:@"icn_range_pressed"] forState:UIControlStateHighlighted];
    
    [button addTarget:self action:@selector(rangeButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    button.tag = 1;
    rangeButtonItem = [[UIBarButtonItem alloc]initWithCustomView:navigationView];
    
    image = [UIImage imageNamed:@"switch_list_active"];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.tag = 1;
    switchButton = button;
    button.frame = CGRectMake(0, 3, width+24,  width-6);
    [button setImage:image forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"switch_list_transition"] forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(switchButtonTapped:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *switchButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItems = @[ searchButtonItem, rangeButtonItem,switchButtonItem];
    self.navigationController.navigationBar.hidden = NO;
    
    
    [self switchButtonTapped:button];
}



#pragma mark - TPPSearchDelegate methods

-(void)didSearchButtonTapped:(NSString *)searchString withSearchObject:(NSDictionary *)searchObject
{
    _requestObject[@"search"] = searchString.trimmedString;
    _requestObject[@"typeId"] = [searchObject[@"tripTypes"] firstObject];
    
    
    [_tripsArray removeAllObjects];
    
    [self callServiceToGetTripList];
    
}


#pragma mark- TableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section

{
    if (!_tripsArray || (_tripsArray.count > 0))
    {
        tableView.backgroundView.hidden = YES;
        
    }
    else
    {
        tableView.backgroundView.hidden = NO;
        
    }
    
    if (shouldShowMap) {
        
        return 1;
        
    }
    
    return _tripsArray.count + 1;

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *listCellIndetifier = @"TPPExploreListCell";
    static NSString *mapCellIndetifier = @"TPPExploreMapCell";
    
        if (indexPath.row>0)
        {
        
        TPPExploreListCell *cell = [tableView dequeueReusableCellWithIdentifier:listCellIndetifier];
        
        if (!cell) {
            
            cell = [[TPPExploreListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:listCellIndetifier];
            
        }
        
        
        
        TPPTripModel *objTrip = _tripsArray.count>0? [_tripsArray objectAtIndex:indexPath.row - 1]:nil;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",objTrip.title];
        
        cell.tripCountLabel.text = [NSString stringWithFormat:@"%ld trips",[objTrip.totalTrips[@"count"] integerValue]];
        
        cell.likeCountLabel.text = [NSString stringWithFormat:@"%ld",objTrip.likeCount];
        
        cell.distanceLabel.text = [NSString stringWithFormat:@"%ld kms away",[objTrip.distance integerValue]/1000 ];
        
        
        
        NSString *imageUrl = objTrip.images.count>0? [objTrip.images objectAtIndex:0]:@"";
        
        [cell.tripImageView showSpinner];
        
        [cell.tripImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"icn_placeholderImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            
            
            [cell.tripImageView hideSpinner];
            
            
            
        }];
        
        return cell;
        
    }
    
    else
        
    {
        
        TPPExploreMapCell *cell = [tableView dequeueReusableCellWithIdentifier:mapCellIndetifier];
        
        if (!cell)
        {
            
            cell = [[TPPExploreMapCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:mapCellIndetifier];
            
        }
        if (shouldShowMap)
        {
            [cell.resetButton setHidden:NO];
        }
        else
        {
            [cell.resetButton setHidden:YES];
        }
        cell.exploreViewController = self;
        self.mapView = cell.mapView;
        [cell drawMarkersOnMap:cell.mapView];
        [self drawCircleOnMap];
        
        return cell;
    }
    
}





-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (shouldShowMap) {
        
        return ScreenHeight;
        
    }
    
    else if (!shouldShowMap && indexPath.row == 0)
        
        return 0;
    
    return 110;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if (indexPath.row>0)
    {
        TPPTripModel *trip = [_tripsArray objectAtIndex:indexPath.row-1];

        [self callServiceToGetTripDetailWithTripId:trip._id];
    }
    
    
    
}


#pragma -mark buttons and utility methods

-(void)searchButtonTapped:(UIButton *)sender

{
   
    
    if (searchViewCntrl == nil) {
        
        searchViewCntrl = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPSearchViewController"];
        
        searchViewCntrl.isHidden = YES;
        searchViewCntrl.delegate = self;
        
    }
    [searchViewCntrl toggleViewOnViewController:self];
    if (!searchViewCntrl.isHidden) {
        [self hideShowCarrotImgView:rangeCarrotImgView flagHidden:YES];
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:NO];
        [rangeButtonItem setEnabled:NO];
    }
    else{
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
        if (switchButton.tag==2) {
            [rangeButtonItem setEnabled:YES];
        }
    
    }
    
}

-(void)rangeButtonTapped:(UIButton *)sender
{
    [searchViewCntrl hideView];
    UIButton *btn = (UIButton *)sender;
    if (btn.tag==1) {
        [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
        [self hideShowCarrotImgView:rangeCarrotImgView flagHidden:NO];
        
        btn.tag = 2;
        [self.sliderContentView setHidden:NO];
    }
    else if(btn.tag==2)
    {
        [self hideShowCarrotImgView:rangeCarrotImgView flagHidden:YES];
        btn.tag = 1;
        [self.sliderContentView setHidden:YES];
    }
}

-(void)switchButtonTapped:(UIButton *)sender
{
    [self hideAllCarrotImgView];
    [searchViewCntrl hideView];
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 1)
    {
        btn.tag = 2;
        [btn setImage:[UIImage imageNamed:@"switch_map_active"] forState:UIControlStateNormal];
        
        [btn setImage:[UIImage imageNamed:@"switch_map_transition"] forState:UIControlStateHighlighted];
        shouldShowMap = YES;
        _requestObject[Txt_Radius] = [NSString stringWithFormat:@"%.2f",radiusInKMs];
        [rangeButtonItem setEnabled:YES];
    }
    else if (btn.tag == 2)
    {
        
        btn.tag = 1;
        
        [btn setImage:[UIImage imageNamed:@"switch_list_active"] forState:UIControlStateNormal];
        
        [btn setImage:[UIImage imageNamed:@"switch_list_transition"] forState:UIControlStateHighlighted];
        
        shouldShowMap = NO;
        
        
        [_requestObject removeObjectForKey:Txt_Radius];
        [rangeButtonItem setEnabled:NO];
        if (!self.sliderContentView.isHidden) {
            [self.sliderContentView setHidden:YES];
        }
        
    }
    
    [self.tableView reloadData];
    
}

- (void)hideShowCarrotImgView:(UIImageView *)imgView flagHidden:(BOOL)flag
{
    
    imgView.hidden = flag;
    
}

-(void)hideAllCarrotImgView
{
    [self hideShowCarrotImgView:rangeCarrotImgView flagHidden:YES];
    [self hideShowCarrotImgView:searchCarrotImgView flagHidden:YES];
}


- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
    //Dispose of any resources that can be recreated.
    
}




#pragma -mark web service calls

- (void)callServiceToGetTripList
{
    if (!refreshControl.refreshing)
        
        [self.view showLoader];
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ExploreRecommendedTripsUrl] data:_requestObject withSuccessBlock:^(id response, NSDictionary *headers)
     
     {
         [refreshControl endRefreshing];
         
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
             [_tripsArray removeAllObjects];
             
             NSError *error;
             
             NSArray *tempArray = response[@"response"][@"trips"];
             
             for (NSDictionary *object in tempArray)
             {
                 
                 TPPTripModel *tripObj = [[TPPTripModel alloc]initWithDictionary:object error:&error];
                 
                 [_tripsArray addObject:tripObj];
                 
             }
             
             if (error)
             {
                 [self.view showToasterWithMessage:error.localizedDescription];
             }
             
             if (_tripsArray.count > 0)
             {
                 self.tableView.backgroundView.hidden = YES;
                 
             }
             else
             {
                 if (shouldShowMap)
                 {
                     [self.view showToasterWithMessage:@"No recommended destinations found"];
                     
                 }
             }
             
             [self.tableView reloadData];
             [self.locationTypeCollectionView reloadData];
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     
     {
         if (_tripsArray.count == 0)
         {
             [self.tableView reloadData];
             self.tableView.backgroundView.hidden = NO;
             noDataFoundView.messageLabel.text = errorMessage;
         }
         else
         {
             self.tableView.backgroundView.hidden = YES;
             [self showErrorMessage:errorMessage];
         }
         
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
         if (shouldShowMap)
         {
             [self showErrorMessage:errorMessage];
             circle.map = nil;
         }
         
     }];
    
    
    
}



- (void)getTypeListFromService
{
    [self.view showLoader];
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlWithToken:TypeListUrl] data:[NSDictionary dictionary] withSuccessBlock:^(id response, NSDictionary *headers) {
        
        [self.view hideLoader];
        
        if ([[response objectForKey:@"code"] integerValue] == 200)
            
        {
            _locationTypeListArry = response[@"response"][@"result"];
            
            [self.locationTypeCollectionView reloadData];
            
        }
        else
        {
            
           
        }
    } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage) {
        
        [self.view hideLoader];
        
        [self showErrorMessage:errorMessage];
        
    }];
    
    
    
    
    
}



- (void)callServiceToGetTripDetailWithTripId:(NSString *)tripId
{
    [self.view showLoader];
    
    //call web service to get trip details to get route
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    requestData[@"token"] = [UserDeafaults objectForKey:Token];
    requestData[@"tripId"] = tripId;
    
    if (_selectedIndex > 0)
    {
        requestData[@"typeId"] = _locationTypeListArry[_selectedIndex-1][@"_id"];
    }
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ExploreRecommendedDetailByIdUrl] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
             
             tripDetailViewController.tripModel = [[TPPTripModel alloc]initWithDictionary:response[@"response"][@"trips"][0] error:nil];
             
             [self.navigationController pushViewController:tripDetailViewController animated:YES];
         }
         else
         {
           
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
    
}





#pragma -mark UICollectionView delegates

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section

{
    
    return _locationTypeListArry.count+1;
    
}





-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *indentifier = @"Cell";
    
    
    
    TPPExploreLocationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:indentifier forIndexPath:indexPath];
    
    
    
    if (indexPath.row == 0) {
        
        [cell configureCell:nil forIndex:indexPath withBadgeValue:_tripsArray.count selectedIndex:_selectedIndex];
        
    }
    
    else
        
    {
        
        [cell configureCell:[_locationTypeListArry objectAtIndex:indexPath.row-1] forIndex:indexPath withBadgeValue:_tripsArray.count selectedIndex:_selectedIndex];
        
    }
    
    return cell;
    
}



-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    NSLog(@"selecet");
    
    _selectedIndex = indexPath.row;
    [_requestObject removeObjectForKey:@"search"];
    
    
    if (indexPath.row == 0) {
        
        [_requestObject removeObjectForKey:@"typeId"];
        
    }
    
    else
        
    {
        
        NSDictionary *locationType = [_locationTypeListArry objectAtIndex:indexPath.row-1];
        
        _requestObject[@"typeId"] =locationType[@"_id"];
        
    }
    
    
    
    [self.locationTypeCollectionView reloadData];
    
    
    
    [_tripsArray removeAllObjects];
    
    
    
    [self callServiceToGetTripList];
    
}



#pragma mark- TPPNoDataFoundDelegate methods

-(void)retryButtonTapped
{
    
    [self callServiceToGetTripList];
    
}





#pragma -mark Custom methods

-(void)pullToRefresh:(id)sender

{
    [_requestObject removeObjectForKey:@"typeId"];
    
    [_requestObject removeObjectForKey:Txt_Radius];
    
    [_requestObject removeObjectForKey:@"search"];
    
    
    
    [self callServiceToGetTripList];
    
}







@end




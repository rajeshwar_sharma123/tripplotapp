//
//  TPPExploreViewController.h
//  TripPlot
//
//  Created by daffolapmac on 08/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TPPExploreLocationCollectionViewCell.h"

@interface TPPExploreViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *locationTypeCollectionView;
@property (strong, nonatomic) GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *sliderContentView;
@property (weak, nonatomic) IBOutlet TPPCustomSlider *radiusSliderView;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;

@property NSMutableDictionary *requestObject;
@property  NSMutableArray *tripsArray;
@property NSArray *typeListSelectedArry;

@property NSArray *locationTypeListArry;

@property NSInteger selectedIndex;


- (void)callServiceToGetTripList;
- (void)callServiceToGetTripDetailWithTripId:(NSString *)tripId;

@end
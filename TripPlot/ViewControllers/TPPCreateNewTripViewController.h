//
//  TPPCreateNewTripViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import "TPPTripMenuStepperView.h"
#import "DWTagList.h"
#import "TPPSourceDestinationModel.h"


@interface TPPCreateNewTripViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,DWTagListDelegate>
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)AddLocationTapped:(id)sender;
- (IBAction)addLocationToTapped:(id)sender;

//@property(retain, nonatomic) TPPCreateTripModel *tripObj;

- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)nextButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property NSMutableArray <TPPSourceDestinationModel *> *sourceDestinationsArray;



@end

//
//  TPPHomeViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPHomeViewController : TPPBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *liveTripButton;
- (IBAction)liveTripButtonTapped:(id)sender;

@end

//
//  TPPHomeViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPHomeViewController.h"
#import "AppDelegate.h"

#import "TPPTripDetailViewController.h"
#import "TPPOngoingTripDetailViewController.h"
#import "TPPLivePlotViewController.h"
#import "TPPContactViewController.h"
#import <Google/Analytics.h>
#import "TPPLeftMenuViewController.h"
#import "TPPAddImagesViewController.h"
#import "UITableView+RefreshControl.h"

@interface TPPHomeViewController ()<UITableViewDataSource,UITableViewDelegate,TPPMenuViewDelegate,TPPCellTapDelegate,TPPSeeMoreButtonDelegate,TPPShareCellDelegate,TPPTripPointTapDelegate,TPPNoDataFoundDelegate>
{

    NSMutableArray *commonModelsArray;
   
    NSInteger pageIndex;
    NSInteger totalFeedsOnServer;
    NSInteger totalFeedsRecieved;
    NSInteger tripStatus;
    
   // UIRefreshControl *refreshControl;
    
   
    TPPNoDataFoundView *noDataFoundView;
    
    BOOL isPaginationRequestInProgress;
    UIView* lineView;
}
@end


@implementation TPPHomeViewController


#pragma mark- LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"TripPlot";
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    TPPLeftMenuViewController *leftMenuVc = (TPPLeftMenuViewController *)self.menuContainerViewController.leftMenuViewController;
    leftMenuVc.homeViewController = self;
    
   // [[TPPSocketUtility sharedInstance] connectToSocket];
    
    // To update the UI if user likes/Follows/Joins etc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userActivitySelector:) name:UserActivityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationTapped:) name:@"notificationTappedNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTripCreatedSelector:) name:@"newTripCreatedNotification" object:nil];
    

    
    [self addRightMenuButtons];
    [self initMoreButton];
    [self initView];
    [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
    
    [self.liveTripButton setHidden:YES];
    
   
    //call service to get live trips count
    [TPPSharedTripCountsModel callServiceToGetLiveTripDetails:^()
     {
         TPPSharedTripCountsModel *trips = [TPPSharedTripCountsModel sharedInstance];
         if (trips.isOngoing)
         {
             [self.liveTripButton setHidden:NO];
             if (trips.onGoingTripId)
             {
                 if([[TPPUtilities getCurrentViewController] isKindOfClass:[TPPHomeViewController class]])
                 {
                     trips.isOngoing = NO;
                     TPPLivePlotViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPLivePlotViewController"];
                     [self.navigationController pushViewController:vc animated:YES];
                 }
                 
             }
             else{
                 [self.view showToasterWithMessage:@"Data Not found"];
             }
         }
         
         NSLog(@"Got live trip");
     }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Home"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 2)];
    lineView.backgroundColor = [UIColor grayColor];
    lineView.hidden = NO;

    [self.navigationController.view addSubview:lineView];
    [self.navigationController.view bringSubviewToFront:lineView];
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    lineView.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initView
{
    totalFeedsRecieved = 0;
    pageIndex = 1;
    tripStatus = 0;
    
    [self registerForCells];
    [self.tableView enablePullToRefreshWithTarget:self selector:@selector(pullToRefresh:)];
//    refreshControl = [[UIRefreshControl alloc]init];
//    [refreshControl addTarget:self action:@selector(pullToRefresh:) forControlEvents:UIControlEventValueChanged];
//    
//    [self.tableView addSubview:refreshControl];
    noDataFoundView = [TPPUtilities addNoDataFoundViewInTableView:self.tableView viewController:self];
}

-(void)initMoreButton
{
    super.dropDownView.dropdownDelegate = self;
}


#pragma mark- Initialization Methods

-(void)registerForCells
{
    // LoadMoreCell
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPLoadMoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPLoadMoreTableViewCell"];
    
    //Share tab cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareUpcomingTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareUpcomingTripTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareUpcomingWithoutJoinTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareUpcomingWithoutJoinTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPInviteFriendShareCell" bundle:nil] forCellReuseIdentifier:@"TPPInviteFriendShareCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareCompletedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareCompletedTripTableViewCell"];
    
    
    // Ongoing trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPOngoingTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPOngoingTripTableViewCell"];
    
    
    // Upcoming trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel0" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel0"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel1"];
    
    
    // Completed trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripExpandedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
    
    
    // Recommended trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel1"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel2" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel2"];
    
}



#pragma mark- BL Methods

-(TPPCommonTripModel *)commonModelForTripModel:(TPPTripModel *)tripModel
{
    for (TPPCommonTripModel *commonModel in commonModelsArray)
    {
        if ([tripModel._id isEqualToString:commonModel.tripModel._id])
        {
            return commonModel;
            
        }
    }
    
    
    return nil;
}


/**
 *  Called when if there is user activity such as like,follow etc.
 *
 *
 *  @param notification
 */
-(void)userActivitySelector:(NSNotification *)notification
{
    UINavigationController *navVC =  self.menuContainerViewController.centerViewController;
    
    // If current screen is Home screen
    if ([navVC.topViewController isKindOfClass:[TPPHomeViewController class]])
    {
        
    }
    // If activity not done from Home screen and Detail screen (Done from other screen such as Following screen)
    else if (notification.userInfo)
    {
        TPPTripModel *recievedTripModel = notification.userInfo[@"tripModel"];
        TPPCommonTripModel *commonModel = [self commonModelForTripModel:recievedTripModel];
        if (!commonModel)
        {
            [self.tableView reloadData];
            return;
        }
        
        //Need to reload data of Home screen to get the latest data and update the UI, if trip is already present on Home screen
        if ([recievedTripModel._id isEqualToString:commonModel.tripModel._id])
        {
            pageIndex = 1;
            totalFeedsRecieved = 0;
            
            [commonModelsArray removeAllObjects];
            [self.tableView reloadData];
            
            [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
        }
    }
    
    [self.tableView reloadData];
}


-(void)pullToRefresh:(id)sender
{
    if (isPaginationRequestInProgress)
    {
        [self.tableView.refreshControl endRefreshing];
        return;
    }

    pageIndex = 1;
    totalFeedsRecieved = 0;
    //tripStatus = 0;
    
    [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
}


-(BOOL)isMoreFeedsAvailable
{
    if(totalFeedsRecieved < totalFeedsOnServer)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


-(TPPCommonTripModel *)getRootTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    NSInteger i = selectedCellIndex;
    for (; i>=0; i--)
    {
        TPPCommonTripModel *commonModel = [commonModelsArray objectAtIndex:i];
        if (commonModel.level == 0)
        {
            return [commonModelsArray objectAtIndex:i];
        }
    }
    
    return nil;
}

-(TPPTripModel *)getTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    TPPCommonTripModel *parentTripModel = [self getRootTripModelForSelectedIndex:selectedCellIndex];
    
    return parentTripModel.tripModel;
}



-(void)setTextColorOfSelectedFilter:(NSInteger)index
{
    for (int i=0; i<5; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        TPPDropDownCell *cell = [self.dropDownView cellForRowAtIndexPath:indexPath];
        
        if (i==index)
        {
            cell.menuNameLabel.textColor = TPPOrangeColor;
        }
        else
        {
            cell.menuNameLabel.textColor = TPPTextColor;
        }
    }
    
}


#pragma mark- Selectors

-(void)newTripCreatedSelector:(NSNotification *)notification
{
    if(isPaginationRequestInProgress || self.tableView.refreshControl.refreshing)
    {
        return;
    }
    
    pageIndex = 1;
    totalFeedsRecieved = 0;
    
    tripStatus = 0;
    
    [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
}


-(void)addButtonTapped:(UIButton *)sender
{
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
     
    
    TPPCommonTripModel *commonModel = commonModelsArray[sender.tag];
    goHereView.tripModel = commonModel.tripModel;
    
    [self.view addSubview:goHereView];

}

-(void)notificationTapped:(NSNotification *)notification
{
    pageIndex = 1;
    totalFeedsRecieved = 0;
    tripStatus = 0;
    
    [commonModelsArray removeAllObjects];
    [self.tableView reloadData];

    
    [self callServiceForTripsWithTripStatus:tripStatus userId:notification.userInfo[@"userId"] searhRequest:nil];

}

#pragma mark - TPPSearchDelegate methods

-(void)didSearchButtonTapped:(NSString *)searchString withSearchObject:(NSDictionary *)searchObject
{
    pageIndex = 1;
    totalFeedsRecieved = 0;

    [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:searchObject];
}


#pragma mark- TPPNoDataFoundDelegate methods

-(void)retryButtonTapped
{
    [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
}


#pragma mark- TPPTripPointTapDelegate methods

-(void)tripPointTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel buttonIndex:(NSInteger)buttonIndex
{
    TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
    tripDetailViewController.tripModel = tripModel;
    tripDetailViewController.selectedButtonIndex = buttonIndex;
    
    [self.navigationController pushViewController:tripDetailViewController animated:YES];
}



#pragma mark- TPPShareCellDelegate methods

-(void)shareButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel snapShot:(UIImage *)snapShot
{

//    UIView *view;
//    NSInteger index = indexPath.row-1;
//    while (index)
//    {
//        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//       
//        if (!view)
//        {
//            view = [[UIView alloc]initWithFrame:cell.frame];
//            view.backgroundColor = [UIColor clearColor];
//        }
//        else
//        {
//            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, (view.frame.size.height + cell.frame.size.height));
//        }
//        
//        [view addSubview: cell];
//        [view bringSubviewToFront:cell];
//        
//        TPPCommonTripModel *commonTripModel = commonModelsArray[index];
//        if (commonTripModel.level == 0)
//        {
//            break;
//        }
//        
//        index--;
//    }
    
    if (snapShot)
    {
        [self defaultShareWithText:@"" image:snapShot url:nil];
    }
    else
    {
        UITableViewCell *parentCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:0]];
        
        UIImage *image = parentCell.snapshotImage;
        [self defaultShareWithText:@"" image:image url:nil];
    }
    
    
}

-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:tripModel.createdBy[@"_id"]] && (tripModel.tripStatus==TripStatus_CompletedTrip))
    {
        // Navigate to Invite screen because button will be invite rather than Go here
        TPPContactViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
        TPPCreateTripModel *newTripObj = [TPPCreateTripModel sharedInstance];
        [newTripObj.addedEmailIDs removeAllObjects];
        [newTripObj.addedContacts removeAllObjects];
        
        VC.shouldEnableSelection = YES;
        VC.shouldEnableInvite = YES;
        VC.shouldChallange = YES;
        VC.trip = tripModel;
        [self.navigationController pushViewController:VC animated:YES];
        
        return;
    }
    
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
    
     
    goHereView.tripModel = tripModel;
    
    [self.view addSubview:goHereView];
}






#pragma mark-  TPPMenuViewDelegate methods

/// filter button tapped
-(void)buttonTappedAtRowIndex:(NSInteger)index
{
    if(isPaginationRequestInProgress || self.tableView.refreshControl.refreshing)
    {
        return;
    }
    
    pageIndex = 1;
    totalFeedsRecieved = 0;
    
    tripStatus = index;
    
//    [commonModelsArray removeAllObjects];
//    [self.tableView reloadData];
   
    [self callServiceForTripsWithTripStatus:index userId:nil searhRequest:nil];
}

#pragma mark-  TPPSeeMoreDelegate methods

-(void)seeMoreButtonTappedWithIndexPath:(NSIndexPath *)indexPath
{
    TPPCommonTripModel *commonModel = [self getRootTripModelForSelectedIndex:indexPath.row];
    
    TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
    tripDetailViewController.tripModel = commonModel.tripModel;
    
    [self.navigationController pushViewController:tripDetailViewController animated:YES];
    
}


#pragma mark- TableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!commonModelsArray || (commonModelsArray.count > 0))
    {
        tableView.backgroundView.hidden = YES;
    }
    else
    {
        tableView.backgroundView.hidden = NO;
    }

    
    if((totalFeedsRecieved > 0) && [self isMoreFeedsAvailable] && !self.tableView.refreshControl.isRefreshing)
    {
        return commonModelsArray.count+1;
    }

    return commonModelsArray.count;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    // for LoadMoreCell
    if ((indexPath.row == commonModelsArray.count) && [self isMoreFeedsAvailable])
    {
        TPPLoadMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPLoadMoreTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPLoadMoreTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPLoadMoreTableViewCell"];
        }
        [cell.spinner startAnimating];

        return cell;
    }
   
    
    UITableViewCell *commonCell;
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    if (commonModel.isShareModel)
    {
        commonCell = [self getShareTabCellForTableView:tableView IndexPath:indexPath model:commonModel];
        return commonCell;
    }
    
    if (commonModel.status == TripStatus_OngoingTrip)
    {
        commonCell = [self getOngoingTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    else if (commonModel.status == TripStatus_UpcomingTrip)
    {
        commonCell = [self getUpcomingCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    if (commonModel.status == TripStatus_CompletedTrip)
    {
         commonCell = [self getCompletedTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    else if (commonModel.status == TripStatus_RecommendedTrip)
    {
        commonCell = [self getRecommendedCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }

    return commonCell;
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView.refreshControl.refreshing && (indexPath.row == commonModelsArray.count)) // Pagination should not work if Pull-To-Refresh is working currently
    {
        TPPLoadMoreTableViewCell *cell1 = (TPPLoadMoreTableViewCell *)cell;
        cell1.spinner.hidden = YES;
        
        return;
    }
    else if(indexPath.row == commonModelsArray.count)
    {
        TPPLoadMoreTableViewCell *cell1 = (TPPLoadMoreTableViewCell *)cell;
        cell1.spinner.hidden = NO;
    }

    if((commonModelsArray.count == indexPath.row) && [self isMoreFeedsAvailable])
    {
        isPaginationRequestInProgress = YES;
        [self callServiceForTripsWithTripStatus:tripStatus userId:nil searhRequest:nil];
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
         
    // For LoadMoreCell
    if (indexPath.row == commonModelsArray.count)
    {
        return 44;
    }
    
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    
    if(commonModel.isShareModel)
    {
        return 50;
    }

    
    if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level==0)
    {
        return 280;
    }
    else if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level == 1)
    {
        return 157;
    }
    else if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level == 2)
    {
        return 57;
    }
    else if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 0)
    {
        return 228;
    }
    else if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 1)
    {
        return 200;
    }

    else if(commonModel.status==TripStatus_OngoingTrip && commonModel.level == 0)
    {
        return 475;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 0)
    {
        return 221;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 1)
    {
        return 276;
    }

    
    return 44;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCellIndex = indexPath.row;
    TPPCommonTripModel *model = [commonModelsArray objectAtIndex:selectedCellIndex];
    
    
    if (model.level == 2 && model.status==TripStatus_RecommendedTrip)
    {
        [self callServiceToGetTripDetailsWithCommonTripModel:model];
        
    }
    else if (model.level == 1 && model.status==TripStatus_UpcomingTrip)
    {
        TPPTripModel *tripModel = [self getTripModelForSelectedIndex:selectedCellIndex];
        
       TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
        tripDetailViewController.strCheck = @"home";
        tripDetailViewController.vcObj = self;
        tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];

        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (model.level == 0 && model.status==TripStatus_OngoingTrip)
    {
        TPPCommonTripModel *commonModel = (TPPCommonTripModel *) commonModelsArray[selectedCellIndex];
        
        TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
        tripDetailViewController.tripModel = commonModel.tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (model.childrenArray.count > 0 && !self.tableView.refreshControl.refreshing)
    {
        // expand or collapse
       
        if (model.isExpanded)
        {
            // Collapse submenus
            [self collapseMenuWithMenuItem:model menuIndexPath:indexPath];
        }
        else
        {
            // expand menu
            [self expandMenuWithMenuItem:model menuIndexPath:indexPath];
        }
    }
    
}


#pragma mark- To get the cells

-(UITableViewCell *)getShareTabCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    if (commonModel.status == TripStatus_UpcomingTrip)
    {
        NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
        TPPTripModel *tripModel = commonModel.tripModel;
        
        if ([tripModel.createdBy[@"_id"] isEqualToString:userProfile[@"_id"]])
        {
            TPPInviteFriendShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPInviteFriendShareCell"];
            if (cell == nil)
            {
                cell = (TPPInviteFriendShareCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPInviteFriendShareCell"];
            }
            cell.tripModel = commonModel.tripModel;
            cell.delegate = self;
            
            return cell;

        }
        else if( !tripModel.isOpen || !tripModel.isGroup)
        {
            TPPShareUpcomingWithoutJoinTableViewCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPShareUpcomingWithoutJoinTableViewCell" owner:self options:nil] objectAtIndex:0];
            
            [cell bindDataWithModel:tripModel];
            cell.shareCellDelegate = self;
            
            return cell;
        }
        else
        {
            TPPShareUpcomingTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPShareUpcomingTripTableViewCell"];
            if (cell == nil)
            {
                cell = (TPPShareUpcomingTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPShareUpcomingTripTableViewCell"];
            }
            [cell bindDataWithModel:commonModel.tripModel];
            cell.shareCellDelegate = self;
            
            return cell;
        }
        
    }
    else
    {
        TPPShareCompletedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPShareCompletedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPShareCompletedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPShareCompletedTripTableViewCell"];
        }
       
        [cell bindDataWithModel:commonModel.tripModel];
        cell.shareCellDelegate = self;
        
        return cell;
    }
}


-(UITableViewCell *)getOngoingTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        
        TPPOngoingTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPOngoingTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPOngoingTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPOngoingTripTableViewCell"];
        }
        cell.delegate = self;
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    return commonCell;
}


-(UITableViewCell *)getUpcomingCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    if (commonModel.level == 0)
    {
        TPPUpcomingTripCellLevel0 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel0"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel0 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel0"];
        }

        [cell bindDataWithModel:commonModel.tripModel];
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPUpcomingTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel1"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel1 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel1"];
        }
        cell.tripPointTapDelegate = self;
        [cell bindDataWithModel:commonModel.tripModel];
        commonCell = cell;
    }
    return commonCell;
}


-(UITableViewCell *)getCompletedTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPCompletedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripTableViewCell"];
        }
       
        [cell bindDataWithModel:commonModel.tripModel];
        cell.cellTapDelegate = self;
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPCompletedTripExpandedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripExpandedTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        }
        [cell bindDataWithModel:commonModel.tripModel];
        [cell reloadInputViews];
        
        commonCell = cell;
        
    }
    return commonCell;
}

-(UITableViewCell *)getRecommendedCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPRecommendedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripTableViewCell"];
        }
        [cell.addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.addButton.tag = indexPath.row;
//        [cell.addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//		cell.addButton.tag =
        cell.cellTapDelegate = self;
		cell.plotTripDelegate = self;
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPRecommendedTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel1"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripCellLevel1 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel1"];
        }

        [cell bindDataWithModel:commonModel.tripModel];

        commonCell = cell;
    }
    else if (commonModel.level == 2)
    {
        TPPRecommendedTripCellLevel2 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel2"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripCellLevel2 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel2"];
        }

        
        if (commonModel.shouldShowSeeMoreButton)
        {
            cell.seeMoreButtonDelegate = self;
            cell.seeMoreButton.hidden = NO;
        }
        else
        {
             cell.seeMoreButton.hidden = YES;
        }
       
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    
    return commonCell;
}



#pragma mark- Expand/Collapse Menu

-(void)collapseMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1)// rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData:menuItem];
    }
    
    
    NSInteger totalChildrenCount = menuItem.childrenArray.count;
    if (totalChildrenCount == 0)
    {
        return;
    }

    // for recommended cell, child cells should also be closed if parent cell is closed
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level==0)
    {
        TPPCommonTripModel *childModel = menuItem.childrenArray[0];
        if (childModel.isExpanded)
        {
            totalChildrenCount = totalChildrenCount + childModel.childrenArray.count;
            childModel.isExpanded = NO;
            
            NSIndexPath *childIndexPath = [NSIndexPath indexPathForRow:menuIndexPath.row+1 inSection:0];
            TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:childIndexPath];
            [menuToBeExpanded rotateArrowWithData:menuItem];
            
        }
    }
    
    
    
    //[self handleCellCollisionWithIndexPath:menuIndexPath];
    
    NSMutableArray * indexPathsToBeDeleted=[[NSMutableArray alloc] init];
    
    for(int i=1;i<=totalChildrenCount;i++)
    {
        [indexPathsToBeDeleted addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,totalChildrenCount)];
    
    
    [self.tableView beginUpdates];
    
    [self handleCellsForCollapseForIndexPaths:indexPathsToBeDeleted];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToBeDeleted withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [commonModelsArray removeObjectsAtIndexes:indexes];
    
    [self.tableView endUpdates];
    menuItem.isExpanded = NO;
    
}



-(void)expandMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1) // rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData: menuItem];
    }
    
    
    NSArray *subMenusArray= menuItem.childrenArray;
    if (subMenusArray.count == 0)
    {
        return;
    }
    
    NSMutableArray * indexPathsToBeAdded=[[NSMutableArray alloc]init];
    
    for(NSInteger i=1;i<=subMenusArray.count;i++)
    {
        [indexPathsToBeAdded addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,[subMenusArray count])];
    
    
    [self.tableView beginUpdates];
    
    [commonModelsArray insertObjects:subMenusArray atIndexes:indexes];
    [self.tableView insertRowsAtIndexPaths:indexPathsToBeAdded withRowAnimation:UITableViewRowAnimationAutomatic];

    [self.tableView endUpdates];
    
    menuItem.isExpanded = YES;
    
    [self.tableView scrollToRowAtIndexPath:menuIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


-(void)handleCellsForCollapseForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths)
    {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        if ([cell isKindOfClass:[TPPCompletedTripExpandedTableViewCell class]])
        {
            TPPCompletedTripExpandedTableViewCell *tempCell =  (TPPCompletedTripExpandedTableViewCell *)cell;
            
            [tempCell.moviePlayerController pause];
        }
    }
}


#pragma mark- TPPCellTapDelegate

-(void)cellTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    if (tripModel.tripStatus == TripStatus_RecommendedTrip)
    {
        TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (tripModel.tripStatus == TripStatus_CompletedTrip)
    {
        TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
   
}

-(void)loadTripFeedsLocally
{
    // to load data from local json file
    [self.tableView.refreshControl endRefreshing];
    [self.view hideLoader];
    
    NSArray *tempArray = [self getCommonModelsArrayForResponse:[self getDummyTripFeeds]];
    if (!commonModelsArray)
    {
        commonModelsArray = [[NSMutableArray alloc]init];
    }
    [commonModelsArray addObjectsFromArray:tempArray];
    [self.tableView reloadData];
}

#pragma mark- Service methods

-(void)callServiceForTripsWithTripStatus:(NSInteger)status userId:(NSString *)userId searhRequest: (NSDictionary *)requestObject
{
    if (!self.tableView.refreshControl.refreshing && pageIndex==1)
    {
        [self.view showLoader];
    }
    
    [self setTextColorOfSelectedFilter:status];
    
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           [NSNumber numberWithInteger:pageIndex],Txt_PageNumber,
                                           [NSNumber numberWithInteger:PageSize],Txt_PageSize,
                                           nil];
    if (requestObject)
    {
        mainDictionary = [requestObject mutableCopy];
        requestObject = nil;
    }
    
    
    if (status != 0)
    {
        [mainDictionary setObject:[NSNumber numberWithInteger:status] forKey:@"tripStatus"];
    }
    if (userId)
    {
        [mainDictionary setObject:[NSNumber numberWithInteger:1] forKey:@"notificationUserTrips"];
        [mainDictionary setObject:userId forKey:@"userId"];
        
    }
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:TripFeedUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         if (self.tableView.refreshControl.refreshing || pageIndex==1)
         {
             totalFeedsRecieved = 0;
             [commonModelsArray removeAllObjects];
         }
         
         self.tableView.backgroundView.hidden = YES;
         [self.tableView.refreshControl endRefreshing];
         [self.view hideLoader];
         
         isPaginationRequestInProgress = NO;
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             
             NSArray *tempArray = [self getCommonModelsArrayForResponse:response];
             
             if (!commonModelsArray)
             {
                 commonModelsArray = [[NSMutableArray alloc]init];
             }
             [commonModelsArray addObjectsFromArray:tempArray];
             
             
             totalFeedsOnServer = [response[@"response"][@"count"] integerValue];
             totalFeedsRecieved = totalFeedsRecieved + tripsArray.count;
             pageIndex++;
             
             [self.tableView reloadData];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         if (commonModelsArray.count == 0)
         {
             self.tableView.backgroundView.hidden = NO;
             noDataFoundView.messageLabel.text = errorMessage;
         }
         else
         {
             self.tableView.backgroundView.hidden = YES;
             [self showErrorMessage:errorMessage];
         }
         
         isPaginationRequestInProgress = NO;
         
         [self.tableView.refreshControl endRefreshing];
         [self.view hideLoader];
                  
     }];
}

-(void) callServiceToGetTripDetailsWithCommonTripModel:(TPPCommonTripModel *)model
{
    [self.view showLoader];
    
    //call web service to get trip details to get route
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    requestData[@"token"] = [UserDeafaults objectForKey:Token];
    requestData[@"_id"] = model.tripModel._id;
    
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_TRIP_DETAILS] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             NSError *error;
             TPPTripModel *tempTripDetailModel;
             if (tripsArray.count > 0)
             {
                 tempTripDetailModel = [[TPPTripModel alloc]initWithDictionary: [tripsArray objectAtIndex:0] error:&error];
             }
             
             if (!error)
             {
                 if (model.tripModel.tripStatus == TripStatus_UpcomingTrip)
                 {
                     TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if(model.tripModel.tripStatus == TripStatus_OngoingTrip)
                 {
                     TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if (model.tripModel.tripStatus == TripStatus_CompletedTrip)
                 {
                     TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
             }
             else
             {
                 [self.view showToasterWithMessage:@"Unable to fetch trip details"];
             }
             //[self navigateToHomeViewController];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
}



-(NSArray *)getCommonModelsArrayForResponse:(NSDictionary *)response
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *feed in response[@"response"][@"trips"])
    {
        NSError *error;
        TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:feed error:&error];
      
        if (tripModel.tripStatus == TripStatus_OngoingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_OngoingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = @[];
            
            [tempArray addObject:commonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_UpcomingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_UpcomingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForUpcomingTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_UpcomingTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_CompletedTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_CompletedTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForCompletedTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_CompletedTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_RecommendedTrip)
        {

            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_RecommendedTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
//            //Add share model
//            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
//            shareCommonTripModel.status = TripStatus_RecommendedTrip;
//            
//            [tempArray addObject:shareCommonTripModel];
        }
    }
    
    return tempArray;
}


-(TPPCommonTripModel *)getShareCommonTripModel
{
    TPPCommonTripModel *shareCommonTripModel = [[TPPCommonTripModel alloc] init];
    shareCommonTripModel.isShareModel = YES;
    shareCommonTripModel.level = 0;
    shareCommonTripModel.childrenArray = @[];
    
    return shareCommonTripModel;
}

#pragma mark- Model initialization for UpcomingTrips cell

-(NSArray *)getChildrenForUpcomingTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}



#pragma mark- Model initialization for CompletedTrips cell

-(NSArray *)getChildrenForCompletedTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}


#pragma mark- Model initialization for Recommended cell

-(NSArray *)getChildrenForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = [self getChildrenForLevel1WithModel:tripModel];

    return @[commonTripModel];
}

-(NSArray *)getChildrenForLevel1WithModel:(TPPTripModel *)tripModel
{
    NSArray *tripsArray = tripModel.totalTrips[@"trips"]; // @[@"aa",@"bb",@"cc",@"dd"];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    int i=0;
    for (NSDictionary *tripDictionary in tripsArray)
    {
        TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
        commonTripModel.status = tripModel.tripStatus;
        commonTripModel.level = 2;
        commonTripModel.tripModel = [[TPPTripModel alloc]initWithDictionary:tripDictionary error:nil];
        commonTripModel.childrenArray = @[];
        
        [tempArray addObject:commonTripModel];
        
        if (i==4)
        {
            commonTripModel.shouldShowSeeMoreButton = YES;
            break;
        }
        
        i++;
    }
    
    
    return tempArray;
}






#pragma -mark UIButton events
- (IBAction)liveTripButtonTapped:(id)sender {

	TPPSharedTripCountsModel *tripCounts = [TPPSharedTripCountsModel sharedInstance];
	if (tripCounts.onGoingTripId) {
		TPPLivePlotViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPLivePlotViewController"];
		[self.navigationController pushViewController:vc animated:YES];
	}
	else{
		[self.view showToasterWithMessage:@"Data Not found"];
	}

}

@end

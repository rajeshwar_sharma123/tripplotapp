//
//  TPPRecommendedDestinationsViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 06/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPRecommendedDestinationsViewController.h"
#import "AppDelegate.h"
#import "TPPTripDetailViewController.h"
#import "TPPOngoingTripDetailViewController.h"
#import "TPPLivePlotViewController.h"
#import "TPPContactViewController.h"


@interface TPPRecommendedDestinationsViewController ()<UITableViewDataSource,UITableViewDelegate,TPPMenuViewDelegate,TPPCellTapDelegate,TPPSeeMoreButtonDelegate,TPPShareCellDelegate,TPPTripPointTapDelegate,TPPNoDataFoundDelegate>
{
    NSMutableArray *commonModelsArray;
    
    NSInteger pageIndex;
    NSInteger totalFeedsOnServer;
    NSInteger totalFeedsRecieved;
    NSInteger tripStatus;
    
    UIRefreshControl *refreshControl;
    
    
    TPPNoDataFoundView *noDataFoundView;
    
    BOOL isPaginationRequestInProgress;
}
@end

@implementation TPPRecommendedDestinationsViewController

#pragma mark- LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // self.title = @"Recommended Destinations";
    self.navigationController.navigationBar.hidden = YES;
    
    // To update the UI if user likes/Follows/Joins etc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userActivitySelector:) name:UserActivityNotification object:nil];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    [self initView];
    [self getTripsFromServiceForTripStatus:tripStatus];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initView
{
    totalFeedsRecieved = 0;
    pageIndex = 1;
    tripStatus = TripStatus_RecommendedTrip;
    
    [self registerForCells];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(pullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refreshControl];
    
    noDataFoundView = [TPPUtilities addNoDataFoundViewInTableView:self.tableView viewController:self];
}


#pragma mark- Initialization Methods

-(void)registerForCells
{
    // LoadMoreCell
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPLoadMoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPLoadMoreTableViewCell"];
    
    
    // Recommended trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel1"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel2" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel2"];
    
}



#pragma mark- BL Methods

-(TPPCommonTripModel *)commonModelForTripModel:(TPPTripModel *)tripModel
{
    for (TPPCommonTripModel *commonModel in commonModelsArray)
    {
        if ([tripModel._id isEqualToString:commonModel.tripModel._id])
        {
            return commonModel;
        }
    }
    
    return nil;
}

-(void)pullToRefresh:(id)sender
{
    if (isPaginationRequestInProgress)
    {
        [refreshControl endRefreshing];
        return;
    }
    
    pageIndex = 1;
    totalFeedsRecieved = 0;
    tripStatus = TripStatus_RecommendedTrip;
    
    [self getTripsFromServiceForTripStatus:tripStatus];
}


-(BOOL)isMoreFeedsAvailable
{
    if(totalFeedsRecieved < totalFeedsOnServer)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}



-(TPPCommonTripModel *)getRootTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    NSInteger i = selectedCellIndex;
    for (; i>=0; i--)
    {
        TPPCommonTripModel *commonModel = [commonModelsArray objectAtIndex:i];
        if (commonModel.level == 0)
        {
            return [commonModelsArray objectAtIndex:i];
        }
    }
    
    return nil;
}

-(TPPTripModel *)getTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    TPPCommonTripModel *parentTripModel = [self getRootTripModelForSelectedIndex:selectedCellIndex];
    
    return parentTripModel.tripModel;
}


#pragma mark- TPPGoHereDelegate methods

-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:tripModel.createdBy[@"_id"]] && (tripModel.tripStatus == TripStatus_CompletedTrip))
    {
        // Navigate to Invite screen because button will be invite rather than Go here
        TPPContactViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
        
        VC.shouldEnableSelection = YES;
        VC.shouldEnableInvite = YES;
        VC.shouldChallange = YES;
        VC.trip = tripModel;
        
        TPPCreateTripModel *newTripObj = [TPPCreateTripModel sharedInstance];
        [newTripObj.addedEmailIDs removeAllObjects];
        [newTripObj.addedContacts removeAllObjects];
        
        [self.navigationController pushViewController:VC animated:YES];
        
        return;
    }
    
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
    goHereView.tripModel = tripModel;
    
    [self.view addSubview:goHereView];
}




#pragma mark- Selectors

-(void)addButtonTapped:(UIButton *)sender
{
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
     
    TPPCommonTripModel *commonModel = commonModelsArray[sender.tag];
    goHereView.tripModel = commonModel.tripModel;
    
    [self.view addSubview:goHereView];
    
}








#pragma mark- TPPNoDataFoundDelegate

-(void)retryButtonTapped
{
    [self getTripsFromServiceForTripStatus:tripStatus];
}



#pragma mark-  TPPSeeMoreDelegate

-(void)seeMoreButtonTappedWithIndexPath:(NSIndexPath *)indexPath
{
    TPPCommonTripModel *commonModel = [self getRootTripModelForSelectedIndex:indexPath.row];
    
    TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
    tripDetailViewController.tripModel = commonModel.tripModel;
    
    [self.navigationController pushViewController:tripDetailViewController animated:YES];
    
}


#pragma mark- TableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!commonModelsArray || (commonModelsArray.count > 0))
    {
        tableView.backgroundView.hidden = YES;
    }
    else
    {
        tableView.backgroundView.hidden = NO;
    }
    
    
    if((totalFeedsRecieved > 0) && [self isMoreFeedsAvailable] && !refreshControl.isRefreshing)
    {
        return commonModelsArray.count+1;
    }
    
    return commonModelsArray.count;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    // for LoadMoreCell
    if ((indexPath.row == commonModelsArray.count) && [self isMoreFeedsAvailable])
    {
        TPPLoadMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPLoadMoreTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPLoadMoreTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPLoadMoreTableViewCell"];
        }
        [cell.spinner startAnimating];
        
        return cell;
    }
    
    
    UITableViewCell *commonCell;
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    
    if (commonModel.status == TripStatus_RecommendedTrip)
    {
        commonCell = [self getRecommendedCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    
    
    return commonCell;
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (refreshControl.refreshing) // Pagination should not work if Pull-To-Refresh is working currently
    {
        return;
    }
    
    if((commonModelsArray.count == indexPath.row) && [self isMoreFeedsAvailable])
    {
        isPaginationRequestInProgress = YES;
        [self getTripsFromServiceForTripStatus:tripStatus];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // For LoadMoreCell
    if (indexPath.row == commonModelsArray.count)
    {
        return 44;
    }
    
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    
    
    if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level==0)
    {
        return 296;
    }
    else if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level == 1)
    {
        return 157;
    }
    else if(commonModel.status==TripStatus_RecommendedTrip && commonModel.level == 2)
    {
        return 57;
    }
    
    
    return 44;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCellIndex = indexPath.row;
    TPPCommonTripModel *model = [commonModelsArray objectAtIndex:selectedCellIndex];
    
    
    if (model.level == 2 && model.status==TripStatus_RecommendedTrip)
    {
        [self callServiceToGetTripDetailsWithCommonTripModel:model];
        
    }
    else if (model.childrenArray.count > 0 && !refreshControl.refreshing)
    {
        // expand or collapse
        
        if (model.isExpanded)
        {
            // Collapse submenus
            [self collapseMenuWithMenuItem:model menuIndexPath:indexPath];
        }
        else
        {
            // expand menu
            [self expandMenuWithMenuItem:model menuIndexPath:indexPath];
        }
    }
    
}



-(UITableViewCell *)getRecommendedCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPRecommendedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripTableViewCell"];
        }
        [cell.addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.addButton.tag = indexPath.row;
        //        [cell.addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        //		cell.addButton.tag =
        cell.cellTapDelegate = self;
        cell.plotTripDelegate = self;
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPRecommendedTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel1"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripCellLevel1 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel1"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    else if (commonModel.level == 2)
    {
        TPPRecommendedTripCellLevel2 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel2"];
        if (cell == nil)
        {
            cell = (TPPRecommendedTripCellLevel2 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel2"];
        }
        
        
        if (commonModel.shouldShowSeeMoreButton)
        {
            cell.seeMoreButtonDelegate = self;
            cell.seeMoreButton.hidden = NO;
        }
        else
        {
            cell.seeMoreButton.hidden = YES;
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    
    return commonCell;
}



#pragma mark- Expand/Collapse Menu

-(void)collapseMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1)// rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData:menuItem];
    }
    
    
    NSInteger totalChildrenCount = menuItem.childrenArray.count;
    if (totalChildrenCount == 0)
    {
        return;
    }
    
    // for recommended cell, child cells should also be closed if parent cell is closed
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level==0)
    {
        TPPCommonTripModel *childModel = menuItem.childrenArray[0];
        if (childModel.isExpanded)
        {
            totalChildrenCount = totalChildrenCount + childModel.childrenArray.count;
            childModel.isExpanded = NO;
            
            NSIndexPath *childIndexPath = [NSIndexPath indexPathForRow:menuIndexPath.row+1 inSection:0];
            TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:childIndexPath];
            [menuToBeExpanded rotateArrowWithData:menuItem];
            
        }
    }
    
    NSMutableArray * indexPathsToBeDeleted=[[NSMutableArray alloc] init];
    
    for(int i=1;i<=totalChildrenCount;i++)
    {
        [indexPathsToBeDeleted addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,totalChildrenCount)];
    
    
    [self.tableView beginUpdates];
    
    [self.tableView deleteRowsAtIndexPaths:indexPathsToBeDeleted withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [commonModelsArray removeObjectsAtIndexes:indexes];
    
    [self.tableView endUpdates];
    menuItem.isExpanded = NO;
    
}



-(void)expandMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1) // rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData: menuItem];
    }
    
    
    NSArray *subMenusArray= menuItem.childrenArray;
    if (subMenusArray.count == 0)
    {
        return;
    }
    
    NSMutableArray * indexPathsToBeAdded=[[NSMutableArray alloc]init];
    
    for(NSInteger i=1;i<=subMenusArray.count;i++)
    {
        [indexPathsToBeAdded addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,[subMenusArray count])];
    
    
    [self.tableView beginUpdates];
    
    [commonModelsArray insertObjects:subMenusArray atIndexes:indexes];
    [self.tableView insertRowsAtIndexPaths:indexPathsToBeAdded withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.tableView endUpdates];
    
    menuItem.isExpanded = YES;
}


#pragma mark- TPPCellTapDelegate

-(void)cellTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    if (tripModel.tripStatus == TripStatus_RecommendedTrip)
    {
        TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
}



#pragma mark- Service methods

-(void)getTripsFromServiceForTripStatus:(NSInteger)status
{
    if (!refreshControl.refreshing && pageIndex==1)
    {
        [self.view showLoader];
    }
    
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                            self.locationString, @"location",
                                           [NSNumber numberWithInteger:pageIndex],Txt_PageNumber,
                                           [NSNumber numberWithInteger:PageSize],Txt_PageSize,
                                           nil];
    
    if (status != 0)
    {
        [mainDictionary setObject:[NSNumber numberWithInteger:status] forKey:@"tripStatus"];
    }
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:RecommendedTripsByLocationUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         if (refreshControl.refreshing)
         {
             totalFeedsRecieved = 0;
             [commonModelsArray removeAllObjects];
         }
         
         self.tableView.backgroundView.hidden = YES;
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
         isPaginationRequestInProgress = NO;
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             
             NSArray *tempArray = [self getCommonModelsArrayForResponse:response];
             
             if (!commonModelsArray)
             {
                 commonModelsArray = [[NSMutableArray alloc]init];
             }
             [commonModelsArray addObjectsFromArray:tempArray];
             
             
             totalFeedsOnServer = [response[@"response"][@"count"] integerValue];
             totalFeedsRecieved = totalFeedsRecieved + tripsArray.count;
             pageIndex++;
             
             [self.tableView reloadData];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         if (commonModelsArray.count == 0)
         {
             self.tableView.backgroundView.hidden = NO;
             noDataFoundView.messageLabel.text = errorMessage;
         }
         else
         {
             self.tableView.backgroundView.hidden = YES;
             [self showErrorMessage:errorMessage];
         }
         
         isPaginationRequestInProgress = NO;
         
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
     }];
}




-(NSArray *)getCommonModelsArrayForResponse:(NSDictionary *)response
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *feed in response[@"response"][@"trips"])
    {
        NSError *error;
        TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:feed error:&error];
        
         if (tripModel.tripStatus == TripStatus_RecommendedTrip)
        {
            
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_RecommendedTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //            //Add share model
            //            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            //            shareCommonTripModel.status = TripStatus_RecommendedTrip;
            //
            //            [tempArray addObject:shareCommonTripModel];
        }
    }
    
    return tempArray;
}

#pragma mark- Model initialization for UpcomingTrips cell

-(NSArray *)getChildrenForUpcomingTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}





#pragma mark- Model initialization for Recommended cell

-(NSArray *)getChildrenForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = [self getChildrenForLevel1WithModel:tripModel];
    
    return @[commonTripModel];
}

-(NSArray *)getChildrenForLevel1WithModel:(TPPTripModel *)tripModel
{
    NSArray *tripsArray = tripModel.totalTrips[@"trips"]; // @[@"aa",@"bb",@"cc",@"dd"];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    int i=0;
    for (NSDictionary *tripDictionary in tripsArray)
    {
        TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
        commonTripModel.status = tripModel.tripStatus;
        commonTripModel.level = 2;
        commonTripModel.tripModel = [[TPPTripModel alloc]initWithDictionary:tripDictionary error:nil];
        commonTripModel.childrenArray = @[];
        
        [tempArray addObject:commonTripModel];
        
        if (i==4)
        {
            commonTripModel.shouldShowSeeMoreButton = YES;
            break;
        }
        
        i++;
    }
    
    
    return tempArray;
}


-(void) callServiceToGetTripDetailsWithCommonTripModel:(TPPCommonTripModel *)model
{
    [self.view showLoader];
    
    //call web service to get trip details to get route
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    requestData[@"token"] = [UserDeafaults objectForKey:Token];
    requestData[@"_id"] = model.tripModel._id;
    
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_TRIP_DETAILS] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             NSError *error;
             TPPTripModel *tempTripDetailModel;
             if (tripsArray.count > 0)
             {
                 tempTripDetailModel = [[TPPTripModel alloc]initWithDictionary: [tripsArray objectAtIndex:0] error:&error];
             }
             
             if (!error)
             {
                 if (model.tripModel.tripStatus == TripStatus_UpcomingTrip)
                 {
                     TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if(model.tripModel.tripStatus == TripStatus_OngoingTrip)
                 {
                     TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if (model.tripModel.tripStatus == TripStatus_CompletedTrip)
                 {
                     TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
             }
             else
             {
                 [self.view showToasterWithMessage:@"Unable to fetch trip details"];
             }
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end










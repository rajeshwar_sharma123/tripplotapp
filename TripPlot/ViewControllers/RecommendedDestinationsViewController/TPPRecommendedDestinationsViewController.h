//
//  TPPRecommendedDestinationsViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 06/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPRecommendedDestinationsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSString *locationString;

@end

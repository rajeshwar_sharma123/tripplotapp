
//  TPPLivePlotViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TPPVerticalMenu.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AWSCore/AWSClientContext.h>
#import <AWSS3/AWSS3TransferUtility.h>
#import "TPPReachedDestinationView.h"
#import "TPPPlaceDetailView.h"
#import "TPPLocationDynamicMenuView.h"

@interface TPPLivePlotViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate,TPPMenuViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,QBImagePickerControllerDelegate,TPPPlaceDetailDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIView *bottomMenuView;
@property (strong,nonatomic) NSMutableArray *amazonImgURLArry;
@property (strong,nonatomic) NSMutableArray *fileNameArry;
@property (strong,nonatomic) CLLocation *currentLocation;

@end



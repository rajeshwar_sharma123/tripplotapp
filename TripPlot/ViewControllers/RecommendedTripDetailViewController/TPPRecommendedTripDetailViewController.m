//
//  TPPRecommendedTripDetailViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 13/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPRecommendedTripDetailViewController.h"
#import "TPPTripDetailViewController.h"
#import "TPPContactViewController.h"
#import "TPPOngoingTripDetailViewController.h"

@interface TPPRecommendedTripDetailViewController ()<UITableViewDataSource,UITableViewDelegate,TPPMenuViewDelegate,TPPCellTapDelegate,TPPSeeMoreButtonDelegate,TPPShareCellDelegate,TPPTripPointTapDelegate,TPPNoDataFoundDelegate>

@end

@implementation TPPRecommendedTripDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    // Recommended trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel1"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPRecommendedTripCellLevel2" bundle:nil] forCellReuseIdentifier:@"TPPRecommendedTripCellLevel2"];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
	 self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
	self.navigationController.navigationBar.hidden = NO;
}


#pragma mark - UITableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *trips = _tripModel.totalTrips[@"trips"];
    
    return trips.count + 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *commonCell;
    
    if (indexPath.row == 0)
    {
        TPPRecommendedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripTableViewCell"];
        if (cell == nil)
        {
            cell = [[TPPRecommendedTripTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripTableViewCell"];
        }
        [cell.addButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.addButton.tag = indexPath.row;
        cell.cellTapDelegate = self;
        cell.plotTripDelegate = self;
        [cell hideHeader];
        
        [cell bindDataWithModel:self.tripModel];
        
        commonCell = cell;
    }
    else if (indexPath.row == 1)
    {
        TPPRecommendedTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel1"];
        if (cell == nil)
        {
            cell = [[TPPRecommendedTripCellLevel1 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel1"];
        }
        cell.arrowImageView.hidden = YES;
        [cell bindDataWithModel:self.tripModel];
        
        commonCell = cell;

    }
    else
    {
        TPPRecommendedTripCellLevel2 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPRecommendedTripCellLevel2"];
        if (cell == nil)
        {
            cell = [[TPPRecommendedTripCellLevel2 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPRecommendedTripCellLevel2"];
        }
        
        TPPTripModel *model = [[TPPTripModel alloc]initWithDictionary:_tripModel.totalTrips[@"trips"][indexPath.row-2] error:nil];
        
        cell.seeMoreButton.hidden = YES;
        [cell bindDataWithModel:model];
       
        commonCell = cell;
    }
    
    return commonCell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row>1)
    {
        TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:_tripModel.totalTrips[@"trips"][indexPath.row-2] error:nil];
        
        [self callServiceToGetTripDetailsWithTripModel:tripModel];
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 292-56;
    }
    else if (indexPath.row == 1) {
        return 157;
    }
    else
    {
        return 57;
    }
}





#pragma mark - IBAction Methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- Selectors

-(void)addButtonTapped:(UIButton *)sender
{
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
    goHereView.tripModel = _tripModel;
    
    [self.view addSubview:goHereView];
    
}


#pragma mark- TPPGoHereDelegate methods

-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:tripModel.createdBy[@"_id"]] && (tripModel.tripStatus == TripStatus_CompletedTrip))
    {
        // Navigate to Invite screen because button will be invite rather than Go here
        TPPContactViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
        
        VC.shouldEnableSelection = YES;
        VC.shouldEnableInvite = YES;
        VC.shouldChallange = YES;
        VC.trip = tripModel;
        
        TPPCreateTripModel *newTripObj = [TPPCreateTripModel sharedInstance];
        [newTripObj.addedEmailIDs removeAllObjects];
        [newTripObj.addedContacts removeAllObjects];
        
        [self.navigationController pushViewController:VC animated:YES];
        
        return;
    }
    
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
    goHereView.tripModel = tripModel;
   
    [self.view addSubview:goHereView];
}


-(void) callServiceToGetTripDetailsWithTripModel:(TPPTripModel *)tripModel
{
    [self.view showLoader];
    
    //call web service to get trip details to get route
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    requestData[@"token"] = [UserDeafaults objectForKey:Token];
    requestData[@"_id"] = tripModel._id;
    
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_TRIP_DETAILS] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             NSError *error;
             TPPTripModel *tempTripDetailModel;
             if (tripsArray.count > 0)
             {
                 tempTripDetailModel = [[TPPTripModel alloc]initWithDictionary: [tripsArray objectAtIndex:0] error:&error];
             }
             
             if (!error)
             {
                 if (tripModel.tripStatus == TripStatus_UpcomingTrip)
                 {
                     TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if(tripModel.tripStatus == TripStatus_OngoingTrip)
                 {
                     TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if (tripModel.tripStatus == TripStatus_CompletedTrip)
                 {
                     TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
             }
             else
             {
                 [self.view showToasterWithMessage:@"Unable to fetch trip details"];
             }
             //[self navigateToHomeViewController];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
}




@end

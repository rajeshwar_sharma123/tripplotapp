//
//  TPPRecommendedTripDetailViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 13/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPRecommendedTripDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property TPPTripModel *tripModel;

@end

//
//  TPPLocationTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPLocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ratingImageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIButton *addRemoveButton;

@property (weak, nonatomic) NSString *placeId;
@property NSInteger recordIndex;
@property BOOL shouldAdd;


@end

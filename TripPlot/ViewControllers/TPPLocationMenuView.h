//
//  TPPLocationMenuView.h
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPLocationMenuView : UIView
@property (weak, nonatomic) IBOutlet UIButton *atmButton;
@property (weak, nonatomic) IBOutlet UIButton *hospitalButton;
@property (weak, nonatomic) IBOutlet UIButton *stopageButton;
@property (weak, nonatomic) IBOutlet UIButton *hotelButton;
@property (weak, nonatomic) IBOutlet UIButton *restaurantButton;
@property (weak, nonatomic) IBOutlet UIButton *fuelStationButton;

-(void)resetAllMenuButtonsImage:(TPPLocationMenuView *)menuView;
-(void)setBadgesWithTripModel:(TPPTripModel *)tripModel;

@end

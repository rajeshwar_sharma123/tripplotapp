//
//  TPPLogInViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPLogInViewController.h"
#import "TPPHomeViewController.h"
#import "TPPRegistrationViewController.h"
#import "TPPForgotPasswordViewController.h"
#import "TPPForgotPasswordView.h"
#import "TPPLeftMenuViewController.h"
#import "IQUIView+IQKeyboardToolbar.h"

@interface TPPLogInViewController ()
{
    IQKeyboardReturnKeyHandler *returnKeyHandler;

}
@end


@implementation TPPLogInViewController

#pragma mark- LifeCycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    TPPLeftMenuViewController *leftMenuVc = (TPPLeftMenuViewController *)self.menuContainerViewController.leftMenuViewController;
    leftMenuVc.logInViewController = self;

	self.passwordTextField.layer.cornerRadius = 2.0f;
	self.passwordTextField.clipsToBounds = YES;
	UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 40)];
	self.passwordTextField.leftView = paddingView1;
	self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;

	UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 40)];
	self.emailTextField.leftView = paddingView2;
	self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
	self.emailTextField.layer.cornerRadius = 2.0f;
	self.emailTextField.clipsToBounds = YES;


	UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 42, 32)];
	[rightButton setImage:[UIImage imageNamed:@"btn_retrievepass_normal"] forState:UIControlStateNormal];

	[rightButton addTarget:self action:@selector(rightButtonTapped) forControlEvents:UIControlEventTouchUpInside];

	[self.passwordTextField setRightViewMode:UITextFieldViewModeAlways];
	[self.passwordTextField setRightView:rightButton];


   // returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    //[self.passwordTextField setEnablePrevious:YES next:NO];
    [self.passwordTextField addDoneOnKeyboardWithTarget:self action:@selector(logInButtonTapped:) shouldShowPlaceholder:YES];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- TPPTextFieldDelegate Methods

-(void)rightButtonTapped
{
//    TPPForgotPasswordViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"TPPForgotPasswordViewController"];
//    vc.email = _emailTextField.text;
//    
//    [self.navigationController pushViewController:vc animated:YES];

	NSArray *nibContents2 = [[NSBundle mainBundle] loadNibNamed:@"TPPForgotPasswordView" owner:nil options:nil];
	 TPPForgotPasswordView *popupView = nibContents2[0];
	[popupView setFrame:self.view.frame];
	CALayer *bottomBorder = [CALayer layer];
	bottomBorder.frame = CGRectMake(0.0f, popupView.emailTextField.frame.size.height - 2, popupView.emailTextField.frame.size.width, 2.0f);
	bottomBorder.backgroundColor = COLOR_ORANGE.CGColor;
	[popupView.emailTextField.layer addSublayer:bottomBorder];
	[popupView.emailTextField becomeFirstResponder];// = YES;
    popupView.emailTextField.text = self.emailTextField.text;
		//popupView.backgroundColor = [UIColor clearColor];
	popupView.parentVC = self;
	[self.view addSubview:popupView];

}


#pragma mark- BL Methods

-(BOOL)isUserInputsValid
{
    if (self.emailTextField.text.trimmedString.length == 0)
    {
        [self showErrorMessage:EMAIL_EMPTY];
        
        return NO;
    }
    if(!self.emailTextField.text.isValidEmail)
    {
        [self showErrorMessage:EMAIL_INVALID];
        return NO;
    }
    else if(self.passwordTextField.text.trimmedString.length == 0)
    {
        [self showErrorMessage:PASSWORD_EMPTY];
        return NO;
    }

    return YES;
}

-(void)clearTextFields
{
    self.emailTextField.text = @"";
    self.passwordTextField.text = @"";
    
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

#pragma mark-



#pragma mark- FB Delegate Methods

-(void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    [UserDeafaults removeObjectForKey:Token];
}


#pragma mark- IBAction Methods

- (IBAction)logInButtonTapped:(id)sender
{
    BOOL isUserInputsValid = [self isUserInputsValid];
    if (isUserInputsValid)
    {
        [self.view showLoader];
        
        NSDictionary *credentialsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                               self.emailTextField.text.trimmedString,@"username",
                                               self.passwordTextField.text.trimmedString, @"password",
                                               nil];
       NSDictionary *mainDictionary = [NSDictionary dictionaryWithObject:credentialsDictionary forKey:@"options"];
        
        
        [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:LogInUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
        {
            [self.view hideLoader];
            
            if ([[response objectForKey:@"code"] integerValue] == 200)
            {
                [self clearTextFields];
                
                [TPPUtilities saveUserProfile: response[@"response"][@"user"]];
                [TPPUtilities saveUserToken: response[@"response"][@"token"]];
                
                [TPPAppServices callServiceToRegisterDeviceForPushNotifications:YES];
                
                [self navigateToHomeViewController];
            }
           else
           {
               
           }
            
        } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
        {
            [self.view hideLoader];
            
            [self showErrorMessage:errorMessage];
            
        }];
    }
}

- (IBAction)signUpButtonTapped:(id)sender
{
    [self pushViewControllerWithIdentifier:@"TPPRegistrationViewController"];
}

- (IBAction)twitterLogInButtonTapped:(id)sender
{
    [self.view showLoader];
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
    {
        if (session)
        {
            NSLog(@"signed in as %@", [session userName]);

            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"twitter", @"provider",
                                  session.authToken, @"access_token",
                                  session.authTokenSecret, @"access_token_secret",
                                  nil];
            NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:dict forKey:@"options"];
            
            [self socialLoginWithCredentials:optionsDictionary];
        }
        else
        {
            [self.view hideLoader];
            [self showErrorMessage:error.localizedDescription];
        }
    }];
    
}
    

    
//    //Get a reference to the application delegate.
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    //Get Twitter account, stored in on the device, for the first time.
//    [appDelegate getTwitterAccountOnCompletion:^(ACAccount *twitterAccount){
//        //If we successfully retrieved a Twitter account
//        if(twitterAccount)
//        {
//            //Make sure anything UI related happens on the main queue
//            dispatch_async(dispatch_get_main_queue(), ^{
//            
//            });
//        }
//    }];





- (IBAction)facebookLogInButtonTapped:(id)sender
{
    [self.view showLoader];
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    
    [loginManager logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error || (result.token == nil))
        {
            [self.view hideLoader];
        }
        else
        {
            NSString *facebookToken = result.token.tokenString;
            if(facebookToken)
            {
                // Token created successfully and you are ready to get profile info
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                 {
                     if (!error)
                     {
                         NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                               facebookToken, @"access_token",
                                               @"facebook", @"provider",
                                               nil];
                         
                         NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:dict forKey:@"options"];
                         
                         [self socialLoginWithCredentials:optionsDictionary];
                     }
                     else
                     {
                         [self.view hideLoader];
                     }
                 }];
            }
            else
            {
                [self.view hideLoader];
            }
        }
    }];

}




@end

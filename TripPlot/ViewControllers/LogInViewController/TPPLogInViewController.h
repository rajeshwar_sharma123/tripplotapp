//
//  TPPLogInViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <TwitterKit/TwitterKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface TPPLogInViewController : TPPBaseViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet TWTRLogInButton *twitterLogInButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookLogInButton;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;


- (IBAction)facebookLogInButtonTapped:(id)sender;


@end

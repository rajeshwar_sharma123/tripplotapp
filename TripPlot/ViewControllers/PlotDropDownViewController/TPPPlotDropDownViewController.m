//
//  TPPPlotDropDownViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPPlotDropDownViewController.h"
#import "TPPPlotDropDownTableViewCell.h"
#import "TPPCreateNewTripViewController.h"
#import "TPPCompletedTripDetailViewController.h"

@interface TPPPlotDropDownViewController ()
{
    __weak IBOutlet UITableView *mainTableView;
    TPPCreateTripModel *newTripObject;
    NSMutableArray *completedTripArry;
    
    __weak IBOutlet UIView *plotNewView;
    __weak IBOutlet UIView *plotPastView;
    
}
@end

@implementation TPPPlotDropDownViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [TPPCreateTripModel clearInstance];
    
    
    // Do any additional setup after loading the view.
    mainTableView.layer.cornerRadius = 6.0;
    mainTableView.clipsToBounds = YES;
    
    plotNewView.layer.cornerRadius = 4.5;
    plotNewView.clipsToBounds = YES;
    plotPastView.layer.cornerRadius = 4.5;
    plotPastView.clipsToBounds = YES;

    
    completedTripArry = [NSMutableArray array];
    [self getCompletedTripsFromService];
    newTripObject = [TPPCreateTripModel sharedInstance];
    newTripObject.addedContacts = [NSMutableArray new];
    newTripObject.addedEmailIDs = [NSMutableArray new];
    
    NSDictionary *profileDetail = [UserDeafaults objectForKey:UserProfile];
    newTripObject.token = [UserDeafaults objectForKey:Token];
    newTripObject.username = profileDetail[@"emailid"];
    newTripObject.profileImage = profileDetail[@"profileImage"];
    newTripObject._id = profileDetail[@"_id"];
    newTripObject.firstname = profileDetail[@"firstname"];
    newTripObject.createdBy = profileDetail[@"_id"];
    mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Table View DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return completedTripArry.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *plotDropDownIdentifiere = @"plotDropDownCell";
    TPPPlotDropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:plotDropDownIdentifiere];
    if (cell == nil) {
        cell = [[TPPPlotDropDownTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:plotDropDownIdentifiere];
    }
    [cell bindDataWithModel:[completedTripArry objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexpath %ld",(long)indexPath.row);
    TPPCompletedTripDetailViewController *completedTrip = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
    completedTrip.tripModel = [completedTripArry objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:completedTrip animated:YES];

}


#pragma mark - Custom Method

- (IBAction)plotNewTapGestureAction:(id)sender {
      NSLog(@"New Tab  clicked");

    [_delegate hideDropDownView];
    newTripObject.isNew = YES;
	newTripObject.tripStatus = TripStatus_UpcomingTrip;

    TPPCreateNewTripViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
    
    [self.navigationController pushViewController:VC animated:YES];
    
    
}
- (IBAction)plotPastTapGestureAction:(id)sender {
    NSLog(@"Past Tab  clicked");
    [_delegate hideDropDownView];
    newTripObject.isNew = NO;
	newTripObject.tripStatus = TripStatus_CompletedTrip;

    TPPCreateNewTripViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCreateNewTripViewController"];
    [self.navigationController pushViewController:VC animated:YES];
}


- (void)getCompletedTripsFromService
{
    [mainTableView showLoader];
    NSDictionary *mainDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UserDeafaults objectForKey:Token],@"token",
                                    [NSNumber numberWithInteger:TripStatus_CompletedTrip],@"tripStatus",
                                   
                                    nil];
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:TripFeedUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [mainTableView hideLoader];

         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tmp =  response[@"response"][@"trips"];
             for (NSDictionary *dic in tmp) {
                 NSError *error;
                 TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:dic error:&error];
                 [completedTripArry addObject:tripModel];
             }
             
             [mainTableView reloadData];
           
         }
         else
         {
             //for fure purpose
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
        
         [self.view hideLoader];
         
         //[self showErrorMessage:errorMessage];
     }];
    
    

}

-(NSDictionary *)getDummyTripFeeds
{
    ///fetch dummy data of food items
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"TripFeedsJson" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return jsonDictionary;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

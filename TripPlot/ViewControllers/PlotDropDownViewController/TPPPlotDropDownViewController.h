//
//  TPPPlotDropDownViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TPPPlotDropDownViewController : UIViewController

@property (weak) id<TPPDropDownViewDelegate> delegate;
@end


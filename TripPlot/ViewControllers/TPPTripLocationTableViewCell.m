//
//  TPPTripLocationTableViewCell.m
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPTripLocationTableViewCell.h"

@implementation TPPTripLocationTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.toLabel.layer.cornerRadius = 4.0;
    self.toLabel.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

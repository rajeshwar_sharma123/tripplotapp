//
//  TPPTripTitleView.h
//  TripPlot
//
//  Created by Daffolap-21 on 06/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPTripTitleView : UIView
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

@end

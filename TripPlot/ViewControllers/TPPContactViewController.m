//
//  TPPContactViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 19/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPContactViewController.h"
#import "TPPEmailListingViewController.h"
#import "TPPContactListingViewController.h"


@interface TPPContactViewController ()

@end

@implementation TPPContactViewController
{
    TPPEmailListingViewController* mailVC;
    TPPContactListingViewController* contactListingVC;
    TPPCreateTripModel *newTripObject;
}
- (void)viewDidLoad {
   // [super viewDidLoad];
    // Do any additional setup after loading the view.
    newTripObject = [TPPCreateTripModel sharedInstance];
    if (newTripObject.addedEmailIDs == nil || newTripObject.addedContacts == nil) {
        newTripObject.addedContacts = [NSMutableArray new];
        newTripObject.addedEmailIDs = [NSMutableArray new];
    }
   

   // if (self.shouldEnableInvite) {
        [self addInviteButton];
   // }

    if (newTripObject.addedEmailIDs.count>0)
    {
        self.emailButton.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)newTripObject.addedEmailIDs.count];
    }
    if (newTripObject.addedContacts.count>0) {
    self.contactButton.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)newTripObject.addedContacts.count];
    }

    [self.contactButton setBadgePostion:BadgePositionon_MiddleRight];
    [self.emailButton setBadgePostion:BadgePositionon_MiddleRight];

    [self.contactButton setTitleColor:TPPTextColor forState:UIControlStateNormal];
    [self.emailButton setTitleColor:TPPTextColor forState:UIControlStateNormal];

    [self.contactButton setTitleColor:COLOR_ORANGE forState:UIControlStateSelected];
    [self.emailButton setTitleColor:COLOR_ORANGE forState:UIControlStateSelected];

    [self.contactButton setTitleColor:COLOR_ORANGE forState:UIControlStateHighlighted];
    [self.emailButton setTitleColor:COLOR_ORANGE forState:UIControlStateHighlighted];

    //start c 84, 77 ,4
    // mid 75, 68, 62
    //end clr 67, 60 , 54

    self.menuView.gradientOrientation = GRKGradientOrientationDown;
    self.menuView.gradientColors = [NSArray arrayWithObjects:RgbToUIColor(75, 68, 62), RgbToUIColor(67, 60, 54),nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.shouldShowMailListing) {
        [self mailButtonTapped:nil];
    }
    else
    {
        [self.view showLoader];
        [self contactButtonTapped:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.view hideLoader];
    if (self.shouldShowMailListing) {
        [self mailButtonTapped:nil];
    }
    else
    {
        [self contactButtonTapped:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)contactButtonTapped:(id)sender {

    [mailVC removeFromParentViewController];
    [self addContactListingVC];
    [self.contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];   [self.emailButton setTitleColor:TPPTextColor forState:UIControlStateNormal];
}

- (IBAction)mailButtonTapped:(id)sender {

    [contactListingVC removeFromParentViewController];
    [self addMailListingVC];
    [self.contactButton setTitleColor:TPPTextColor forState:UIControlStateNormal];
    [self.emailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(void)addMailListingVC
{
  mailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPEmailListingViewController"];
    mailVC.shouldEnableSelection = self.shouldEnableSelection;
    [self addChildViewController:mailVC];
    mailVC.view.frame = self.contentView.frame;
    [self.view addSubview:mailVC.view];
    [mailVC didMoveToParentViewController:self];
}

-(void)addContactListingVC
{
     contactListingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactListingViewController"];
    contactListingVC.shouldEnableSelection = self.shouldEnableSelection;
    [self addChildViewController:contactListingVC];
    contactListingVC.view.frame = self.contentView.frame;
    [self.view addSubview:contactListingVC.view];
    [contactListingVC didMoveToParentViewController:self];
    [contactListingVC.tableView reloadData];
}

-(void) addInviteButton
{
    UIBarButtonItem *inviteButton;
    inviteButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"btn_confirm"] style:UIBarButtonItemStyleDone target:self action:@selector(inviteFriendsTapped:)];
    inviteButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = inviteButton;
    [self enableRightBarButton];
}

-(void)enableRightBarButton
{
    if (newTripObject.addedEmailIDs.count + newTripObject.addedContacts.count > 0) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

-(void)inviteFriendsTapped:(id)sender
{
    NSLog(@"invite button tapped");
    
    if (self.shouldEnableInvite) {
        NSMutableDictionary *request = [NSMutableDictionary new];
        request[@"tripId"]= self.trip._id;
        request[@"token"] = [UserDeafaults objectForKey:Token];
        request[@"tripTitle"] = self.trip.title;
        
        if (self.shouldChallange) {
            request[@"challengeTo"] = [newTripObject convertContactsToJSONModel];
            request[@"challengeFrom"] = self.trip.createdBy[@"_id"];
            request[@"createdByFirstname"] = self.trip.createdBy[@"firstname"];
            [self callServiceToChallenge:request];
        }
        else{
            request[@"tripMembers"] = [newTripObject convertContactsToJSONModel];
            request[@"createdById"] = self.trip.createdBy[@"_id"];
            [self callServiceToInvite:request];
        }
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }

}

-(void) callServiceToInvite:(NSMutableDictionary *) requestData
{

	[self.view showLoader];
	[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_INVITE] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 [self.view hideLoader];

		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {

			 newTripObject.addedEmailIDs = [NSMutableArray new];
			 newTripObject.addedContacts = [NSMutableArray new];

			[self showSuccessMessage:MSG_FOR_INVITE];
			 [self.navigationController popViewControllerAnimated:YES];
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 [self.view hideLoader];

		 newTripObject.addedEmailIDs = [NSMutableArray new];
		 newTripObject.addedContacts = [NSMutableArray new];
		 [self showErrorMessage:errorMessage];
		 [self.navigationController popViewControllerAnimated:YES];

	 }];
}

-(void) callServiceToChallenge:(NSMutableDictionary *) requestData
{

	[self.view showLoader];
	[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_CHALLANGE] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 [self.view hideLoader];

		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 newTripObject.addedEmailIDs = [NSMutableArray new];
			 newTripObject.addedContacts = [NSMutableArray new];
			 [self showSuccessMessage:MSG_FOR_CHALLANGE];
			 [self.navigationController popViewControllerAnimated:YES];
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 [self.view hideLoader];
		 newTripObject.addedEmailIDs = [NSMutableArray new];
		 newTripObject.addedContacts = [NSMutableArray new];

		 [self showErrorMessage:errorMessage];
		 [self.navigationController popViewControllerAnimated:YES];
		 
	 }];
}
@end

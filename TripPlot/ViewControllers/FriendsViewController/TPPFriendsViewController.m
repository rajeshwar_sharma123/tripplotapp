//
//  TPPFriendsViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPFriendsViewController.h"
#import "TPPAddEmailPopupView.h"

@interface TPPFriendsViewController ()

@end

@implementation TPPFriendsViewController
{
    TPPAddEmailPopupView *popup;
    NSMutableDictionary *request;
    NSMutableArray *emailList;
    TPPCreateTripModel *newTripObject;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // Do any additional setup after loading the view.
    newTripObject = [TPPCreateTripModel sharedInstance];
    // selectedArray = [NSMutableArray new];
    emailList = [NSMutableArray new];
    [self configureAddButton];
    request = [NSMutableDictionary new];
    request[@"token"] = [UserDeafaults objectForKey:Token];
    request[@"_id"] = [UserDeafaults objectForKey:UserProfile][@"_id"];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [self callServiceToGetMailIDs:request];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma -mark UITableView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return emailList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* simpleTableIdentifier = @"defaultCell";

    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }

    cell.textLabel.text = [NSString stringWithFormat:@"%@",[emailList objectAtIndex:indexPath.row]];
    cell.textLabel.textColor = [UIColor whiteColor];
   // cell.detailTextLabel.text = [NSString stringWithFormat:@"subtitle: %ld",(long)indexPath.section];
   // cell.imageView.image = [UIImage imageNamed:@"avatar_tripplot"];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    [cell layoutIfNeeded];
    return cell;
}


#pragma -mark Utility methods
-(void)configureAddButton
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width-48;
    CGFloat screenHeight = screenRect.size.height-160;
    UIButton *goToTop = [UIButton buttonWithType:UIButtonTypeCustom];
    goToTop.frame = CGRectMake(screenWidth, screenHeight, 40, 40);
    [goToTop setTitle:@"+" forState:UIControlStateNormal];
    [goToTop addTarget:self action:@selector(showAddPopup:) forControlEvents:UIControlEventTouchUpInside];
    [goToTop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // [goToTop setBackgroundColor:[UIColor blackColor]  forState:UIControlStateNormal];
    goToTop.backgroundColor = [UIColor blackColor];
    [goToTop setTitleColor:COLOR_ORANGE forState:UIControlStateHighlighted];
    [goToTop.layer setBorderColor:[[UIColor blackColor] CGColor]];
    goToTop.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30];
    goToTop.layer.cornerRadius = goToTop.frame.size.height/2;
    goToTop.clipsToBounds = YES;
    //[self.view insertSubview:goToTop aboveSubview:self.tableView];
    [self.view addSubview:goToTop];
    [goToTop bringSubviewToFront:self.tableView];
}

-(void)showAddPopup:(id) sender
{
    NSArray *nibContents2 = [[NSBundle mainBundle] loadNibNamed:@"TPPAddEmailPopupView" owner:nil options:nil];
    popup = nibContents2[0];
    [popup setFrame:self.view.frame];
    [popup.doneButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:popup];
}

-(void)addButtonTapped:(UIButton *)sender
{
    if ([emailList isExistString:popup.textField.text.trimmedString] >= 0) {
        [self showErrorMessage:MSG_FOR_EMAIL_EXISTS];
    }
    else{
        if ([popup.textField.text.trimmedString length]>0 && [popup.textField.text isValidEmail]) {
            NSArray *tempMails = @[popup.textField.text.trimmedString];
            request[@"emailList"] = tempMails;
            [self callServiceToSaveMailID:request];
        }
        else
        {
            [self showErrorMessage:EMAIL_INVALID];
        }
    }

    NSLog(@"add button tapped: %@",popup.textField.text);
}



-(void) callServiceToSaveMailID:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_SAVE_EMAIL_LIST] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];

         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             //searchLocation = response[@"response"][@"result"];

             [emailList addObject:popup.textField.text.trimmedString];
             [popup removeFromSuperview];
             [self.tableView reloadData];

         }
         else
         {

         }

     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];


     }];
}

-(void) callServiceToGetMailIDs:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_EMAIL_LIST] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];

         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSDictionary *tempResult = [response[@"response"][@"result"] objectAtIndex:0];
             emailList = tempResult[@"emaillist"];
             [self.tableView reloadData];

         }
         else
         {

         }

     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];
         
         
     }];
}
@end

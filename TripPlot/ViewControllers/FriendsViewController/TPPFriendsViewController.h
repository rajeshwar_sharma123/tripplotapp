//
//  TPPFriendsViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPFriendsViewController : TPPBaseViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

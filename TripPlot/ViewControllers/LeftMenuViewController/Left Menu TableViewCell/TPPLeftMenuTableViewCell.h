//
//  TPPLeftMenuTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 05/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPLeftMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;

-(void)setbadgeValue:(NSInteger)value;
-(void) addBadgeWithValue:(int)value;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewLeadingConstant;

@end

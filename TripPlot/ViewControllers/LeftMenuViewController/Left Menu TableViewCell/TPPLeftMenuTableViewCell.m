//
//  TPPLeftMenuTableViewCell.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 05/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPLeftMenuTableViewCell.h"

@implementation TPPLeftMenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [self.badgeLabel makeCircular];
    self.tableView.separatorColor = COLOR_ORANGE;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setbadgeValue:(NSInteger)value
{
    _badgeLabel.hidden = NO;
    if (value>99)
    {
        _badgeLabel.text = [NSString stringWithFormat:@"%ld",value];
        [_badgeLabel sizeToFit];
    }
    else if(value>0)
    {
        _badgeLabel.text = [NSString stringWithFormat:@"%ld",value];
    }
    else
    {
        _badgeLabel.hidden = YES;
    }
}
-(void) addBadgeWithValue:(int)value
{
	if (value > 0)
    {
		UILabel *badgeLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width -50,self.frame.size.height/2 - 10, 20, 20)];
		badgeLabel.backgroundColor = [UIColor redColor];
		badgeLabel.textColor = [UIColor whiteColor];
		badgeLabel.layer.cornerRadius = 10;
		badgeLabel.clipsToBounds = YES;
		badgeLabel.textAlignment = NSTextAlignmentCenter;
		if (value>99) {
			badgeLabel.text = [NSString stringWithFormat:@"%d",value];
			[badgeLabel sizeToFit];
		}
		else
		{
			badgeLabel.text = [NSString stringWithFormat:@"%d",value];
		}
			//[badgeLabel sizeToFit];
		[self.contentView addSubview:badgeLabel];
	}
}

@end

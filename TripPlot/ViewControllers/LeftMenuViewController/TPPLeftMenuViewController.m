//
//  TPPLeftMenuViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//


#import "TPPLeftMenuViewController.h"

@interface TPPLeftMenuViewController ()
{
    NSArray *menuContentsArray;
    
    TPPFriendsViewController *friendsViewController;
    TPPInformationViewController *informationViewController;
    TPPInvitesViewController *invitesViewController;
    TPPRentVehicleViewController *rentVehicleViewController;
    TPPSettingsViewController *settingsViewController;
    TPPTermsViewController *termsViewController;
    
    TPPPageMenuBaseViewController *pageMenuBaseViewController;
    
    TPPUserProfileModel *userProfileModel;
    UIImageView *addressImgView;
	TPPSharedTripCountsModel *tripCounts;
    
}

@end

@implementation TPPLeftMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    menuContentsArray = @[@"TimeLine",@"Trips" ,@"Friends" ,@"Invites" ,@"Information" ,@"Settings" ,@"Terms & conditions" ,@"Sign out"];
    addressImgView = [[UIImageView alloc]init];
    addressImgView.image = [UIImage imageNamed:@"icn_location"];
    addressImgView.backgroundColor = [UIColor clearColor];
    [_userAddressLabel addSubview:addressImgView];

	tripCounts = [TPPSharedTripCountsModel sharedInstance];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMenu:) name:@"LeftSideMenuNotification" object:nil];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sideMenuStateChangedSelector:) name:@"MFSideMenuStateNotificationEvent" object:nil];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"MFSideMenuStateNotificationEvent" object:nil];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) reloadMenu: (id)sender
{
	[self.sideMenuTableView reloadData];
}

#pragma mark - IBAction methods

- (IBAction)topViewTapGestureAction:(id)sender
{
    [self handlePageMenuSelectionWithCurrentIndex:0 indexPath:nil];
}


#pragma mark- BL Methods

-(UIImage *)getIconForIndex:(NSInteger)index
{
    NSString *imageName;
    switch (index)
    {
        
        case 0:
        {
            imageName = @"logo_WithoutText";
            break;
        }
        case 1:
        {
            imageName = @"icn_trips";
            break;
        }
        case 2:
        {
            imageName = @"icn_friends";
            break;
        }
        case 3:
        {
            imageName = @"icn_invites";
            break;
        }
        case 4:
        {
            imageName = @"icn_information";
            break;
        }
//        case 5:
//        {
//            imageName = @"icn_rent";
//            break;
//        }
        case 5:
        {
            imageName = @"icn_settings";
            break;
            
        }
        case 6:
        {
            imageName = @"icn_TnC";
            break;
            
        }
        case 7:  // LogOut
        {
            imageName = @"icn_signout";
            break;
        }
    }
    
    return  [UIImage imageNamed:imageName];
    
}



#pragma mark- MFSideMenuStateNotificationEvent selector

-(void)sideMenuStateChangedSelector:(NSNotification *)notification
{
    NSInteger eventType = [[notification.userInfo objectForKey:@"eventType"] integerValue];
    
    if(eventType == 0)  // menu will open
    {
        userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
        _profilePicImageView.layer.cornerRadius =25;
        _profilePicImageView.clipsToBounds = YES;
       
        UIImage *placeholderImage = [UIImage imageNamed:@"icn_defaultuser"];
        if (_profilePicImageView.image)
        {
            placeholderImage = _profilePicImageView.image;
        }
        [_profilePicImageView sd_setImageWithURL:[NSURL URLWithString:userProfileModel.profileImage]
                                placeholderImage:placeholderImage
                                         options:SDWebImageRefreshCached
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
         }];
        
        
        _userNameLabel.text = [NSString stringWithFormat:@"%@ %@",userProfileModel.firstname,userProfileModel.lastname];
        
        UIFont *font = [UIFont fontWithName:@"Helvetica" size:15];
        NSDictionary *userAttributes = @{NSFontAttributeName: font, NSForegroundColorAttributeName: [UIColor whiteColor]};
        const CGSize textSize = [userProfileModel.locations sizeWithAttributes: userAttributes];
        self.userAddressLabel.text = userProfileModel.locations;
        addressImgView.frame = CGRectMake(_userAddressLabel.frame.size.width/2-textSize.width/2-17, 2, 16, 16);
        if (userProfileModel.locations.length == 0)addressImgView.hidden = YES;
        else addressImgView.hidden = NO;
    
        
        
  
    }
    else if(eventType == 2)  // menu will close
    {
        
    }
    
}


#pragma mark- UITableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuContentsArray.count;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    TPPLeftMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPLeftMenuTableViewCell"];
    if (cell == nil)
    {
        cell = [[TPPLeftMenuTableViewCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPLeftMenuTableViewCell"];
    }
    cell.nameLabel.text = menuContentsArray[indexPath.row];
    cell.menuImageView.image = [self getIconForIndex:indexPath.row];
   // [cell.menuImageView applyTintColor:[UIColor greenColor]];

    
    
    if (indexPath.row<=3)
    {
        cell.imgViewLeadingConstant.constant = 22;
    }
    else
    {
        cell.imgViewLeadingConstant.constant = 12;
    }
    
    
	if (indexPath.row == 1)
    {
        [cell setbadgeValue:tripCounts.myTripCount];

		//[cell addBadgeWithValue:tripCounts.myTripCount];
	}
    else if (indexPath.row == 3)
    {
        [cell setbadgeValue:tripCounts.inviteCount];
                
	}
    else
    {
        cell.badgeLabel.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    UIViewController* viewController;
   
    switch (indexPath.row)
    {
        case 0:
        {
            if (_homeViewController == nil)
            {
                viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPHomeViewController"];
                _homeViewController = (TPPHomeViewController *)viewController;
            }
            else
            {
                viewController = _homeViewController;
            }
            break;
        }
        case 1:
        {
            [self handlePageMenuSelectionWithCurrentIndex:1 indexPath:indexPath];
            return;
        }
        case 2:
        {
            [self handlePageMenuSelectionWithCurrentIndex:2 indexPath:indexPath];
            return;
            
        }
        case 3:
        {
            [self handlePageMenuSelectionWithCurrentIndex:3 indexPath:indexPath];
            return;
        }
        case 4:
        {
            if (informationViewController == nil)
            {
                viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPInformationViewController"];
                informationViewController = (TPPInformationViewController *)viewController;
                
            }
            else
            {
               viewController = informationViewController;
            }

            break;
        }
//        case 5:
//        {
//            if (rentVehicleViewController == nil)
//            {
//                viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRentVehicleViewController"];
//                rentVehicleViewController = (TPPRentVehicleViewController *)viewController;
//            }
//            else
//            {
//                viewController = rentVehicleViewController;
//            }
//
//            break;
//        }
        case 5:
        {
            if (settingsViewController == nil)
            {
                viewController  = [self.storyboard instantiateViewControllerWithIdentifier: @"TPPSettingsViewController"];
                settingsViewController = (TPPSettingsViewController *)viewController;
            }
            else
            {
                viewController = settingsViewController;
            }

            break;
        }
        case 6:
        {
            if (termsViewController == nil)
            {
                viewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTermsViewController"];
                termsViewController = (TPPTermsViewController *) viewController;
            }
            else
            {
                viewController = termsViewController;
            }

            break;
        }
        case 7:  // LogOut
        {
            
            [self showConfirmationAlert];
            
            return;
        }
    }
    
    self.menuContainerViewController.centerViewController = [[UINavigationController alloc]initWithRootViewController:viewController];
    [self toggleLeftSide];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)handlePageMenuSelectionWithCurrentIndex:(NSInteger)currentIndex indexPath:(NSIndexPath*)indexPath
{
    if (indexPath)
    {
        [self.sideMenuTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    [self toggleLeftSide];
    

    pageMenuBaseViewController = (TPPPageMenuBaseViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"TPPPageMenuBaseViewController"];
    pageMenuBaseViewController.currentIndex = currentIndex;
    
    
    if (_homeViewController == nil)
    {
        _homeViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPHomeViewController"];
    }
    self.menuContainerViewController.centerViewController = [[UINavigationController alloc] initWithRootViewController:_homeViewController];
    [self.menuContainerViewController.centerViewController pushViewController:pageMenuBaseViewController animated:YES];
    
}


-(void)showConfirmationAlert
{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Logout"
                                          message:@"Are you sure you want to logout?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"YES action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self logoutUser];
                               }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"CANCEL", @"CANCEL")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)logoutUser
{
    [[TPPSocketUtility sharedInstance].socket close];
    [TPPSocketUtility sharedInstance].isSocketConnected = NO;
    
    [[TPPCoreDataHandler sharedInstance] deleteAllObjectsInCoreData];
    
    UIViewController  *viewController;
    if (_logInViewController == nil)
    {
        viewController  = [self.storyboard instantiateViewControllerWithIdentifier: @"TPPLogInViewController"];
        _logInViewController = (TPPLogInViewController *)viewController;
        
    }
    else
    {
        viewController = _logInViewController;
    }
    
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    
    [[Twitter sharedInstance] logOut];
    
    [UserDeafaults removeObjectForKey:Token];
    [UserDeafaults synchronize];
    
    self.menuContainerViewController.centerViewController = [[UINavigationController alloc]initWithRootViewController:viewController];
    [self toggleLeftSide];
    
    [TPPAppServices callServiceToRegisterDeviceForPushNotifications:NO];
}


@end

//
//  TPPLeftMenuViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPHomeViewController.h"
#import "TPPLogInViewController.h"
#import "TPPFriendsViewController.h"
#import "TPPInformationViewController.h"
#import "TPPInvitesViewController.h"
#import "TPPRentVehicleViewController.h"
#import "TPPSettingsViewController.h"
#import "TPPTermsViewController.h"
#import "TPPPageMenuBaseViewController.h"
#import "TPPLeftMenuTableViewCell.h"
#import "TPPUserProfileModel.h"


@interface TPPLeftMenuViewController : TPPBaseViewController


@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;

@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property TPPHomeViewController *homeViewController;
@property TPPLogInViewController *logInViewController;

@end

//
//  TPPChangePasswordViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPChangePasswordViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;


@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

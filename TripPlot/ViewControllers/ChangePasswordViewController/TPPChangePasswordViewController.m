//
//  TPPChangePasswordViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPChangePasswordViewController.h"

@interface TPPChangePasswordViewController ()

@end

@implementation TPPChangePasswordViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Change Password";
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)isUserInputsValid
{
    if ([self.oldPasswordTextField.text.trimmedString isEqualToString:@""])
    {
        [self showErrorMessage:PASSWORD_EMPTY];
        
        return NO;
    }
    else if ([self.passwordTextField.text.trimmedString isEqualToString:@""])
    {
        [self showErrorMessage: PASSWORD_EMPTY];
        return NO;
    }
    else if ([self.confirmPasswordTextField.text.trimmedString isEqualToString:@""])
    {
        [self showErrorMessage: PASSWORD_EMPTY];
        return NO;
    }
    else if ([self.oldPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self showErrorMessage: @"Existing and new password must be different"];
        return NO;
    }
    else if (![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text])
    {
        [self showErrorMessage: PASSWORD_MISMATCH];
        return NO;
    }
    
    
    return YES;
}


- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)submitButtonTapped:(id)sender
{
    if (![self isUserInputsValid])
    {
        return;
    }
    
	[self.view showLoader]; //oldPassword

    NSDictionary *credentialsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           self.passwordTextField.text.trimmedString, @"newPassword",
										   self.oldPasswordTextField.text.trimmedString, @"oldPassword",
                                           nil];
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:ChangePasswordUrl] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [self showSuccessMessage:PASSWORD_CHANGED];
             
             [self.navigationController popViewControllerAnimated:YES];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {

         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
    

}


@end

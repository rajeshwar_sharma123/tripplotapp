//
//  TPPNewLocationTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPNewLocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic)UITapGestureRecognizer *sourceLabelTapRecognizer;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (weak, nonatomic)UITapGestureRecognizer *destinationLabelTapRecognizer;
@end

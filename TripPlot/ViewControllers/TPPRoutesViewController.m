//

//  TPPRoutesViewController.m

//  TripPlot

//

//  Created by Daffolap-21 on 03/01/16.

//  Copyright © 2016 Daffodil. All rights reserved.

//



#import "TPPRoutesViewController.h"
#import "TPPTripMenuStepperView.h"
#import "TPPSliderTableViewCell.h"
#import "TPPMapDetailTableViewCell.h"
#import "TPPLocationTableViewCell.h"
#import "TPPLocationMenuView.h"
#import "TPPRouteDirectionServices.h"
#import "TPPPlaceDetailView.h"
#import "TPPAddMembersViewController.h"
#import "TPPAddMediaViewController.h"

#define DEFAULT_RADIUS_VALUE 10

@interface TPPRoutesViewController ()
{
    NSArray *_coordinates;
    NSArray *routesArray;
    NSMutableArray *placesArray;
    NSMutableArray *selectedPlacesArray;
    NSInteger selectedPlaceMenuOption;
    NSString *pageTokenForPlaces;
    NSInteger placesCount;
    NSInteger radius;
    TPPLocationMenuView *menuView;
    CLLocation *origin;
    NSString *placeTypeString;
    TPPPlaceDetailView *placeDetailPopupView;
    NSMutableArray *selectedFuelArray;
    NSMutableArray *selectedFoodArray;
    NSMutableArray *selectedPOIArray;
    NSMutableArray *selectedATMArray;
    NSMutableArray *selectedMedicalArray;
    NSMutableArray *selectedRestRoomArray;
    TPPCreateTripModel *newTripObject;
    NSInteger selectRouteIndex;
		//new change for "Halt Point" feature
	long selectedRouteDistance;
	NSArray *selectedRouteCoordinates;
	NSArray *selectedRouteDistanceBreakup;
	CLLocation *currentSearchPoint;
}

@end



@implementation TPPRoutesViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    radius = DEFAULT_RADIUS_VALUE;
    selectedPlacesArray = [NSMutableArray new];
    selectRouteIndex = 0;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 60;
    self.tableView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TPPPlaceDetailView" owner:nil options:nil];
    placeDetailPopupView = nibContents[0];
    [placeDetailPopupView setFrame:self.view.frame];
    placeDetailPopupView.placeNameTxtFld.enabled = NO;
    placeDetailPopupView.distanceTxtFld.enabled = NO;
    placeDetailPopupView.delegate = nil;
    [self.view addSubview:placeDetailPopupView];
    [placeDetailPopupView setHidden:YES];
    selectedFuelArray = [NSMutableArray new];
    selectedFoodArray = [NSMutableArray new];
    selectedPOIArray = [NSMutableArray new];
    selectedATMArray = [NSMutableArray new];
    selectedMedicalArray = [NSMutableArray new];
    selectedRestRoomArray = [NSMutableArray new];
    [self.cancelButton setImage:[UIImage imageNamed:@"btn_Next_normal"] forState:UIControlStateNormal];
    [self.nextButton setImage:[UIImage imageNamed:@"btn_next"] forState:UIControlStateNormal];
    newTripObject = [TPPCreateTripModel sharedInstance];
    _coordinates = [self convertToCLLocations:newTripObject.tripLocations];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TPPTripMenuStepperView *menuStepper = [[[NSBundle mainBundle] loadNibNamed:@"TPPTripMenuStepperView" owner:self options:nil] objectAtIndex:0];
    CGRect frame = self.menuView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [menuStepper setFrame:frame];
    [self.menuView addSubview:menuStepper];
    [menuStepper setSelectedIndex:2 isNew:newTripObject.isNew];
    [self.tableView reloadData];
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}





#pragma -mark UITableView life cycle



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    
    return 2;
    
}



-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return 1;
    else
    {
        if (selectedPlaceMenuOption == 0) {
            return 0;
        }
        if (pageTokenForPlaces) {
            return placesCount + 2;
        }
        else
        {
            return placesCount +1;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 54;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        if (!menuView) {
            menuView = [[[NSBundle mainBundle] loadNibNamed:@"TPPLocationMenuView" owner:self options:nil] objectAtIndex:0];
            [menuView.restaurantButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [menuView.fuelStationButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [menuView.stopageButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [menuView.atmButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [menuView.hotelButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [menuView.hospitalButton addTarget:self action:@selector(munuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            menuView.backgroundColor = COLOR_FOR_MENU_CONTENT;
        }
        return menuView;
    }
    return nil;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (selectedPlaceMenuOption == 0) {
            return self.view.bounds.size.height -(250);
        }
        else
        {
            return 242;
        }
    }
    else if (indexPath.section == 1 && indexPath.row == 0)
    {
        return 110;
    }
    else
    {
        return UITableViewAutomaticDimension;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == placesCount + 1) {
        [self callServiceTogetPlaces:currentSearchPoint and:placeTypeString shouldResetData:NO];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
	else
	{
		NSLog(@"tapped at: %ld", indexPath.row);
		UIButton *tempButton = [[UIButton alloc]init];
		tempButton.tag = indexPath.row - 1;
		[self addPlaceButtonTapped:tempButton];
	}

		//[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    //rows in section // should not be selectable
    if (indexPath.section == 0 || (indexPath.section == 1 && indexPath.row == 0)) {
        // By default, allow row to be selected
        return nil;
    }
    return indexPath;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.section == 0) {
        TPPMapDetailTableViewCell *cell = (TPPMapDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mapDetailCell"];
        
        if (cell == nil) {
            cell = [[TPPMapDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mapDetailCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.mapView.delegate = self;
        //CLLocation *location = [_coordinates objectAtIndex:0];
        [self boundCameraToFitAllMarkers:_coordinates onMap:cell.mapView];
        
        //cell.mapView = [GMSMapView mapWithFrame:cell.bounds camera:camera];
        cell.mapView.myLocationEnabled = NO;
        if (!routesArray || routesArray.count == 0) {
            [cell.mapView showLoader];
            [TPPRouteDirectionServices getRoutesWithLocations:_coordinates  andCompletitionBlock:^(id routes, NSError *error) {
                [cell.mapView hideLoader];
                if (error)
                {
                    NSLog(@"%@", error);
                    
            
                    
                }
                else
                {
                    routesArray = routes ;
                    NSDictionary *selecteRouteDic;
                    selecteRouteDic =routesArray.count >0? [routesArray objectAtIndex:selectRouteIndex]:nil;
                    newTripObject.route = selecteRouteDic;
						//selectedRouteDistance =  [selecteRouteDic[@"distance"] doubleValue];

					if(routesArray.count>0)
					{
						cell.routeCountLabel.text = [NSString stringWithFormat:@"Routes %ld/%ld", (long)selectRouteIndex+1,(unsigned long)routesArray.count];
						cell.summaryLabel.text = [NSString stringWithFormat:@"via %@",selecteRouteDic[@"summary"]];
							// decode polylineOverview
						selectedRouteCoordinates = [TPPRouteDirectionServices decodePolylineWithEncodedString:newTripObject.route[@"overviewPolyline"]];
						selectedRouteDistanceBreakup = [TPPRouteDirectionServices getAllDistanceBreakUp:selectedRouteCoordinates];
						currentSearchPoint = [TPPRouteDirectionServices findNewCenterLocation:DEFAULT_RADIUS_VALUE*1000 and:selectedRouteDistanceBreakup withLocations:selectedRouteCoordinates];
					}
					else
					{
						cell.routeCountLabel.text = [NSString stringWithFormat:@"Routes %ld/%ld", (long)selectRouteIndex,(unsigned long)routesArray.count];
						cell.summaryLabel.text = [NSString stringWithFormat:@"via ..."];
					}

                    cell.distanceLabel.text = [NSString stringWithFormat:@"%.1f kms",[selecteRouteDic[@"distance"] doubleValue]/1000.0];
                    cell.durationLabel.text = [NSString stringWithFormat:@"%.1f hrs",[selecteRouteDic[@"duration"] doubleValue]/(60.0*60.0)];
                    [self drawRoutes:cell.mapView];
                }
                NSLog(@"Completion block executed");
            }];
        }
        else
        {
            [self drawRoutes:cell.mapView];
            NSDictionary *selecteRouteDic;
            selecteRouteDic =routesArray.count >0? [routesArray objectAtIndex:selectRouteIndex]:nil;
			if(routesArray.count>0)
			{
				cell.routeCountLabel.text = [NSString stringWithFormat:@"Routes %ld/%ld", (long)selectRouteIndex+1,(unsigned long)routesArray.count];
			}
			else
			{
				cell.routeCountLabel.text = [NSString stringWithFormat:@"Routes %ld/%ld", (long)selectRouteIndex,(unsigned long)routesArray.count];
			}

            cell.summaryLabel.text = [NSString stringWithFormat:@"via %@",selecteRouteDic[@"summary"]];

            cell.distanceLabel.text = [NSString stringWithFormat:@"%.1f kms",[selecteRouteDic[@"distance"] doubleValue]/1000.0];
            cell.durationLabel.text = [NSString stringWithFormat:@"%.1f hrs",[selecteRouteDic[@"duration"] doubleValue]/(60.0*60.0)];
        }
        
        return cell;
    }
    
    else if(indexPath.section ==1 && indexPath.row == 0)
    {
        TPPSliderTableViewCell *cell = (TPPSliderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"sliderCell"];
        
        if (cell == nil) {
            cell = [[TPPSliderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sliderCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString *text = [NSString stringWithFormat:@"%ld Destinations",(long)placesCount];
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
        NSRange range = [text rangeOfString:@" "];
        [string addAttribute:NSForegroundColorAttributeName value:COLOR_LIGHT_GREEN range:NSMakeRange(0,range.location)];
        
        cell.locationsCountLabel.attributedText = string;
        _distanceLabel = cell.distanceLabel;
        cell.distanceLabel.text = [NSString stringWithFormat:@"%ld kms",(long)radius];
        cell.backgroundColor = [UIColor clearColor];

			double distance =  [newTripObject.route[@"distance"] doubleValue]/1000.0; //in meter
		

		cell.distanceSliderView.value = radius/distance; //radius
        [cell.distanceSliderView addTarget:self action:@selector(sliderValueChanging:) forControlEvents:UIControlEventValueChanged];
        [cell.distanceSliderView addTarget:self action:@selector(sliderReleased:) forControlEvents:UIControlEventTouchUpOutside];
		[cell.distanceSliderView addTarget:self action:@selector(sliderReleased:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    else if(indexPath.section ==1 && indexPath.row == placesCount + 1)
    {
        static NSString* simpleTableIdentifier = @"defaultCell";
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.textLabel.text = @"Load More...";
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    else{
        
        TPPLocationTableViewCell *cell = (TPPLocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationCell"];
        
        if (cell == nil) {
            cell = [[TPPLocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"locationCell"];
        }
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary * placeDict;
        // if (placeTypeString) {
        
        placeDict = [placesArray objectAtIndex:indexPath.row-1];
        cell.backgroundColor = [UIColor clearColor];
        cell.nameLabel.text = placeDict[@"name"];
        cell.placeId = placeDict[@"place_id"];
        cell.recordIndex = indexPath.row - 1;
        cell.addRemoveButton.tag = indexPath.row - 1;
        if ([placeDict objectForKey:@"rating"]) {
            [cell.ratingImageView setHidden:NO];
            cell.ratingLabel.text = [NSString stringWithFormat:@"%@/5",placeDict[@"rating"]];
        }
        
        else
        {
            cell.ratingLabel.text = nil;
            [cell.ratingImageView setHidden:YES];
        }
        
        if([self isAlreadyAdded:cell.placeId])
            
        {
            [cell.addRemoveButton addTarget:self action:@selector(removePlaceButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.addRemoveButton setBackgroundImage:[UIImage imageNamed:@"btn_point_remove"] forState:UIControlStateNormal];
        }
        
        else
        {
            [cell.addRemoveButton addTarget:self action:@selector(addPlaceButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.addRemoveButton setBackgroundImage:[UIImage imageNamed:@"btn_point_add"] forState:UIControlStateNormal];
        }
        return cell;
    }
}



#pragma -mark Utility Methods

//-(void)addMarkersOn:(GMSMapView *)map locations:(NSArray *)locations
//{
//
//}
- (void)boundCameraToFitAllMarkers:(NSMutableArray *)allMarkersMArray onMap:(GMSMapView *)map
{
    GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] init];
    
    for (NSInteger atIndex = 0;atIndex <allMarkersMArray.count; atIndex ++) {
        
        CLLocation *location = [allMarkersMArray objectAtIndex:atIndex];
        
        if (location.coordinate.longitude > 0 && location.coordinate.latitude > 0) {
            bounds = [bounds includingCoordinate:location.coordinate];
        }
        //add markers
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        
        if (atIndex == 0) {
            marker.icon = [UIImage imageNamed:@"circle"];
        }
        else if (atIndex == allMarkersMArray.count-1)
        {
            marker.icon = [UIImage imageNamed:@"circle_Red"];
        }
        else
        {
            marker.icon = [UIImage imageNamed:@"circle_blue"];
        }
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = map;
    }
    [map animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:16]];
}

-(void)drawRoutes:(GMSMapView *)map

{
    NSArray *polylines;
    
    for (NSInteger atIndex = 0; atIndex <routesArray.count; atIndex++ ) {
        
        NSDictionary *routeDict = [routesArray objectAtIndex:atIndex];
        
        if (atIndex == selectRouteIndex) {
            continue;
        }
        NSArray *polylines = routeDict[@"polylines"];
        [self drawPolyline:polylines map:map shouldSelected:NO withTitle:atIndex];
    }
    
    NSDictionary *routeDict = routesArray.count>0?routesArray[selectRouteIndex]:@{};
    polylines = routeDict[@"polylines"];
    [self drawPolyline:polylines map:map shouldSelected:YES withTitle:selectRouteIndex];
    // [self.tableView reloadData];
}

-(void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay{
    
    NSLog(@"tagged at : %@",overlay.title);
    NSInteger tappedPolyline = [overlay.title integerValue];
    if (selectRouteIndex != tappedPolyline) {
        selectRouteIndex = tappedPolyline;
		radius = DEFAULT_RADIUS_VALUE;
		NSDictionary *selecteRouteDic;
		selecteRouteDic =routesArray.count >0? [routesArray objectAtIndex:selectRouteIndex]:nil;
		newTripObject.route = selecteRouteDic;

		selectedRouteCoordinates = [TPPRouteDirectionServices decodePolylineWithEncodedString:newTripObject.route[@"overviewPolyline"]];
		selectedRouteDistanceBreakup = [TPPRouteDirectionServices getAllDistanceBreakUp:selectedRouteCoordinates];
		currentSearchPoint = [TPPRouteDirectionServices findNewCenterLocation:DEFAULT_RADIUS_VALUE*1000 and:selectedRouteDistanceBreakup withLocations:selectedRouteCoordinates];


//			 [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
//                      withRowAnimation:UITableViewRowAnimationNone];

		[self.tableView reloadData];
    }
}


-(void) drawPolyline:(NSArray *) polylines map:(GMSMapView *)map shouldSelected:(BOOL) isSelected withTitle:(NSInteger ) title
{
    for (NSInteger atIndex2 = 0; atIndex2 <polylines.count; atIndex2++ ) {
        NSDictionary *routeOverviewPolyline = [polylines objectAtIndex:atIndex2];
        NSString *points = [routeOverviewPolyline objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:points];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.title = [NSString stringWithFormat:@"%ld", (long)title];
        polyline.tappable = YES;
        polyline.strokeWidth = 4;
        if (isSelected) {
            polyline.strokeColor = COLOR_FOR_MAP_ROUTE;
        }
        else
        {
            polyline.strokeColor = COLOR_FOR_MENU_CONTENT;
        }
        polyline.map = map;
    }
}


-(void)removePlaceButtonTapped:(UIButton *)sender
{
    [placeDetailPopupView setHidden:YES];
    NSDictionary *placeDict = [placesArray objectAtIndex:sender.tag];
    NSString *placeId = placeDict[@"place_id"];
    switch (selectedPlaceMenuOption) {
        case 1:
            for (NSInteger atIndex = 0; atIndex <selectedFuelArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedFuelArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedFuelArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
        case 2:
            
            for (NSInteger atIndex = 0; atIndex <selectedFoodArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedFoodArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedFoodArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
            
        case 3:
            for (NSInteger atIndex = 0; atIndex <selectedRestRoomArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedRestRoomArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedRestRoomArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
            
        case 4:
            for (NSInteger atIndex = 0; atIndex <selectedPOIArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedPOIArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedPOIArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
            
        case 5:
            for (NSInteger atIndex = 0; atIndex <selectedMedicalArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedMedicalArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedMedicalArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
            
        default:
            for (NSInteger atIndex = 0; atIndex <selectedATMArray.count; atIndex++) {
                NSDictionary *tempDict = [selectedATMArray objectAtIndex:atIndex];
                if ([tempDict[@"place_id"] isEqualToString:placeId]) {
                    [selectedATMArray removeObjectAtIndex:atIndex];
                    break;
                }
            }
            break;
    }
    NSLog(@"removePlaceButtonTapped");
    [self resetBadgeCounts];
}



-(void)addPlaceButtonTapped:(UIButton *)sender

{

    placeDetailPopupView.doneButton.tag = sender.tag;
    switch (selectedPlaceMenuOption) {
        case 1:
            placeDetailPopupView.TitleLabel.text = @"Fuel Station";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_fuel_normal"]];
            break;
        case 2:
            placeDetailPopupView.TitleLabel.text = @"Food Point";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_eat_normal"]];
            break;
        case 3:
            placeDetailPopupView.TitleLabel.text = @"Rest Room";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_restroom_normal"]];
            break;
        case 4:
            placeDetailPopupView.TitleLabel.text = @"POI";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_poi_normal"]];
            break;
        case 5:
            placeDetailPopupView.TitleLabel.text = @"Medical";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_medical_normal"]];
            break;
        default:
            placeDetailPopupView.TitleLabel.text = @"ATM";
            [placeDetailPopupView.placeImageView setImage:[UIImage imageNamed:@"btn_atm_normal"]];
            break;
    }
    
    NSDictionary *placeDict = [placesArray objectAtIndex:sender.tag];
    placeDetailPopupView.placeNameTxtFld.text = [NSString stringWithFormat:@"  %@", placeDict[@"name"]];
    NSDictionary *geometryLocation = placeDict[@"geometry"][@"location"];
    CLLocation *placeLocation = [[CLLocation alloc]initWithLatitude:[geometryLocation[@"lat"] floatValue] longitude:[geometryLocation[@"lng"] floatValue]];
    [placeDetailPopupView showLoader];
    [TPPRouteDirectionServices getDistanceWithLocations:origin andDestination: placeLocation andCompletitionBlock:^(id distance, NSError *error){
        [placeDetailPopupView hideLoader];
        if (error)
        {
            NSLog(@"%@", error);
        }
        else
        {
            double distanceInMetre = [distance[@"value"] floatValue];
            placeDetailPopupView.distanceTxtFld.text = [NSString stringWithFormat:@"  %.2f",distanceInMetre/1000.00 ];
        }
    }];

	//check whether it is already added or not
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag+1 inSection:1];
	TPPLocationTableViewCell *cell = (TPPLocationTableViewCell*) [self tableView:self.tableView cellForRowAtIndexPath:indexPath];
	NSString *placeId = cell != nil? cell.placeId:@"";
	if([self isAlreadyAdded:placeId])
	{
		[placeDetailPopupView.doneButton setEnabled:NO];
	}
	else{

    [placeDetailPopupView.doneButton addTarget:self action:@selector(addSelectedPlace:) forControlEvents:UIControlEventTouchUpInside];
		[placeDetailPopupView.doneButton setEnabled:YES];
	}
    [placeDetailPopupView setHidden:NO];
   
}



-(void ) addSelectedPlace:(UIButton *)sender
{
    NSDictionary *placeDict = [placesArray objectAtIndex:sender.tag];
    switch (selectedPlaceMenuOption) {
        case 1:
            [selectedFuelArray addObject:placeDict];
            break;
        case 2:
            [selectedFoodArray addObject:placeDict];
            break;
        case 3:
            [selectedRestRoomArray addObject:placeDict];
            break;
        case 4:
            [selectedPOIArray addObject:placeDict];
            break;
        case 5:
            [selectedMedicalArray addObject:placeDict];
            break;
        default:
            [selectedATMArray addObject:placeDict];
            break;
    }
    [placeDetailPopupView setHidden:YES];
    [self resetBadgeCounts];
}



-(void) resetBadgeCounts
{
    if (selectedFuelArray.count > 0) {
        menuView.fuelStationButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedFuelArray.count];
        menuView.fuelStationButton.badgePostion = BadgePositionon_MiddleRight;
    }
    else
    {
        menuView.fuelStationButton.badgeValue = nil;
    }
    if (selectedFoodArray.count > 0) {
        menuView.restaurantButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedFoodArray.count];
        menuView.restaurantButton.badgePostion = BadgePositionon_MiddleRight;
    }
    else
        
    {
        menuView.restaurantButton.badgeValue = nil;
    }
    if (selectedPOIArray.count > 0) {
        
        menuView.stopageButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedPOIArray.count];
        menuView.stopageButton.badgePostion = BadgePositionon_MiddleRight;
    }
    else
    {
        
        menuView.stopageButton.badgeValue = nil;
    }
    if (selectedMedicalArray.count > 0) {
        
        menuView.hospitalButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedMedicalArray.count];
        
        menuView.hospitalButton.badgePostion = BadgePositionon_MiddleRight;
    }
    else
    {
        menuView.hospitalButton.badgeValue = nil;
    }
    if (selectedATMArray.count > 0) {
        menuView.atmButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedATMArray.count];
        menuView.atmButton.badgePostion = BadgePositionon_MiddleRight;
    }
    else
    {
        menuView.atmButton.badgeValue = nil;
    }
    
    if (selectedRestRoomArray.count > 0) {
        menuView.hotelButton.badgeValue = [NSString stringWithFormat:@"%ld", (unsigned long)selectedRestRoomArray.count];
        menuView.hotelButton.badgePostion = BadgePositionon_MiddleRight;
    }
    
    else
    {
        menuView.hotelButton.badgeValue = nil;
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1]
                  withRowAnimation:UITableViewRowAnimationNone];
}



-(BOOL)isAlreadyAdded:(NSString *)placeId
{
    NSArray *tempPlaces;
    switch (selectedPlaceMenuOption) {
        case 1:
            tempPlaces = [selectedFuelArray copy];
            break;
        case 2:
            tempPlaces = [selectedFoodArray copy];
            break;
        case 3:
            tempPlaces = [selectedRestRoomArray copy];
            break;
        case 4:
            tempPlaces = [selectedPOIArray copy];
            break;
        case 5:
            tempPlaces = [selectedMedicalArray copy];
            break;
        default:
            tempPlaces = [selectedATMArray copy];
            break;
    }
    for (NSInteger atIndex = 0; atIndex <tempPlaces.count; atIndex++) {
        NSDictionary *tempDict = [tempPlaces objectAtIndex:atIndex];
        if ([tempDict[@"place_id"] isEqualToString:placeId]) {
            return YES;
        }
    }
    return NO;
}

-(void)munuButtonTapped:(UIButton *)menuButton
{
    origin = [_coordinates objectAtIndex:0];
    NSInteger tappedIndex = menuButton.tag;
    selectedPlaceMenuOption = tappedIndex;
    [menuView resetAllMenuButtonsImage:menuView];
    NSLog(@"menu button index %ld tapped",(long)menuButton.tag);
    switch (tappedIndex) {
        case 1:
            placeTypeString =  @"fuel";
            [menuView.fuelStationButton setBackgroundImage:[UIImage imageNamed:@"btn_fuel_active"] forState:UIControlStateNormal];
            break;
            
        case 2:
            placeTypeString =  @"food";
            [menuView.restaurantButton setBackgroundImage:[UIImage imageNamed:@"btn_eat_active"] forState:UIControlStateNormal];
            break;
        case 3:
            placeTypeString =  @"restroom";
            [menuView.hotelButton setBackgroundImage:[UIImage imageNamed:@"btn_stay_active"] forState:UIControlStateNormal];
            break;
        case 4:
            placeTypeString =  @"poi";
            [menuView.stopageButton setBackgroundImage:[UIImage imageNamed:@"btn_POI_active"] forState:UIControlStateNormal];
            break;
        case 5:
            placeTypeString =  @"medical";
            [menuView.hospitalButton setBackgroundImage:[UIImage imageNamed:@"btn_medical_active"] forState:UIControlStateNormal];
            break;
        default:
            placeTypeString =  @"atm";
            [menuView.atmButton setBackgroundImage:[UIImage imageNamed:@"btn_atm_active"] forState:UIControlStateNormal];
            break;
    }
    placesArray = [[NSMutableArray alloc]init];
    placesCount = 0;
	[self callServiceTogetPlaces:currentSearchPoint and:placeTypeString shouldResetData:NO];
}


-(void)callServiceTogetPlaces:(CLLocation *)location and:(NSString *)placeType shouldResetData:(BOOL) shouldReset
{
    [self.view showLoader];
    [TPPRouteDirectionServices getPlacesWithLocations:location travelMode:placeType radius:DEFAULT_RADIUS_VALUE*1000 pagetoken:pageTokenForPlaces andCompletitionBlock:^(id places, NSError *error) {
        [self.view hideLoader];
        if (error)
        {
            NSLog(@"%@", error);
        }
        else
        {
			if (shouldReset) {
				placesArray = nil;
				placesArray = [NSMutableArray new];
				placesCount = 0;
			}
            NSArray *tempPlaces = places[@"results"];
            [placesArray addObjectsFromArray:tempPlaces];
            placesCount = placesCount + tempPlaces.count;
            if ([places objectForKey:@"next_page_token"]) {
                pageTokenForPlaces = [places objectForKey:@"next_page_token"];
            }
            else{
                
                pageTokenForPlaces = nil;
            }
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1]
                          withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (IBAction)cancelButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonTapped:(id)sender{
    NSMutableDictionary *tripPoints = [NSMutableDictionary new];
    tripPoints[@"fuel"] = selectedFuelArray;
    tripPoints[@"food"] = selectedFoodArray;
    tripPoints[@"poi"] = selectedPOIArray;
    tripPoints[@"restroom"] = selectedRestRoomArray;
    tripPoints[@"medical"] = selectedMedicalArray;
    tripPoints[@"atm"] = selectedATMArray;
    newTripObject.tripPoints = [tripPoints copy];
   
    newTripObject.distance = newTripObject.route[@"distance"];
    newTripObject.duration = newTripObject.route[@"duration"];

    // newTripObject.route = routesArray.count> 0 ? [routesArray objectAtIndex:0]:nil;

    //check for routes availability
    if (routesArray.count>0)
    {
        newTripObject.route = [routesArray objectAtIndex:selectRouteIndex];
        if (newTripObject.isNew) {
            TPPAddMembersViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPAddMembersViewController"];
            [self.navigationController pushViewController:vc animated:NO];
        }
        else
        {
            TPPAddMediaViewController *vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPAddMediaViewController"];
            [self.navigationController pushViewController:vc animated:NO];
        }
    }else
    {
        [self showErrorMessage:MSG_NO_ROUTE_FOUND];
    }
}

-(NSArray *)convertToCLLocations:(NSArray *)locations
{
    NSMutableArray *tempArray = [NSMutableArray new];
    for (NSInteger atIndex = 0; atIndex < locations.count; atIndex++) {
        TPPTripLocation *location = (TPPTripLocation *)[locations objectAtIndex:atIndex];
        NSDictionary *tempDict = location.geoCode;
        [tempArray addObject:[[CLLocation alloc] initWithLatitude:[tempDict[@"latitude"] doubleValue] longitude:[tempDict[@"longitude"] doubleValue]]];
    }
    return [tempArray copy];
}

#pragma -mark UISLider Methods

-(void) sliderValueChanging:(UISlider *)sender
{
    NSLog(@"sliderValue: %f",sender.value*10);
	long distance =  [newTripObject.route[@"distance"] longValue]; //in meter
    radius = sender.value*(distance/1000);
    _distanceLabel.text = [NSString stringWithFormat:@"%ld kms",(long)radius];
  
}
-(void) sliderReleased:(UISlider *)sender
{
	currentSearchPoint = [TPPRouteDirectionServices findNewCenterLocation:radius*1000 and:selectedRouteDistanceBreakup withLocations:selectedRouteCoordinates];
    [self callServiceTogetPlaces:currentSearchPoint and:placeTypeString shouldResetData:YES];
}

@end






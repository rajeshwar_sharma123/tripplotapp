//
//  TPPPageMenuBaseViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/8/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPPageMenuBaseViewController : UIViewController
@property (nonatomic) NSInteger currentIndex;
@end

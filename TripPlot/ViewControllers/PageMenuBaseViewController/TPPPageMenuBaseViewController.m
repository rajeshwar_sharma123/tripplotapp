//
//  TPPPageMenuBaseViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/8/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPPageMenuBaseViewController.h"
#import "CarbonKit.h"
#import "CustomBadge.h"
#import "TPPTripMenuBaseViewController.h"
@interface TPPPageMenuBaseViewController ()<CarbonTabSwipeNavigationDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    TPPTripMenuBaseViewController *tripMenuBaseVC;
}


@end

@implementation TPPPageMenuBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.title = @"Profile";
    
        items = @[[UIImage imageNamed:@"icn_userinfo_active"], [UIImage imageNamed:@"icn_trips_active"],
                  [UIImage imageNamed:@"icn_friends_active"], [UIImage imageNamed:@"icn_requests_active"]];
    
    
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [tripMenuBaseVC viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.hidesBackButton = NO;
    [carbonTabSwipeNavigation setCurrentTabIndex:_currentIndex];
}

- (void)style {
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:ColorFromRGB(0xE56823)];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:3];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = ColorFromRGB(0x131010);
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:ColorFromRGB(0x797778)
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:ColorFromRGB(0xFFFFFF)
                                          font:[UIFont boldSystemFontOfSize:14]];
    
    [self setBadges];
    
    
    
}
# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            // TPPUserProfileViewController
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPUserProfileViewController"];
            
        case 1:
            //return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPFollowingTripsViewController"];
            tripMenuBaseVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripMenuBaseViewController"];
            return tripMenuBaseVC;
            
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPContactViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPInvitesViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    
    _currentIndex = index;
    switch(index) {
            
        case 0:
            self.title = @"Profile";
            break;
        case 1:
            self.title = @"Trips";
            break;
        case 2:
            self.title = @"Friends";
            break;
        default:
            self.title = @"Invites";
            break;
    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custome Method
- (void)setBadges
{
	TPPSharedTripCountsModel *tripCounts = [TPPSharedTripCountsModel sharedInstance];

    [carbonTabSwipeNavigation.carbonSegmentedControl setBadgeNumber:tripCounts.myTripCount   forSegmentAtIndex:1 usingBlock:^(CustomBadge *badge)
     {
         // See CustomBadge.h for other badge properties that can be changed here
         badge.badgeFrameColor = [UIColor blackColor];
         badge.badgeInsetColor = [UIColor redColor];
         badge.badgeTextColor = [UIColor whiteColor];
     }
     ];
   
    [carbonTabSwipeNavigation.carbonSegmentedControl setBadgeNumber:tripCounts.inviteCount forSegmentAtIndex:3 usingBlock:^(CustomBadge *badge)
     {
         // See CustomBadge.h for other badge properties that can be changed here
         badge.badgeFrameColor = [UIColor blackColor];
         badge.badgeInsetColor = [UIColor redColor];
         badge.badgeTextColor = [UIColor whiteColor];
     }
     ];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

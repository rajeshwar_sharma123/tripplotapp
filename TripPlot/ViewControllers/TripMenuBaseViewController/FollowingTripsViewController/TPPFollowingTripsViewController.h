//
//  TPPFollowingTripsViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPFollowingTripsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

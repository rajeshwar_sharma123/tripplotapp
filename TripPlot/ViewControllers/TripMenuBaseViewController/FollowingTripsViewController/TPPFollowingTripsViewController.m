//
//  TPPFollowingTripsViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 28/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPFollowingTripsViewController.h"
#import "AppDelegate.h"

#import "TPPTripDetailViewController.h"
#import "TPPOngoingTripDetailViewController.h"


@interface TPPFollowingTripsViewController ()<UITableViewDataSource,UITableViewDelegate,TPPShareCellDelegate,TPPTripPointTapDelegate,TPPNoDataFoundDelegate,TPPCellTapDelegate>
{
    
    NSMutableArray *commonModelsArray;
    
    NSInteger pageIndex;
    NSInteger totalFeedsOnServer;
    NSInteger totalFeedsRecieved;
    NSInteger tripStatus;
    
    UIRefreshControl *refreshControl;
    
    
    TPPNoDataFoundView *noDataFoundView;
    
    BOOL isPaginationRequestInProgress;
}

@end

@implementation TPPFollowingTripsViewController



#pragma mark- LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [self initView];
    
    [self callServiceForFollowingTrips];
    
    // To update the UI if user likes/Follows/Joins etc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userActivitySelector:) name:UserActivityNotification object:nil];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
   
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




#pragma mark- Initialization Methods

-(void)initView
{
    totalFeedsRecieved = 0;
    pageIndex = 1;
    
    [self registerForCells];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(pullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refreshControl];
    
    noDataFoundView = [TPPUtilities addNoDataFoundViewInTableView:self.tableView viewController:self];
}



-(void)registerForCells
{
    // LoadMoreCell
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPLoadMoreTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPLoadMoreTableViewCell"];
    
    //Share tab cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareUpcomingTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareUpcomingTripTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareUpcomingWithoutJoinTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareUpcomingWithoutJoinTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPInviteFriendShareCell" bundle:nil] forCellReuseIdentifier:@"TPPInviteFriendShareCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPShareCompletedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPShareCompletedTripTableViewCell"];
    

    
    // Ongoing trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPOngoingTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPOngoingTripTableViewCell"];
    
    
    // Upcoming trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel0" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel0"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel1"];
    
    
    // Completed trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripExpandedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
    
}



#pragma mark- BL Methods

-(void)userActivitySelector:(NSNotification *)notification
{
    if (notification.userInfo == nil)
    {
//        pageIndex = 1;
//        totalFeedsRecieved = 0;
//        isPaginationRequestInProgress = NO;
//        
//        [commonModelsArray removeAllObjects];
//        [self.tableView reloadData];
//        
//        [self callServiceForFollowingTrips];
    }
    else if ([notification.userInfo[Txt_IsFollowedUnfollowed] boolValue])
    {
        [commonModelsArray removeTripModelForRecievedNotification:notification];
    }
   
    [self.tableView reloadData];
}



-(void)pullToRefresh:(id)sender
{
    if (isPaginationRequestInProgress)
    {
        [refreshControl endRefreshing];
        return;
    }
    
    pageIndex = 1;
    totalFeedsRecieved = 0;
    
    [self callServiceForFollowingTrips];
}


-(BOOL)isMoreFeedsAvailable
{
    if(totalFeedsRecieved < totalFeedsOnServer)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}


-(TPPCommonTripModel *)getRootTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    NSInteger i = selectedCellIndex;
    for (; i>=0; i--)
    {
        TPPCommonTripModel *commonModel = [commonModelsArray objectAtIndex:i];
        if (commonModel.level == 0)
        {
            return [commonModelsArray objectAtIndex:i];
        }
    }
    
    return nil;
}

-(TPPTripModel *)getTripModelForSelectedIndex:(NSInteger)selectedCellIndex
{
    TPPCommonTripModel *parentTripModel = [self getRootTripModelForSelectedIndex:selectedCellIndex];
    
    return parentTripModel.tripModel;
}


#pragma mark- TPPCellTapDelegate

-(void)cellTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    if (tripModel.tripStatus == TripStatus_RecommendedTrip)
    {
        TPPRecommendedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPRecommendedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (tripModel.tripStatus == TripStatus_CompletedTrip)
    {
        TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    
}













#pragma mark- TPPNoDataFoundDelegate

-(void)retryButtonTapped
{
    [self callServiceForFollowingTrips];
}


#pragma mark- TPPTripPointTapDelegate

-(void)tripPointTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel buttonIndex:(NSInteger)buttonIndex
{
    TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
    tripDetailViewController.tripModel = tripModel;
    tripDetailViewController.selectedButtonIndex = buttonIndex;
    
    [self.navigationController pushViewController:tripDetailViewController animated:YES];
}



#pragma mark- TPPShareCellDelegate

-(void)shareButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel snapShot:(UIImage *)snapShot
{
    if (snapShot)
    {
        [self defaultShareWithText:@"" image:snapShot url:nil];
    }
    else
    {
        UITableViewCell *parentCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:0]];
        
        UIImage *image = parentCell.snapshotImage;
        [self defaultShareWithText:@"" image:image url:nil];
    }
}

-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    if ([userProfile[@"_id"] isEqualToString:tripModel.createdBy[@"_id"]] && (tripModel.tripStatus == TripStatus_CompletedTrip))
    {
        // Navigate to Invite screen because button will be invite rather than Go here
        return;
    }
    
    
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    
//    [self.view removeSubviewsOfKindOfClasses:@[@"TPPGoHereView"]];
//    
//    TPPGoHereView *goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    goHereView.frame = self.view.frame;
     
    goHereView.tripModel = tripModel;
    
    goHereView.center = self.view.center;
    
    [self.view addSubview:goHereView];
}



#pragma mark- TableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!commonModelsArray || (commonModelsArray.count > 0))
    {
        tableView.backgroundView.hidden = YES;
    }
    else
    {
        tableView.backgroundView.hidden = NO;
    }
    
    
    if((totalFeedsRecieved > 0) && [self isMoreFeedsAvailable] && !refreshControl.isRefreshing)
    {
        return commonModelsArray.count+1;
    }
    
    return commonModelsArray.count;
}


- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    // for LoadMoreCell
    if ((indexPath.row == commonModelsArray.count) && [self isMoreFeedsAvailable])
    {
        TPPLoadMoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPLoadMoreTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPLoadMoreTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPLoadMoreTableViewCell"];
        }
        [cell.spinner startAnimating];
        
        return cell;
    }
    
    
    UITableViewCell *commonCell;
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    if (commonModel.isShareModel)
    {
        commonCell = [self getShareTabCellForTableView:tableView IndexPath:indexPath model:commonModel];
        
        return commonCell;
    }
    
    if (commonModel.status == TripStatus_OngoingTrip)
    {
        commonCell = [self getOngoingTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    else if (commonModel.status == TripStatus_UpcomingTrip)
    {
        commonCell = [self getUpcomingCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    if (commonModel.status == TripStatus_CompletedTrip)
    {
        commonCell = [self getCompletedTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    
    
    return commonCell;
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (refreshControl.refreshing) // Pagination should not work if Pull-To-Refresh is working currently
    {
        return;
    }
    
    if((commonModelsArray.count == indexPath.row) && [self isMoreFeedsAvailable])
    {
        isPaginationRequestInProgress = YES;
        [self callServiceForFollowingTrips];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // For LoadMoreCell
    if (indexPath.row == commonModelsArray.count)
    {
        return 44;
    }
    
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    
    if(commonModel.isShareModel)
    {
        return 50;
    }
    
    
    if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 0)
    {
        return 228;
    }
    else if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 1)
    {
        return 200;
    }
    
    else if(commonModel.status==TripStatus_OngoingTrip && commonModel.level == 0)
    {
        return 498;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 0)
    {
        return 221;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 1)
    {
        return 276;
    }
    
    
    return 44;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCellIndex = indexPath.row;
    TPPCommonTripModel *model = [commonModelsArray objectAtIndex:selectedCellIndex];
    
    
    if (model.level == 1 && model.status==TripStatus_UpcomingTrip)
    {
        TPPTripModel *tripModel = [self getTripModelForSelectedIndex:selectedCellIndex];
        
        TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
        tripDetailViewController.strCheck = @"following";
        tripDetailViewController.vcObj = self;
        tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
        tripDetailViewController.tripModel = tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (model.level == 0 && model.status==TripStatus_OngoingTrip)
    {
        TPPCommonTripModel *commonModel = (TPPCommonTripModel *) commonModelsArray[selectedCellIndex];
        
        TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
        tripDetailViewController.tripModel = commonModel.tripModel;
        
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    else if (model.childrenArray.count > 0 && !refreshControl.refreshing)
    {
        // expand or collapse
        
        if (model.isExpanded)
        {
            // Collapse submenus
            [self collapseMenuWithMenuItem:model menuIndexPath:indexPath];
        }
        else
        {
            // expand menu
            [self expandMenuWithMenuItem:model menuIndexPath:indexPath];
        }
    }
    
}


#pragma mark- To get the cells

-(UITableViewCell *)getShareTabCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    if (commonModel.status == TripStatus_UpcomingTrip)
    {
        NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
        TPPTripModel *tripModel = commonModel.tripModel;
        
        if ([tripModel.createdBy[@"_id"] isEqualToString:userProfile[@"_id"]])
        {
            TPPInviteFriendShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPInviteFriendShareCell"];
            if (cell == nil)
            {
                cell = (TPPInviteFriendShareCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPInviteFriendShareCell"];
            }
            cell.tripModel = tripModel;
            cell.delegate = self;
            
            return cell;
            
        }
        else if( !tripModel.isOpen || !tripModel.isGroup)
        {
            TPPShareUpcomingWithoutJoinTableViewCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPShareUpcomingWithoutJoinTableViewCell" owner:self options:nil] objectAtIndex:0];
            
            [cell bindDataWithModel:tripModel];
            cell.shareCellDelegate = self;
            
            return cell;
        }
        else
        {
            TPPShareUpcomingTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPShareUpcomingTripTableViewCell"];
            if (cell == nil)
            {
                cell = (TPPShareUpcomingTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPShareUpcomingTripTableViewCell"];
            }
            [cell bindDataWithModel:commonModel.tripModel];
            cell.shareCellDelegate = self;
            
            return cell;
        }
        
    }
    else
    {
        TPPShareCompletedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPShareCompletedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPShareCompletedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPShareCompletedTripTableViewCell"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        cell.shareCellDelegate = self;
        
        return cell;
    }
}


-(UITableViewCell *)getOngoingTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPOngoingTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPOngoingTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPOngoingTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPOngoingTripTableViewCell"];
        }
        cell.delegate = self;
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    return commonCell;
}


-(UITableViewCell *)getUpcomingCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    if (commonModel.level == 0)
    {
        TPPUpcomingTripCellLevel0 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel0"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel0 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel0"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPUpcomingTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel1"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel1 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel1"];
        }
        cell.tripPointTapDelegate = self;
        [cell bindDataWithModel:commonModel.tripModel];
        commonCell = cell;
    }
    return commonCell;
}


-(UITableViewCell *)getCompletedTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPCompletedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripTableViewCell"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        cell.cellTapDelegate = self;
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPCompletedTripExpandedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripExpandedTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        }
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
        
    }
    return commonCell;
}




#pragma mark- Expand/Collapse Menu

-(void)collapseMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    
    
    NSInteger totalChildrenCount = menuItem.childrenArray.count;
    if (totalChildrenCount == 0)
    {
        return;
    }
    
    NSMutableArray * indexPathsToBeDeleted=[[NSMutableArray alloc] init];
    
    for(int i=1;i<=totalChildrenCount;i++)
    {
        [indexPathsToBeDeleted addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,totalChildrenCount)];
    
    
    [self.tableView beginUpdates];
    
    [self handleCellsForCollapseForIndexPaths:indexPathsToBeDeleted];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToBeDeleted withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [commonModelsArray removeObjectsAtIndexes:indexes];
    
    [self.tableView endUpdates];
    menuItem.isExpanded = NO;
    
}



-(void)expandMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    
    NSArray *subMenusArray= menuItem.childrenArray;
    if (subMenusArray.count == 0)
    {
        return;
    }
    
    NSMutableArray * indexPathsToBeAdded=[[NSMutableArray alloc]init];
    
    for(NSInteger i=1;i<=subMenusArray.count;i++)
    {
        [indexPathsToBeAdded addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,[subMenusArray count])];
    
    
    [self.tableView beginUpdates];
    
    [commonModelsArray insertObjects:subMenusArray atIndexes:indexes];
    [self.tableView insertRowsAtIndexPaths:indexPathsToBeAdded withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.tableView endUpdates];
    
    menuItem.isExpanded = YES;
}


-(void)handleCellsForCollapseForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths)
    {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        if ([cell isKindOfClass:[TPPCompletedTripExpandedTableViewCell class]])
        {
            TPPCompletedTripExpandedTableViewCell *tempCell =  (TPPCompletedTripExpandedTableViewCell *)cell;
            
            [tempCell.moviePlayerController pause];
        }
    }
}





#pragma mark- Service methods

-(void)callServiceForFollowingTrips
{
    if (!refreshControl.refreshing && pageIndex==1)
    {
        [self.view showLoader];
    }
    
    
    
    NSDictionary *profile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           @"follow",@"key",
                                           profile[@"_id"],@"value",
                                           [NSNumber numberWithInteger:pageIndex],Txt_PageNumber,
                                           [NSNumber numberWithInteger:PageSize],Txt_PageSize,
                                           nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:MyTripsUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         if (refreshControl.refreshing)
         {
             totalFeedsRecieved = 0;
             [commonModelsArray removeAllObjects];
         }
         
         self.tableView.backgroundView.hidden = YES;
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
         isPaginationRequestInProgress = NO;
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             
             NSArray *tempArray = [self getCommonModelsArrayForResponse:response];
             
             if (!commonModelsArray)
             {
                 commonModelsArray = [[NSMutableArray alloc]init];
             }
             [commonModelsArray addObjectsFromArray:tempArray];
             
             
             totalFeedsOnServer = [response[@"response"][@"count"] integerValue];
             totalFeedsRecieved = totalFeedsRecieved + tripsArray.count;
             pageIndex++;
             
             [self.tableView reloadData];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
         if (commonModelsArray.count == 0)
         {
             self.tableView.backgroundView.hidden = NO;
             noDataFoundView.messageLabel.text = errorMessage;
         }
         else
         {
             self.tableView.backgroundView.hidden = YES;
             [self showErrorMessage:errorMessage];
         }
         
         isPaginationRequestInProgress = NO;
         
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
     }];
}




-(void) callServiceToGetTripDetailsWithCommonTripModel:(TPPCommonTripModel *)model
{
    [self.view showLoader];
    
    //call web service to get trip details to get route
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
    requestData[@"token"] = [UserDeafaults objectForKey:Token];
    requestData[@"_id"] = model.tripModel._id;
    
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_TRIP_DETAILS] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSArray *tripsArray = response[@"response"][@"trips"];
             NSError *error;
             TPPTripModel *tempTripDetailModel;
             if (tripsArray.count > 0)
             {
                 tempTripDetailModel = [[TPPTripModel alloc]initWithDictionary: [tripsArray objectAtIndex:0] error:&error];
             }
             
             if (!error)
             {
                 if (model.tripModel.tripStatus == TripStatus_UpcomingTrip)
                 {
                     TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if(model.tripModel.tripStatus == TripStatus_OngoingTrip)
                 {
                     TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
                 else if (model.tripModel.tripStatus == TripStatus_CompletedTrip)
                 {
                     TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
                     tripDetailViewController.tripModel = tempTripDetailModel;
                     
                     [self.navigationController pushViewController:tripDetailViewController animated:YES];
                 }
             }
             else
             {
                 [self.view showToasterWithMessage:@"Unable to fetch trip details"];
             }
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
         
     }];
}



-(NSArray *)getCommonModelsArrayForResponse:(NSDictionary *)response
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *feed in response[@"response"][@"trips"])
    {
        NSError *error;
        TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:feed error:&error];
        
        if (tripModel.tripStatus == TripStatus_OngoingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_OngoingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = @[];
            
            [tempArray addObject:commonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_UpcomingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_UpcomingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForUpcomingTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_UpcomingTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_CompletedTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_CompletedTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForCompletedTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_CompletedTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        
    }
    
    return tempArray;
}


-(TPPCommonTripModel *)getShareCommonTripModel
{
    TPPCommonTripModel *shareCommonTripModel = [[TPPCommonTripModel alloc] init];
    shareCommonTripModel.isShareModel = YES;
    shareCommonTripModel.childrenArray = @[];
    
    return shareCommonTripModel;
}

#pragma mark- Model initialization for UpcomingTrips cell

-(NSArray *)getChildrenForUpcomingTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}



#pragma mark- Model initialization for CompletedTrips cell

-(NSArray *)getChildrenForCompletedTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}



@end

//
//  TPPMyTripsViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/25/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPMyTripsViewController : TPPBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *tripsCountLabel;


@end

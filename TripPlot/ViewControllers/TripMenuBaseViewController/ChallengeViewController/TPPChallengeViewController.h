//
//  TPPChallengeViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/25/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPChallengeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

//
//  TPPTripMenuBaseViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/25/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPTripMenuBaseViewController.h"
#import "TPPMyTripsViewController.h"
#import "TPPBucketListViewController.h"
#import "TPPChallengeViewController.h"
#import "TPPFollowingTripsViewController.h"

@interface TPPTripMenuBaseViewController ()<CarbonTabSwipeNavigationDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    
    TPPFollowingTripsViewController *followingTripsViewController;
}


@end

@implementation TPPTripMenuBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Trips";
    items = @[@"My Trips", @"Following",@"Bucket List",@"Challenge"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [followingTripsViewController viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}

- (void)style {
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorHeight:10];
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor clearColor]];
    [carbonTabSwipeNavigation setindicatorImage:@"icn_carrot_gray"];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:ScreenWidth/4 forSegmentAtIndex:3];
    carbonTabSwipeNavigation.carbonSegmentedControl.backgroundColor = ColorFromRGB(0x3E3933);
    [carbonTabSwipeNavigation setNormalColor:ColorFromRGB(0x797778)
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:ColorFromRGB(0xFFFFFF)
                                          font:[UIFont boldSystemFontOfSize:14]];
    
   // [self setBadges];
    
    
    
}
# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index)
    {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPMyTripsViewController"];
            
        case 1:
            followingTripsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPFollowingTripsViewController"];
            return followingTripsViewController;
            
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPBucketListViewController"];
            
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier:@"TPPChallengeViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    
   }

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

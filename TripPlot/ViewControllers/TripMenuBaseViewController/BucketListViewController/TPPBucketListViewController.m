//
//  TPPBucketListViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/28/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBucketListViewController.h"
#import "TPPMakeTripShareCell.h"
#import "TPPTripDetailViewController.h"
#import "TPPOngoingTripDetailViewController.h"

@interface TPPBucketListViewController ()<UITableViewDataSource,UITableViewDelegate,TPPCellTapDelegate,TPPShareCellDelegate>
{
    TPPUserProfileModel *userProfileModel;
    NSMutableArray *commonModelsArray;
   
    TPPCreateTripModel *newTripObject;

    UIRefreshControl *refreshControl;
    TPPNoDataFoundView *noDataFoundView;
    
}
@end

@implementation TPPBucketListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"RemoveFromBucket" object:nil];
    
    [self initialSetup];
    [self callServiceToGetBucketList];
    
}

-(void)initialSetup
{
    
    [self registerForCells];
    userProfileModel = [[TPPUserProfileModel alloc]initWithDictionary:[[UserDeafaults objectForKey:UserProfile] mutableCopy] error:nil];
    

    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(pullToRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:refreshControl];
    
    noDataFoundView = [TPPUtilities addNoDataFoundViewInTableView:self.tableView viewController:self];

}


-(void)pullToRefresh:(id)sender
{
    
    [self callServiceToGetBucketList];
}


- (void)receiveNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"RemoveFromBucket"])
    {

        newTripObject = [TPPCreateTripModel sharedInstance];

        TPPCommonTripModel *model1 = [commonModelsArray objectAtIndex:newTripObject.selectedIndex-1];
        TPPCommonTripModel *model2;
        if (newTripObject.selectedIndex>1) {
            model2 = [commonModelsArray objectAtIndex:newTripObject.selectedIndex-2];
        }
        
        if (model1.isExpanded || model2.isExpanded) {
            
            [commonModelsArray removeObjectAtIndex:newTripObject.selectedIndex];
            [commonModelsArray removeObjectAtIndex:newTripObject.selectedIndex-1];
            [commonModelsArray removeObjectAtIndex:newTripObject.selectedIndex-2];
            
        }
        else
        {
            [commonModelsArray removeObjectAtIndex:newTripObject.selectedIndex];
            [commonModelsArray removeObjectAtIndex:newTripObject.selectedIndex-1];
        }
        [_tableView reloadData];

        
    }
}
-(void)registerForCells
{
    //Share tab cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPMakeTripShareCell" bundle:nil] forCellReuseIdentifier:@"TPPMakeTripShareCell"];
    
    // Ongoing trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPOngoingTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPOngoingTripTableViewCell"];
    
    // Completed trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPCompletedTripExpandedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
    
    // Upcoming trip cells
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel0" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel0"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TPPUpcomingTripCellLevel1" bundle:nil] forCellReuseIdentifier:@"TPPUpcomingTripCellLevel1"];
}



#pragma mark- TPPNoDataFoundDelegate

-(void)retryButtonTapped
{
    [self callServiceToGetBucketList];
}



#pragma mark- TableView methods

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!commonModelsArray || (commonModelsArray.count > 0))
    {
        tableView.backgroundView.hidden = YES;
    }
    else
    {
        tableView.backgroundView.hidden = NO;
    }

    return commonModelsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    UITableViewCell *commonCell;
    
    if (commonModel.isShareModel)
    {
        commonCell = [self getShareTabCellForTableView:tableView IndexPath:indexPath model:commonModel];
        return commonCell;
    }
    
    if (commonModel.status == TripStatus_OngoingTrip)
    {
        commonCell = [self getOngoingTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    else if (commonModel.status == TripStatus_UpcomingTrip)
    {
        commonCell = [self getUpcomingCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    if (commonModel.status == TripStatus_CompletedTrip)
    {
        commonCell = [self getCompletedTripsCellForTableView:tableView IndexPath:indexPath model:commonModel];
    }
    
        
    return commonCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TPPCommonTripModel *commonModel = commonModelsArray[indexPath.row];
    
    if(commonModel.isShareModel)
    {
        return 50;
    }
    
    if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 0)
    {
        return 228;    }
    else if(commonModel.status==TripStatus_CompletedTrip && commonModel.level == 1)
    {
        return 200;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 0)
    {
        return 221;
    }
    else if(commonModel.status==TripStatus_UpcomingTrip && commonModel.level == 1)
    {
        return 276;
    }
    else if(commonModel.status==TripStatus_OngoingTrip && commonModel.level == 0)
    {
        return 425;
       
    }


    
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCellIndex = indexPath.row;
    TPPCommonTripModel *model = [commonModelsArray objectAtIndex:selectedCellIndex];
    
    
    if (model.level == 1 && model.status==TripStatus_UpcomingTrip)
    {        
        TPPTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTripDetailViewController"];
        tripDetailViewController.tripModel = model.tripModel;
        
        tripDetailViewController.strCheck = @"bucket";
        tripDetailViewController.vcObj = self;
        tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
        [self.navigationController pushViewController:tripDetailViewController animated:YES];
        
    }
    else if (model.level == 0 && model.status==TripStatus_OngoingTrip)
    {
        
        
        TPPOngoingTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPOngoingTripDetailViewController"];
        tripDetailViewController.tripModel = model.tripModel;
        tripDetailViewController.strCheck = @"bucket";
        tripDetailViewController.vcObj = self;
        tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];

        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }

    
    else if (model.childrenArray.count > 0 && !refreshControl.refreshing)
    {
        // expand or collapse
        
        if (model.isExpanded)
        {
            // Collapse submenus
            [self collapseMenuWithMenuItem:model menuIndexPath:indexPath];
        }
        else
        {
            // expand menu
            [self expandMenuWithMenuItem:model menuIndexPath:indexPath];
        }
    }
    
}

#pragma mark- To get the cells


-(UITableViewCell *)getShareTabCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    TPPMakeTripShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPMakeTripShareCell"];
    if (cell == nil)
    {
        cell = (TPPMakeTripShareCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPMakeTripShareCell"];
    }
    [cell bindDataWithModel:commonModel.tripModel];
    cell.shareCellDelegate = self;
    
    return cell;
}



-(UITableViewCell *)getOngoingTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPOngoingTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPOngoingTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPOngoingTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPOngoingTripTableViewCell"];
        }
        cell.delegate = self;
        [cell bindDataWithModel:commonModel.tripModel];
        [cell setUpForBucketListScreen];
        commonCell = cell;
    }
    return commonCell;
}



-(UITableViewCell *)getUpcomingCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPUpcomingTripCellLevel0 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel0"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel0 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel0"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPUpcomingTripCellLevel1 *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPUpcomingTripCellLevel1"];
        if (cell == nil)
        {
            cell = (TPPUpcomingTripCellLevel1 *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPUpcomingTripCellLevel1"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
    }
    
    return commonCell;
}


-(UITableViewCell *)getCompletedTripsCellForTableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath model:(TPPCommonTripModel *)commonModel
{
    UITableViewCell *commonCell;
    
    if (commonModel.level == 0)
    {
        TPPCompletedTripTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripTableViewCell"];
        }
        
        [cell bindDataWithModel:commonModel.tripModel];
        cell.cellTapDelegate = self;
        
        commonCell = cell;
    }
    else if (commonModel.level == 1)
    {
        TPPCompletedTripExpandedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        if (cell == nil)
        {
            cell = (TPPCompletedTripExpandedTableViewCell *) [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TPPCompletedTripExpandedTableViewCell"];
        }
        [cell bindDataWithModel:commonModel.tripModel];
        
        commonCell = cell;
        
    }
    return commonCell;
}



//-(TPPTripModel *)getTripModelForSelectedIndex:(NSInteger)selectedCellIndex
//{
//        TPPCommonTripModel *parentTripModel = [self getRootTripModelForSelectedIndex:selectedCellIndex];
//    
//        return parentTripModel.tripModel;
//    
//}
#pragma mark- Expand/Collapse Menu

-(void)collapseMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1)// rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData:menuItem];
    }
    
    
    NSInteger totalChildrenCount = menuItem.childrenArray.count;
    if (totalChildrenCount == 0)
    {
        return;
    }
    
    // for recommended cell, child cells should also be closed if parent cell is closed
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level==0)
    {
        TPPCommonTripModel *childModel = menuItem.childrenArray[0];
        if (childModel.isExpanded)
        {
            totalChildrenCount = totalChildrenCount + childModel.childrenArray.count;
            childModel.isExpanded = NO;
            
            NSIndexPath *childIndexPath = [NSIndexPath indexPathForRow:menuIndexPath.row+1 inSection:0];
            TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:childIndexPath];
            [menuToBeExpanded rotateArrowWithData:menuItem];
            
        }
    }
    
    
    
    //[self handleCellCollisionWithIndexPath:menuIndexPath];
    
    NSMutableArray * indexPathsToBeDeleted=[[NSMutableArray alloc] init];
    
    for(int i=1;i<=totalChildrenCount;i++)
    {
        [indexPathsToBeDeleted addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,totalChildrenCount)];
    
    
    [self.tableView beginUpdates];
    
    [self handleCellsForCollapseForIndexPaths:indexPathsToBeDeleted];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToBeDeleted withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [commonModelsArray removeObjectsAtIndexes:indexes];
    
    [self.tableView endUpdates];
    menuItem.isExpanded = NO;
}


-(void)expandMenuWithMenuItem:(TPPCommonTripModel *)menuItem menuIndexPath:(NSIndexPath *)menuIndexPath
{
    if (menuItem.status == TripStatus_RecommendedTrip && menuItem.level == 1) // rotate arrow
    {
        TPPRecommendedTripCellLevel1 * menuToBeExpanded = [self.tableView cellForRowAtIndexPath:menuIndexPath];
        [menuToBeExpanded rotateArrowWithData: menuItem];
    }
    
    
    NSArray *subMenusArray= menuItem.childrenArray;
    if (subMenusArray.count == 0)
    {
        return;
    }
    
    NSMutableArray * indexPathsToBeAdded=[[NSMutableArray alloc]init];
    
    for(NSInteger i=1;i<=subMenusArray.count;i++)
    {
        [indexPathsToBeAdded addObject:[NSIndexPath indexPathForRow:menuIndexPath.row+i inSection:menuIndexPath.section]];
    }
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:
                           NSMakeRange(menuIndexPath.row+1,[subMenusArray count])];
    
    
    [self.tableView beginUpdates];
    
    [commonModelsArray insertObjects:subMenusArray atIndexes:indexes];
    [self.tableView insertRowsAtIndexPaths:indexPathsToBeAdded withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.tableView endUpdates];
    
    menuItem.isExpanded = YES;
    
}

-(void)handleCellsForCollapseForIndexPaths:(NSArray *)indexPaths
{
    for (NSIndexPath *indexPath in indexPaths)
    {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        if ([cell isKindOfClass:[TPPCompletedTripExpandedTableViewCell class]])
        {
            TPPCompletedTripExpandedTableViewCell *tempCell =  (TPPCompletedTripExpandedTableViewCell *)cell;
            
            [tempCell.moviePlayerController pause];
        }
    }
}



#pragma mark- WebServices Calling
- (void)callServiceToGetBucketList
{
    if (!refreshControl.refreshing)
    {
        [self.view showLoader];
    }
    

    
    NSDictionary *dadaDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [UserDeafaults objectForKey:Token],@"token",
                             userProfileModel._id ,@"userId",
                              nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:BucketListUrl] data:dadaDict withSuccessBlock:^(id response, NSDictionary *headers)
     {
         if (refreshControl.refreshing)
         {
             [commonModelsArray removeAllObjects];
         }
         
         self.tableView.backgroundView.hidden = YES;
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
             NSArray *tempArray = [self getCommonModelsArrayForResponse:response];
             if (!commonModelsArray)
             {
                 commonModelsArray = [[NSMutableArray alloc]init];
             }
             [commonModelsArray addObjectsFromArray:tempArray];
             
             [self.tableView reloadData];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         if (commonModelsArray.count == 0)
         {
             self.tableView.backgroundView.hidden = NO;
         }
         else
         {
             self.tableView.backgroundView.hidden = YES;
             [self showErrorMessage:errorMessage];
         }
         
         noDataFoundView.messageLabel.text = errorMessage;
         [refreshControl endRefreshing];
         [self.view hideLoader];
         
     }];
    
}

-(NSArray *)getCommonModelsArrayForResponse:(NSDictionary *)response
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *feed in response[@"response"][@"trips"])
    {
        NSError *error;
        TPPTripModel *tripModel = [[TPPTripModel alloc]initWithDictionary:feed error:&error];
        if (tripModel.tripStatus == TripStatus_OngoingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_OngoingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = @[];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_OngoingTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
            
            
        }
        else if (tripModel.tripStatus == TripStatus_UpcomingTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_UpcomingTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForUpcomingTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_UpcomingTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        else if (tripModel.tripStatus == TripStatus_CompletedTrip)
        {
            TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
            commonTripModel.status = TripStatus_CompletedTrip;
            commonTripModel.level = 0;
            commonTripModel.tripModel = tripModel;
            commonTripModel.childrenArray = [self getChildrenForCompletedTripForLevel0WithModel:tripModel];
            
            [tempArray addObject:commonTripModel];
            
            //Add share model
            TPPCommonTripModel *shareCommonTripModel = [self getShareCommonTripModel];
            shareCommonTripModel.status = TripStatus_CompletedTrip;
            shareCommonTripModel.tripModel = tripModel;
            
            [tempArray addObject:shareCommonTripModel];
        }
        
        
        
    }
    
    return tempArray;
}
-(TPPCommonTripModel *)getShareCommonTripModel
{
    TPPCommonTripModel *shareCommonTripModel = [[TPPCommonTripModel alloc] init];
    shareCommonTripModel.isShareModel = YES;
    shareCommonTripModel.level = 0;
    shareCommonTripModel.childrenArray = @[];
    
    return shareCommonTripModel;
}

#pragma mark- Model initialization for UpcomingTrips cell

-(NSArray *)getChildrenForUpcomingTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}


#pragma mark- Model initialization for CompletedTrips cell

-(NSArray *)getChildrenForCompletedTripForLevel0WithModel:(TPPTripModel *)tripModel
{
    TPPCommonTripModel *commonTripModel = [[TPPCommonTripModel alloc] init];
    commonTripModel.status = tripModel.tripStatus;
    commonTripModel.level = 1;
    commonTripModel.tripModel = tripModel;
    commonTripModel.childrenArray = @[];
    
    return @[commonTripModel];
}

#pragma mark- TPPCellTapDelegate

-(void)cellTappedAtIndexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
    if (tripModel.tripStatus == TripStatus_CompletedTrip)
    {
        TPPCommonTripModel *commonModel = [commonModelsArray objectAtIndex:indexPath.row];
        TPPCompletedTripDetailViewController *tripDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPCompletedTripDetailViewController"];
        tripDetailViewController.tripModel = tripModel;
        tripDetailViewController.strCheck = @"bucket";
        tripDetailViewController.vcObj = self;
        
        if (commonModel.isExpanded ) {
            tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+2 inSection:indexPath.section];
        }
        else
        {
            tripDetailViewController.selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
        }

        [self.navigationController pushViewController:tripDetailViewController animated:YES];
    }
    
}

#pragma mark- TPPShareCellDelegate

-(void)shareButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel snapShot:(UIImage *)snapShot
{
       if (snapShot)
    {
        [self defaultShareWithText:@"" image:snapShot url:nil];
    }
    else
    {
        UITableViewCell *parentCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:0]];
        
        UIImage *image = parentCell.snapshotImage;
        [self defaultShareWithText:@"" image:image url:nil];
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  TPPBucketListViewController.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/28/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPBucketListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

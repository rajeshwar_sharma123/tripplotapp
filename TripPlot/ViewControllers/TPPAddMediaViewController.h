//
//  TPPAddMediaViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import "TPPTripMenuStepperView.h"

@interface TPPAddMediaViewController : TPPBaseViewController
@property (weak, nonatomic) IBOutlet TPPTripMenuStepperView *menuView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

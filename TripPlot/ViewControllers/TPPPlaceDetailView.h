//
//  TPPPlaceDetailView.h
//  TripPlot
//
//  Created by Daffolap-21 on 05/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@protocol TPPPlaceDetailDelegate <NSObject>
- (void)updateBadgesForPlaces:(TPPSharedTripCountsModel *)shareModel;

@end
@interface TPPPlaceDetailView : UIView
{

}
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIImageView *placeImageView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *placeNameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *distanceTxtFld;
@property ( nonatomic) NSInteger tagIndex;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (weak, nonatomic) 	TPPSharedTripCountsModel *tripCountsWithDetail;

@property (weak) id<TPPPlaceDetailDelegate> delegate;

- (void)setCurrentLocation:(CLLocation *)location model:(TPPSharedTripCountsModel *)model info:(NSDictionary *)dic index:(NSInteger )index;
- (void)callServiceToAddPlaces:(TPPTripModel *)tripModel;
@end


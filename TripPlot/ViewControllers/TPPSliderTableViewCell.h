//
//  TPPSliderTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPSliderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *locationsCountLabel;

@property (weak, nonatomic) IBOutlet TPPCustomSlider *distanceSliderView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@end

//
//  TPPEmailListingViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 12/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPEmailListingViewController.h"
#import "TPPAddEmailPopupView.h"
#import "TPPContactViewController.h"



@implementation TPPEmailListingViewController
{
    TPPAddEmailPopupView *popup;
    NSMutableDictionary *request;
    NSMutableArray *emailList;
    TPPCreateTripModel *newTripObject;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newTripObject = [TPPCreateTripModel sharedInstance];
    // selectedArray = [NSMutableArray new];
    emailList = [NSMutableArray new];
    //newTripObject.addedEmailIDs = [NSMutableArray new];
    [self configureAddButton];
    request = [NSMutableDictionary new];
    request[@"token"] = [UserDeafaults objectForKey:Token];
    request[@"_id"] = [UserDeafaults objectForKey:UserProfile][@"_id"];
    [self.tableView setSeparatorColor:[UIColor whiteColor]];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    if(self.shouldEnableSelection)
//    {
//        
//    }
//    else{
     //   [self configureAddButton];
   // }



    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self callServiceToGetMailIDs:request];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

#pragma -mark UITableView delegate methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return emailList.count;//selectedArray.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* simpleTableIdentifier = @"defaultCell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[emailList objectAtIndex:indexPath.row]];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.numberOfLines=0;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.backgroundColor=[UIColor clearColor];
    
    NSInteger foundAt = newTripObject!=nil? [newTripObject.addedEmailIDs isExistString:[emailList objectAtIndex:indexPath.row]]:-1;
    if (foundAt >= 0) {
        [cell setBackgroundColor:COLOR_ORANGE];
    }
    else
    {
        [cell setBackgroundColor:[UIColor clearColor]];
    }
    
    if (!self.shouldEnableSelection) {
        cell.userInteractionEnabled = NO;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger foundAt = [newTripObject.addedEmailIDs isExistString:[emailList objectAtIndex:indexPath.row]];
    if (foundAt >= 0) {
        [newTripObject.addedEmailIDs removeObjectAtIndex:foundAt];
    }
    else
    {
        [newTripObject.addedEmailIDs addObject:[emailList objectAtIndex:indexPath.row]];
    }
    [self.tableView reloadData];
    
    
    TPPContactViewController *vc = (TPPContactViewController *) self.parentViewController;
    [vc.emailButton setBadgePostion:BadgePositionon_MiddleRight];
    vc.emailButton.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)newTripObject.addedEmailIDs.count];
    [vc.emailButton setBadgePostion:BadgePositionon_MiddleRight];
    [vc enableRightBarButton];
}

#pragma -mark Utility methods
-(void)configureAddButton
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width-58;
    CGFloat screenHeight = self.shouldEnableSelection?screenRect.size.height-165:screenRect.size.height-220;
    UIButton *goToTop = [UIButton buttonWithType:UIButtonTypeCustom];
    goToTop.frame = CGRectMake(screenWidth, screenHeight, 40, 40);
		//[goToTop setTitle:@"+" forState:UIControlStateNormal];
	[goToTop setImage:[UIImage imageNamed:@"btn_add_normal"] forState:UIControlStateNormal];
	[goToTop setImage:[UIImage imageNamed:@"btn_add_pressed"] forState:UIControlStateSelected];
	[goToTop setImage:[UIImage imageNamed:@"btn_add_pressed"] forState:UIControlStateHighlighted];
    [goToTop addTarget:self action:@selector(showAddPopup:) forControlEvents:UIControlEventTouchUpInside];
    [goToTop setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // [goToTop setBackgroundColor:[UIColor blackColor]  forState:UIControlStateNormal];
	//  goToTop.backgroundColor = [UIColor blackColor];
  ////  [goToTop setTitleColor:COLOR_ORANGE forState:UIControlStateHighlighted];
		//   [goToTop.layer setBorderColor:[[UIColor blackColor] CGColor]];
//    goToTop.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30];
    goToTop.layer.cornerRadius = goToTop.frame.size.height/2;
    goToTop.clipsToBounds = YES;
    //[self.view insertSubview:goToTop aboveSubview:self.tableView];
    [self.view addSubview:goToTop];
    [goToTop bringSubviewToFront:self.tableView];
}

-(void)backToPreviousScreen:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showAddPopup:(id) sender
{
    NSArray *nibContents2 = [[NSBundle mainBundle] loadNibNamed:@"TPPAddEmailPopupView" owner:nil options:nil];
    popup = nibContents2[0];
    [popup setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    popup.topConstraintConstant.constant = self.shouldEnableSelection?100:50;
    [popup.doneButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:popup];
}

-(void)addButtonTapped:(UIButton *)sender
{
    if ([emailList isExistString:popup.textField.text.trimmedString] >= 0) {
        [self showErrorMessage:MSG_FOR_EMAIL_EXISTS];
    }
    else{
        if ([popup.textField.text.trimmedString length]>0 && [popup.textField.text isValidEmail]) {
            NSArray *tempMails = @[popup.textField.text.trimmedString];
            request[@"emailList"] = tempMails;
            [self callServiceToSaveMailID:request];
        }
        else
        {
            [self showErrorMessage:EMAIL_INVALID];
        }
    }
    
    NSLog(@"add button tapped: %@",popup.textField.text);
}



-(void) callServiceToSaveMailID:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_SAVE_EMAIL_LIST] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             //searchLocation = response[@"response"][@"result"];
             
             [emailList addObject:popup.textField.text.trimmedString];
             [popup removeFromSuperview];
             [self.tableView reloadData];
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];
         
         
     }];
}

-(void) callServiceToGetMailIDs:(NSDictionary *)requestDictionary
{
    [self.view showLoader];
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_EMAIL_LIST] data:requestDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSDictionary *tempResult = [response[@"response"][@"result"] objectAtIndex:0];
             [emailList addObjectsFromArray:tempResult[@"emaillist"]];
             
             [self.tableView reloadData];
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [self.view hideLoader];
         [self showErrorMessage:errorMessage];
         
         
     }];
}

@end




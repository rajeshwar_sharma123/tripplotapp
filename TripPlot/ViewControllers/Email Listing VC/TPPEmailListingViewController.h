//
//  TPPEmailListingViewController.h
//  TripPlot
//
//  Created by Daffolap-21 on 12/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TPPEmailListingViewController: TPPBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property BOOL shouldEnableSelection;
@property BOOL shouldEnableInvite;

@end

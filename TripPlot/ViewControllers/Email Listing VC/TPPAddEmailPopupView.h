//
//  TPPAddEmailPopupView.h
//  TripPlot
//
//  Created by Daffolap-21 on 12/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPAddEmailPopupView : UIView
@property (weak, nonatomic) IBOutlet TPPTextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)cancelButtonTapped:(id)sender;
-(TPPAddEmailPopupView *) initPopView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintConstant;

@end

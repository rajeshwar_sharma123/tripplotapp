//
//  TPPAddImagesViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPAddImagesViewController.h"
#import "TPPUploadImagesViewController.h"
#import "TPPMontageVideoController.h"
@implementation TPPAddImagesViewController

{
    TPPPhotoAssets *photoAssets;
    NSMutableArray *imagePaths;
    NSMutableArray *imageURLs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.cancelBtn setImage:[UIImage imageNamed:@"btn_Next_normal"] forState:UIControlStateNormal];
    [self.nextBtn setTitle:@"Skip >>" forState:UIControlStateNormal];

   photoAssets = [TPPPhotoAssets sharedInstance];
    imagePaths = [NSMutableArray new];

    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}



- (IBAction)cancelButtonTapped:(id)sender {
   

    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)doneButtonTapped:(id)sender {


    //fetch all photo URLs

    if (photoAssets.assets.count > 0) {



        }else
    {
        TPPMontageVideoController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPMontageVideoController"];
        //controller.data = @"second controller added";
        [self addChildViewController:controller];
        CGRect  rect = CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height);
        controller.view.frame = rect;
        [self.view addSubview:controller.view];
        [controller didMoveToParentViewController:self];
    }
}

- (IBAction)addImageButtonTapped:(id)sender {

    
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.mediaType = QBImagePickerMediaTypeAny;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = YES;

    for (NSInteger atIndex = 0; atIndex < photoAssets.assets.count; atIndex++) {
        [imagePickerController.selectedAssets addObject:(PHAsset *)[photoAssets.assets objectAtIndex:atIndex]];
         }


    [self presentViewController:imagePickerController animated:YES completion:NULL];
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    NSLog(@"Selected assets:");
    [imagePaths removeAllObjects];
    photoAssets.assets = [assets mutableCopy];
    photoAssets.flag = YES;
    for (NSInteger atIndex = 0; atIndex <photoAssets.assets.count; atIndex++) {
        PHAsset *asset = [photoAssets.assets objectAtIndex:atIndex];
        
        if (asset) {
            // get photo info from this asset
            PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
            imageRequestOptions.synchronous = YES;
            
            [[PHImageManager defaultManager] requestImageDataForAsset:asset options:imageRequestOptions
                                                        resultHandler:^(NSData *imageData, NSString *dataUTI,
                                                                        UIImageOrientation orientation,
                                                                        NSDictionary *info)
             {
                 if ([info objectForKey:@"PHImageFileURLKey"]) {
                     NSString *path = [info objectForKey:@"PHImageFileURLKey"];
                     [imageURLs addObject:path];
                     
                     
                 }
                 
                 [imagePaths addObject:[NSData getCompressedImageDataFromImageData:imageData width:800 height:600]];
                 
                 if(atIndex == photoAssets.assets.count - 1)
                 {
                     photoAssets.imagePaths = imagePaths;
                     photoAssets.imageURLs = imageURLs;
                     
                     [self moveToUploadVC];
                 }
             }];
        }
    }

     [self dismissViewControllerAnimated:YES completion:NULL];



}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
   [self.nextBtn setTitle:@"Skip >>" forState:UIControlStateNormal];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)moveToUploadVC
{
    TPPUploadImagesViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPUploadImagesViewController"];
    //controller.data = @"second controller added";
    [self addChildViewController:controller];
    CGRect  rect = CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height);
    controller.view.frame = rect;
    [self.view addSubview:controller.view];
    [controller didMoveToParentViewController:self];
}
@end

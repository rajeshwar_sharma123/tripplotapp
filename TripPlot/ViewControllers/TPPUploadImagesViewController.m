//
//  TPPUploadImagesViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPUploadImagesViewController.h"
#import "TPPMontageVideoController.h"


@implementation TPPUploadImagesViewController

{
    TPPPhotoAssets *photoAssets;
    NSDictionary *awsCredentials;
    NSString *accessKeyId;
    NSString *secretAccessKey;
    int64_t totalBytes;
    int64_t sentBytes;
    TPPCreateTripModel *pastTripObject;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pastTripObject = [TPPCreateTripModel sharedInstance];
    photoAssets = [TPPPhotoAssets new];
    [self.cancelBtn setImage:[UIImage imageNamed:@"btn_Next_normal"] forState:UIControlStateNormal];
    [self.nextBtn setImage:[UIImage imageNamed:@"btn_next"] forState:UIControlStateNormal];
    self.nextBtn.enabled = NO;
    self.cancelBtn.enabled = NO;
    
    [self callServiceToGetAWSCredentials];
    
    
    _amazonImgURLArry = [[NSMutableArray alloc]init];
    _fileNameArry = [[NSMutableArray alloc]init];
    
    _progressView.color = ColorFromRGB(0xB1F464);
    _progressView.showText = @NO;
    _progressView.progress = 0.0;
    _progressView.borderRadius = @3;
    _progressView.animate = @NO;
    _progressView.type = LDProgressSolid;
    _iLbl.layer.cornerRadius = _iLbl.frame.size.width/2;
    _iLbl.clipsToBounds = YES;
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    photoAssets.imagePaths = nil;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self.view removeFromSuperview];
    
}

- (IBAction)doneButtonTapped:(id)sender {
    
    [self callServiceToCreateMontage];
    
}

-(void)callServiceToGetAWSCredentials
{
    NSDictionary *requestData = @{@"token":[UserDeafaults objectForKey:Token]};
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_AWS_CREDENTIALS_URL] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
             // allTypesArray = response[@"response"][@"result"];
            
             awsCredentials = response[@"response"];
             accessKeyId = awsCredentials[@"accessKeyId"];
             secretAccessKey = awsCredentials[@"accessKeyId"];
             NSMutableArray *items = [NSMutableArray new];
             for (NSData *image in photoAssets.imagePaths) {
                 
                 NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".jpg"];
                 DKCarouselViewItem *view = [DKCarouselViewItem new];
                 UIImageView *iView = [[UIImageView alloc]init];
                 UIImage *temp = [UIImage imageWithData:image scale:1.0];
                 iView.image = temp;
                 [self callAWSServiceToupload:image withFileName:fileName];
                 view.view  = (UIImageView *)iView;
                 [items addObject:view];
                 
             }
             [_carouselView setItems:items];
            // [_carouselView setAutoPagingForInterval:2];
             _carouselView.defaultImage = [UIImage imageNamed:@"btn_addfriend_pressed"];
             items = nil;
             photoAssets.imagePaths = nil;
             
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
         
     }];
}


- (void)callServiceToCreateMontage
{
  
   // [self.view showLoaderWithMessage:@"Creating Montage..."];
    NSDictionary *dadaDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [UserDeafaults objectForKey:Token],@"token",
                              pastTripObject.title,@"tripName",
                              _amazonImgURLArry,@"imageUrl",
                              nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:CreateMontageUrl] data:dadaDict withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             [self.view hideLoader];
             pastTripObject.montage = [[response objectForKey:@"response"] objectForKey:@"url"];
             pastTripObject.images = [NSArray arrayWithArray:_amazonImgURLArry];
             
             TPPMontageVideoController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPMontageVideoController"];
             
             //controller.data = @"second controller added";
             [self addChildViewController:controller];
             CGRect  rect = CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height);
             controller.view.frame = rect;
             [self.view addSubview:controller.view];
             [controller didMoveToParentViewController:self];
             
             
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
         [self.view hideLoader];
         
         [self showErrorMessage:errorMessage];
     }];
    
}




-(void) callAWSServiceToupload:(NSData *)imageData withFileName:(NSString *)fileName
{
    
    totalBytes = totalBytes + imageData.length;
    fileName = [NSString stringWithFormat:@"%@/%@",awsCredentials[@"folderDetails"][@"tripFolder"],fileName];
    //AWS configuration
   
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:[awsCredentials[@"regionType"] regionTypeEnumFromString] identityPoolId:awsCredentials[@"poolId"]];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:[awsCredentials[@"region"] regionTypeEnumFromString] credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    
    NSString *stringURL = [NSString stringWithFormat:@"https://%@.%@/%@",awsCredentials[@"bucketName"],awsCredentials[@"endPoint"],fileName];
    [_amazonImgURLArry addObject:stringURL];
    [_fileNameArry addObject:fileName];
    NSLog(@"amazon url...%@",stringURL);
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    expression.uploadProgress = ^(AWSS3TransferUtilityTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AWSS3TransferUtilityTask *task1 = task;
             NSLog(@"task.. %@",task1.key);
             NSLog(@"byte sent... %lld",bytesSent);
             NSLog(@"total byte sent... %lld",totalBytesSent);
             NSLog(@"total byte expected to sent... %lld",totalBytesExpectedToSend);
            sentBytes = sentBytes + bytesSent;
            _progressView.progress = (CGFloat)sentBytes/totalBytes;
        });
    };
    
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Alert a user for transfer completion.
            // On failed uploads, `error` contains the error object.
            NSLog(@"task.. completed %@",task.key);
            
            for (NSString *str in _fileNameArry) {
                if ([str isEqualToString:task.key]) {
                    int i = (int)[_fileNameArry indexOfObject:str];
                    [photoAssets.assets removeObjectAtIndex:i];
                    [_fileNameArry removeObjectAtIndex:i];
                    break;
                }
            }
            if (photoAssets.assets.count == 0) {
                self.nextBtn.enabled = YES;
                self.cancelBtn.enabled = YES;
                photoAssets.flag = NO;
                [self showSuccessMessage:@"Photos uploaded successfully..."];
            }
            
            
        });
    };
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    [[transferUtility uploadData:imageData
                          bucket:awsCredentials[@"bucketName"]
                             key:fileName
                     contentType:@"image/jpg"
                      expression:expression
                completionHander:completionHandler] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
        }
        if (task.exception) {
            NSLog(@"Exception: %@", task.exception);
        }
        if (task.result) {
            AWSS3TransferUtilityUploadTask *uploadTask = task.result;
            
            // Do something with uploadTask.
        }
        
        return nil;
    }];
}

//-(NSString *) getImagePath:(NSString *)fileName
//{
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:fileName];
//
//    return  getImagePath;//[NSURL URLWithString:getImagePath];
//}
//
//-(void)saveImage:(NSData *)imageData fileURL:(NSString *)url
//{
//
//    [imageData writeToFile:url atomically:NO];
//
//}


@end

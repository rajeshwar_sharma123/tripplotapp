//
//  TPPAddMediaViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPAddMediaViewController.h"
#import "TPPAddImagesViewController.h"
#import "TPPUploadImagesViewController.h"
#import "TPPMontageVideoController.h"

@implementation TPPAddMediaViewController
{
    TPPCreateTripModel *newTripObject;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newTripObject = [TPPCreateTripModel sharedInstance];
    

    TPPTripMenuStepperView *menuStepper = [[[NSBundle mainBundle] loadNibNamed:@"TPPTripMenuStepperView" owner:self options:nil] objectAtIndex:0];
    CGRect frame = self.menuView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [menuStepper setFrame:frame];

    [self.menuView addSubview:menuStepper];
    [menuStepper setSelectedIndex:3 isNew:newTripObject.isNew];
    
    TPPAddImagesViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPAddImagesViewController"];
    [self addChildViewController:controller];
    CGRect  rect = CGRectMake(0, 0, _contentView.frame.size.width,  _contentView.frame.size.height);
    controller.view.frame = rect;
    [_contentView addSubview:controller.view];
    [controller didMoveToParentViewController:self];
    
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

-(void)viewDidAppear:(BOOL)animated
{
   
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
@end

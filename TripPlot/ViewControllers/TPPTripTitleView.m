//
//  TPPTripTitleView.m
//  TripPlot
//
//  Created by Daffolap-21 on 06/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPTripTitleView.h"

@implementation TPPTripTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
	[self.titleTextField setValue:TPPTextColor forKeyPath:@"_placeholderLabel.textColor"];
}


@end

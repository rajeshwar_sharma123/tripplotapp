//
//  TPPTripDetailViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 09/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>


@interface TPPTripDetailViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *travellerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *expandableView;

@property (weak, nonatomic) IBOutlet UILabel *routeLabel;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet GRKGradientView *gradientBGView;

@property TPPTripModel *tripModel;
@property NSInteger selectedButtonIndex;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIView *tripPointsView;
@property (weak, nonatomic) IBOutlet UIView *shareView;

//@property (strong, nonatomic) UITableViewCell *cell;
@property (strong, nonatomic) NSString *strCheck;
@property (weak, nonatomic) UIViewController *vcObj;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;
@end

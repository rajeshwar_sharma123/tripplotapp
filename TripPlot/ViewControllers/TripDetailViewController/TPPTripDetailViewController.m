//
//  TPPTripDetailViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 09/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPTripDetailViewController.h"
#import "TPPAcceptRejectTableViewCell.h"
#import "TPPMakeTripShareCell.h"
@interface TPPTripDetailViewController ()<GMSMapViewDelegate>
{
    // GMSMapView *mapView;
    UIButton *boundMarkersButton;
    TPPLocationMenuView *tripPointsView;
}
@end

@implementation TPPTripDetailViewController

#pragma mark- LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.selectedButtonIndex)
    {
        UIButton *button = [UIButton new];
        button.tag = self.selectedButtonIndex;
        [self tripPointButtonTapped:button];
        
    }
    
    // To update the UI if user likes/Follows/Joins etc
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userActivitySelector:) name:UserActivityNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    self.navigationController.navigationBar.hidden = YES;
    
    [self initViews];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initialiseBoundButton];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.menuContainerViewController.panMode = MFSideMenuPanModeDefault;
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Initializations

-(NSString *)getTravellerDescription
{
    NSString *name = _tripModel.createdBy[@"firstname"];
    NSString *statusLable;
    if (_tripModel.tripStatus == TripStatus_UpcomingTrip)
    {
        statusLable = @"Upcoming trip by";
    }
    else if (_tripModel.tripStatus == TripStatus_OngoingTrip)
    {
        statusLable = @"Ongoing trip by";
    }
    
    NSString *travellerDescription = [NSString stringWithFormat:@"%@ %@",statusLable,name];
    
    
    
    
    //    NSRange range = [travellerDescription rangeOfString:@"y"];
    //    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:travellerDescription];
    //    [string addAttribute:NSForegroundColorAttributeName value:COLOR_LIGHT_GREEN range:NSMakeRange(range.location+2,name.length)];
    //
    
    
    
    return travellerDescription;
    
}

-(void)initViews
{
    self.gradientBGView.gradientOrientation = GRKGradientOrientationDown;
    if (_tripModel.tripStatus == TripStatus_UpcomingTrip)
    {
        
        self.gradientBGView.gradientColors = [NSArray arrayWithObjects:TPPUpcomingCellGradientColorStart, TPPUpcomingCellGradientColorMidle,TPPUpcomingCellGradientColorEnd,nil];
    }
    else if (_tripModel.tripStatus == TripStatus_OngoingTrip)
    {
        self.gradientBGView.gradientColors = [NSArray arrayWithObjects:TPPOngoingCellGradientColorStart, TPPOngoingCellGradientColorMidle,TPPOngoingCellGradientColorEnd,nil];
        
    }
    
    
    self.travellerDescriptionLabel.text = _tripModel.createdBy[@"firstname"];
    self.titleLabel.text = _tripModel.title;
    
    if (_tripModel.tripLocations.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"%@ to %@",_tripModel.tripLocations[0][@"name"],[_tripModel.tripLocations lastObject][@"name"]];
        self.locationDescriptionLabel.text = str;
    }

    
    self.tripDurationLabel.text = [TPPUtilities getTripDurationStringFromDictionary:_tripModel.tripDate];
    
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height/2;
    self.userImageView.clipsToBounds = YES;
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:_tripModel.createdBy[@"profileImage"]]
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    
    self.routeLabel.text = [TPPUtilities getRouteForTrip:_tripModel];
    
    
    [self initShareTab];
    [self initMapView];
    [self addTripPointsView];
    [tripPointsView setBadgesWithTripModel:_tripModel];
    
    [self.expandableView removeSubviewsOfKindOfClasses:@[@"TPPScrollMenu"]];
    TPPScrollMenu *menu = [[TPPScrollMenu alloc]initWithFrame:self.expandableView.frame];
    menu.tripModel = _tripModel;
    menu.backgroundColorForButtons = Color_UpcomingTripCellButtonBackground;
    
    [self.expandableView addSubview:menu];
    [menu configure];
    
    [self drawPolyline:_tripModel.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
    
    
    [self configureCellForBottomView];
    
}
- (void)configureCellForBottomView
{
    if ([self.strCheck isEqualToString:@"invite"]) {
        
        
        TPPAcceptRejectTableViewCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPAcceptRejectTableViewCell" owner:self options:nil] objectAtIndex:0];
        
        [cell bindDataWithModel:_tripModel];
        cell.delegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0, ScreenWidth-16, self.shareView.frame.size.height);
        cell.bottomView.frame = cell.frame;
        cell.bottomView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = _selectedIndexPath;
        [self.shareView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [self.shareView addSubview:cell];
        
    }
    else if ([self.strCheck isEqualToString:@"bucket"])
    {
        TPPMakeTripShareCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPMakeTripShareCell" owner:self options:nil] objectAtIndex:0];
        
        [cell bindDataWithModel:_tripModel];
        cell.shareCellDelegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0,ScreenWidth-16, self.shareView.frame.size.height);
        cell.bottomView.frame = cell.frame;
        cell.bottomView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = self.selectedIndexPath;
        [self.shareView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [self.shareView addSubview:cell];
    }
    else if ([self.strCheck isEqualToString:@"mytrip"])
    {
        TPPInviteFriendShareCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPInviteFriendShareCell" owner:self options:nil] objectAtIndex:0];
        
        cell.tripModel = self.tripModel;
        cell.delegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0,ScreenWidth-16, self.shareView.frame.size.height);
        cell.mainView.frame = cell.frame;
        cell.mainView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = self.selectedIndexPath;
        [self.shareView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [self.shareView addSubview:cell];
    }
    else if ([self.strCheck isEqualToString:@"following"]||[self.strCheck isEqualToString:@"home"])
    {
        NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
        
        if ([self.tripModel.createdBy[@"_id"] isEqualToString:userProfile[@"_id"]])
        {
            TPPInviteFriendShareCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPInviteFriendShareCell" owner:self options:nil] objectAtIndex:0];
            
            cell.tripModel = self.tripModel;
            cell.delegate = _vcObj;
            cell.frame = CGRectMake(0, 0,ScreenWidth-16, self.shareView.frame.size.height);
            cell.mainView.frame = cell.frame;
            cell.mainView.translatesAutoresizingMaskIntoConstraints = YES;
            cell.backgroundColor = [UIColor blackColor];
            cell.selectedIndexPath = self.selectedIndexPath;
            
            [self.shareView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
            [self.shareView addSubview:cell];
            
        }
        else if(!self.tripModel.isOpen || !self.tripModel.isGroup)
        {
            TPPShareUpcomingWithoutJoinTableViewCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPShareUpcomingWithoutJoinTableViewCell" owner:self options:nil] objectAtIndex:0];
            
            [cell bindDataWithModel:self.tripModel];
            cell.shareCellDelegate = (id)_vcObj;
            cell.frame = CGRectMake(0, 0,ScreenWidth-16, self.shareView.frame.size.height);
            cell.mainView.frame = cell.frame;
            cell.mainView.translatesAutoresizingMaskIntoConstraints = YES;
            cell.backgroundColor = [UIColor blackColor];
            cell.selectedIndexPath = self.selectedIndexPath;
            
            [self.shareView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
            [self.shareView addSubview:cell];
            
        }
        
        
    }
    
    
    
    
}

-(void)initShareTab
{
    if (_tripModel.isFollowedByMe)
    {
        [self.followButton setTitle:Txt_UnFollow forState:UIControlStateNormal];
    }
    else
    {
        [self.followButton setTitle:Txt_Follow forState:UIControlStateNormal];
    }
    
    if (_tripModel.isJoinedByMe)
    {
        [self.joinButton setTitle:Txt_UnJoin forState:UIControlStateNormal];
    }
    else
    {
        [self.joinButton setTitle:Txt_Join forState:UIControlStateNormal];
    }
}

-(void)initMapView
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:28.4700
                                                            longitude:77.0300
                                                                 zoom:6];
    _mapView.camera = camera;
    _mapView.delegate=self;
    
    //[_mapView animateToZoom:kGMSMinZoomLevel];
}

-(void)initialiseBoundButton
{
    boundMarkersButton = [[UIButton alloc]initWithFrame:CGRectMake(_mapView.frame.size.width-50,_mapView.frame.size.height-50,40,40)];
    [boundMarkersButton setImage:[UIImage imageNamed:@"btn_explore_normal"]  forState:UIControlStateNormal];
    [boundMarkersButton addTarget:self  action:@selector(boundCameraToFitAllMarkers)    forControlEvents:UIControlEventTouchUpInside];
    boundMarkersButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    [_mapView addSubview:boundMarkersButton];
    
    [self boundCameraToFitAllMarkers:_tripModel.tripLocations onMap:self.mapView];
    
}

-(void)boundCameraToFitAllMarkers
{
    [self boundCameraToFitAllMarkers:_tripModel.tripLocations onMap:self.mapView];
    
}


-(void) drawPolyline:(NSArray *) polylines map:(GMSMapView *)map shouldSelected:(BOOL) isSelected withTitle:(NSInteger ) title
{
    for (NSInteger atIndex2 = 0; atIndex2 <polylines.count; atIndex2++ ) {
        NSDictionary *routeOverviewPolyline = [polylines objectAtIndex:atIndex2];
        NSString *points = [routeOverviewPolyline objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:points];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.title = [NSString stringWithFormat:@"%ld", title];
        polyline.tappable = YES;
        polyline.strokeWidth = 4;
        if (isSelected) {
            polyline.strokeColor = COLOR_FOR_MAP_ROUTE;
        }
        else
        {
            polyline.strokeColor = COLOR_FOR_MENU_CONTENT;
        }
        polyline.map = map;
    }
}


#pragma mark- IBAction methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)addTripPointsView
{
    [self.view removeSubviewsOfKindOfClasses:@[@"TPPLocationMenuView"]];
    
    tripPointsView = [[[NSBundle mainBundle] loadNibNamed:@"TPPLocationMenuView" owner:self options:nil] objectAtIndex:0];
    [tripPointsView.restaurantButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.fuelStationButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.stopageButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.atmButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.hotelButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [tripPointsView.hospitalButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    tripPointsView.backgroundColor = COLOR_FOR_MENU_CONTENT;
    tripPointsView.frame = CGRectMake(0, 0,self.tripPointsView.frame.size.width,self.tripPointsView.frame.size.height);
    
    [self.tripPointsView addSubview:tripPointsView];
    
}

-(void)tripPointButtonTapped:(UIButton *)sender
{
    [tripPointsView resetAllMenuButtonsImage:tripPointsView];
    
    NSArray *placesArray;
    // NSLog(@"tapped index %d",sender.tag);
    
    switch (sender.tag)
    {
        case 1:
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_fuel_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"fuel"];
            break;
            
        case 2:
            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_eat_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"food"];
            
            break;
        case 3:
           
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_stay_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"restroom"];
            
            break;
        case 4:
            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_POI_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"poi"];
            
            break;
        case 5:
            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_medical_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"medical"];
            
            break;
        default:
            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_atm_active"] forState:UIControlStateNormal];
            placesArray = _tripModel.tripPoints[@"atm"];
            
            break;
    }
    if (placesArray.count>0) {
        [self drawMarkers:placesArray onMap:self.mapView];
    }
    else
    {
        [self drawMarkers:placesArray onMap:self.mapView];
        [self.view showToasterWithMessage:MSG_No_Places_To_show];
    }
}

- (IBAction)shareButtonTapped:(id)sender
{
    [self defaultShareWithText:@"" image:_gradientBGView.snapshotImage url:nil];
}

- (IBAction)joinButtonTapped:(id)sender
{
    [TPPAppServices callServiceForJoinUnjoinTripWithTripModel:_tripModel button:sender indexPath:nil];
}

- (IBAction)followButtonTapped:(UIButton *)sender
{
    
    [TPPAppServices callServiceForFollowUnfollowTripWithTripModel:_tripModel button:sender indexPath:nil];
}
//
-(void)userActivitySelector:(NSNotification *)notification
{
    if ([notification.userInfo[Txt_IsFollowedUnfollowed] boolValue] && ![self.strCheck isEqualToString:@"home"])
    {
        
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [self viewWillAppear:NO];
}



#pragma mark- Map related

-(void)drawMarkers:(NSArray *) places onMap:(GMSMapView *)map
{
    [self.mapView clear];
    [self boundCameraToFitAllMarkers:_tripModel.tripLocations onMap:self.mapView];
    for (NSInteger atIndex = 0; atIndex <places.count; atIndex++)
    {
        NSDictionary *placeDetails = [places objectAtIndex:atIndex];
        NSDictionary *geometry = placeDetails[@"geometry"];
        NSDictionary *geoCode = geometry[@"location"];
        
        
        CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"lat"] doubleValue] longitude:[geoCode[@"lng"] doubleValue]];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        //marker.icon = [UIImage imageNamed:@"icn__0000_place"];
        marker.tappable = YES;
        marker.snippet = placeDetails[@"name"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = map;
    }
}


- (void)boundCameraToFitAllMarkers:(NSArray *)allMarkersMArray onMap:(GMSMapView *)map

{
    GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] init];
    
    for (NSInteger atIndex = 0;atIndex <allMarkersMArray.count; atIndex ++) {
        NSDictionary *tripLocation = [allMarkersMArray objectAtIndex:atIndex];
        NSDictionary *geoCode = tripLocation[@"geoCode"];
        
        CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"latitude"] doubleValue] longitude:[geoCode[@"longitude"] doubleValue]];
        
        
        if (location.coordinate.longitude > 0 && location.coordinate.latitude > 0) {
            bounds = [bounds includingCoordinate:location.coordinate];
        }
        //add markers
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        
        if (atIndex == 0) {
            marker.icon = [UIImage imageNamed:@"circle"];
        }
        else if (atIndex == allMarkersMArray.count-1)
        {
            marker.icon = [UIImage imageNamed:@"circle_Red"];
        }
        else
        {
            marker.icon = [UIImage imageNamed:@"circle_blue"];
        }
        marker.tappable = YES;
        marker.snippet = tripLocation[@"name"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = map;
        
    }
    [map animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:32]];
    
    [self drawPolyline:_tripModel.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
}

@end

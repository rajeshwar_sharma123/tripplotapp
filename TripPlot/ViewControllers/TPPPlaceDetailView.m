//
//  TPPPlaceDetailView.m
//  TripPlot
//
//  Created by Daffolap-21 on 05/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPPlaceDetailView.h"

@implementation TPPPlaceDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.distanceTxtFld.layer.cornerRadius = 4.0;
    self.distanceTxtFld.clipsToBounds = YES;

    self.placeNameTxtFld.layer.cornerRadius = 4.0;
    self.placeNameTxtFld.clipsToBounds = YES;
}

- (void)setCurrentLocation:(CLLocation *)location model:(TPPSharedTripCountsModel *)model info:(NSDictionary *)dic index:(NSInteger )index
{
    _currentLocation = location;
    _tripCountsWithDetail = model;
    _tagIndex = index;
    _placeImageView.image = [UIImage imageNamed:dic[@"image"]];
    _TitleLabel.text = dic[@"name"];
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    
    [self setHidden:YES];
    
}


//- (IBAction)doneButtonapped:(id)sender {
//    
//    [self callServiceToAddPlaces:self.tripCountsWithDetail.liveTrip];
//}

- (void)callServiceToAddPlaces:(TPPTripModel *)tripModel
{
    if (![self isUserInputsValid]) {
        return;
    }
    [self showLoader];
    NSMutableDictionary *mainDictionary =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 [UserDeafaults objectForKey:Token],@"token",
                                                 tripModel._id ,@"tripId",
                                                 nil];
 
        NSDictionary *dic = @{
                                @"geometry": @{
                                    @"location": @{
                                       @"lat": [NSString stringWithFormat:@"%f", _currentLocation.coordinate.latitude],
                                       @"lng":[NSString stringWithFormat:@"%f", _currentLocation.coordinate.longitude]
                                    }
                                },
                                @"icon":@"",
                                @"id":@"",
                                @"name":_placeNameTxtFld.text,
                                @"photos":@[],
                                @"place_id":@"",
                                @"rating":@"",
                                @"reference":@"",
                                @"scope":@"",
                                @"types":@[],
                                @"vicinity":@""
                                };
    
    NSArray *arr = @[dic];
    
        
    switch (_tagIndex) {
        case 1:
            [mainDictionary setObject:arr forKey:@"fuel"];
            break;
        case 2:
            [mainDictionary setObject:arr  forKey:@"food"];
            
            break;
        case 3:
            [mainDictionary setObject:arr  forKey:@"restroom"];
            
            break;
        case 4:
            [mainDictionary setObject:arr  forKey:@"poi"];
            
            break;
        case 5:
            [mainDictionary setObject:arr  forKey:@"medical"];
            
            break;
        case 6:
            [mainDictionary setObject:arr forKey: @"atm"];
            
            break;
        default:
            break;
    }
    
    
    
[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:SaveTrippointsUrl] data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             UIViewController *vc = (UIViewController *)_delegate;
             [vc showSuccessMessage:@"Halt point added"];
             [self updateTripModelTripPoints:dic];
             [self cancelButtonTapped:nil];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         UIViewController *vc = [TPPUtilities getCurrentViewController];
         [vc showErrorMessage:@"Cannot add halt point"];
         [self cancelButtonTapped:nil];
         [self hideLoader];
     }];
}


- (void)updateTripModelTripPoints:(NSDictionary *)dataDic
{
    NSMutableDictionary  *dic = [_tripCountsWithDetail.liveTrip.tripPoints mutableCopy];
    NSMutableArray *muArry;
    switch (self.tagIndex) {
        case 1:
            muArry = [dic[@"fuel"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"fuel"];
            break;
        case 2:
            muArry = [dic[@"food"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"food"];
            break;
        case 3:
            muArry = [dic[@"restroom"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"restroom"];
            break;
        case 4:
            muArry = [dic[@"poi"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"poi"];
            break;
        case 5:
            muArry = [dic[@"medical"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"medical"];
            break;
        case 6:
            muArry = [dic[@"atm"] mutableCopy];
            [muArry addObject:dataDic];
            [dic setObject:muArry forKey:@"atm"];
            break;
        default:
            break;
    }
     _tripCountsWithDetail.liveTrip.tripPoints = dic;
    [_delegate updateBadgesForPlaces:_tripCountsWithDetail];

}
-(BOOL)isUserInputsValid
{
    if (self.placeNameTxtFld.text.trimmedString.length == 0)
    {
        UIViewController *vc = [TPPUtilities getCurrentViewController];
        
        [vc showErrorMessage:PLACE_NAME_EMPTY];
        
        return NO;
    }
    if (self.distanceTxtFld.text.trimmedString.length == 0)
    {
        UIViewController *vc = [TPPUtilities getCurrentViewController];
        
        [vc showErrorMessage:DISTANCE_FIELD_EMPTY];
        
        return NO;
    }
       
    
    return YES;
}

@end

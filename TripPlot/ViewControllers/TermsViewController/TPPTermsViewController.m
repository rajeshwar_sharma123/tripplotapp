//
//  TPPTermsViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPTermsViewController.h"

@interface TPPTermsViewController ()

@end

@implementation TPPTermsViewController

- (void)viewDidLoad
{
    if (_shouldHideSideMenu)
    {
        
    } 
    else
    {
         [super viewDidLoad];
    }
    
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"Terms & Conditions";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"termsandcondition" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

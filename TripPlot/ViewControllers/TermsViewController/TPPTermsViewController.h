//
//  TPPTermsViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 03/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPTermsViewController : TPPBaseViewController


@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property BOOL shouldHideSideMenu;

@end

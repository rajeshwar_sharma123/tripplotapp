//
//  TPPMapDetailTableViewCell.h
//  TripPlot
//
//  Created by Daffolap-21 on 03/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface TPPMapDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *routeDetailaView;

//today's change
@property (weak, nonatomic) IBOutlet UILabel *routeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end

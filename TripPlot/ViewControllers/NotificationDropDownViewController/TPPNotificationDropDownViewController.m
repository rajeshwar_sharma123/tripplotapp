//
//  TPPNotificationDropDownViewController.m
//  TripPlot
//
//  Created by Daffodil iPhone on 12/31/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPNotificationDropDownViewController.h"
#import "TPPNotificationDropDownTableViewCell.h"
#import "TPPNoNotificationView.h"
@interface TPPNotificationDropDownViewController ()<NSFetchedResultsControllerDelegate>
{
    __weak IBOutlet UITableView *mainTableView;
    TPPNoNotificationView *notificationView;
    
}
@end

@implementation TPPNotificationDropDownViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	mainTableView.estimatedRowHeight = 65;
	mainTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];

		// Initialize Fetch Request
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"TripEntity"];
		// Add Sort Descriptors
	[fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"updatedOn" ascending:NO]]];

    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[TPPCoreDataHandler sharedInstance].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error)
    {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    notificationView = [[[NSBundle mainBundle] loadNibNamed:@"TPPNoNotificationView" owner:self options:nil] objectAtIndex:0];
    notificationView.frame = self.view.bounds;
    mainTableView.backgroundView = notificationView;
    mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark Table View DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    if([sectionInfo numberOfObjects]>0)mainTableView.backgroundView.hidden = YES;
    else mainTableView.backgroundView.hidden = NO;
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *plotDropDownIdentifiere = @"notificationDropDownCell";
    TPPNotificationDropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:plotDropDownIdentifiere];
    if (cell == nil)
    {
        cell = [[TPPNotificationDropDownTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:plotDropDownIdentifiere];
    }
    [cell configureCellWithFetchedResultsController:self.fetchedResultsController indexPath:indexPath];
    
    return cell;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSArray *sections = [self.fetchedResultsController sections];
//    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
//    
//    if ([sectionInfo numberOfObjects] == 0)
//    {
//		return @"No new notifications";
//	}
//	return nil;
//}
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
//{
//		// Background color
//	view.tintColor = [UIColor whiteColor];//[UIColor colorWithRed:77.0/255.0 green:162.0/255.0 blue:217.0/255.0 alpha:1.0];
//										  // Text Color
//	UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//	[header.textLabel setTextColor:TPPTextColor];
//	header.textLabel.textAlignment = NSTextAlignmentCenter;
//
//}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Fetch Record
    [_delegate hideDropDownView];
    TripEntity *trip = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MemberEntity *member = [[trip.members allObjects] lastObject];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationTappedNotification" object:self userInfo:@{@"userId":member.userId}];
    
    [self.notificationButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}


#pragma mark- NSFetchedResultsControllerDelegate methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [mainTableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [mainTableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
        {
            [mainTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationTop];
            break;
        }
        case NSFetchedResultsChangeDelete:
        {
            [mainTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate:
        {
            TPPNotificationDropDownTableViewCell *cell =(TPPNotificationDropDownTableViewCell *)[mainTableView cellForRowAtIndexPath:indexPath];
            [cell configureCellWithFetchedResultsController:self.fetchedResultsController indexPath:indexPath];
            
            [mainTableView moveRowAtIndexPath:indexPath toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            
            break;
        }
        case NSFetchedResultsChangeMove:
        {
            [mainTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [mainTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
            break;
        }
    }
}



@end

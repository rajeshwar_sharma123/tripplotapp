//
//  TPPLivePlotViewController.m
//  TripPlot
//
//  Created by Daffolap-21 on 30/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPLivePlotViewController.h"
#import "TPPRouteDirectionServices.h"
@interface TPPLivePlotViewController ()

@end

@implementation TPPLivePlotViewController
{
	TPPSharedTripCountsModel *tripCountsWithDetail;
	UIButton *boundMarkersButton;
	TPPLocationMenuView *tripPointsView;
    TPPLocationDynamicMenuView *locationMenuView;
	BOOL firstLocationUpdate_;
    TPPVerticalMenu *verticalMenu;
     NSDictionary *awsCredentials;
    
    TPPPhotoAssets *photoAssets;
    NSMutableArray *imagePaths;
    NSMutableArray *imageURLs;
    TPPReachedDestinationView *reachedView;
    TPPPlaceDetailView *placeDetailPopupView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	tripCountsWithDetail = [TPPSharedTripCountsModel sharedInstance];
	//call service to fetch trip details
	[self callServiceToGetTripDetails];
	[self setUserLocation];
    _mapView.myLocationEnabled = YES;
		// Listen to the myLocation property of GMSMapView.
//	[_mapView addObserver:self
//			   forKeyPath:@"myLocation"
//				  options:NSKeyValueObservingOptionNew
//				  context:NULL];

	self.title = @"Live Plot";
    photoAssets = [TPPPhotoAssets sharedInstance];
    _amazonImgURLArry = [[NSMutableArray alloc]init];
    _fileNameArry = [[NSMutableArray alloc]init];
    imagePaths = [NSMutableArray new];
    
    self.mapView.settings.compassButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) viewDidAppear:(BOOL)animated
{
	[self addTripPointsView];
}

-(void)viewDidDisappear:(BOOL)animated
{
	//[_mapView removeObserver:self forKeyPath:@"myLocation"];
}


-(void)setUserLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    if(SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        _locationManager.delegate = self;
        
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
        [_locationManager startUpdatingLocation];
        
    }
    else
    {
        _locationManager.delegate = self;
        
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
        if(authorizationStatus==kCLAuthorizationStatusNotDetermined||authorizationStatus==kCLAuthorizationStatusDenied)
        {
            [_locationManager requestWhenInUseAuthorization];
        }
        if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
            authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse)
        {
            _locationManager.delegate = self;
            
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
            
            [_locationManager startUpdatingLocation];
            
        }
    }
}

#pragma -mark Location delegates
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
	[_mapView animateToLocation:newLocation.coordinate];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _currentLocation = [locations lastObject];
    //[self setCameraOnUserLocation];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError: %@", error);
	[self.view showToasterWithMessage:error.localizedDescription];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusDenied)
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"TripPlot"
                                              message:@"Kindly allow TripPlot to access your location"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                      
                                       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                   }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [self.navigationController popViewControllerAnimated:YES];
                                       }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways ||
        authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        _locationManager.delegate = self;
        
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
        [_locationManager startUpdatingLocation];
    }
}


#pragma mark - TPPPlaceDetailDelegate

-(void)updateBadgesForPlaces:(TPPSharedTripCountsModel *)shareModel
{
    [locationMenuView setBadgesForPlaces:shareModel];
    [self addPlacesMarkerOnMapView];
}

#pragma mark - TPPMenuViewDelegate
-(void)buttonTappedAtRowIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            [self showSelectProfileActionSheet];
            break;
        case 1:
        {
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
            
        case 2:
            
            break;
        case 3:
        {
            reachedView = [[[NSBundle mainBundle] loadNibNamed:@"TPPReachedDestinationView" owner:self options:nil] objectAtIndex:0];
            reachedView.parrentObj = self;
            [reachedView bindDataWithModel:tripCountsWithDetail.liveTrip];
            reachedView.frame = self.view.frame;
            [self.view addSubview:reachedView];
        }
            break;
        default:
            break;
    }
}


-(void)locationMenuTapped:(id)sender info:(NSDictionary *)infoDic atIndex:(long)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TPPPlaceDetailView" owner:nil options:nil];
    placeDetailPopupView = nibContents[0];
    placeDetailPopupView.delegate = self;
    [placeDetailPopupView.doneButton addTarget:self action:@selector(placeDetailPopupViewDoneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [placeDetailPopupView setCurrentLocation:_currentLocation model:tripCountsWithDetail info:infoDic index:index];
    [placeDetailPopupView setFrame:self.view.frame];
    [self.view addSubview:placeDetailPopupView];
    
    [TPPRouteDirectionServices getDistanceWithLocations:[self getOriginPointLocation] andDestination:_currentLocation  andCompletitionBlock:^(id distance, NSError *error){
        [placeDetailPopupView hideLoader];
        if (error)
        {
            NSLog(@"%@", error);
        }
        else
        {
            double distanceInMetre = [distance[@"value"] floatValue];
            placeDetailPopupView.distanceTxtFld.text = [NSString stringWithFormat:@"  %.2f",distanceInMetre/1000.00 ];
        }
    }];
    
}

- (void)placeDetailPopupViewDoneBtnAction:(id)sender {
      [placeDetailPopupView callServiceToAddPlaces:tripCountsWithDetail.liveTrip];
}

- (void)showSelectProfileActionSheet
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Profile Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
}
#pragma mark- UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                    {
                        UIImagePickerController *imagePickerCamera = [[UIImagePickerController alloc] init];
                        [imagePickerCamera setSourceType:UIImagePickerControllerSourceTypeCamera];
                        [imagePickerCamera setDelegate:self];
                        [self presentViewController:imagePickerCamera animated:YES completion:nil];
                    }
                    else
                    {
                        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your device doesn't have a camera." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                    }
                    break;
                case 1:
                {
                    QBImagePickerController *imagePickerController = [QBImagePickerController new];
                    imagePickerController.delegate = self;
                    imagePickerController.mediaType = QBImagePickerMediaTypeImage;
                    imagePickerController.allowsMultipleSelection = YES;
                    imagePickerController.showsNumberOfSelectedAssets = YES;
                    
                    for (NSInteger atIndex = 0; atIndex < photoAssets.assets.count; atIndex++) {
                        [imagePickerController.selectedAssets addObject:(PHAsset *)[photoAssets.assets objectAtIndex:atIndex]];
                    }
                    
                    
                    [self presentViewController:imagePickerController animated:YES completion:NULL];

                    
                }
                    break;
                    
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


#pragma mark- UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [imagePaths removeAllObjects];
    [_amazonImgURLArry removeAllObjects];
    [_fileNameArry removeAllObjects];
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    if(image != nil){
        [imagePaths addObject:[NSData getCompressedImageDataFromImage:image width:800 height:600]];
         photoAssets.imagePaths = imagePaths;
        [self callServiceToGetAWSCredentials];
        
    }
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    NSLog(@"Selected assets:");
    [imagePaths removeAllObjects];
    [_fileNameArry removeAllObjects];
    [_amazonImgURLArry removeAllObjects];
    photoAssets.assets = [assets mutableCopy];
    
    for (NSInteger atIndex = 0; atIndex <photoAssets.assets.count; atIndex++) {
        PHAsset *asset = [photoAssets.assets objectAtIndex:atIndex];
        
        if (asset) {
            // get photo info from this asset
            PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
            imageRequestOptions.synchronous = YES;
            
            [[PHImageManager defaultManager] requestImageDataForAsset:asset options:imageRequestOptions
                                                        resultHandler:^(NSData *imageData, NSString *dataUTI,
                                                                        UIImageOrientation orientation,
                                                                        NSDictionary *info)
             {
                 if ([info objectForKey:@"PHImageFileURLKey"]) {
                     NSString *path = [info objectForKey:@"PHImageFileURLKey"];
                     [imageURLs addObject:path];
                     
                     
                 }
                 
                 [imagePaths addObject:[NSData getCompressedImageDataFromImageData:imageData width:800 height:600]];
                 
                 if(atIndex == photoAssets.assets.count - 1)
                 {
                     photoAssets.imagePaths = imagePaths;
                     //photoAssets.imageURLs = imageURLs;
                     
                     [self callServiceToGetAWSCredentials];
                 }
             }];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)callServiceToGetAWSCredentials
{
    [self.view showLoader];
    NSDictionary *requestData = @{@"token":[UserDeafaults objectForKey:Token]};
    
    [TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:GET_AWS_CREDENTIALS_URL] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
     {
         
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             
              awsCredentials = response[@"response"];
             
              for (NSData *image in photoAssets.imagePaths) {
                 
                 NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".jpg"];
                [self callAWSServiceToupload:image withFileName:fileName];
                 
             }
            
             photoAssets.imagePaths = nil;
           
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
        [self.view hideLoader];
         
     }];
}

- (void)callServiceToSaveTripImages
{
   
    NSDictionary *dadaDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [UserDeafaults objectForKey:Token],@"token",
                              tripCountsWithDetail.liveTrip._id,@"tripId",
                              _amazonImgURLArry,@"imageUrls",
                              nil];
    
    
    [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:SaveTripimageUrl] data:dadaDict withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [self.view hideLoader];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
              [self.view showToasterWithMessage:@"Photos uploaded successfully..."];
             
            }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         
         [self.view hideLoader];
         
         
     }];
    
}

-(void) callAWSServiceToupload:(NSData *)imageData withFileName:(NSString *)fileName
{
    
   
    fileName = [NSString stringWithFormat:@"%@/%@",awsCredentials[@"folderDetails"][@"tripFolder"],fileName];
    //AWS configuration
    
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:[awsCredentials[@"regionType"] regionTypeEnumFromString] identityPoolId:awsCredentials[@"poolId"]];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:[awsCredentials[@"region"] regionTypeEnumFromString] credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    
    NSString *stringURL = [NSString stringWithFormat:@"https://%@.%@/%@",awsCredentials[@"bucketName"],awsCredentials[@"endPoint"],fileName];
    [_amazonImgURLArry addObject:stringURL];
    [_fileNameArry addObject:fileName];
    NSLog(@"amazon url...%@",stringURL);
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    expression.uploadProgress = ^(AWSS3TransferUtilityTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AWSS3TransferUtilityTask *task1 = task;
            NSLog(@"task.. %@",task1.key);
            NSLog(@"byte sent... %lld",bytesSent);
            NSLog(@"total byte sent... %lld",totalBytesSent);
            NSLog(@"total byte expected to sent... %lld",totalBytesExpectedToSend);
            
           
        });
    };
    
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Alert a user for transfer completion.
            // On failed uploads, `error` contains the error object.
            NSLog(@"task.. completed %@",task.key);
            
            for (int i = 0 ; i< _fileNameArry.count;i++) {
                NSLog(@"index....%d",i);
                NSString *str = [_fileNameArry objectAtIndex:i];
                if ([str isEqualToString:task.key]) {
                    if (photoAssets.assets.count !=0) {
                        [photoAssets.assets removeObjectAtIndex:i];
                        [_fileNameArry removeObjectAtIndex:i];
                        break;
                        
                        
                    }
                }
            }
            if (photoAssets.assets.count == 0) {
                [self callServiceToSaveTripImages];
               
            }
            
            
        });
    };
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    [[transferUtility uploadData:imageData
                          bucket:awsCredentials[@"bucketName"]
                             key:fileName
                     contentType:@"image/jpg"
                      expression:expression
                completionHander:completionHandler] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            [self.view hideLoader];
        }
        if (task.exception) {
            [self.view hideLoader];
            NSLog(@"Exception: %@", task.exception);
        }
        if (task.result) {
            AWSS3TransferUtilityUploadTask *uploadTask = task.result;
            
            // Do something with uploadTask.
        }
        
        return nil;
    }];
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context {
	if (!firstLocationUpdate_) {
			// If the first location update has not yet been recieved, then jump to that
			// location.
		firstLocationUpdate_ = YES;
		CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
			//_mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
			//				 zoom:6];
		[_mapView animateToLocation:location.coordinate];
	}
}

#pragma mark-  Custome methods

- (IBAction)verticalMenuButtonAction:(id)sender {
    
    if (verticalMenu == nil) {
     
    NSArray *arrData = [NSArray arrayWithObjects:
                        @{@"name":@"Click",@"img_normal":[UIImage imageNamed:@"btn_click_normal"],@"img_pressed":[UIImage imageNamed:@"btn_click_pressed"]},
                        @{@"name":@"Timeline",@"img_normal":[UIImage imageNamed:@"btn_timeline_normal"],@"img_pressed":[UIImage imageNamed:@"btn_timeline_pressed"]},
                        @{@"name":@"Pause trip",@"img_normal":[UIImage imageNamed:@"btn_pause_normal"],@"img_pressed":[UIImage imageNamed:@"btn_pause_pressed"]},
                        @{@"name":@"End trip",@"img_normal":[UIImage imageNamed:@"btn_endtrip_normal"],@"img_pressed":[UIImage imageNamed:@"btn_endtrip_pressed"]},
                        nil];
    
    verticalMenu = [[TPPVerticalMenu alloc]initWithData:arrData height:400 width:65 bottomGap:30];
    verticalMenu.delegate = self;
    [self.view addSubview:verticalMenu];
        
    }
    [self.view bringSubviewToFront:(UIButton *)sender];
    [verticalMenu toggleView];
}

#pragma mark- Map customisation methods

-(void)initialiseBoundButton
{
	boundMarkersButton = [[UIButton alloc]initWithFrame:CGRectMake(_mapView.frame.size.width-50,_mapView.frame.size.height-150,40,40)];
	[boundMarkersButton setImage:[UIImage imageNamed:@"icn_compass_normal"]  forState:UIControlStateNormal];
	[boundMarkersButton addTarget:self  action:@selector(boundCameraToFitAllMarkers)    forControlEvents:UIControlEventTouchUpInside];
	boundMarkersButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[_mapView addSubview:boundMarkersButton];
	[self boundCameraToFitAllMarkers:tripCountsWithDetail.liveTrip.tripLocations onMap:self.mapView];
}


-(void)boundCameraToFitAllMarkers
{
	[self boundCameraToFitAllMarkers:tripCountsWithDetail.liveTrip.tripLocations onMap:self.mapView];

}


-(void) drawPolyline:(NSArray *) polylines map:(GMSMapView *)map shouldSelected:(BOOL) isSelected withTitle:(NSInteger ) title
{
	for (NSInteger atIndex2 = 0; atIndex2 <polylines.count; atIndex2++ ) {
		NSDictionary *routeOverviewPolyline = [polylines objectAtIndex:atIndex2];
		NSString *points = [routeOverviewPolyline objectForKey:@"points"];
		GMSPath *path = [GMSPath pathFromEncodedPath:points];
		GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
		polyline.title = [NSString stringWithFormat:@"%ld", title];
		polyline.tappable = YES;
		polyline.strokeWidth = 4;
		if (isSelected) {
			polyline.strokeColor = COLOR_FOR_MAP_ROUTE;
		}
		else
		{
			polyline.strokeColor = COLOR_FOR_MENU_CONTENT;
		}
		polyline.map = map;
	}
    
}

-(void)addTripPointsView
{
//	[self.view removeSubviewsOfKindOfClasses:@[@"TPPLocationMenuView"]];
//
//	tripPointsView = [[[NSBundle mainBundle] loadNibNamed:@"TPPLocationMenuView" owner:self options:nil] objectAtIndex:0];
//	[tripPointsView.restaurantButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	[tripPointsView.fuelStationButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	[tripPointsView.stopageButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	[tripPointsView.atmButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	[tripPointsView.hotelButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	[tripPointsView.hospitalButton addTarget:self action:@selector(tripPointButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//	tripPointsView.backgroundColor = COLOR_FOR_MENU_CONTENT;
//		//[self.view addSubview:tripPointsView];self	TPPLivePlotViewController *	0x7e88a3e0	0x7e88a3e0
    
    CGRect rect = CGRectMake(40, 0, ScreenWidth-45, 73);
    [tripPointsView setFrame: rect];
    [locationMenuView removeFromSuperview];
    NSArray *dataArray = @[@{@"name":@"Fuel",@"image":@"btn_fuel_normal"},@{@"name":@"Food",@"image":@"btn_eat_normal"},@{@"name":@"Restroom",@"image":@"btn_stay_normal"},@{@"name":@"POI",@"image":@"btn_POI_normal"},@{@"name":@"Medical",@"image":@"btn_medical_normal"},@{@"name":@"ATM",@"image":@"btn_atm_normal"}];
    
    locationMenuView = [[TPPLocationDynamicMenuView alloc]initWithFrame:rect data:dataArray];
    locationMenuView.delegate = self;
    locationMenuView.backgroundColor = [UIColor clearColor];
    self.bottomMenuView.backgroundColor = COLOR_FOR_TRANSPARENT_VIEW;
    [self.bottomMenuView addSubview:locationMenuView];
}

-(void) setBadgesForPlaces
{

	if ([tripCountsWithDetail.liveTrip.tripPoints[@"fuel"] count]>0) {
		tripPointsView.fuelStationButton.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"fuel"] count]] ;
		tripPointsView.fuelStationButton.badgePostion = BadgePositionon_MiddleRight;
	}

	if ([tripCountsWithDetail.liveTrip.tripPoints[@"food"] count]>0) {
		tripPointsView.restaurantButton.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"food"] count]];
		tripPointsView.restaurantButton.badgePostion = BadgePositionon_MiddleRight;
	}

	if ([tripCountsWithDetail.liveTrip.tripPoints[@"restroom"] count]>0) {
		tripPointsView.hotelButton.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"restroom"] count]];
		tripPointsView.hotelButton.badgePostion = BadgePositionon_MiddleRight;
	}

	if ([tripCountsWithDetail.liveTrip.tripPoints[@"poi"] count]>0) {
		tripPointsView.stopageButton.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"poi"] count]];
		tripPointsView.stopageButton.badgePostion = BadgePositionon_MiddleRight;
	}


	if ([tripCountsWithDetail.liveTrip.tripPoints[@"medical"] count]>0) {
		tripPointsView.hospitalButton.badgeValue  = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"medical"] count]];
		tripPointsView.hospitalButton.badgePostion = BadgePositionon_MiddleRight;
	}

	if ([tripCountsWithDetail.liveTrip.tripPoints[@"atm"] count]>0) {
		tripPointsView.atmButton.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"atm"] count]];
		tripPointsView.atmButton.badgePostion = BadgePositionon_MiddleRight;
	}
}

//-(void)tripPointButtonTapped:(UIButton *)sender
//{
//
//    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TPPPlaceDetailView" owner:nil options:nil];
//    placeDetailPopupView = nibContents[0];
//    [placeDetailPopupView setFrame:self.view.frame];
//    [self.view addSubview:placeDetailPopupView];
//
//    [TPPRouteDirectionServices getDistanceWithLocations:[self getOriginPointLocation] andDestination:_currentLocation  andCompletitionBlock:^(id distance, NSError *error){
//        [placeDetailPopupView hideLoader];
//        if (error)
//        {
//            NSLog(@"%@", error);
//        }
//        else
//        {
//            double distanceInMetre = [distance[@"value"] floatValue];
//            placeDetailPopupView.distanceTxtFld.text = [NSString stringWithFormat:@"  %.2f",distanceInMetre/1000.00 ];
//        }
//    }];
//	NSArray *placesArray;
//		// NSLog(@"tapped index %d",sender.tag);
//
//	switch (sender.tag) {
//		case 1:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"fuel"];
//			break;
//
//		case 2:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"food"];
//
//			break;
//		case 3:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"restroom"];
//
//			break;
//		case 4:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"poi"];
//
//			break;
//		case 5:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"medical"];
//
//			break;
//		default:
//			placesArray = tripCountsWithDetail.liveTrip.tripPoints[@"atm"];
//
//			break;
//	}
//	if (placesArray.count>0) {
//		[self drawMarkers:placesArray onMap:self.mapView];
//	}
//	else
//	{
//		[self drawMarkers:placesArray onMap:self.mapView];
//		//[self.view showToasterWithMessage:MSG_No_Places_To_show];
//	}
//}


- (void)addPlacesMarkerOnMapView
{
    NSMutableArray *placesArry = [NSMutableArray array];
    NSMutableDictionary  *dic = [tripCountsWithDetail.liveTrip.tripPoints mutableCopy];
    if ([dic[@"fuel"] count]>0) {
        [placesArry addObject:dic[@"fuel"]];
    }
    if ([dic[@"food"] count]>0) {
        [placesArry addObject:dic[@"food"]];
    }
    if ([dic[@"restroom"] count]>0) {
        [placesArry addObject:dic[@"restroom"]];
    }
    if ([dic[@"poi"] count]>0) {
        [placesArry addObject:dic[@"poi"]];
    }
    if ([dic[@"medical"] count]>0) {
        [placesArry addObject:dic[@"medical"]];
    }
    if ([dic[@"atm"] count]>0) {
        [placesArry addObject:dic[@"atm"]];
    }
    if (placesArry.count!=0) {
        [self drawMarkers:placesArry onMap:self.mapView];
    }
   
}

- (CLLocation *)getOriginPointLocation
{
    NSDictionary *tripLocation = [tripCountsWithDetail.liveTrip.tripLocations firstObject];
    NSDictionary *geoCode = tripLocation[@"geoCode"];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"latitude"] doubleValue] longitude:[geoCode[@"longitude"] doubleValue]];
    return location;
}


-(void)drawMarkers:(NSArray *) places onMap:(GMSMapView *)map
{
	[self.mapView clear];
	[self boundCameraToFitAllMarkers:tripCountsWithDetail.liveTrip.tripLocations onMap:self.mapView];
	for (NSInteger atIndex = 0; atIndex <places.count; atIndex++)
	{
        NSArray *placesArr = [places objectAtIndex:atIndex];
        for (NSInteger i=0; i< placesArr.count; i++) {
            
            NSDictionary *placeDetails = [placesArr objectAtIndex:i];
            NSDictionary *geometry = placeDetails[@"geometry"];
            NSDictionary *geoCode = geometry[@"location"];
            
            
            CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"lat"] doubleValue] longitude:[geoCode[@"lng"] doubleValue]];
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
            //marker.icon = [UIImage imageNamed:@"icn__0000_place"];
            marker.tappable = YES;
            marker.snippet = placeDetails[@"name"];
            marker.appearAnimation = kGMSMarkerAnimationPop;
            marker.map = map;
        }
       
	}
    
}

-(void)setCameraOnUserLocation
{
    CLLocation *location = self.mapView.myLocation;
    _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
    				 zoom:15];
    [_mapView animateToLocation:location.coordinate];
}

- (void)boundCameraToFitAllMarkers:(NSArray *)allMarkersMArray onMap:(GMSMapView *)map
{
	GMSCoordinateBounds* bounds = [[GMSCoordinateBounds alloc] init];

    
	for (NSInteger atIndex = 0;atIndex <allMarkersMArray.count; atIndex ++)
    {
		NSDictionary *tripLocation = [allMarkersMArray objectAtIndex:atIndex];
		NSDictionary *geoCode = tripLocation[@"geoCode"];

		CLLocation *location = [[CLLocation alloc]initWithLatitude:[geoCode[@"latitude"] doubleValue] longitude:[geoCode[@"longitude"] doubleValue]];


		if (location.coordinate.longitude > 0 && location.coordinate.latitude > 0) {
			bounds = [bounds includingCoordinate:location.coordinate];
		}
			//add markers
		GMSMarker *marker = [[GMSMarker alloc] init];
		marker.position=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);

		if (atIndex == 0)
        {
			marker.icon = [UIImage imageNamed:@"circle"];
		}
		else if (atIndex == allMarkersMArray.count-1)
		{
			marker.icon = [UIImage imageNamed:@"circle_Red"];
		}
		else
		{
			marker.icon = [UIImage imageNamed:@"circle_blue"];
		}
		marker.tappable = YES;
		marker.snippet = tripLocation[@"name"];
		marker.appearAnimation = kGMSMarkerAnimationPop;
		marker.map = map;

	}
	[map animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:120]];
    
   // [self setCameraOnUserLocation];
    
	[self drawPolyline:tripCountsWithDetail.liveTrip.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
}


#pragma -mark Web Service methods
-(void) callServiceToGetTripDetails
{
	[self.view showLoader];

	//call web service to get trip details to get route
	NSMutableDictionary *requestData = [[NSMutableDictionary alloc] init];
	requestData[@"token"] = [UserDeafaults objectForKey:Token];
	requestData[@"_id"] = tripCountsWithDetail.onGoingTripId;

	[TPPAppServices getServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_TRIP_DETAILS] data:requestData withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 [self.view hideLoader];
		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 NSArray *tripsArray = response[@"response"][@"trips"];
			 NSError *error;
				 // TPPTripModel *tempTripDetailModel;
			 if (tripsArray.count > 0)
			 {
				 tripCountsWithDetail.liveTrip = [[TPPTripModel alloc]initWithDictionary: [tripsArray objectAtIndex:0] error:&error];
					 // tripCountsWithDetail.liveTrip = tempTripDetailModel;
			 }

			 if (!error)
			 {

			 //Draw the route
				 [self initialiseBoundButton];
				 [self drawPolyline:tripCountsWithDetail.liveTrip.route[@"polylines"] map:self.mapView shouldSelected:YES withTitle:1];
                 [self addPlacesMarkerOnMapView];
				//[self setBadgesForPlaces];
                [locationMenuView setBadgesForPlaces:tripCountsWithDetail];


			 }
			 else
			 {
				 [self.view showToasterWithMessage:@"Unable to fetch trip details"];
			 }
				 //[self navigateToHomeViewController];
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 [self.view hideLoader];

		 [self showErrorMessage:errorMessage];

	 }];
}


@end

//
//  TPPOngoingTripDetailViewController.h
//  TripPlot
//
//  Created by Vishwas Singh on 25/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPBaseViewController.h"

@interface TPPOngoingTripDetailViewController : TPPBaseViewController

@property (weak, nonatomic) IBOutlet UILabel *travellerNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@property (retain,atomic) TPPTripModel *tripModel;
@property (strong, nonatomic) NSString *strCheck;
@property (weak, nonatomic) UIViewController *vcObj;
@property(strong,nonatomic) NSIndexPath *selectedIndexPath;

@end


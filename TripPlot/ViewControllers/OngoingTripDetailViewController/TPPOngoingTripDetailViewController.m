//
//  TPPOngoingTripDetailViewController.m
//  TripPlot
//
//  Created by Vishwas Singh on 25/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPOngoingTripDetailViewController.h"
#import "TPPStackView.h"
#import "TPPOngoingTripCardMapView.h"
#import "TPPOngoingTripCardGalleryView.h"
#import "TPPMakeTripShareCell.h"
@interface TPPOngoingTripDetailViewController ()<TPPStackViewDelegate,TPPStackViewShiftDelegate>

@property (weak, nonatomic) IBOutlet TPPStackView *stack;

@end

@implementation TPPOngoingTripDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self initViews];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_stack reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)initViews
{
    [self.userImageView makeCircular];
    
    self.travellerNameLabel.text = _tripModel.createdBy[@"firstname"];
    
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:_tripModel.createdBy[@"profileImage"]]
                          placeholderImage:[UIImage imageNamed:@"icn_defaultuser"]
                                   options:SDWebImageRefreshCached
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         
     }];
    
    
    [self initStackView];
    
}

-(void)initStackView
{
    _stack.delegate = self;
    _stack.shiftDelegate = self;
    _stack.slidingTransparentEffect = YES;
    _stack.typeSliding = TPPStackViewTypeSlidingHorizontal;
    
    [_stack reloadData];
}



#pragma mark - CSVStackViewDelegate

-(NSInteger)numberOfViews {
    return 2;
}

-(UIView *)stackView:(TPPStackView *)stackView viewForRowAtIndex:(NSInteger)index
{
    UIView *view;
    
    
    if (index == 0)
    {
        [_stack removeSubviewsOfKindOfClasses:@[@"TPPOngoingTripCardMapView"]];
        
        TPPOngoingTripCardMapView *mapView = [[[NSBundle mainBundle] loadNibNamed:@"TPPOngoingTripCardMapView" owner:self options:nil] objectAtIndex:0];
        mapView.delegate = self;
        [mapView bindDataWithModel:_tripModel];
        [mapView initMapView];
        [self configureCellForBottomView:mapView.bottomView];
        view = mapView;
    }
    else
    {
        [_stack removeSubviewsOfKindOfClasses:@[@"TPPOngoingTripCardGalleryView"]];
        
        TPPOngoingTripCardGalleryView *galleryView = [[[NSBundle mainBundle] loadNibNamed:@"TPPOngoingTripCardGalleryView" owner:self options:nil] objectAtIndex:0];
        galleryView.delegate = self;
        [galleryView bindDataWithModel:_tripModel];
        [self configureCellForBottomView:galleryView.bottomView];

        view = galleryView;
    }
    
    view.frame =  CGRectMake(0, 0, _stack.frame.size.width, _stack.frame.size.height);
    
    
    return view;
}
- (void)configureCellForBottomView:(UIView *)bottomView
{
     if ([self.strCheck isEqualToString:@"bucket"])
    {
        TPPMakeTripShareCell  *cell = [[[NSBundle mainBundle] loadNibNamed:@"TPPMakeTripShareCell" owner:self options:nil] objectAtIndex:0];
        
        [cell bindDataWithModel:_tripModel];
        cell.shareCellDelegate = (id)_vcObj;
        cell.frame = CGRectMake(0, 0, ScreenWidth-16, bottomView.frame.size.height);
        cell.bottomView.frame = cell.frame;
        cell.bottomView.translatesAutoresizingMaskIntoConstraints = YES;
        cell.backgroundColor = [UIColor blackColor];
        cell.selectedIndexPath = self.selectedIndexPath;
        [bottomView removeSubviewsOfKindOfClasses:@[@"UIButton"]];
        [bottomView addSubview:cell];
    }
    
    
    
}


-(UIImage *)imageFromView :(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0f);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

-(void)stackView:(TPPStackView *)stackView didSelectViewAtIndex:(NSInteger)index {
    NSLog(@"Вы тапнули на View под индексом: %@", @(index));
}

-(void)stackView:(TPPStackView *)stackView willChangeViewAtIndex:(NSInteger)index {
    NSLog(@"willChangeViewAtIndex: %@", @(index));
}

-(void)stackView:(TPPStackView *)stackView didChangeViewAtIndex:(NSInteger)index {
    NSLog(@"didChangeViewAtIndex: %@", @(index));
    
   // [self.view layoutIfNeeded];
}

#pragma mark - CSVStackViewShiftDelegate

-(CGFloat)sizeOfShiftStack
{
    return 40.0f;
}

-(CGRect)stackItem:(UIView *)stackItem rectViewAtIndex:(NSInteger)index andShift:(CGFloat)shift
{
    CGSize newSize = [self sizeByShiftWidth:shift * index andSize:stackItem.frame.size];
    CGPoint newPoint = [self pointByShift:shift * index andPoint:stackItem.center];
    return CGRectMake(newPoint.x - (newSize.width / 2), newPoint.y - (newSize.height / 2), newSize.width, newSize.height);
}

//helpers shift calculate

-(CGPoint)pointByShift:(CGFloat)shift andPoint:(CGPoint)point {
    return CGPointMake(point.x, point.y + shift);
}

-(CGSize)sizeByShiftWidth : (CGFloat)shift andSize:(CGSize)size {
    
    CGFloat k = (size.width - shift) / size.width;
    CGFloat new_width = k * size.width;
    CGFloat new_height = k * size.height;
    
    return CGSizeMake(new_width, new_height);
}




#pragma mark- TPPShareCellDelegate

-(void)shareButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel snapShot:(UIImage *)snapShot
{
    
    [self defaultShareWithText:@"" image:snapShot url:nil];
}

-(void)goHereButtonTapped:(UIButton *)button indexPath:(NSIndexPath *)indexPath tripModel:(TPPTripModel *)tripModel
{
   
    TPPGoHereView *goHereView = (TPPGoHereView *)[self.view subViewOfKindOfClass:[TPPGoHereView class]];
    if (!goHereView) {
        goHereView = [[[NSBundle mainBundle] loadNibNamed:@"TPPGoHereView" owner:self options:nil] objectAtIndex:0];
    }

    goHereView.frame = self.view.frame;
     
    goHereView.tripModel = tripModel;
    
    [self.view addSubview:goHereView];
}






@end

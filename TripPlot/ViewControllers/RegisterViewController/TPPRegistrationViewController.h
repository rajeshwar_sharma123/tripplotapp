//
//  TPPRegistrationViewController.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TwitterKit/TwitterKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface TPPRegistrationViewController : TPPBaseViewController


@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;
@property (weak, nonatomic) IBOutlet UIButton *facebookLogInButton;
@property (weak, nonatomic) IBOutlet TWTRLogInButton *twitterLogInButton;
@property (weak, nonatomic) IBOutlet TPPTextField *emailTextField;
@property (weak, nonatomic) IBOutlet TPPTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet TPPTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet TPPTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet TPPTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet TPPTextField *contactNumbertextField;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
- (IBAction)alreadyAccountButtonTapped:(id)sender;


@end

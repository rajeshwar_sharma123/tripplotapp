//
//  TPPRegistrationViewController.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPRegistrationViewController.h"
#import "TPPHomeViewController.h"
#import "TPPForgotPasswordViewController.h"
#import "TPPTermsViewController.h"


@interface TPPRegistrationViewController ()
{
    
}
@end

@implementation TPPRegistrationViewController

#pragma mark - LifeCycle Methods

- (void)viewDidLoad {
    //[super viewDidLoad];

    self.title = @"Sign Up";
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //self.navigationController.navigationBar.hidden = YES;
}


- (IBAction)checkButtonTapped:(id)sender
{
    [self.checkButton setSelected:!self.checkButton.selected];
}


#pragma mark - BL Methods

-(BOOL)isUserInputsValid
{
    if (self.emailTextField.text.trimmedString.length == 0)
    {
        [self showErrorMessage:EMAIL_EMPTY];
        
        return NO;
    }
    if(!self.emailTextField.text.isValidEmail)
    {
        [self showErrorMessage:EMAIL_INVALID];
        return NO;
    }
	if(self.contactNumbertextField.text.trimmedString.length > 0 && self.contactNumbertextField.text.trimmedString.length < 10)
	{
		[self showErrorMessage:CONTACT_INVALID];
		return NO;
	}

    if(self.firstNameTextField.text.trimmedString.length == 0)
    {
        [self showErrorMessage:@"Required First name"];
        return NO;
    }
   
    

    if(self.passwordTextField.text.trimmedString.length == 0)
    {
        [self showErrorMessage:PASSWORD_EMPTY];
        return NO;
    }

    if(![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        [self showErrorMessage:@"Password and Confirm password must be same."];
        return NO;
    }

    if (!self.checkButton.selected)
    {
        [self showErrorMessage:MSG_TnCRequired];
        return NO;
    }

    
    return YES;
}



#pragma mark - IBAction Methods

- (IBAction)termsButtonTapped:(id)sender
{
    TPPTermsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TPPTermsViewController"];
    vc.shouldHideSideMenu = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}



- (IBAction)facebookLogInButtonTapped:(id)sender
{
    [self.view showLoader];
        
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        
        [loginManager logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error)
            {
                [self.view hideLoader];
            }
            else
            {
                NSString *facebookToken = result.token.tokenString;
                if(facebookToken)
                {
                    // Token created successfully and you are ready to get profile info
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email"}]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                     {
                         if (!error)
                         {
                             NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   facebookToken, @"access_token",
                                                   @"facebook", @"provider",
                                                   nil];
                             
                             NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:dict forKey:@"options"];
                             
                             [self socialLoginWithCredentials:optionsDictionary];
                         }
                         else
                         {
                             [self.view hideLoader];
                         }
                     }];
                }
                else
                {
                    [self.view hideLoader];
                }
            }
        }];
        
}



- (IBAction)twitterLogInButtonTapped:(id)sender
{
    [self.view showLoader];
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
     {
         if (session)
         {
             NSLog(@"signed in as %@", [session userName]);
             
             NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"twitter", @"provider",
                                   session.authToken, @"access_token",
                                   session.authTokenSecret, @"access_token_secret",
                                   nil];
             NSDictionary *optionsDictionary = [NSDictionary dictionaryWithObject:dict forKey:@"options"];
             
             [self socialLoginWithCredentials:optionsDictionary];
         }
         else
         {
             [self.view hideLoader];
             [self showErrorMessage:error.localizedDescription];
         }

     }];
    

}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)signUpButtonTapped:(id)sender
{

    if ([self isUserInputsValid])
    {
        [self.view showLoader];
		NSString *contactNumber = [NSString stringWithFormat:@"+91%@",self.contactNumbertextField.text.trimmedString];
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              _emailTextField.text.trimmedString,@"username",
							  contactNumber,@"contact",
                              _firstNameTextField.text.trimmedString,@"firstname",
                              _lastNameTextField.text.trimmedString,@"lastname",
                              _passwordTextField.text.trimmedString,@"password",
                              [NSNumber numberWithInteger:2],@"userType",
                               nil];
        
        [TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:RegisterUrl] data:dict withSuccessBlock:^(id response, NSDictionary *headers)
        {
            [self.view hideLoader];
            
            
            if ([[response objectForKey:@"code"] integerValue] == 200)
            {                
                [self showSuccessMessage:@"Verification link send to your mail.Please check."];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [self showErrorMessage: response[@"message"]];
            }
            
        } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage){
            
            [self.view hideLoader];
            [self showErrorMessage:errorMessage];
        }];
    }
}


- (IBAction)alreadyAccountButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}




@end

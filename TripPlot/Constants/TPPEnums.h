//
//  TPPEnums.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 05/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#ifndef TPPEnums_h
#define TPPEnums_h





typedef NS_ENUM(NSInteger, TripStatus)
{
    TripStatus_OngoingTrip = 1,
    TripStatus_UpcomingTrip = 2,
    TripStatus_CompletedTrip = 3,
    TripStatus_RecommendedTrip = 4
};

typedef NS_ENUM(NSInteger, CreateTripSource )
{
    CreateTripSource_Home = 1,
    CreateTripSource_Challenge = 2,
    CreateTripSource_BucketList = 3,
    
};
typedef NS_ENUM(NSInteger, RequestOption )
{
    RequestOption_Accept = 1,
    RequestOption_Reject = 2,
    
};
typedef NS_ENUM(NSInteger, TagType)
{
    Type = 1,
    Mode = 2,
};






#endif /* TPPEnums_h */

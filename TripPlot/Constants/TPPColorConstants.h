
//
//  TPPColorConstants.h
//  TripPlot
//
//  Created by Daffolap-21 on 05/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#ifndef TPPColorConstants_h
#define TPPColorConstants_h






#define RgbToUIColor(r,g,b) [UIColor colorWithRed:(r / 255.0) green:(g/ 255.0) blue:(b/ 255.0) alpha:1.0]

#define COLOR_LIGHT_GREEN  [UIColor colorWithRed:(137.0 / 255.0) green:(197.0 / 255.0) blue:(67.0 / 255.0) alpha:1.0]

#define COLOR_FOR_MENU_CONTENT  [UIColor colorWithRed:(57.0 / 255.0) green:(52.0 / 255.0) blue:(49.0 / 255.0) alpha:1.0]

#define COLOR_ORANGE  [UIColor colorWithRed:(251.0 / 255.0) green:(130.0 / 255.0) blue:(47.0 / 255.0) alpha:1.0]

#define COLOR_FOR_TYPE_TAG  [UIColor colorWithRed:(234.0 / 255.0) green:(80.0 / 255.0) blue:(66.0 / 255.0) alpha:1.0]

#define COLOR_FOR_MODE_TAG  [UIColor colorWithRed:(40.0 / 255.0) green:(158.0 / 255.0) blue:(150.0 / 255.0) alpha:1.0]

#define COLOR_FOR_MAP_ROUTE [UIColor colorWithRed:(90.0 / 255.0) green:(115.0 / 255.0) blue:(252.0 / 255.0) alpha:1.0]

#define COLOR_FOR_TRANSPARENT_VIEW  [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]

#define ColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 \
alpha:1.0]

#define TPPTextColor      [UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]
#define Badge_BlueColor      [UIColor colorWithRed:93.0/255 green:137.0/255 blue:182.0/255 alpha:1.0]
#define TPPGreenColor      [UIColor colorWithRed:138.0/255 green:196.0/255 blue:65.0/255 alpha:1.0]
#define TPPOrangeColor      [UIColor colorWithRed:255.0/255 green:126.0/255 blue:0.0 alpha:1.0]
#define TPPNavigationBarColor      [UIColor colorWithRed:24.0/255 green:20.0/255 blue:20.0/255 alpha:1.0]



#define TPPUpcomingCellGradientColorStart      [UIColor colorWithRed:232.0/255 green:174.0/255 blue:75.0/255 alpha:1.0]
#define TPPUpcomingCellGradientColorMidle      [UIColor colorWithRed:226.0/255 green:123.0/255 blue:64.0/255 alpha:1.0]
#define TPPUpcomingCellGradientColorEnd      [UIColor colorWithRed:217.0/255 green:75.0/255 blue:53.0/255 alpha:1.0]


#define TPPOngoingCellGradientColorStart      [UIColor colorWithRed:105.0/255 green:176.0/255 blue:96.0/255 alpha:1.0]
#define TPPOngoingCellGradientColorMidle      [UIColor colorWithRed:131.0/255 green:190.0/255 blue:124.0/255 alpha:1.0]
#define TPPOngoingCellGradientColorEnd      [UIColor colorWithRed:156.0/255 green:203.0/255 blue:149.0/255 alpha:1.0]




#define Color_UpcomingTripCellButtonBackground      [UIColor colorWithWhite:1.0 alpha:0.1]
#define Color_CompletedTripCellButtonBackground     [UIColor colorWithRed:235.0/255 green:232.0/255 blue:227.0/255 alpha:1.0]





#endif /* TPPColorConstants_h */

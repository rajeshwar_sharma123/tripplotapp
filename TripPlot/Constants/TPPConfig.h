//
//  TPPConfig.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#ifndef TPPConfig_h
#define TPPConfig_h



// ************************************* Url Constants ****************************************

#define Akshay_Url           @"http://172.18.3.24:5111"


#define Staging_Url           @"http://52.90.160.68/tripplot"
#define Dev_Url               @"http://52.90.160.68/tripplottest"

#define Socket_Staging_Url    @"http://52.90.160.68:5100/tripevents"
#define Socket_Dev_Url        @"http://52.90.160.68:5111/tripevents"

#define URL_FOR_BASE_SOCKET   Socket_Dev_Url
#define BaseUrl               Dev_Url

#define LogInUrl              @"/rest/connect"
#define RegisterDeviceUrl     @"/rest/service/savedevice"
#define DeRegisterDeviceUrl   @"/rest/service/removedevice"

#define SocilaLogInUrl        @"/rest/connect"
#define RegisterUrl           @"/rest/service/adduser"
#define ForgotPasswordUrl     @"/rest/service/forgetpassword"
#define ChangePasswordUrl     @"/rest/service/resetpassword"
#define TripFeedUrl           @"/rest/service/tripfeed"

#define LikeUrl               @"/rest/service/liketrip"
#define UnLikeUrl             @"/rest/service/unliketrip"
#define FollowUrl             @"/rest/service/followtrip"
#define UnFollowUrl           @"/rest/service/unfollowtrip"
#define JoinUrl               @"/rest/service/jointrip"
#define UnJoinUrl             @"/rest/service/unjointrip"
#define InviteListUrl         @"/rest/service/invitelist"
#define AcceptInviteUrl       @"/rest/service/acceptinvite"
#define RejectInviteUrl       @"/rest/service/invitereject"
#define ADDInBucket           @"/rest/service/addinbucket"
#define ChallengeTripListUrl  @"/rest/service/challengetrips"
#define AcceptChallengeUrl    @"/rest/service/acceptchallenge"
#define BucketListUrl         @"/rest/service/bucketlist"
#define RemoveBucketUrl       @"/rest/service/removebucket"
#define MyTripsUrl            @"/rest/service/usertrips"
#define EndTripUrl            @"/rest/service/endtrip"
#define SaveTrippointsUrl      @"/rest/service/savetrippoints"

//pradeep's
#define GET_TYPE_LIST_URL       @"/rest/service/typelist"
#define GET_MODE_LIST_URL		@"/rest/service/modelist"
#define CREATE_TRIP_URL			@"/rest/service/createtrip"
#define GET_AWS_CREDENTIALS_URL @"/rest/service/cdn"
#define GET_SEARCH_LOCATION_URL @"/rest/service/locationlist"
#define URL_FOR_CREATE_TRIP		@"/rest/service/createtrip"
#define UserTripInfoUrl         @"/rest/service/usertripinfo"
#define UserLocationInfoUrl     @"/rest/service/userlocations"
#define SaveUserProfileUrl      @"/rest/service/saveuser"
#define CreateMontageUrl        @"/rest/service/createmontage"
#define URL_TO_SAVE_EMAIL_LIST	@"/rest/service/savemaillist"
#define URL_TO_GET_EMAIL_LIST	@"/rest/service/getmaillist"
#define URL_TO_GET_TRIP_DETAILS @"/rest/service/gettrip"
#define URL_TO_INVITE			@"/rest/service/inviteusersfortrip"
#define URL_TO_CHALLANGE		@"/rest/service/createchallengetrip"
#define TypeListUrl             @"/rest/service/typelist"
#define URL_TO_GET_COUNT		@"/rest/service/countonlogin"
#define SaveTripimageUrl        @"/rest/service/savetripimage"
#define RecommendedTripsByLocationUrl     @"/rest/service/recomendeddetailbylocation"
#define ExploreRecommendedTripsUrl @"/rest/service/explorerecommended"
#define ExploreRecommendedDetailByIdUrl  @"/rest/service/explorerecomendedbyid"
#define URL_TO_VALIDATE_URL @"/rest/service/validatetrip"

// *******************************  Basic Config Constants ****************************

#define  Twitter_ConsumerKey       @"AhlMtGLa5Lh62T2YIzfjuUvSw"
#define  Twitter_ConsumerSecretKey @"tO6hpkGFUxVbLRCKQPr4EqeSKMXj0LgvozjY8NOR9K0DY7TFd2"
#define  Google_API_Key            @"AIzaSyCpuO0tIx_gJXLpAgdO-PoHxyvs7KqdnQU"
#define  tripPlotBrowserKey       @"AIzaSyACAo2r76xdlz2vFTsx4JbCbk3SDvkR9_4"
#define  NewRelic_API_Key         @"AAe3e5d176e3a79a5d71839ec8b5e5ac9cba6be68a"


//socket events Name
#define inviteAcceptEvent			@"inviteAcceptEvent"
#define inviteRejectEvent			@"inviteRejectEvent"
#define challengeAcceptEvent		@"challengeAcceptEvent"
#define challengeRejectEvent		@"challengeRejectEvent"
#define likeEvent					@"likeEvent"
#define unlikeEvent					@"unlikeEvent"
#define followEvent					@"followEvent"
#define unfollowEvent				@"unfollowEvent"
#define joinEvent					@"joinEvent"
#define unJoinEvent			     	@"unjoinEvent"
#define endTripEvent                @"endTripEvent"


//socket broadcast event
#define unfollowBroadcast			@"unfollowBroadcast"
#define inviteAcceptBroadcast		@"inviteAcceptBroadcast"
#define followBroadcast				@"followBroadcast"
#define likeBroadcast				@"likeBroadcast"
#define unlikeBroadcast				@"unlikeBroadcast"
#define challengeRejectBroadcast	@"challengeRejectBroadcast"
#define cronjobBroadcastEvent		@"cronjobBroadcastEvent"
#define challengeAcceptBroadcast	@"challengeAcceptBroadcast"
#define inviteRejectBroadcast		@"inviteRejectBroadcast"
#define joinEventBroadcast			@"joinEventBroadcast"
#define unJoinEventBroadcast        @"unjoinEventBroadcast"
#define endTripEventBroadcast       @"endTripBroadcast"



#endif /* TPPConfig_h */

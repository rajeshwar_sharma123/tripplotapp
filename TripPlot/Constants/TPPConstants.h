//
//  TPPConstants.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#ifndef TPPConstants_h
#define TPPConstants_h





#define  AnimationDuration  0.5
#define  PageSize                15

// ***********************************  Basic String Constants ****************************************


#define  UserDeafaults           [NSUserDefaults standardUserDefaults]
#define  DeviceToken             @"deviceToken"
#define  FacebookToken          @"facebookToken"
#define  TwitterToken           @"twitterToken"

#define  TokenType              @"tokenType"
#define  Token                  @"token"
#define  UserProfile            @"userProfile"

#define  Txt_PageSize               @"pageSize"
#define  Txt_PageNumber             @"pageNumber"


// *************************************  check for device ****************************************
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) // iPhone and       iPod touch style UI

#define IS_IPHONE_4         [[UIScreen mainScreen] bounds].size.height == 480.0f
#define IS_IPHONE_5         [[UIScreen mainScreen] bounds].size.height == 568.0f
#define IS_IPHONE_6         [[UIScreen mainScreen] bounds].size.height == 667.0f
#define IS_IPHONE_6_PLUS    [[UIScreen mainScreen] bounds].size.height == 736.0f

#define ScreenWidth  [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight  [[UIScreen mainScreen] bounds].size.height


// *************************************   Error constants ****************************************
#define  k_Error_Session_Expired              @"Session Expired"
#define  k_Error_Some_Erorr_Occurred          @"Some error has been occurred, Please try again."
#define  k_Error_Internet_Connection_Error    @"internet connectivity issue"


// ************************************* Validation messages ****************************************
#define REQUIRED_ALL_FIELDS		@"All fields are mandetory"
#define FIRST_NAME_EMPTY        @"Required First name"
#define LAST_NAME_EMPTY         @"Required Last name"
#define EMAIL_EMPTY             @"Required Email ID"
#define EMAIL_INVALID           @"Invalid email address"
#define CONTACT_INVALID           @"Invalid contact number"
#define PASSWORD_EMPTY          @"Password must have 8 characters"
#define CONFIRM_PASSWORD_EMPTY  @"Required Confirm password"
#define PASSWORD_MISMATCH       @"Confirm Password Not match"

#define NEW_PASSWORD_EMPTY      @"New password can't be empty"
#define PASSWORD_CHANGED        @"Password changed successfully"
#define PLACE_NAME_EMPTY        @"Name of the halt point shouldn't be blank"
#define DISTANCE_FIELD_EMPTY    @"Distance field can't be empty"


//validate create trip
#define MSG_TRIP_NAME_REQ		@"Name Your Trip"
#define MSG_TRIP_TYPE_REQ		@"Select Trip Type"
#define MSG_TRIP_MODE_REQ		@"Select Trip Mode"
#define MSG_TRIP_LOCATION_REQ	@"Enter Origin and Destination"
#define MSG_TRIP_INVITE_REQ		@"Turn on invite option above"
#define MSG_NO_ROUTE_FOUND		@"No route found"
#define MSG_FOR_INVITE			@"Invite request has been sent successfully"
#define MSG_FOR_CHALLANGE		@"Challange request has been sent successfully"


#define selected_route_not_valid_error			@"Selected route not valid"
#define turn_on_invite_option_above_error		@"Turn on invite option above"
#define no_route_found_error					@"No route found"
#define you_have_already_add_8_location_error   @"You have already added 8 limited location"
#define trip_cannot_added_to_bucket				@"This trip cannot be added to your bucket list"
#define trip_added_to_bucket					@"Trip added to your bucket list"
#define unknown_location						@"Unknown to Unknown"
#define unknown_text							@"Unknown"
#define internal_server_issue					@"Internal Server Error"
#define internet_connectivity_issue				@"internet connectivity issue"
#define network_connectivity_issue				@"Network Connectivity Problem"
#define verification_text						@"Verification link send to your mail. Please follow the instructions in the email to complete the verification process"
#define sign_out_text							@"Sign Out<"
#define tnc_text								@"Terms & Conditions"
#define settings_text							@"Settings"
#define information_text						@"Information"
#define invites_text							@"Invites"
#define friends_text							@"Friends"
#define trip_text								@"Trips"
#define profile_text							@"Profile"
#define MSG_FOR_EMAIL_EXISTS					@"This emailID already added"
#define MSG_No_Places_To_show					@"No place to show"
#define MSG_TnCRequired                         @"Please accept Terms & Conditions"
#define MSG_Invalid_Date                        @"End date must be greater than start date"

//  *********************** ASWS3 *********************************
#define CognitoRegionType AWSRegionUSEast1
#define DefaultServiceRegionType AWSRegionUSEast1
#define S3BucketName @"tripplotdemo"
#define CognitoIdentityPoolId @"us-east-1:dcd0b6f2-9c22-454d-8e53-330d30664468"

//#define CognitoRegionType AWSRegionAPNortheast1
//#define DefaultServiceRegionType AWSRegionAPNortheast1
//#define CognitoIdentityPoolId @"ap-northeast-1:954ef270-1e53-4808-b25e-15d64b74f53b"
//#define S3BucketName @"triplot"
#define S3EndPoint @"s3.amazonaws.com"



//    String Constants     ***

#define Txt_Like     @" Like"
#define Txt_UnLike   @" Unlike"
#define Txt_Follow     @" Follow"
#define Txt_UnFollow   @" Unfollow"
#define Txt_Join     @" Join"
#define Txt_UnJoin   @" Unjoin"
#define Txt_GoHere   @" Go here"
#define Txt_ChallengeTrip  @" Challenge"

#define Txt_Accept   @"1"
#define Txt_Reject   @"2"

#define Txt_IsFollowedUnfollowed  @"isFollowedUnfollowed"
#define Txt_IsLikedUnliked        @"isLikedUnliked"
#define Txt_IsJoinedUnjoined      @"isJoinedUnjoined"

#define Txt_Radius            @"radius"


// Notification constants

#define  UserActivityNotification    @"userActivityNotification"
#define  SocketNotification          @"socketNotification"




#endif /* TPPConstants_h */

//
//  TPPPhotoAssets.m
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPPhotoAssets.h"

@implementation TPPPhotoAssets
static TPPPhotoAssets *photoAssets = nil;

+ (id) sharedInstance
{
    if (!photoAssets) {

        photoAssets = [[TPPPhotoAssets alloc] init];
    
    }
    return photoAssets;
}

- (id)init
{
    if (! photoAssets) {

        photoAssets = [super init];
        _assets = [[NSMutableArray alloc]init];
        // Uncomment the following line to see how many times is the init method of the class is called
        // NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return photoAssets;
}

@end

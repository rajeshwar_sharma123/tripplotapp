//
//  TPPTripLocation.h
//  TripPlot
//
//  Created by Daffolap-21 on 06/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface TPPTripLocation : JSONModel
@property NSString<Optional> *_id;
@property NSString<Optional> *name;
@property NSString<Optional> *country;
@property NSDictionary<Optional> *geoCode;
@property NSString<Optional> *abbrState;
@end

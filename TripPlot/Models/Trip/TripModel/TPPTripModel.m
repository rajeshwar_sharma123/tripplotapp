//
//  TPPTripModel.m
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPTripModel.h"

@implementation TPPTripModel


+(instancetype)sharedInstance
{
    static TPPTripModel *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
       // instance.dealsCount = 0;
       // instance.trackOrderCount = 0;
    });

    return instance;
}


+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description" : @"descriptions"
                                                       
                                                       }];
}


//+(BOOL)propertyIsIgnored:(NSString *)propertyName
//{
//    if ([propertyName isEqualToString:@"isOpen"] || [propertyName isEqualToString:@"isDelete"] || [propertyName isEqualToString:@"isNew"] || [propertyName isEqualToString:@"isActive"] || [propertyName isEqualToString:@"likeCount"] || [propertyName isEqualToString:@"tripStatus"] )
//    {
//        return YES;
//    }
//
//    return NO;
//}
//
+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"isOpen"] || [propertyName isEqualToString:@"isDelete"] || [propertyName isEqualToString:@"isNew"] || [propertyName isEqualToString:@"isActive"] || [propertyName isEqualToString:@"isGroup"] || [propertyName isEqualToString:@"likeCount"] || [propertyName isEqualToString:@"tripStatus"] || [propertyName isEqualToString:@"isLikedByMe"]|| [propertyName isEqualToString:@"isFollowedByMe"]|| [propertyName isEqualToString:@"isJoinedByMe"])
    {
        return YES;
    }
   
    return NO;
    
}


-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    
    [self setUserPreferences];
    [self removeUnacceptedMembers];
    [self removeDuplicateTripMembers];
    
    return self;
}

-(void)setUserPreferences
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    // set Bool for isFollowedByMe
    self.isFollowedByMe = NO;
    for (NSDictionary *person in self.follow)
    {
        if([person[@"_id"] isEqualToString:userProfile[@"_id"]])
        {
            self.isFollowedByMe = YES;
            break;
        }
    }
    
    // set Bool for isLikedByMe
    self.isLikedByMe = NO;
    for (NSDictionary *person in self.like)
    {
        if([person[@"_id"] isEqualToString:userProfile[@"_id"]])
        {
            self.isLikedByMe = YES;
            break;
        }
    }
    
    
    // set Bool for isJoinedByMe
    self.isJoinedByMe = NO;
    for (NSDictionary *person in self.tripMembers)
    {
        if([person[@"userId"] isEqualToString:userProfile[@"_id"]])
        {
            self.isJoinedByMe = YES;
            break;
        }
    }
}

-(void)removeUnacceptedMembers
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (NSDictionary *member in self.tripMembers)
    {
        if ([member[@"accept"] integerValue] != RequestOption_Accept)
        {
            [tempArray addObject:member];
        }
    }
    
    [self.tripMembers removeObjectsInArray:tempArray];
}

-(void)removeDuplicateTripMembers
{
    NSMutableArray *duplicateObjects = [[NSMutableArray alloc]init];
    
    NSInteger count = self.tripMembers.count;
    for (int i=0 ; i<count; i++)
    {
        NSDictionary *objectI = self.tripMembers[i];
        for (int j=i+1; j<count; j++)
        {
            NSDictionary *objectJ = self.tripMembers[j];
            if ([objectI[@"userId"] isEqualToString:objectJ[@"userId"]])
            {
                [duplicateObjects addObject:objectJ];
            }
        }
    }
    
    [self.tripMembers removeObjectsInArray:duplicateObjects];
}


@end

//
//  TPPTripModel.h
//  TripPlot
//
//  Created by Vishwas Singh on 24/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <JSONModel/JSONModel.h>


//@protocol TPPTripModel
//@end


@interface TPPTripModel : JSONModel


@property NSString<Optional> *_id;
@property NSString<Optional> *title;
@property NSDictionary<Optional> *tripDate;
@property NSArray<Optional> *images;
@property NSString<Optional> *termConditionText;
@property NSString<Optional> *updatedOn;
@property NSMutableArray<Optional> *tripMembers;
@property NSMutableArray<Optional> *follow;
@property NSMutableArray<Optional> *like;
@property NSString<Optional> *distance;
@property NSString<Optional> *duration;
@property NSDictionary<Optional> *createdBy;
@property NSArray<Optional> *tripTypes;
@property NSArray<Optional> *tripModes;
@property NSArray<Optional> *tripLocations;
@property NSDictionary<Optional> *route;
@property NSString<Optional> *descriptions;
@property NSString<Optional> *locationDescription;
@property NSDictionary<Optional> *tripPoints;
@property NSString<Optional> *statusLable;
@property NSDictionary<Optional> *totalTrips;
@property NSDictionary<Optional> *__txs__;
@property NSString<Optional> *montage;
@property NSDictionary<Optional> *challenge;

@property BOOL isOpen;
@property BOOL isDelete;
@property BOOL isNew;
@property BOOL isActive;
@property BOOL isGroup;
@property NSInteger likeCount;
@property NSInteger tripStatus;


@property BOOL isLikedByMe;
@property BOOL isFollowedByMe;
@property BOOL isJoinedByMe;






-(id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err;


+(instancetype)sharedInstance;


@end


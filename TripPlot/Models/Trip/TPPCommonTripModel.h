//
//  TPPCommonTripModel.h
//  TripPlot
//
//  Created by Vishwas Singh on 27/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPCommonTripModel : NSObject

@property NSInteger level;
@property NSInteger status;


@property BOOL isExpanded;
@property BOOL hasParent;
@property BOOL isShareModel;
@property BOOL shouldShowSeeMoreButton;
@property BOOL isAcceptRejectModel;



@property (retain,nonatomic) TPPTripModel *tripModel;
@property NSArray *childrenArray;

@end

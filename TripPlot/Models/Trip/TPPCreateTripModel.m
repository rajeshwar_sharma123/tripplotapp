//
//  TPPCreateTripModel.m
//  TripPlot
//
//  Created by Daffolap-21 on 11/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPCreateTripModel.h"

@implementation TPPCreateTripModel

static TPPCreateTripModel *instance;
+(instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.addedEmailIDs = [NSMutableArray new];
        instance.addedContacts = [NSMutableArray new];
    });
    
    return instance;
}


+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description" : @"descriptions"
                                                       
                                                       }];
}


//+(BOOL)propertyIsIgnored:(NSString *)propertyName
//{
//    if ([propertyName isEqualToString:@"isOpen"] || [propertyName isEqualToString:@"isDelete"] || [propertyName isEqualToString:@"isNew"] || [propertyName isEqualToString:@"isActive"] || [propertyName isEqualToString:@"likeCount"] || [propertyName isEqualToString:@"tripStatus"] )
//    {
//        return YES;
//    }
//
//    return NO;  isGroup
//}
//
+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    if ([propertyName isEqualToString:@"isOpen"] || [propertyName isEqualToString:@"isDelete"] || [propertyName isEqualToString:@"isNew"] || [propertyName isEqualToString:@"isActive"] || [propertyName isEqualToString:@"likeCount"] || [propertyName isEqualToString:@"tripStatus"] || [propertyName isEqualToString:@"isGroup"])
    {
        return YES;
    }
    
    return NO;
    
}


+(void) clearInstance
{
    instance = [[self alloc] init];
    
}

-(NSArray *)convertContactsToJSONModel
{
    NSMutableArray * tempArray = [NSMutableArray new];
    for (NSInteger atIndex = 0; atIndex <self.addedEmailIDs.count; atIndex++) {
        
        NSMutableDictionary *contactDict = [NSMutableDictionary new];
        contactDict[@"key"] = @"username";
        contactDict[@"value"] = [self.addedEmailIDs objectAtIndex:atIndex];
        [tempArray addObject:contactDict];
    }
    
    //for phone contacts
    
    for (NSInteger atIndex = 0; atIndex <self.addedContacts.count; atIndex++) {
        NSMutableDictionary *contactDict = [NSMutableDictionary new];
        contactDict[@"key"] = @"contact";
        contactDict[@"value"] = [[self.addedContacts objectAtIndex:atIndex] convertToPhoneNumberFormat];
        [tempArray addObject:contactDict];
    }
    
    return [tempArray copy];
}

@end

//
//  TPPCreateTripModel.h
//  TripPlot
//
//  Created by Daffolap-21 on 11/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "JSONModel.h"
#import "TPPTripPoints.h"

@interface TPPCreateTripModel : JSONModel

@property(weak,nonatomic) NSString<Optional> *_id;
@property NSString<Optional> *token;
@property NSString<Optional> *title;
@property NSString<Optional> *username;
@property NSString<Optional> *firstname;
@property NSString<Optional> *contact;
@property NSArray<Optional> *profileImage;
@property NSArray<Optional> *modeNames;
@property NSArray<Optional> *typeNames;
@property NSArray<Optional> *tripMembers;

@property NSString<Optional> *distance;
@property NSString<Optional> *duration;
@property NSString<Optional> *createdBy;
@property NSArray<Optional> *tripTypes;
@property NSArray<Optional> *tripModes;
@property NSMutableArray<Optional> *tripLocations;
@property NSDictionary<Optional> *route;
@property NSDictionary<Optional> *tripPoints;
@property NSString<Optional> *startDate;
@property NSString<Optional> *endDate;
@property NSArray<Optional> *images;
@property NSString<Optional> *montage;
@property (retain,atomic) TPPTripModel *tripModel;

@property int isOpen;
@property int isGroup;
@property BOOL isDelete;
@property int isNew;
@property BOOL isActive;
@property NSInteger tripStatus;
@property CreateTripSource createTripSource;
@property NSInteger selectedIndex;

+(instancetype)sharedInstance;
+(void) clearInstance;
-(NSArray *)convertContactsToJSONModel;

//properties for local uses
@property NSMutableArray<Optional>  *addedEmailIDs;
@property NSMutableArray<Optional>  *addedContacts;



@end

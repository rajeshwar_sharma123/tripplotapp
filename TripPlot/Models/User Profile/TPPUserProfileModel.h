//
//  TPPUserProfile.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 07/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPUserProfileModel : JSONModel



@property NSString<Optional> *_id;
@property NSString<Optional> *contact;
@property NSString<Optional> *country;
@property NSString<Optional> *emailid;
@property NSString<Optional> *firstname;
@property NSString<Optional> *fullname;
@property NSString<Optional> *lastname;
@property NSString<Optional> *profileImage;
@property NSString<Optional> *status;
@property NSString<Optional> *userType;
@property NSString<Optional> *username;
@property NSString<Optional> *locations;

//@property NSArray <Optional> *locations;



@end

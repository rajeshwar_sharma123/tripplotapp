//
//  TPPSharedTripCountsModel.h
//  TripPlot
//
//  Created by Daffolap-21 on 30/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "JSONModel.h"

@interface TPPSharedTripCountsModel : JSONModel

@property int myTripCount;
@property int inviteCount;
@property BOOL isOngoing;
@property NSString<Optional> *onGoingTripId;
@property TPPTripModel<Optional> *liveTrip;

+(instancetype)sharedInstance;
	//+(void) callServiceToGetLiveTripDetails;
+(void) callServiceToGetLiveTripDetails:(void (^)())completionBlock;


@end

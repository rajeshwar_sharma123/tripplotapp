//
//  TPPTripPoints.h
//  TripPlot
//
//  Created by Daffolap-21 on 11/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "JSONModel.h"

@interface TPPTripPoints : JSONModel

@property NSArray<Optional> *fuel;
@property NSArray<Optional> *atm;
@property NSArray<Optional> *poi;
@property NSArray<Optional> *food;
@property NSArray<Optional> *medical;
@property NSArray<Optional> *restroom;

@end

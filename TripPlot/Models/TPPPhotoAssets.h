//
//  TPPPhotoAssets.h
//  TripPlot
//
//  Created by Daffolap-21 on 08/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "JSONModel.h"

@interface TPPPhotoAssets : JSONModel

@property(retain, nonatomic) NSMutableArray *assets;
@property(retain, nonatomic) NSArray *imagePaths;
@property(retain, nonatomic) NSArray *imageURLs;
@property(nonatomic) BOOL flag;

+ (id) sharedInstance;

@end

//
//  TPPNotificationModel.h
//  TripPlot
//
//  Created by Vishwas Singh on 04/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPNotificationModel : JSONModel

@property NSString<Optional> *userId;
@property NSString<Optional> *firstname;
@property NSString<Optional> *lastname;
@property NSString<Optional> *profileImage;
@property NSString<Optional> *tripId;
@property NSString<Optional> *tripTitle;
@property NSString<Optional> *updatedOn;

@property NSString<Optional> *event;
@property NSString<Optional> *message;


@end




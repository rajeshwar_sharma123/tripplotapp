//
//  TPPSharedTripCountsModel.m
//  TripPlot
//
//  Created by Daffolap-21 on 30/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "TPPSharedTripCountsModel.h"

@implementation TPPSharedTripCountsModel



static TPPSharedTripCountsModel *instance;
+(instancetype)sharedInstance
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[self alloc] init];
			//instance.addedEmailIDs = [NSMutableArray new];
			//instance.addedContacts = [NSMutableArray new];
	});

	return instance;
}

+ (JSONKeyMapper*)keyMapper
{
	return [[JSONKeyMapper alloc] initWithDictionary:@{
													   @"description" : @"descriptions"

													   }];
}

+(BOOL)propertyIsOptional:(NSString *)propertyName
{
	if ([propertyName isEqualToString:@"myTripCount"] || [propertyName isEqualToString:@"inviteCount"] || [propertyName isEqualToString:@"isOngoing"] || [propertyName isEqualToString:@"onGoingTripId"])
	{
		return YES;
	}

	return NO;

}

+(void) callServiceToGetLiveTripDetails:(void (^)())completionBlock
{
	NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
	NSMutableDictionary *credentialsDictionary = [NSMutableDictionary new];
	credentialsDictionary[@"token"]= [UserDeafaults objectForKey:Token];
	credentialsDictionary[@"username"] = userProfile[@"username"];
	credentialsDictionary[@"contact"] = userProfile[@"contact"];
	credentialsDictionary[@"userId"] = userProfile[@"_id"];

	[TPPAppServices postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:URL_TO_GET_COUNT] data:credentialsDictionary withSuccessBlock:^(id response, NSDictionary *headers)
	 {
		 if ([[response objectForKey:@"code"] integerValue] == 200)
		 {
			 NSDictionary *result = response[@"response"];
			 TPPSharedTripCountsModel *trips = [TPPSharedTripCountsModel sharedInstance];
			 trips.myTripCount = [result[@"myTripCount"] intValue];
			 trips.inviteCount = [result[@"inviteCount"] intValue];
			 trips.isOngoing = [result[@"liveTrip"][@"isOngoing"] intValue];
			 trips.onGoingTripId = result[@"liveTrip"][@"onGoingTripId"];

			 [[NSNotificationCenter defaultCenter] postNotificationName:@"LeftSideMenuNotification" object:nil];

			 completionBlock();
		 }
		 else
		 {

		 }

	 } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
	 {
		 TPPSharedTripCountsModel *trips = [TPPSharedTripCountsModel sharedInstance];
		 
	 }];
}
@end

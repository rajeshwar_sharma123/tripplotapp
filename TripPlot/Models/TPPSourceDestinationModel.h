//
//  TPPSourceDestinationModel.h
//  TripPlot
//
//  Created by Daffolap-21 on 06/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "TPPTripLocation.h"

@interface TPPSourceDestinationModel : JSONModel
@property TPPTripLocation<Optional> *source;
@property TPPTripLocation<Optional> *destination;

@end

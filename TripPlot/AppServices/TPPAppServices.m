//
//  TPPAppServices.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPAppServices.h"


@implementation TPPAppServices

/**
 *  Called for like/unlike
 *
 *  @param tripModel
 *  @param sender    tapped button, to update the text correspondingly
 *  @param indexPath if indexpath is not nil, means activity has been done from detail screen rather than cell of a tableview
 */

+(void)callServiceForLikeUnlikeTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath
{
    [sender showSpinnerWithUserInteractionDisabled:YES];
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    NSDictionary *mainDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UserDeafaults objectForKey:Token],@"token",
                                    userProfile[@"_id"] ,@"userId",
                                    tripModel._id ,@"tripId",
                                    tripModel.createdBy[@"_id"] ,@"createdById",
                                    nil];
    NSString *likeUnlikeUrl;

    if (tripModel.isLikedByMe)
    {
        likeUnlikeUrl = [TPPServiceUrls getCompleteUrlFor:UnLikeUrl];
    }
    else
    {
        likeUnlikeUrl = [TPPServiceUrls getCompleteUrlFor:LikeUrl];
    }

    [self postServiceWithUrl:likeUnlikeUrl data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [sender hideSpinner];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSDictionary *myProfile = [UserDeafaults objectForKey:UserProfile];
             
             if (tripModel.isLikedByMe)
             {
                 tripModel.isLikedByMe = NO;
                 [sender setTitle:Txt_Like forState:UIControlStateNormal];
                 
                 
                 [TPPUtilities removeUserProfileFromArray:tripModel.like isJoin:NO];
                 tripModel.likeCount--;
             }
             else
             {
                 tripModel.isLikedByMe = YES;
                 [sender setTitle:Txt_UnLike forState:UIControlStateNormal];
                 
                 [tripModel.like addObject:myProfile];
                 tripModel.likeCount++;
             }
             
             NSDictionary *userInfo;
             if (indexPath)
             {
                 userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                             indexPath, @"indexPath",
                             tripModel, @"tripModel",
                             [NSNumber numberWithBool:YES], Txt_IsLikedUnliked,
                             nil];
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:UserActivityNotification object:nil userInfo:userInfo];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [sender hideSpinner];
         UIViewController *currentVC= [TPPUtilities getCurrentViewController];
         [currentVC showErrorMessage:errorMessage];
     }];
    
}

/**
 *  Called for follow/unfollow
 *
 *  @param tripModel
 *  @param sender    tapped button, to update the text correspondingly
 *  @param indexPath if indexpath is not nil, means activity has been done from detail screen rather than cell of a tableview
 */
+(void)callServiceForFollowUnfollowTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath
{
    [sender showSpinnerWithUserInteractionDisabled:YES];
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id ,@"tripId",
                                           tripModel.createdBy[@"_id"] ,@"createdById",
                                           nil];
    
    NSString *followUnfollowUrl;
    if (tripModel.isFollowedByMe)
    {
        followUnfollowUrl = [TPPServiceUrls getCompleteUrlFor:UnFollowUrl];
    }
    else
    {
        followUnfollowUrl = [TPPServiceUrls getCompleteUrlFor:FollowUrl];
    }
    
    
    [self postServiceWithUrl:followUnfollowUrl data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [sender hideSpinner];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             NSDictionary *myProfile = [UserDeafaults objectForKey:UserProfile];
             
             if (tripModel.isFollowedByMe)
             {
                 tripModel.isFollowedByMe = NO;
                 [sender setTitle:Txt_Follow forState:UIControlStateNormal];
                 
                 [TPPUtilities removeUserProfileFromArray:tripModel.follow isJoin:NO];
             }
             else
             {
                 tripModel.isFollowedByMe = YES;
                 [sender setTitle:Txt_UnFollow forState:UIControlStateNormal];
                 
                 [tripModel.follow addObject:myProfile];
             }
             
             NSDictionary *userInfo;
             if (indexPath)
             {
                userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                       indexPath, @"indexPath",
                                       tripModel, @"tripModel",
                                        [NSNumber numberWithBool:YES], Txt_IsFollowedUnfollowed,
                                       nil];
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:UserActivityNotification object:nil userInfo:userInfo];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [sender hideSpinner];
         UIViewController *currentVC= [TPPUtilities getCurrentViewController];
         [currentVC showErrorMessage:errorMessage];
     }];
    
}

/**
 *  Called for join/unUnjoin
 *
 *  @param tripModel
 *  @param sender    tapped button, to update the text correspondingly
 *  @param indexPath if indexpath is not nil, means activity has been done from detail screen rather than cell of a tableview
 */

+(void)callServiceForJoinUnjoinTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath
{
    [sender showSpinnerWithUserInteractionDisabled:YES];
    
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [UserDeafaults objectForKey:Token],@"token",
                                           userProfile[@"_id"] ,@"userId",
                                           tripModel._id ,@"tripId",
                                           tripModel.createdBy[@"_id"] ,@"createdById",
                                           nil];
    
    
    NSString *joinUnjoinUrl;
    if (tripModel.isJoinedByMe)
    {
        joinUnjoinUrl = [TPPServiceUrls getCompleteUrlFor:UnJoinUrl];
    }
    else
    {
        [mainDictionary setObject:userProfile[@"contact"] forKey:@"contact"];
        [mainDictionary setObject:userProfile[@"profileImage"] forKey:@"profileImage"];
        
        
        joinUnjoinUrl = [TPPServiceUrls getCompleteUrlFor:JoinUrl];
    }
    
    
    [self postServiceWithUrl:joinUnjoinUrl data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         [sender hideSpinner];
         
         if ([[response objectForKey:@"code"] integerValue] == 200)
         {
             if (tripModel.isJoinedByMe)
             {
                 tripModel.isJoinedByMe = NO;
                 [sender setTitle:Txt_Join forState:UIControlStateNormal];
                 
                 [TPPUtilities removeUserProfileFromArray:tripModel.tripMembers isJoin:YES];
             }
             else
             {
                 NSMutableDictionary *myProfile = [[NSMutableDictionary alloc]initWithDictionary:[UserDeafaults objectForKey:UserProfile]];
                 [myProfile setObject:myProfile[@"_id"] forKey:@"userId"];
                 
                 
                 tripModel.isJoinedByMe = YES;
                 [sender setTitle:Txt_UnJoin forState:UIControlStateNormal];
                 
                 [tripModel.tripMembers addObject:myProfile];
             }
             
             NSDictionary *userInfo;
             if (indexPath)
             {
                 userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                             indexPath, @"indexPath",
                             tripModel, @"tripModel",
                             [NSNumber numberWithBool:YES], Txt_IsJoinedUnjoined,
                             nil];
             }
             [[NSNotificationCenter defaultCenter] postNotificationName:UserActivityNotification object:nil userInfo:userInfo];
         }
         else
         {
             
         }
         
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         [sender hideSpinner];
         UIViewController *currentVC= [TPPUtilities getCurrentViewController];
         [currentVC showErrorMessage:errorMessage];
         
     }];
    
}


#pragma mark- Register/DeRegister device for Push notifications


+(void)callServiceToRegisterDeviceForPushNotifications:(BOOL)shouldRegister
{
    NSDictionary *userProfile = [UserDeafaults objectForKey:UserProfile];
    
    if (!userProfile)
    {
        return;
    }
    
    NSMutableDictionary *mainDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           userProfile[@"_id"] ,@"_id",
                                           [UserDeafaults objectForKey:DeviceToken] ,@"deviceId",
                                           [NSNumber numberWithInteger:2], @"deviceType",
                                           [UserDeafaults objectForKey:Token], @"token",
                                           
                                           nil];
    
    NSString *url;
    if (shouldRegister)
    {
        url = RegisterDeviceUrl;
    }
    else
    {
        url = DeRegisterDeviceUrl;
    }
    
    [self postServiceWithUrl:[TPPServiceUrls getCompleteUrlFor:url]  data:mainDictionary withSuccessBlock:^(id response, NSDictionary *headers)
     {
         NSLog(@"Registered for Push.................");
     } withErrorBlock:^(NSError *error, NSDictionary *headers, id responseObject, NSString *errorMessage)
     {
         NSLog(@"UnRegistered for Push.................");
     }];
    
}

+(void) postServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError * error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock
{
    
    NSString *urlRegistration=serviceURL;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"YEPCjQfWl95kI4ZtLbs/rw==" forHTTPHeaderField:@"apikey"];
   
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];

    //check if token exist
    if([UserDeafaults objectForKey:Token])
    {
      //  [manager.requestSerializer setValue:[UserDeafaults objectForKey:UserToken] forHTTPHeaderField:@"Authorization"];
    }
    NSLog(@"token auth: %@",[UserDeafaults objectForKey:Token] );
    [manager POST:urlRegistration
     
       parameters:requestData
     
          success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
              
              NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
              NSMutableDictionary *headers=[headersCollection mutableCopy];
              headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
              
              
              successBlock(responseObject,headers);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
              NSMutableDictionary *headers=[headersCollection mutableCopy];
              headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
              
              
              errorBlock(error,headers,operation.responseObject,[TPPErrorUtility getErrorMessageForResponseObject: operation.responseObject response: operation.response error:error]);
              
          }];
    
}


+(void) putServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock
{
    NSString *urlRegistration=serviceURL;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    [manager.requestSerializer setValue:@"YEPCjQfWl95kI4ZtLbs/rw==" forHTTPHeaderField:@"apikey"];
    NSLog(@"%@",[[UserDeafaults dictionaryRepresentation] allValues]);
    [manager.requestSerializer setValue:[UserDeafaults objectForKey:Token] forHTTPHeaderField:@"Authorization"];
    [manager PUT:urlRegistration
      parameters:requestData
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
             NSMutableDictionary *headers=[headersCollection mutableCopy];
             headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
             successBlock(responseObject,headers);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
             NSMutableDictionary *headers=[headersCollection mutableCopy];
             headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
             
             errorBlock(error,headers,operation.responseObject,[TPPErrorUtility getErrorMessageForResponseObject: operation.responseObject response: operation.response error:error]);
             
         }];
    
}

+(void) getServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock
{
    NSString *urlRegistration=serviceURL;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer= [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    [manager.requestSerializer setValue:@"YEPCjQfWl95kI4ZtLbs/rw==" forHTTPHeaderField:@"apikey"];
    [manager.requestSerializer setValue:[UserDeafaults objectForKey:Token] forHTTPHeaderField:@"Authorization"];
    NSLog(@"%@",[[UserDeafaults dictionaryRepresentation] allValues]);
    NSLog(@"token: %@",[UserDeafaults objectForKey:Token]);
    [manager GET:urlRegistration
      parameters:requestData
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
             NSMutableDictionary *headers=[headersCollection mutableCopy];
             headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
             successBlock(responseObject,headers);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
             NSMutableDictionary *headers=[headersCollection mutableCopy];
             headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
             
             errorBlock(error,headers,operation.responseObject,[TPPErrorUtility getErrorMessageForResponseObject: operation.responseObject response: operation.response error:error]);
             
         }];
    
    
}

+(void) UploadImageServiceWithUrl:(NSString *)serviceURL image:(UIImage *)image withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock
{
    NSString *urlUploadImage=serviceURL;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:[UserDeafaults objectForKey:Token] forHTTPHeaderField:@"Authorization"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    
    [manager POST:urlUploadImage parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.png" mimeType:@"image/png"];
    }
     
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
              NSMutableDictionary *headers=[headersCollection mutableCopy];
              headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
              successBlock(responseObject,headers);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSDictionary *headersCollection=[[(NSDictionary *)operation valueForKey:@"response"]valueForKey:@"allHeaderFields"];
              NSMutableDictionary *headers=[headersCollection mutableCopy];
              headers[@"statusCode"]=[NSNumber numberWithInteger:operation.response.statusCode];
              
              errorBlock(error,headers,operation.responseObject,[TPPErrorUtility getErrorMessageForResponseObject: operation.responseObject response: operation.response error:error]);
              
          }];
    
}




@end

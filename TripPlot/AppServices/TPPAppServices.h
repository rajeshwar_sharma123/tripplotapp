//
//  TPPAppServices.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 02/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "TPPTripModel.h"


@interface TPPAppServices : NSObject

+(void) postServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock;

+(void) putServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock;

+(void) getServiceWithUrl:(NSString *)serviceURL data:(NSDictionary *)requestData withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock;

+(void) UploadImageServiceWithUrl:(NSString *)serviceURL image:(UIImage *)image withSuccessBlock:(void (^)(id  response,NSDictionary *headers))successBlock withErrorBlock:(void (^)(NSError *error,NSDictionary *headers,id responseObject,NSString *errorMessage))errorBlock;


/**
 *  To register/DeRegister device for Push Notifications
 *
 *  @param shouldRegister YES to Register device, No to DeRegister device
 */
+(void)callServiceToRegisterDeviceForPushNotifications:(BOOL)shouldRegister;


+(void)callServiceForLikeUnlikeTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath;

+(void)callServiceForFollowUnfollowTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath;

+(void)callServiceForJoinUnjoinTripWithTripModel:(TPPTripModel *)tripModel button:(UIButton *)sender indexPath:(NSIndexPath *)indexPath;


@end

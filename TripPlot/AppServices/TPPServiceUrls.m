//
//  TPPServiceUrls.m
//  TripPlot
//
//  Created by Daffolap-mac-22 on 06/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPServiceUrls.h"

@implementation TPPServiceUrls



//+(NSString *)getRegisterUrl
//{
//    return [NSString stringWithFormat:@"%@%@",BaseUrl,RegisterUrl];
//}
//
//+(NSString *)getLoginUrl
//{
//    return [NSString stringWithFormat:@"%@%@",BaseUrl,LogInUrl];
//}

+(NSString *)getCompleteUrlFor:(NSString *)url
{
    return [NSString stringWithFormat:@"%@%@",BaseUrl,url];
}

+(NSString *)getCompleteUrlWithToken:(NSString *)url
{
    return [NSString stringWithFormat:@"%@%@?token=%@",BaseUrl,url,[UserDeafaults objectForKey:Token]];
}

+(NSString *)getCompleteUrlForGetRequest:(NSString *)url _id:(NSString *)_id
{
    return [NSString stringWithFormat:@"%@%@?_id=%@&token=%@",BaseUrl,url,_id,[UserDeafaults objectForKey:Token]];
}


@end

//
//  TPPServiceUrls.h
//  TripPlot
//
//  Created by Daffolap-mac-22 on 06/11/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPPServiceUrls : NSObject


//+(NSString *)getRegisterUrl;
//+(NSString *)getLoginUrl;
//
+(NSString *)getCompleteUrlFor:(NSString *)url;
+(NSString *)getCompleteUrlWithToken:(NSString *)url;
+(NSString *)getCompleteUrlForGetRequest:(NSString *)url _id:(NSString *)_id;

@end

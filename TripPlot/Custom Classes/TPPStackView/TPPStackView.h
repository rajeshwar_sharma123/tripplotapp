//
//  TPPStackView.h
//  TripPlot
//
//  Created by Vishwas Singh on 21/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPPStackView;


typedef NS_ENUM(NSInteger, TPPStackViewTypeSliding)
{
    TPPStackViewTypeSlidingDefault,
    TPPStackViewTypeSlidingHorizontal
};

@protocol TPPStackViewDelegate <NSObject>
- (NSInteger)numberOfViews;
- (UIView *)stackView:(TPPStackView *)stackView viewForRowAtIndex:(NSInteger)index;
@optional
-(void)stackView:(TPPStackView *)stackView didSelectViewAtIndex:(NSInteger)index;
-(void)stackView:(TPPStackView *)stackView willChangeViewAtIndex:(NSInteger)index;
-(void)stackView:(TPPStackView *)stackView didChangeViewAtIndex:(NSInteger)index;
@end

@protocol TPPStackViewShiftDelegate <NSObject>
-(CGFloat)sizeOfShiftStack;
-(CGRect)stackItem:(UIView *)stackItem rectViewAtIndex:(NSInteger)index andShift:(CGFloat)shift;
@end





@interface TPPStackView : UIView

@property (weak, nonatomic) IBOutlet id<TPPStackViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet id<TPPStackViewShiftDelegate> shiftDelegate;

@property (nonatomic) enum TPPStackViewTypeSliding typeSliding;
@property (nonatomic, getter = isSlidingTransparentEffect) BOOL slidingTransparentEffect;

-(void)reloadData;

@end
//
//  TPPTextField.h
//  TripPlot
//
//  Created by Vishwas Singh on 21/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TPPTextFieldDelegate <NSObject>


-(void)textFieldRightButtonTapped;


@end




@interface TPPTextField : UITextField<UITextFieldDelegate>


@property (nonatomic, weak) id <TPPTextFieldDelegate> buttonDelegate;

-(void)addButtonOnRightWithImageName:(NSString *)imageName;



@end

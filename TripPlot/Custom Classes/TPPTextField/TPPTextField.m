//
//  TPPTextField.m
//  TripPlot
//
//  Created by Vishwas Singh on 21/12/15.
//  Copyright © 2015 Daffodil. All rights reserved.
//

#import "TPPTextField.h"

@implementation TPPTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if(self)
    {
        self.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.05];
        self.textColor= TPPTextColor;
        
        [self setValue:TPPTextColor forKeyPath:@"_placeholderLabel.textColor"];
        
        
        self.layer.cornerRadius = 1.0;
        self.clipsToBounds = YES;
        
        
//        self.autocorrectionType=UITextAutocorrectionTypeNo;
//        self.spellCheckingType=UITextSpellCheckingTypeNo;
//        self.font=[UIFont fontWithName:@"Roboto-Light" size:14.0f];
//        
        
    }
    
    return self;
}


-(CGRect)textRectForBounds:(CGRect)bounds {
    
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y,
                      bounds.size.width, bounds.size.height);
}


- (CGRect) leftViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super leftViewRectForBounds:bounds];
    
    textRect.origin.x=bounds.origin.x + 15;
    
    return textRect;
}



- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}


//-(void)setText:(NSString *)text
//{
//    [super setText:text];
//    [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:self userInfo:nil];
//}





-(void)addButtonOnRightWithImageName:(NSString *)imageName;
{
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 62, 32)];
    [rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    [rightButton addTarget:self action:@selector(rightButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self setRightViewMode:UITextFieldViewModeAlways];
    [self setRightView:rightButton];
    
}



-(void)rightButtonTapped
{
    [self.buttonDelegate textFieldRightButtonTapped];
}






@end

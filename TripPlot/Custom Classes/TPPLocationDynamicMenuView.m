//
//  TPPLocationMenuView.m
//  Demo
//
//  Created by daffolapmac on 05/02/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

#import "TPPLocationDynamicMenuView.h"

@implementation TPPLocationDynamicMenuView
-(TPPLocationDynamicMenuView *)initWithFrame:(CGRect )frame data:(NSArray *)arr
{
    self = [super init];
    self.frame = frame;
    dataArray = arr;
    self.contentSize = CGSizeMake(arr.count*70.0+20, self.frame.size.height);
    [self setShowsHorizontalScrollIndicator:NO];
    float xOffset = 5.0;
    float yOffset = 0.0;
    
    for (int i = 0; i<arr.count; i++) {
        NSDictionary *dic = [arr objectAtIndex:i];
        UIView *subView = [[UIView alloc]initWithFrame:CGRectMake(xOffset, yOffset, 70.0f, self.frame.size.height)];
        subView.backgroundColor = [UIColor clearColor];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = [UIColor clearColor];
        [button setImage:[UIImage imageNamed:dic[@"image"]] forState:UIControlStateNormal];
        button.tag = i+1;
        button.frame = CGRectMake(12.0f, 4.0f, 46.0, 46.0);
        [subView addSubview:button];
        
        UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(1,button.frame.origin.y+button.frame.size.height+3, subView.frame.size.width-2, 15)];
			//lblName.tag = i+1;
        lblName.backgroundColor = [UIColor clearColor];
        lblName.textAlignment = NSTextAlignmentCenter;
        lblName.text = dic[@"name"];
        lblName.textColor = [UIColor whiteColor];
        lblName.font = [UIFont systemFontOfSize:11];
        [subView addSubview:lblName];
        
        UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        subView.userInteractionEnabled = YES;
        [subView addGestureRecognizer:singleFingerTap];
        [self addSubview:subView];
        xOffset = xOffset+subView.frame.size.width;
    }
    return self;
}

-(void)buttonClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %ld", (long)sender.tag);
	
    [self.delegate  locationMenuTapped:(id)sender info:[dataArray objectAtIndex: (long)sender.tag-1] atIndex: (long)sender.tag];
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    //Do stuff here...
    
   // CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
}

- (void)setBadgesForPlaces:(TPPSharedTripCountsModel *)shareModel
{
    tripCountsWithDetail = shareModel;
    
    NSArray *subViews =  self.subviews;
    
    for (int i = 0; i< subViews.count; i++) {
        
        UIButton *btn = [[subViews objectAtIndex:i] viewWithTag:i+1];
        [self setBadges:btn];
    }
}



- (void)setBadges:(UIButton *)btn
{
    switch (btn.tag) {
        case 1:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"fuel"] count]>0) {
                btn.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"fuel"] count]] ;
                btn.badgePostion = BadgePositionon_MiddleRight;
            }
            break;
        case 2:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"food"] count]>0) {
                btn.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"food"] count]];
                btn.badgePostion = BadgePositionon_MiddleRight;
            }
            break;
        case 3:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"restroom"] count]>0) {
                btn.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"restroom"] count]];
                btn.badgePostion = BadgePositionon_MiddleRight;
            }
            break;
        case 4:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"poi"] count]>0) {
                btn.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"poi"] count]];
                btn.badgePostion = BadgePositionon_MiddleRight;
            }
            break;
        case 5:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"medical"] count]>0) {
                btn.badgeValue  = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"medical"] count]];
                btn.badgePostion = BadgePositionon_MiddleRight;
            }

            break;
        case 6:
            if ([tripCountsWithDetail.liveTrip.tripPoints[@"atm"] count]>0) {
                btn.badgeValue = [NSString stringWithFormat:@"%u", [tripCountsWithDetail.liveTrip.tripPoints[@"atm"] count]];
                btn.badgePostion = BadgePositionon_MiddleRight;
            }

            break;
        default:
            break;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

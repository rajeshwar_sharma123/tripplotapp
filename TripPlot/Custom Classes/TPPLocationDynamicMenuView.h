//
//  TPPLocationMenuView.h
//  Demo
//
//  Created by daffolapmac on 05/02/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPPDelegates.h"
@interface TPPLocationDynamicMenuView : UIScrollView
{
	TPPSharedTripCountsModel *tripCountsWithDetail;
    NSArray *dataArray;
}
@property (nonatomic, weak) id <TPPMenuViewDelegate> delegate;

-(TPPLocationDynamicMenuView *)initWithFrame:(CGRect )frame data:(NSArray *)arr;
- (void)setBadgesForPlaces:(TPPSharedTripCountsModel *)tripCountsWithDetail;
@end

//
//  TPPScrollMenu.h
//  TripPlotXibDemo-Pradeep
//
//  Created by Daffolap-21 on 13/01/16.
//  Copyright © 2016 Daffolap-21. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPPScrollMenu : UIView

@property (weak, nonatomic) IBOutlet UILabel *label;
-(instancetype)init;

- (instancetype)initWithFrame:(CGRect)frame;
@property (weak, nonatomic) IBOutlet UIButton *button;
- (IBAction)typeButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeViewContentWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *typeContentView;

@property NSInteger isTypeContentExpanded;
@property NSInteger isModeContentExpanded;
@property NSInteger isPeopleContentExpanded;

@property (weak, nonatomic) IBOutlet UIView *modeContentView;
- (IBAction)modeButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *peopleContentView;
- (IBAction)peopleButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modeViewContentWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *peopleViewContentWidthConstraint;

@property (weak, nonatomic) IBOutlet UIView *distanceContentView;
@property (weak, nonatomic) IBOutlet UIView *timeContentView;

-(void)configure;
@property (weak, nonatomic) IBOutlet UIButton *modeButton;
@property (weak, nonatomic) IBOutlet UIButton *typeButton;
@property (weak, nonatomic) IBOutlet UIButton *peopleButton;

@property (nonatomic,strong)TPPTripModel *tripModel;

@property (weak, nonatomic) IBOutlet UILabel *distanceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (nonatomic)UIColor *backgroundColorForButtons;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *distanceWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *distanceLeadingConstraint;

@property (weak, nonatomic) IBOutlet UILabel *kmsLabel;

-(void)hideDistance;

@end

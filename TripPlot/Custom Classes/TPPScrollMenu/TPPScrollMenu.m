//
//  TPPScrollMenu.m
//  TripPlotXibDemo-Pradeep
//
//  Created by Daffolap-21 on 13/01/16.
//  Copyright © 2016 Daffolap-21. All rights reserved.
//

#import "TPPScrollMenu.h"
#define DEFAULT_CONTENT_WIDTH 50


@implementation TPPScrollMenu



-(instancetype)init{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TPPScrollMenu" owner:self options:nil];
    id mainView = [subviewArray objectAtIndex:0];
    return mainView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    frame.origin.x = 0;
    frame.origin.y = 0;

    self = [super initWithFrame:frame];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TPPScrollMenu" owner:self options:nil];
        id mainView = [subviewArray objectAtIndex:0];
        [mainView setFrame:frame];
       // [self configure];
        return mainView;
    }
    return self;
}


//_tripModel isn't the accessor
-(void)setTripModel:(TPPTripModel *)tripModel
{
    _tripModel = tripModel;
    
    [self configure];
}


-(void)configure
{
    if (!self.tripModel) {
        
    }
    
   // [self addItems:5 on:self.typeContentView];
    self.typeContentView.layer.cornerRadius = self.typeContentView.frame.size.height/2;
    self.typeContentView.clipsToBounds = YES;
    self.modeContentView.layer.cornerRadius = self.modeContentView.frame.size.height/2;
    self.modeContentView.clipsToBounds = YES;
    self.peopleContentView.layer.cornerRadius = self.peopleContentView.frame.size.height/2;
    self.peopleContentView.clipsToBounds = YES;

    self.timeContentView.layer.cornerRadius = self.timeContentView.frame.size.height/2;
    self.timeContentView.clipsToBounds = YES;

    self.distanceContentView.layer.cornerRadius = self.distanceContentView.frame.size.height/2;
    self.distanceContentView.clipsToBounds = YES;

    self.modeButton.layer.cornerRadius = self.modeButton.frame.size.height/2;
    self.modeButton.clipsToBounds = YES;

    self.typeButton.layer.cornerRadius = self.typeButton.frame.size.height/2;
    self.typeButton.clipsToBounds = YES;

    self.peopleButton.layer.cornerRadius = self.peopleButton.frame.size.height/2;
    self.peopleButton.clipsToBounds = YES;
    
    //set data model
    self.typeLabel.text = _tripModel.tripTypes.count >0 ? [_tripModel.tripTypes objectAtIndex:0][@"name"]: @"N/A";
    self.modeLabel.text = _tripModel.tripModes.count >0 ? [_tripModel.tripModes objectAtIndex:0][@"name"]: @"N/A";
    
    NSInteger kms = [_tripModel.distance floatValue]/1000;
    NSString *kmsString = [NSString stringWithFormat:@"%ld",kms];
    self.distanceValueLabel.text = _tripModel.distance != nil ? kmsString:@"N/A";
    
    
    NSInteger noOfDays = [TPPUtilities getNoOfDaysBetweenStartDate:_tripModel.tripDate[@"startDate"] endDate:_tripModel.tripDate[@"endDate"]];

    self.tripDurationLabel.text = [NSString stringWithFormat:@"%ld",noOfDays];

    [self.peopleButton setTitle:[NSString stringWithFormat:@"%ld",_tripModel.tripMembers.count] forState:UIControlStateNormal];
    
    if (_tripModel.tripTypes.count >0)
    {
        NSDictionary *dict = _tripModel.tripTypes[0];
        self.typeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.typeButton.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"image"]]
                         placeholderImage:[UIImage imageNamed:@"icn_placeholder_squire"]
                                  options:SDWebImageRefreshCached
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self.typeButton setBackgroundImage:image forState:UIControlStateNormal];
             }
             else
             {
                 [self.typeButton setBackgroundImage:[UIImage imageNamed:@"icn_placeholder_squire"] forState:UIControlStateNormal];
             }
         }];

    }
    
    if (_tripModel.tripModes.count >0)
    {
        self.modeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
       
        NSDictionary *dict = _tripModel.tripModes[0];
    
        [self.modeButton.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"image"]]
                                     placeholderImage:[UIImage imageNamed:@"icn_placeholder_squire"]
                                              options:SDWebImageRefreshCached
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [self.modeButton setBackgroundImage:image forState:UIControlStateNormal];
             }
             else
             {
                  [self.modeButton setBackgroundImage:[UIImage imageNamed:@"icn_placeholder_squire"] forState:UIControlStateNormal];
             }
         }];
        
    }

    
    if (_tripModel.tripModes.count >1)
    {
        [self.modeButton setImage:[UIImage imageNamed:@"people_arrow"] forState:UIControlStateNormal];
    self.modeButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.modeButton.imageView.frame.size.width, 0, self.modeButton.imageView.frame.size.width);
    self.modeButton.imageEdgeInsets = UIEdgeInsetsMake(0, self.modeButton.frame.size.width-8, 0, -self.modeButton.titleLabel.frame.size.width);
    }
    else
    {
        [self.modeButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }
    
    
    if (_tripModel.tripTypes.count >1)
    {
        [self.typeButton setImage:[UIImage imageNamed:@"people_arrow"] forState:UIControlStateNormal];
        self.typeButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.typeButton.imageView.frame.size.width, 0, self.typeButton.imageView.frame.size.width);
        self.typeButton.imageEdgeInsets = UIEdgeInsetsMake(0, self.typeButton.frame.size.width-8, 0, -self.typeButton.titleLabel.frame.size.width);
    }
    else
    {
        [self.modeButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }

    
        [self.peopleButton setImage:[UIImage imageNamed:@"people_arrow"] forState:UIControlStateNormal];
        self.peopleButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.peopleButton.imageView.frame.size.width, 0, self.peopleButton.imageView.frame.size.width);
        self.peopleButton.imageEdgeInsets = UIEdgeInsetsMake(0, self.peopleButton.frame.size.width-8, 0, -self.peopleButton.titleLabel.frame.size.width);

}

-(void)hideDistance
{
    _distanceLeadingConstraint.constant = 0;
    _distanceWidthConstraint.constant = 0;
    _kmsLabel.hidden = YES;
}

-(void)expand:(UIButton *)sender
{
    NSLog(@"tapped tag:  %ld",sender.tag);
}

- (IBAction)typeButtonTapped:(id)sender {
    [self configure];
    if (_tripModel.tripTypes.count >1) {
    if(_isTypeContentExpanded)
    {
        [self rotateArrowImageIn:sender isExpanded:YES];
        
        self.typeViewContentWidthConstraint.constant = DEFAULT_CONTENT_WIDTH;
        _isTypeContentExpanded = NO;
        [self removeItemsFormView:self.typeContentView];
    }
    else{
        [self rotateArrowImageIn:sender isExpanded:NO];
        
        self.typeViewContentWidthConstraint.constant = [self calculateContentWidthForItems:self.tripModel.tripTypes.count-1];
        _isTypeContentExpanded = YES;
        [self addItems:self.tripModel.tripTypes on:self.typeContentView isForProfile:NO];
    }
    }
}
- (IBAction)modeButtonTapped:(id)sender {
//[self setNeedsUpdateConstraints];
    if (_tripModel.tripModes.count >1)
    {
        if(_isModeContentExpanded)
        {
            [self rotateArrowImageIn:sender isExpanded:YES];
            
            self.modeViewContentWidthConstraint.constant = DEFAULT_CONTENT_WIDTH;
            _isModeContentExpanded = NO;
            
            [self removeItemsFormView:self.modeContentView];
        }
        else{
            [self rotateArrowImageIn:sender isExpanded:NO];
            
            self.modeViewContentWidthConstraint.constant = [self calculateContentWidthForItems:self.tripModel.tripModes.count-1];
            _isModeContentExpanded = YES;
            [self addItems:self.tripModel.tripModes on:self.modeContentView isForProfile:NO];
        }
    }
}
- (IBAction)peopleButtonTapped:(id)sender
{
    
    if(_isPeopleContentExpanded)
    {
        [self rotateArrowImageIn:sender isExpanded:YES];
        self.peopleViewContentWidthConstraint.constant = DEFAULT_CONTENT_WIDTH;
        _isPeopleContentExpanded = NO;
        [self removeItemsFormView:self.peopleContentView];
    }
    else
    {
        [self rotateArrowImageIn:sender isExpanded:NO];
        self.peopleViewContentWidthConstraint.constant = [self calculateContentWidthForItems:self.tripModel.tripMembers.count];
        _isPeopleContentExpanded = YES;
        [self addItems:self.tripModel.tripMembers on:self.peopleContentView isForProfile:YES];
    }
}

-(void)addItems:(NSArray *)data on:(UIView *)view isForProfile:(BOOL)isProfile
{
    // CGRect rectOfParent = view.frame;
    NSInteger startIndex = 1;
    if (isProfile)
    {
        startIndex = 0;
    }
    for (NSInteger atIndex = startIndex; atIndex < data.count; atIndex++)
    {
        NSDictionary *dict = [data objectAtIndex:atIndex];
        
        CGRect rect;
        if (startIndex == 1)
        {
            rect = CGRectMake(45 *atIndex+8, 5, 40, 40);
        }
        else
        {
            rect = CGRectMake(45 *(atIndex+1), 5, 40, 40);
        }
        
        UIButton *btn= [[UIButton alloc]initWithFrame:rect];
        btn.tag = atIndex +1;
        btn.layer.cornerRadius = btn.frame.size.height/2;
        btn.clipsToBounds = YES;
        NSString *keyForImage;
        if (isProfile) {
            keyForImage = @"profileImage";
        }
        else
        {
            keyForImage = @"image";
        }
        
        UIImage *defaultImage = [UIImage imageNamed:@"icn_placeholder_squire"];
        [btn.imageView sd_setImageWithURL:[NSURL URLWithString:dict[keyForImage]]
                         placeholderImage:defaultImage
                                  options:SDWebImageRefreshCached
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if (image)
             {
                 [btn setImage:image forState:UIControlStateNormal];
             }
             else if (isProfile)
             {
                 [btn setImage:[UIImage imageNamed:@"icn_defaultuser"] forState:UIControlStateNormal];
             }
             else
             {
                 [btn setImage:defaultImage forState:UIControlStateNormal];
             }
         }];
        
        //[btn setBackgroundColor:[UIColor redColor]];
        [view addSubview:btn];
    }
    
    [UIView animateWithDuration:AnimationDuration animations:^{
        [self layoutIfNeeded];
    }];
}

-(void) removeItemsFormView:(UIView *)view
{
    [UIView animateWithDuration:AnimationDuration animations:^{
        
        [self layoutIfNeeded];
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(AnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
       
        for (UIButton *btn in view.subviews){
            if (btn.tag>0)
            {
                [btn removeFromSuperview]; //remove buttons
            }
        }
    });
}

-(NSInteger )calculateContentWidthForItems:(NSInteger) count
{
    return DEFAULT_CONTENT_WIDTH + count*40 + count*5;
}




-(void)rotateArrowImageIn:(UIButton *)button isExpanded:(BOOL)isExpanded
{
    float radians = isExpanded?0 : M_PI;
    
    [UIView animateWithDuration:AnimationDuration
                     animations:^{
                         button.imageView.transform = CGAffineTransformMakeRotation(radians);
                     }];
    
}


-(void)setBackgroundColorForButtons:(UIColor *)backgroundColorForButtons
{
    self.modeContentView.backgroundColor = backgroundColorForButtons;
    self.typeContentView.backgroundColor = backgroundColorForButtons;
    self.peopleContentView.backgroundColor = backgroundColorForButtons;
    self.distanceContentView.backgroundColor = backgroundColorForButtons;
    self.timeContentView.backgroundColor = backgroundColorForButtons;
    
}


@end

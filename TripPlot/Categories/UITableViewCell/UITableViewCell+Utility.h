//
//  UITableViewCell+Utility.h
//  TripPlot
//
//  Created by Vishwas Singh on 18/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITableViewCell(Utility)

-(UITableView *)tableView;
-(NSIndexPath *)indexPath;


@end

//
//  UITableViewCell+Utility.m
//  TripPlot
//
//  Created by Vishwas Singh on 18/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "UITableViewCell+Utility.h"

@implementation UITableViewCell(Utility)



-(UITableView *)tableView
{
    UITableView *tableView = (UITableView *)self.superview.superview;
    
    return tableView;
}

-(NSIndexPath *)indexPath
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
 
    return indexPath;
}




@end

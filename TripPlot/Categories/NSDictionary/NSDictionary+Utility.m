//
//  NSDictionary+Utility.m
//  TripPlot
//
//  Created by Vishwas Singh on 27/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSDictionary+Utility.h"

@implementation NSDictionary(Utility)


-(NSMutableDictionary *)removeAllNullValues
{
    NSMutableDictionary *temp =[[NSMutableDictionary alloc]initWithDictionary:self];
    NSArray *allKeys = [temp allKeys];
    
    for (NSInteger atIndex = 0; atIndex< [allKeys count]; atIndex++)
    {
        if ([temp objectForKey:[allKeys objectAtIndex:atIndex ]] == nil ||[temp objectForKey:[allKeys objectAtIndex:atIndex] ]==[NSNull null])
        {
            [temp removeObjectForKey:[allKeys objectAtIndex:atIndex ]];
        }
    }
    
    return temp;
}


@end

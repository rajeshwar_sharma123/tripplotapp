//
//  NSDictionary+Utility.h
//  TripPlot
//
//  Created by Vishwas Singh on 27/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(Utility)

-(NSMutableDictionary *)removeAllNullValues;

@end

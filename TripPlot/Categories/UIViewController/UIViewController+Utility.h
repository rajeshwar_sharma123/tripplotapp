//
//  UIViewController+Utility.h
//  TripPlot
//
//  Created by Vishwas Singh on 18/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController(Utility)


-(void)defaultShareWithText:(NSString *)text image:(UIImage *)image url:(NSURL *)url;
-(void) popToSpecificViewController:(id ) viewController;
-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

-(void)showSuccessMessage:(NSString *)message;
-(void)showErrorMessage:(NSString *)message;


-(void)toggleLeftSide;
-(void)pushViewControllerWithIdentifier:(NSString *)identifier;

@end

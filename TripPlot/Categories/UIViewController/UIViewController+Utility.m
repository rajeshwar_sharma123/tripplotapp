//
//  UIViewController+Utility.m
//  TripPlot
//
//  Created by Vishwas Singh on 18/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "UIViewController+Utility.h"

@implementation UIViewController(Utility)

-(void)defaultShareWithText:(NSString *)text image:(UIImage *)image url:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text)
    {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    [self presentViewController:activityController animated:YES completion:^{
        
    }];
    
}


///pop to specific view controller
-(void) popToSpecificViewController:(id ) viewController
{
    // iterate in navigation stack
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[viewController class]])
        {
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)showSuccessMessage:(NSString *)message
{
    [CSNotificationView showInViewController:self style:CSNotificationViewStyleSuccess message:message];
}


-(void)showErrorMessage:(NSString *)message
{
    [CSNotificationView showInViewController:self style:CSNotificationViewStyleError message:message];
}

-(void)pushViewControllerWithIdentifier:(NSString *)identifier
{
    [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:identifier] animated:YES];
}



-(void)toggleLeftSide
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}




@end

//
//  NSString+TPPEnumParser.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/22/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//
#import <AWSCore/AWSCore.h>
#import <Foundation/Foundation.h>

@interface NSString (TPPEnumParser)
- (AWSRegionType)regionTypeEnumFromString;
@end

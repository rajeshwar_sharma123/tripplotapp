//
//  UITableView+RefreshControl.h
//  TripPlot
//
//  Created by Vishwas Singh on 28/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>


@interface UITableView(RefreshControl)

@property(nonatomic,weak) UIRefreshControl *refreshControl;
-(void)enablePullToRefreshWithTarget:(id)target selector:(SEL)selector;

@end

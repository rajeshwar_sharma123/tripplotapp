//
//  UITableView+RefreshControl.m
//  TripPlot
//
//  Created by Vishwas Singh on 28/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "UITableView+RefreshControl.h"


@implementation UITableView(RefreshControl)
@dynamic refreshControl;

- (NSString*)refreshControl
{
    return objc_getAssociatedObject(self, @selector(refreshControl));
}

-(void)setRefreshControl:(UIRefreshControl *)refreshControl
{
    objc_setAssociatedObject(self, @selector(refreshControl), refreshControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}




-(void)enablePullToRefreshWithTarget:(id)target selector:(SEL)selector
{
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:target action:selector forControlEvents:UIControlEventValueChanged];
    
    [self addSubview:self.refreshControl];
}



@end

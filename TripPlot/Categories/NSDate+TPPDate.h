//
//  NSDate+TPPDate.h
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (TPPDate)

-(NSString *)getStringFromat;
//-(NSDate *)getDateFromString:(NSString *)dateString;

-(NSDate *)getDateAfterDay:(NSInteger )daysToAdd;

-(NSString *)getUniqueFileName;
@end

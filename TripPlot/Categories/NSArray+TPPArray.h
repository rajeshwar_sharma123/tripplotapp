//
//  NSArray+TPPArray.h
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (TPPArray)

-(NSArray *)filterArrayOnKey:(NSString *) key value:(NSString *)searchString;
-(NSArray *)getLocationsArrayInRequestForamt;
-(NSMutableArray *) toArray;

-(NSMutableArray *)getLocationsInSourceDestinationModelFormat:(NSArray *) array;

-(NSArray *) tokensNameArray;

@end

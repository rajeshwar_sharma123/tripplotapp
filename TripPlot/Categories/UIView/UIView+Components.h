//
//  UIView+Components.h
//  FDCustomer
//
//  Created by Vipul Sharma on 25/08/15.
//  Copyright (c) 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Components)


-(void)showLoader;
-(void)hideLoader;


-(void)showToasterWithMessage:(NSString *)msg;



-(void)showSpinner;
-(void)hideSpinner;

-(void)showSpinnerWithUserInteractionDisabled:(BOOL)shouldDisableUserInteraction;

-(void)removeSubviewsOfKindOfClasses:(NSArray *)classesArray;
-(UIView *)subViewOfKindOfClass:(Class)subViewClass;
-(BOOL)isContainingSubViewOfKindOfClasses:(NSArray *)classesArray;

-(void)roundCornersOnTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;

- (UIImage *)snapshotImage;
-(void)makeCircular;
-(void)applyLightBlackTint;

@end




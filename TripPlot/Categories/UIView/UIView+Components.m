//
//  UIView+Components.m
//  FDCustomer
//
//  Created by Vipul Sharma on 25/08/15.
//  Copyright (c) 2015 Daffodil. All rights reserved.
//

#import "UIView+Components.h"
#import "MBProgressHUD.h"



UIActivityIndicatorView *spinner;


@implementation UIView (Components)


#pragma mark- For Loaders

-(void)showLoaderWithMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.labelText = message;
}

-(void)showLoader
{
    [MBProgressHUD showHUDAddedTo:self animated:YES];
}



-(void)hideLoader
{
    [MBProgressHUD hideAllHUDsForView:self animated:YES];
}

-(void)showToasterWithMessage:(NSString *)msg
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = msg;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}


-(void)showSpinner
{
    spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.frame = CGRectMake(self.frame.size.width/2 -15, self.frame.size.height/2 -15, 30, 30);
    
    [spinner startAnimating];

    [self addSubview:spinner];
    [self bringSubviewToFront:spinner];
}

-(void)hideSpinner
{
    self.userInteractionEnabled = YES;
    self.alpha = 1.0;
    
    [spinner removeFromSuperview];
    spinner = nil;
    
    [self removeSubviewsOfKindOfClasses:@[@"UIActivityIndicatorView"]];
}


-(void)showSpinnerWithUserInteractionDisabled:(BOOL)shouldDisableUserInteraction
{
    if (shouldDisableUserInteraction)
    {
        self.alpha = 0.8;
        self.userInteractionEnabled = NO;
    }
    
    spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.frame = CGRectMake(self.frame.size.width/2 -15, self.frame.size.height/2 -15, 30, 30);
    
   
    [spinner startAnimating];
    
    [self addSubview:spinner];
    [self bringSubviewToFront:spinner];
}


#pragma mark- Remove subviews

-(void)removeSubviewsOfKindOfClasses:(NSArray *)classesArray
{
    for (UIView *view in [self subviews])
    {
        for (NSString *str in classesArray)
        {
            if ([view isKindOfClass:[NSClassFromString(str) class]])
            {
                [view removeFromSuperview];
            }
        }
    }
}

/**
 *  To get the subView of particular kind of Class
 *
 *  @param subViewClass class of subView to be found
 *
 *  @return subview
 */
-(UIView *)subViewOfKindOfClass:(Class)subViewClass
{
    for (UIView *view in [self subviews])
    {
        if ([view isKindOfClass: subViewClass])
        {
            return view;
        }
    }
    
    return nil;
}

-(BOOL)isContainingSubViewOfKindOfClasses:(NSArray *)classesArray
{
    for (UIView *view in [self subviews])
    {
        for (NSString *str in classesArray)
        {
            if ([view isKindOfClass:[NSClassFromString(str) class]])
            {
                return YES;
            }
        }
    }
    
    return NO;
}



-(void)roundCornersOnTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br)
    {
        UIRectCorner corner=0; //holds the corner
        //Determine which corner(s) should be changed
        if (tl) {
            corner = corner | UIRectCornerTopLeft;
        }
        if (tr) {
            corner = corner | UIRectCornerTopRight;
        }
        if (bl) {
            corner = corner | UIRectCornerBottomLeft;
        }
        if (br) {
            corner = corner | UIRectCornerBottomRight;
        }
        
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y+1, self.bounds.size.width, self.bounds.size.height-1);//view.bounds;
        maskLayer.path = maskPath.CGPath;
       
        self.layer.mask = maskLayer;
    }
    else
    {

    }
}


#pragma mark- Take ScreenShot

- (UIImage *)snapshotImage
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    
    // old style [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
   
    UIGraphicsEndImageContext();
    
    return image;
}


-(void)makeCircular
{
    self.layer.cornerRadius = self.frame.size.height/2;
    self.clipsToBounds = YES;
}

-(void)applyLightBlackTint
{
    [self removeSubviewsOfKindOfClasses:@[@"UIView"]];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    
    [self addSubview:view];
}




@end

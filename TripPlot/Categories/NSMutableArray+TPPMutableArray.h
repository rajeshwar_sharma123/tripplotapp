//
//  NSMutableArray+TPPMutableArray.h
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (TPPMutableArray)

-(NSInteger) isExistString:(NSString *)string;
-(void)removeTripModelForRecievedNotification:(NSNotification *)notification;


@end

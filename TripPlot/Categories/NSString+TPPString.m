//
//  NSString+TPPString.m
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSString+TPPString.h"

#define DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

@implementation NSString (TPPString)


-(NSDate *)getDateFromString
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:DATE_FORMAT];
    
    NSDate *date;
    
    if (self== nil || [self isEqualToString:@""]) {
        date =  [NSDate date];
    }
    else
    {
        date = [format dateFromString:self];
        if (!date) {
            [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
            date = [format dateFromString:self];
        }
    }
    
    return date;
}

-(NSString *)getDisplayFormatFromString
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:DATE_FORMAT];
    //[format setDateFormat:@"dd MMM, yyyy at HH:mm Z"];
    NSDate *date;
    if (self== nil || [self isEqualToString:@""]) {
        date =  [NSDate date];
    }
    else
    {
        date = [format dateFromString:self];
    }
    
    [format setDateFormat:@"dd MMM, yyyy"];
    NSString *dateString = [format stringFromDate:date];
    [format setDateFormat:@"HH:mm a"];
    NSString *timeString = [format stringFromDate:date];
    
    NSString *toBeDisplay = [NSString stringWithFormat:@"%@  at  %@",dateString,timeString];
    
    return toBeDisplay;
}

-(NSInteger ) getDaysDifference:(NSString *) fromDate
{
    NSDate *date1 = [self getDateFromString];
    NSDate *date2 = [fromDate getDateFromString];
    NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    NSInteger numberOfDays = secondsBetween / 86400;
    NSLog(@"There are %ld days in between the two dates.", (long)numberOfDays);
    return numberOfDays;
}

-(NSInteger ) getTimeIntervalDifference:(NSString *) fromDate
{
    NSDate *to = [self getDateFromString];
    NSDate *since = [fromDate getDateFromString];
    NSTimeInterval secondsBetween = [to timeIntervalSinceDate:since];
    return secondsBetween;
}

-(NSString *)convertToPhoneNumberFormat
{
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    // get country code, e.g. ES (Spain), FR (France), etc.
    NSString *countryCodeInNumeric;
    if ([countryCode caseInsensitiveCompare:@"IN"] == NSOrderedSame) {
        countryCodeInNumeric = @"91";
    }
    else if ([countryCode caseInsensitiveCompare:@"US"] == NSOrderedSame || [countryCode caseInsensitiveCompare:@"USA"] == NSOrderedSame ){
        countryCodeInNumeric = @"1";
    }
    else
    {
        countryCodeInNumeric = @"0";
    }
    
    
    NSString *cleanedString = [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
    if (cleanedString.length <= 10) {
        //[nsst]:@"%@%@",@"91", cleanedString];
        cleanedString = [NSString stringWithFormat:@"%@%@",countryCodeInNumeric, cleanedString];
    }
    return cleanedString;
}
@end

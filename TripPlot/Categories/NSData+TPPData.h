//
//  NSData+TPPData.h
//  TripPlot
//
//  Created by Daffodil iPhone on 1/18/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (TPPData)

+ (NSData *)getCompressedImageDataFromImage:(UIImage *)image width:(float )width height:(float)height ;
+ (NSData *)getCompressedImageDataFromImageData:(NSData *)imageData width:(float )width height:(float)height ;

@end

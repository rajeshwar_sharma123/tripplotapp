//
//  NSString+TPPString.h
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TPPString)
-(NSDate *)getDateFromString;

-(NSString *)getDisplayFormatFromString;

-(NSInteger ) getDaysDifference:(NSString *) fromDate;
-(NSInteger ) getTimeIntervalDifference:(NSString *) fromDate;

-(NSString *)convertToPhoneNumberFormat;

@end


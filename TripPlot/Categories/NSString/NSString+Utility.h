//
//  NSString+FDCString.h
//  FDCustomer
//
//  Created by Vipul Sharma on 19/08/15.
//  Copyright (c) 2015 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

-(NSString *)trimmedString;

-(CGSize)dynamicSize;
-(CGSize)dynamicSizeForWidth:(CGFloat)width;
-(CGSize)dynamicSizeForWidth:(CGFloat)width font:(UIFont*)font;

-(NSString *)formattedDateString;


-(BOOL) isValidEmail;

-(NSString *)timeAgoString;
-(NSString *)currentDayWithStartDateString;
@end


//
//  NSString+FDCString.m
//  FDCustomer
//
//  Created by Vipul Sharma on 19/08/15.
//  Copyright (c) 2015 Daffodil. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

- (NSString*)trimmedString
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


-(CGSize)dynamicSize
{
    UILabel *label = [UILabel new];
    label.numberOfLines = 0;
    label.text = self;
    
    if([self isEqualToString:@""])
    {
        label.text = @"Test"; // for minimum one line
    }
    
    CGSize newSize = [label sizeThatFits:CGSizeMake(414, CGFLOAT_MAX)];
    
    return newSize;
}

-(CGSize)dynamicSizeForWidth:(CGFloat)width
{
    UILabel *label = [UILabel new];
    label.numberOfLines = 0;
    label.text = self;
    label.font = [UIFont fontWithName:@"Aileron-Light" size:14];
    
    if([self isEqualToString:@""])
    {
        label.text = @"Test"; // for minimum one line
    }
    
    CGSize newSize = [label sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    
    return newSize;
}


-(CGSize)dynamicSizeForWidth:(CGFloat)width font:(UIFont*)font
{
    UILabel *label = [UILabel new];
    label.numberOfLines = 0;
    label.text = self;
    label.font = font;
    
    if([self isEqualToString:@""])
    {
        label.text = @"Test"; // for minimum one line
    }
    
    CGSize newSize = [label sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    
    return newSize;
}


-(NSString *)formattedDateString
{
    NSString *resultString;
    
    float min = [self floatValue];
    
    if (min>59)
    {
        float h = min/60;
        float m = (int)min%60;
        
        if (m>0)
        {
            resultString = [NSString stringWithFormat:@"%dh, %dm",(int)h,(int)m];
        }
        else
        {
            resultString = [NSString stringWithFormat:@"%d hour(s)",(int)h];
        }
    }
    else
    {
        resultString = [NSString stringWithFormat:@"%d min",(int)min];
    }
    
    return resultString;
}


-(BOOL) isValidEmail
{
    BOOL stricterFilter = NO;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self.trimmedString];
}


-(NSString *)timeAgoString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    
    NSDate *fromDate = [dateFormatter dateFromString:self];
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval secondsBetweenDates = [currentDate timeIntervalSinceDate:fromDate];
    
    
    NSInteger minsBetweenDates = secondsBetweenDates / 60;
    NSInteger hoursBetweenDates = secondsBetweenDates / 3600;
    NSInteger daysBetweenDates = secondsBetweenDates / (24*3600);
    
    NSString *retunString;
    if (daysBetweenDates>0)
    {
        retunString = [NSString stringWithFormat:@"%ld days ago",daysBetweenDates];
    }
    else if (hoursBetweenDates>0)
    {
        retunString = [NSString stringWithFormat:@"%ld hours ago",hoursBetweenDates];
    }
    else if (minsBetweenDates>0)
    {
        retunString = [NSString stringWithFormat:@"%ld mins ago",minsBetweenDates];
    }
    else if(secondsBetweenDates<0)
    {
        retunString = @"Just now";
    }
    else
    {
        retunString = [NSString stringWithFormat:@"%ld sec ago",(NSInteger)secondsBetweenDates];
    }

    
    return retunString;
}

-(NSString *)currentDayWithStartDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *startDate = [dateFormatter dateFromString:self];
    [dateFormatter setDateFormat:@"MMM dd,yyyy"];
    NSString *startDateString = [dateFormatter stringFromDate:startDate];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *fromDate = [dateFormatter dateFromString:self];
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval secondsBetweenDates = [currentDate timeIntervalSinceDate:fromDate];
    
    NSInteger daysBetweenDates = secondsBetweenDates / (24*3600);
    
    NSString *retunString;
    if (daysBetweenDates>=0)
    {
        if (daysBetweenDates == 0) {
             retunString = [NSString stringWithFormat:@"DAY 1 | %@",startDateString];
        }
        else
        {
        daysBetweenDates++;
        retunString = [NSString stringWithFormat:@"DAY %ld | %@",daysBetweenDates,startDateString];
        }
    }

    return retunString;
}

@end

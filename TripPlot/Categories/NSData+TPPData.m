//
//  NSData+TPPData.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/18/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSData+TPPData.h"

@implementation NSData (TPPData)
+ (NSData *)getCompressedImageDataFromImage:(UIImage *)image width:(float)width height:(float)height
{
    // Resize the image
    CGSize originalSize = image.size;
    NSData *data;
    if (originalSize.width>800 && originalSize.height >600) {
        CGSize newSize = CGSizeMake(width, height);
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(newImage, 0.8);
        return data;
    }
    data = UIImageJPEGRepresentation(image, 1.0);
    return data;
}
+ (NSData *)getCompressedImageDataFromImageData:(NSData *)imageData width:(float )width height:(float)height
{
    UIImage *image = [UIImage imageWithData:imageData];
    CGSize originalSize = image.size;
    NSData *data;
    if (originalSize.width>800 && originalSize.height >600) {
        CGSize newSize = CGSizeMake(width, height);
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(newImage, 0.8);
        return data;
    }
    data = UIImageJPEGRepresentation(image, 1.0);
    return data;
    
    
}

@end

//
//  GMSMapView+Utility.h
//  TripPlot
//
//  Created by Vishwas Singh on 13/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GMSMapView(Utility)

@property(nonatomic,strong) NSMutableArray *markers;

-(void)boundAllMarkersWithPadding:(float)padding shouldIncludeMyLocation:(BOOL)shouldIncludeMyLocation;


@end

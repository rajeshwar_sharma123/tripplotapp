//
//  GMSMapView+Utility.m
//  TripPlot
//
//  Created by Vishwas Singh on 13/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "GMSMapView+Utility.h"
#import <objc/runtime.h>


@implementation GMSMapView(Utility)
@dynamic markers;

-(void)setMarkers:(NSMutableArray *)markers
{
    objc_setAssociatedObject(self, @selector(markers), markers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString*)markers
{
    return objc_getAssociatedObject(self, @selector(markers));
}


-(void)boundAllMarkersWithPadding:(float)padding shouldIncludeMyLocation:(BOOL)shouldIncludeMyLocation
{
    CLLocationCoordinate2D firstLocation = ((GMSMarker *)self.markers.firstObject).position;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:firstLocation coordinate:firstLocation];
    
    for (GMSMarker *marker in self.markers)
    {
        bounds = [bounds includingCoordinate:marker.position];
    }
    if (shouldIncludeMyLocation)
    {
        [bounds includingCoordinate:self.myLocation.coordinate];
    }
    
    [self animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:padding]];
    
}



@end

//
//  NSDate+TPPDate.m
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSDate+TPPDate.h"

#define DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

@implementation NSDate (TPPDate)


-(NSString *)getStringFromat
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:DATE_FORMAT];

    //[format setDateFormat:@"dd MMM, yyyy at HH:mm Z"];



    NSString *dateString = [format stringFromDate:self];

    return dateString;
}

-(NSDate *)getDateAfterDay:(NSInteger )daysToAdd
{

    NSDate *newDate = [self dateByAddingTimeInterval:60*60*24*daysToAdd];

    return newDate;
}


-(NSString *)getUniqueFileName
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"ddMMMyyyyHHmmSSsss"];
    
    
    
    NSString *dateString = [NSString stringWithFormat:@"%@_%@",[UserDeafaults objectForKey:Token], [format stringFromDate:self]];
    
    return dateString;
}


//-(NSString *) getTimeDifference:(NSDate *) fromDate
//{
// 
//    
//    NSTimeInterval secondsBetween = [fromDate timeIntervalSinceDate:self];
//    
//    
//    if (secondsBetween< 59) {
//        return [NSString stringWithFormat:@"%@",@"Just"];
//    }
//    else if(secondsBetween< 3600)
//    {
//        return [NSString stringWithFormat:@"%ld min %@",secondsBetween/3600, @"ago"];
//    }
//    
//    return numberOfDays;
//}


@end

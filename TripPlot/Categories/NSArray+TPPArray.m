//
//  NSArray+TPPArray.m
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSArray+TPPArray.h"

@implementation NSArray (TPPArray)


-(NSArray *)filterArrayOnKey:(NSString *) key value:(NSString *)searchString
{
    NSMutableArray *tempArray = [NSMutableArray new];

    for (NSInteger atIndex = 0; atIndex < self.count; atIndex++) {
        NSDictionary *tempDict = [self objectAtIndex:atIndex];

        if ([tempDict[key] containsString:searchString]) {
            [tempArray addObject:tempDict];
        }
    }

    return [tempArray copy];
}

-(NSArray *)getLocationsArrayInRequestForamt
{
    TPPSourceDestinationModel *obj = [TPPSourceDestinationModel new];
    NSMutableArray *tempArray = [NSMutableArray new];
    if(self.count >0)
    {
        obj = (TPPSourceDestinationModel *)[self objectAtIndex:0];
        if (obj.source ==nil || obj.destination == nil) {
            return nil;
        }
        [tempArray addObject:obj.source];
        [tempArray addObject:obj.destination];
    }
    for (NSInteger atIndex = 1; atIndex < self.count; atIndex++) {
        obj = (TPPSourceDestinationModel *)[self objectAtIndex:atIndex];
		if (obj.destination) {
			 [tempArray addObject:obj.destination];
		}

    }

    return [tempArray copy];
}

-(NSMutableArray *)getLocationsInSourceDestinationModelFormat:(NSArray *) array
{
	NSMutableArray <TPPSourceDestinationModel *> *tempArray = [NSMutableArray new];

	NSError *error;
	for (NSInteger atIndex = 0; atIndex < array.count; atIndex = atIndex+2) {
		TPPSourceDestinationModel *obj = [TPPSourceDestinationModel new];
		TPPTripLocation *source = [[TPPTripLocation alloc]initWithDictionary:[array objectAtIndex:atIndex] error:&error];
		TPPTripLocation *destination;

		if (atIndex+1 < array.count) {
			destination = [[TPPTripLocation alloc]initWithDictionary:[array objectAtIndex:atIndex+1] error:&error];
		}
		else
		{
			destination = [[TPPTripLocation alloc]init];
		}
		obj.source = source;
		obj.destination = destination;
		[tempArray addObject:obj];
	}
	return [tempArray mutableCopy];
}

-(NSMutableArray *) toArray
{
    NSMutableArray  *tempLoc = [NSMutableArray new];

    for (NSInteger atIndex = 0; atIndex < self.count; atIndex++) {
        NSDictionary *tempDict = [[self objectAtIndex:atIndex] toDictionary];
        [tempLoc addObject:tempDict];

    }
    return tempLoc;
}

-(NSArray *) tokensNameArray
{
    NSMutableArray *temp = [NSMutableArray new];
    for (NSInteger atIndex = 0; atIndex < self.count; atIndex++) {
        NSDictionary *dic = [self objectAtIndex:atIndex];
        
        [temp addObject:dic[@"name"]];
    }
    return [temp copy];
}

@end

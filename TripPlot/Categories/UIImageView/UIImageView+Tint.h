//
//  UIImageView+Tint.h
//  TripPlot
//
//  Created by Vishwas Singh on 26/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImageView(Tint)

-(void)applyTintColor:(UIColor *)color;

@end

//
//  UIImageView+Tint.m
//  TripPlot
//
//  Created by Vishwas Singh on 26/02/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "UIImageView+Tint.h"

@implementation UIImageView(Tint)


-(void)applyTintColor:(UIColor *)color
{
    self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setTintColor:color];

}

@end

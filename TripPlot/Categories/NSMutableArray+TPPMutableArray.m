//
//  NSMutableArray+TPPMutableArray.m
//  TripPlot
//
//  Created by Daffolap-21 on 07/01/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSMutableArray+TPPMutableArray.h"

@implementation NSMutableArray (TPPMutableArray)

-(NSInteger) isExistString:(NSString *)string
{
    for (NSInteger atIndex = 0; atIndex <self.count; atIndex++) {
        if ([self[atIndex] caseInsensitiveCompare:string] == NSOrderedSame)
        {
            NSLog(@"found at: %ld",(long)atIndex);
            return atIndex;
        }
        
    }
    return -1;
}


-(void)removeTripModelForRecievedNotification:(NSNotification *)notification
{
    if (!notification.userInfo)
    {
        return;
    }
    
    NSIndexPath *indexPath = notification.userInfo[@"indexPath"];
    if (indexPath.row>(self.count-1))
    {
        return;
    }
    
    for (NSInteger i=indexPath.row; i>=0; i--)
    {
        TPPCommonTripModel *commonModel = self[i];
        [self removeObjectAtIndex:i];
        
        if (commonModel.isShareModel)
        {
            continue;
        }
        else if (commonModel.level == 0)
        {
            break;
        }
    }

}


@end

//
//  NSString+TPPEnumParser.m
//  TripPlot
//
//  Created by Daffodil iPhone on 1/22/16.
//  Copyright © 2016 Daffodil. All rights reserved.
//

#import "NSString+TPPEnumParser.h"
#define CASE(str)                       if ([__s__ isEqualToString:(str)])
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT


@implementation NSString (TPPEnumParser)
- (AWSRegionType)regionTypeEnumFromString{
    
    AWSRegionType regionType;
    
      SWITCH (self) {
        CASE (@"AWSRegionUnknown") {
            regionType = AWSRegionUnknown;
            break;
        }
        CASE (@"AWSRegionUSEast1") {
            regionType = AWSRegionUSEast1;
            break;
        }
        CASE (@"AWSRegionUSWest1") {
            regionType = AWSRegionUSWest1;
            break;
        }
        CASE (@"AWSRegionUSWest2") {
            regionType = AWSRegionUSWest2;
            break;
        }
        CASE (@"AWSRegionEUWest1") {
            regionType = AWSRegionEUWest1;
            break;
        }
        CASE (@"AWSRegionEUCentral1") {
            regionType = AWSRegionEUCentral1;
            break;
        }
        CASE (@"AWSRegionAPSoutheast1") {
            regionType = AWSRegionAPSoutheast1;
            break;
        }
        CASE (@"AWSRegionAPNortheast1") {
            regionType = AWSRegionAPNortheast1;
            break;
        }
        CASE (@"AWSRegionAPNortheast2") {
            regionType = AWSRegionAPNortheast2;
            break;
        }
        CASE (@"AWSRegionAPSoutheast2") {
            regionType = AWSRegionAPSoutheast2;
            break;
        }
        CASE (@"AWSRegionSAEast1") {
            regionType = AWSRegionSAEast1;
            break;
        }
        CASE (@"AWSRegionCNNorth1") {
            regionType = AWSRegionCNNorth1;
            break;
        }
        CASE (@"AWSRegionUSGovWest1") {
            regionType = AWSRegionUSGovWest1;
            break;
        }
        
        DEFAULT {
            break;
        }
    }
    
    return regionType;
    
}

@end
